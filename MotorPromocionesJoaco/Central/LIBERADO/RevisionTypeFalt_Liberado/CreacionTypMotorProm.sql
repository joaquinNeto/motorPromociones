/* Formatted on 06/12/2021 10:47:52 p. m. (QP5 v5.294) */
CREATE OR REPLACE TYPE VELIT.TYPOBJESTRATGPR AS OBJECT
(
   FIPAISID NUMBER (3),
   FIESTRATEGIAPRCID NUMBER (18)
);
/

CREATE OR REPLACE TYPE VELIT.TYPARRESTRATGPR
   AS TABLE OF VELIT.TYPOBJESTRATGPR;
/

GRANT EXECUTE ON VELIT.TYPARRESTRATGPR TO USRVELITWS;
GRANT EXECUTE ON VELIT.TYPOBJESTRATGPR TO USRVELITWS;

GRANT EXECUTE ON VELIT.TYPARRESTRATGPR TO USRVELITWSIND;
GRANT EXECUTE ON VELIT.TYPOBJESTRATGPR TO USRVELITWSIND;

GRANT EXECUTE ON VELIT.TYPARRESTRATGPR TO USRVELITWSGENERICOODIN;
GRANT EXECUTE ON VELIT.TYPOBJESTRATGPR TO USRVELITWSGENERICOODIN;