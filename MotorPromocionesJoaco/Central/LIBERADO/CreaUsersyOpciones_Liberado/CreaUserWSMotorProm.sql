/*************************************************************
Proyecto: Motor de Promociones
Descripci�n: Creacion de Usuarios Motor de Promociones
Creador: Omar Perez Alavez
Fecha de creaci�n: 08/02/2021
*************************************************************/




--Creacion de usuario para WSEstrategiasCentral

CREATE USER USRWSESTRTGCENT
IDENTIFIED BY M0t0rPr0m0s_Ct1;

GRANT CREATE SESSION TO USRWSESTRTGCENT;


--Creacion de usuario para WSEstrategiasTienda

CREATE USER USRWSESTRTGTDA
IDENTIFIED BY M0t0rPr0m0s_lC1;

GRANT CREATE SESSION TO USRWSESTRTGTDA;
