﻿/* Formatted on 01/12/2021 12:26:44 p. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA01
IS
   vg_Proceso                  VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                  := 'VELIT.PAWSREPORTESCOMPRAS';   -- Proceso
   vg_Tabla                    VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                  VELIT.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId               VELIT.TABITACORAERRORES.FIBITACORAID%TYPE := 0; -- Id de la bitacora
   vg_DscError                 VELIT.TABITACORAERRORES.FCDESERROR%TYPE := ''; -- Almacena la descripción del error de oracle.
   vg_CdgError                 VELIT.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion               EXCEPTION;           -- Excepcion personalizada
   csg_IndError       CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_IndAlerta      CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                  := 2 ; -- Indica si es una alerta por lo tanto no se aplica commit dentro del store.
   csg_ErrorCarga     CONSTANT NUMBER (1) := 2; -- Codigo de error cuando la carga no fue completa
   csg_ErrorTabla     CONSTANT NUMBER (1) := 3; -- Codigo de error cuando la tabla no existe o esta incompleta
   csg_ErrorNOCD      CONSTANT NUMBER (1) := 4; -- Codigo de error cuando la tienda no tiene CD Relacionado
   csg_Activado       CONSTANT NUMBER (1) := 1;            -- Estatus activado
   csg_Desactivado    CONSTANT NUMBER (1) := 0;         -- Estatus desactivado
   csg_Cero           CONSTANT NUMBER (1) := 0;               -- Valor de cero
   csg_Uno            CONSTANT NUMBER (1) := 1;                 -- Valores = 1
   csg_Dos            CONSTANT NUMBER (1) := 2;                 -- Valores = 2
   csg_Tres           CONSTANT NUMBER (1) := 3;                 -- Valores = 3
   csg_Cuatro         CONSTANT NUMBER (1) := 4;                 -- Valores = 4
   csg_Ocho           CONSTANT NUMBER (1) := 8;                 -- Valores = 8
   csg_Once           CONSTANT NUMBER (2) := 11;               -- Valores = 11
   csg_EntDirecta     CONSTANT NUMBER (1) := 1;     -- Tipo de entrega directa
   csg_EntAmbas       CONSTANT NUMBER (1) := 3;       -- Tipo de entrega ambas
   csg_EntCEDIS       CONSTANT NUMBER (1) := 2;       -- Tipo de entrega CEDIS
   csg_AreaSurtido    CONSTANT NUMBER (1) := 3;      -- Area de surtido tienda
   csg_ArtVigente     CONSTANT NUMBER (1) := 5; -- Estatus de articulo vigente
   csg_ArtBloqdo      CONSTANT NUMBER (1) := 6; -- Estatus de articulo bloqueado a la compras
   csg_ArtBaja        CONSTANT NUMBER (2) := 12; -- Estatus de articulo baja definitiva
   csg_DivServicios   CONSTANT NUMBER (1) := 4;    -- Id de division servicios
   csg_SistemaId      CONSTANT NUMBER (1) := 1;              -- Id del sistema
   csg_ModuloId       CONSTANT NUMBER (1) := 3;              -- Id  del modulo
   csg_SubModArtId    CONSTANT NUMBER (1) := 2;  -- Id del submodulo articulos
   csg_ConfDiasVta    CONSTANT NUMBER (2) := 10; -- Id de configuracion Tda Inactiva
   csg_MonedaPeso     CONSTANT NUMBER (1) := 1;          -- Id de moneda pesos
   csg_Pieza          CONSTANT NUMBER (1) := 1;         -- Unidad medida Pieza
   csg_Caja           CONSTANT NUMBER (1) := 4;          -- Unidad medida Caja
   csg_Display        CONSTANT NUMBER (1) := 5;       -- Unidad medida Display
   csg_Kilo           CONSTANT NUMBER (1) := 2;          -- Unidad medida Kilo
   csg_Gramo          CONSTANT NUMBER (1) := 6;         -- Unidad medida Gramo
   csg_FormatoAnio    CONSTANT VARCHAR2 (12 CHAR) := 'DD/MM/YYYY'; -- Formato de Fecha para el anio
   csg_Redondeo       CONSTANT NUMBER (1) := 2;       -- Indicador de redondeo
   csg_Autorizado     CONSTANT NUMBER (1) := 2; -- Estatus de Movimiento Autorizado
   csg_TipoTda        CONSTANT NUMBER (1) := 1;       -- Indicador para tienda
   csg_TipoCedis      CONSTANT NUMBER (1) := 0;        -- Indicador para Cedis
   csg_EstrVigente    CONSTANT VELIT.TAESTATUSESTRTG.FIESTATUSESTRTGID%TYPE
                                  := 5 ;

   PROCEDURE SPCONSULTACATARTXTDA (
      ptab_PaisId         IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId       IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      pa_EstatusArtId     IN     NUMBER,
      pa_TipoArticuloId   IN     NUMBER,
      pa_TipoEntregaId    IN     NUMBER,
      pa_Categoria        IN     NUMBER,
      pa_ProveedorId      IN     NUMBER,
      prec_CurArticulos      OUT VELIT.TYPES.CURSORTYP,
      pa_CdgError            OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      pa_DscError            OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Consultar el catalogo de articulos disponibles en la tienda
   Parámetros de entrada: -ptab_PaisId         -> Id del pais
                          -ptab_TiendaId       -> Id de la tienda
                          -pa_EstatusArtId   -> estatus de articulos a buscar
                          -pa_TipoArticuloId -> tipo de Articulo a buscar
                          -pa_TipoEntregaId  -> tipo de entrega a buscar
                          -pa_Categoria      -> Categoria de articulos a buscar
                          -pa_ProveedorId    -> Id de proveedor
   Parámetros de salida:  -pa_CurArticulos   -> Cursor de articulos
                          -pa_CdgError       -> Código de error generado por ORACLE
                          -pa_DscError      -> Descripción del error generado por ORACLE
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 27/10/2021
   ***********************************************************************************************/
   PROCEDURE SPCONSULTAESTRATEGIAXART (
      ptab_PaisId       IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId     IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_ArticuloId   IN     VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      prec_Articulos       OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError        OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError        OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
         Proyecto: Sistema Integral de Operaciones NETO
         Descripción: Consultar el catalogo de articulos disponibles en la tienda
         Parámetros de entrada: -ptab_PaisId     -> Id del pais
                                -ptab_TiendaId   -> Id de la tienda
                                -ptab_ArticuloId -> Id del articulo
         Parámetros de salida:  -rec_Articulos   -> Cursor de articulos
                                -ptab_CdgError   -> Código de error generado por ORACLE
                                -ptab_DscError   -> Descripción del error generado por ORACLE
         Parámetros de entrada-salida:N/A
         Precondiciones:
         Creador: Joaquin Jaime Sanchez Salgado
         Fecha de creación: 27/10/2021
         ***********************************************************************************************/
   FUNCTION FNEXISTEPROMOART (
      ptab_PaisId       IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId     IN VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_ArticuloId   IN VELIT.TAARTICULOS.FIARTICULOID%TYPE)
      RETURN NUMBER;

   /***********************************************************************************************
         Proyecto: Sistema Integral de Operaciones NETO
         Descripción: Consultar el catalogo de articulos disponibles en la tienda
         Parámetros de entrada: -ptab_PaisId     -> Id del pais
                                -ptab_TiendaId   -> Id de la tienda
                                -ptab_ArticuloId -> Id del articulo
         Parámetros de salida:  NUMBER
         Parámetros de entrada-salida:N/A
         Precondiciones:
         Creador: Joaquin Jaime Sanchez Salgado
         Fecha de creación: 27/10/2021
         ***********************************************************************************************/
   PROCEDURE SPCONSULTAESTRATEGIAXTDA (
      ptab_PaisId      IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId    IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      prec_Articulos      OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError       OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError       OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Consultar el catalogo de articulos disponibles en la tienda
   Parámetros de entrada: -ptab_PaisId     -> Id del pais
                          -ptab_TiendaId   -> Id de la tienda
   Parámetros de salida:  -rec_Estrategias -> Cursor de estrategias
                          -ptab_CdgError   -> Código de error generado por ORACLE
                          -ptab_DscError   -> Descripción del error generado por ORACLE
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 27/10/2021
   ***********************************************************************************************/
   FUNCTION FNCAMBIOARTICULO (
      ptab_ArticuloId   IN VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      pa_TipoMovto      IN NUMBER)
      RETURN VARCHAR2;
/***********************************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Consultar si existe un cambio de precio por articulo
Parámetros de entrada: -ptab_ArticuloId -> Id del articulo
Parámetros de salida:  -VARCHAR2
Parámetros de entrada-salida:N/A
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 27/10/2021
***********************************************************************************************/
END PAWSMOTORPROMTDA01;
/

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA01
IS
   PROCEDURE SPCONSULTACATARTXTDA (
      ptab_PaisId         IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId       IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      pa_EstatusArtId     IN     NUMBER,
      pa_TipoArticuloId   IN     NUMBER,
      pa_TipoEntregaId    IN     NUMBER,
      pa_Categoria        IN     NUMBER,
      pa_ProveedorId      IN     NUMBER,
      prec_CurArticulos      OUT VELIT.TYPES.CURSORTYP,
      pa_CdgError            OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      pa_DscError            OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   IS
      /***********************************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO
      Descripción: Consultar el catalogo de articulos disponibles en la tienda
      Parámetros de entrada: -ptab_PaisId         -> Id del pais
                             -ptab_TiendaId       -> Id de la tienda
                             -pa_EstatusArtId   -> estatus de articulos a buscar
                             -pa_TipoArticuloId -> tipo de Articulo a buscar
                             -pa_TipoEntregaId  -> tipo de entrega a buscar
                             -pa_Categoria      -> Categoria de articulos a buscar
                             -pa_ProveedorId    -> Id de proveedor
      Parámetros de salida:  -prec_CurArticulos   -> Cursor de articulos
                             -pa_CdgError       -> Código de error generado por ORACLE
                             -pa_DscError      -> Descripción del error generado por ORACLE
      Parámetros de entrada-salida:N/A
      Precondiciones:
      Creador: Joaquin Jaime Sanchez Salgado
      Fecha de creación: 27/10/2021
      ***********************************************************************************************/
      csl_FechaDefault   CONSTANT VARCHAR2 (10 CHAR) := '01/01/1901';
      csl_FiltroGral     CONSTANT NUMBER (1) := -1; --Valor para Filtro generales
      csl_VentiOcho      CONSTANT NUMBER (2) := 28;             --- Valor = 28
      csl_Diezocho       CONSTANT NUMBER (2) := 18;
      csl_Dieznueve      CONSTANT NUMBER (2) := 19;
      vl_CedisTda                 VELIT.TATIENDAS.FITIENDAID%TYPE := 0; --Almacena el Cedis de la tienda-
      vl_AgrupaId                 VELIT.TAARTICULOS.FIAGRUPACIONID%TYPE;
      vl_DivsionId                VELIT.TAARTICULOS.FIDIVISIONID%TYPE;
      vl_TdaIva8                  NUMBER (3) := 0; --valor del modificador de tienda con iva8
      vl_SolMovto                 NUMBER (3) := 0; -- Indicador de movimiento autorizador
      cur_Divisiones              VELIT.TYPES.CURSORTYP;
      vlva_datosart               VELIT.TYPARREPARTTDA
                                     := VELIT.TYPARREPARTTDA ();

      CURSOR cur_datosArt
      IS
         SELECT FIARTICULOID,
                NOMBRE,
                FITIPOARTICULOID,
                FITIPOMARCAID,
                CATEGORIA,
                FIPROVEEDORID,
                FITIPOENTREGAID,
                PRECIOTDA,
                NORMAEMPAQUE,
                MINIMO,
                DIASINVCONFIG,
                CASE
                   WHEN DIASINVACTUAL < csg_Cero THEN csg_Cero
                   ELSE DIASINVACTUAL
                END
                   AS DIASINVACTUAL,
                NOMBRECORTO,
                DESCRIPCION,
                CASE FDFECHAACTUALIZA
                   WHEN csl_FechaDefault THEN FDFECHAALTA
                   ELSE FDFECHAACTUALIZA
                END
                   AS FDFECHAACTUALIZA,
                CASE
                   WHEN EXISTEPROMO > csg_Cero THEN csg_Uno
                   ELSE csg_Cero
                END
                   AS EXISTEPROMO
           FROM (WITH Promos
                      AS (SELECT AR.FIARTICULOID, AR.FNPRECIO
                            FROM VELIT.TAPROMOCIONESXTDA PXT
                                 INNER JOIN VELIT.TAPROMOCIONES PROM
                                    ON PXT.FIPROMOCIONID = PROM.FIPROMOCIONID
                                 INNER JOIN VELIT.TAARTICULOSXPROMOCION AR
                                    ON PROM.FIPROMOCIONID = AR.FIPROMOCIONID
                           WHERE     PROM.FIESTATUSMOVTOID = csg_Autorizado
                                 AND PROM.FITIPOPROMOCIONID = csg_Activado
                                 AND PROM.FIVIGENTE = csg_Activado
                                 AND PXT.FITIENDAID = ptab_TiendaId
                                 AND PROM.FDFECHAFINAL >= TRUNC (SYSDATE)),
                      INVENTARIOTDA
                      AS (SELECT INV.FIPAISID,
                                 INV.FITIENDAID,
                                 INV.FIARTICULOID,
                                 INV.FNEXISTENCIA,
                                 INV.FNVENTAPROMEDIOPZAS
                            FROM VELIT.TAINVENTARIOXTDAXDIA INV
                           WHERE     TRUNC (INV.FDFECHAMUESTRA) =
                                        TRUNC (SYSDATE - csg_Uno)
                                 AND INV.FIPAISID = ptab_PaisId
                                 AND INV.FITIENDAID = ptab_TiendaId)
                 SELECT AR.FIARTICULOID,
                        UPPER (
                              AR.FCNOMBREARTICULO
                           || ' '
                           || AR.FCMARCA
                           || ' '
                           || AR.FCESPECIFICACION
                           || ' '
                           || AR.FCPRESENTACION)
                           AS NOMBRE,
                        AR.FITIPOARTICULOID,
                        AR.FITIPOMARCAID,
                        TO_NUMBER (
                              AR.FIDIVISIONID
                           || AR.FIAGRUPACIONID
                           || AR.FICATEGORIAID)
                           AS CATEGORIA,
                        CASE PXA.FITIPOENTREGAID
                           WHEN csg_EntCEDIS THEN vl_CedisTda
                           ELSE AXT.FIPROVEEDORID
                        END
                           AS FIPROVEEDORID,
                        PXA.FITIPOENTREGAID,
                        NVL (
                           PROM.FNPRECIO,
                           CASE
                              WHEN vl_TdaIva8 = csg_Uno
                              THEN
                                 PXP.FNPRECIOIVAOCHO
                              ELSE
                                 PXP.FNPRECIOVENTA
                           END)
                           AS PRECIOTDA,
                        VELIT.PAWSREPORTESCOMPRAS.FNNORMAEMPAQUEXREGLA (
                           AR.FIARTICULOID,
                           csg_Uno)
                           AS NORMAEMPAQUE,
                        CASE
                           WHEN PXA.FITIPOENTREGAID IN (csg_Dos, csg_Tres)
                           THEN
                              ROUND (
                                   NVL (AXT.FNMININVENTARIO, csg_Cero)
                                 / VELIT.PAWSPEDIDOSSUGERIDOS.FNUNIDADMEDIDA (
                                      ptab_PaisId,
                                      AR.FIARTICULOID,
                                      csg_Dos),
                                 csg_Cero)
                           ELSE
                              AXT.FNMININVENTARIO
                        END
                           AS MINIMO,
                        AXT.FINUMDIASCONSULTAMOVTOS            AS DIASINVCONFIG,
                        CASE
                           WHEN NVL (INVT.FNVENTAPROMEDIOPZAS, csg_Cero) =
                                   csg_Cero
                           THEN
                              csg_Cero
                           ELSE
                              ROUND (
                                 (  NVL (INVT.FNEXISTENCIA, csg_Cero)
                                  / ROUND (
                                         NVL (INVT.FNVENTAPROMEDIOPZAS,
                                              csg_Cero)
                                       / csl_VentiOcho,
                                       csg_Dos)),
                                 csg_Cero)
                        END
                           AS DIASINVACTUAL,
                        UPPER (AR.FCNOMBREARTICULO)            AS NOMBRECORTO,
                        UPPER (
                              AR.FCMARCA
                           || ' '
                           || AR.FCESPECIFICACION
                           || ' '
                           || AR.FCPRESENTACION)
                           AS DESCRIPCION,
                        VELIT.PAWSMOTORPROMTDA01.FNCAMBIOARTICULO (
                           AR.FIARTICULOID,
                           vl_SolMovto)
                           AS FDFECHAACTUALIZA,
                        TO_CHAR (AR.FDFECHAALTA, 'DD/MM/YYYY') FDFECHAALTA,
                        VELIT.PAWSMOTORPROMTDA01.FNEXISTEPROMOART (
                           ptab_PaisId,
                           ptab_TiendaId,
                           AR.FIARTICULOID)
                           AS EXISTEPROMO
                   FROM VELIT.TAARTICULOS AR
                        INNER JOIN VELIT.TAARTICULOSXTIENDA AXT
                           ON     AXT.FIPAISID = ptab_PaisId
                              AND AXT.FITIENDAID = ptab_TiendaId
                              AND AXT.FIARTICULOID = AR.FIARTICULOID
                              AND AXT.FIESTATUSASIGNACION = csg_Activado
                        INNER JOIN VELIT.TAPRECIOSARTICULOSXPAIS PXP
                           ON     PXP.FIPAISID = AXT.FIPAISID
                              AND PXP.FIMONEDAID = csg_Uno
                              AND PXP.FIARTICULOID = AXT.FIARTICULOID
                        LEFT JOIN INVENTARIOTDA INVT
                           ON     INVT.FIPAISID = AXT.FIPAISID
                              AND INVT.FITIENDAID = AXT.FITIENDAID
                              AND INVT.FIARTICULOID = AXT.FIARTICULOID
                        LEFT JOIN Promos PROM
                           ON AXT.FIARTICULOID = PROM.FIARTICULOID
                        INNER JOIN VELIT.TAPROVEEDORESXARTICULO PXA
                           ON     AXT.FIPAISID = PXA.FIPAISID
                              AND AXT.FIPROVEEDORID = PXA.FIPROVEEDORID
                              AND AXT.FIARTICULOID = PXA.FIARTICULOID
                  WHERE     AR.FIDIVISIONID = vl_DivsionId
                        AND AR.FIAGRUPACIONID = vl_AgrupaId
                        AND (   AR.FIESTATUSARTICULOID = pa_EstatusArtId
                             OR (    pa_EstatusArtId = csl_FiltroGral
                                 AND AR.FIESTATUSARTICULOID IN
                                        (csg_ArtBloqdo,
                                         csg_ArtBaja,
                                         csg_ArtVigente)))
                        AND (   PXA.FITIPOENTREGAID = pa_TipoEntregaId
                             OR pa_TipoEntregaId = csl_FiltroGral)
                        AND (   AXT.FIPROVEEDORID = pa_ProveedorId
                             OR (    PXA.FITIPOENTREGAID = csg_EntCEDIS
                                 AND pa_ProveedorId = vl_CedisTda)
                             OR pa_ProveedorId = csl_FiltroGral)
                        AND (   TO_NUMBER (
                                   AR.FIDIVISIONID || AR.FIAGRUPACIONID || AR.FICATEGORIAID) =
                                   pa_Categoria
                             OR pa_Categoria = csl_FiltroGral)
                        AND (   AR.FITIPOARTICULOID = pa_TipoArticuloId
                             OR pa_TipoArticuloId = csl_FiltroGral));
   BEGIN
      vg_Proceso := 'SPCONSULTACATALOGOARTICULOSTDA';
      vg_Tabla := 'TAARTICULOSXTIENDA';
      vg_ExecSql :=
            'ptab_PaisId '
         || ptab_PaisId
         || '|'
         || 'ptab_TiendaId '
         || ptab_TiendaId
         || '|'
         || 'pa_EstatusArtId '
         || pa_EstatusArtId
         || '|'
         || 'pa_TipoArticuloId '
         || pa_TipoArticuloId
         || '|'
         || 'pa_TipoEntregaId '
         || pa_TipoEntregaId
         || '|'
         || 'pa_Categoria '
         || pa_TipoEntregaId
         || '|'
         || 'pa_ProveedorId '
         || pa_TipoEntregaId;

      BEGIN
         SELECT COUNT (FITIENDAID)
           INTO vl_TdaIva8
           FROM VELIT.TATDANUEVOIMPT
          WHERE     FIPAISID = csg_Uno
                AND FITIENDAID = ptab_TiendaId
                AND FIESTATUSID = csg_Uno;

         SELECT FITIENDASURTEID
           INTO vl_CedisTda
           FROM VELIT.TATRANSFERPERMITIDAS
          WHERE     FIPAISID = csg_Uno
                AND FITIENDASOLICITAID = ptab_TiendaId
                AND FITIPOTRANSFERENCIAID = csg_Uno
                AND FIESTATUS = csg_Uno;
      EXCEPTION
         WHEN OTHERS
         THEN
            vl_CedisTda := csg_Cero;
      END;

      IF (vl_TdaIva8 > csg_Cero)
      THEN
         vl_SolMovto := csl_Dieznueve;
      ELSE
         vl_SolMovto := csl_Diezocho;
      END IF;

      OPEN cur_Divisiones FOR
         SELECT FIDIVISIONID, FIAGRUPACIONID
           FROM VELIT.TAAGRUPACIONES
          WHERE FIDIVISIONID != csg_DivServicios;

      LOOP
         FETCH cur_Divisiones INTO vl_DivsionId, vl_AgrupaId;

         EXIT WHEN cur_Divisiones%NOTFOUND;

         FOR D IN cur_datosArt
         LOOP
            EXIT WHEN cur_datosArt%NOTFOUND;

            vlva_datosart.EXTEND;
            vlva_datosart (vlva_datosart.COUNT) :=
               VELIT.TYPOBJREPARTTDA (
                  D.FIARTICULOID,
                  D.NOMBRE,
                  D.FITIPOARTICULOID,
                  D.FITIPOMARCAID,
                  D.CATEGORIA,
                  D.FIPROVEEDORID,
                  D.FITIPOENTREGAID,
                  D.PRECIOTDA,
                  D.NORMAEMPAQUE,
                  D.MINIMO,
                  D.DIASINVCONFIG,
                  D.DIASINVACTUAL,
                  D.NOMBRECORTO,
                  D.DESCRIPCION,
                  TO_DATE (D.FDFECHAACTUALIZA, 'DD/MM/YYYY'),
                  D.EXISTEPROMO);
         END LOOP;
      END LOOP;

      OPEN prec_CurArticulos FOR
           SELECT FIARTICULOID,
                  FCNOMBRE       AS NOMBRE,
                  FITIPOARTICULOID AS FITIPOARTICULOID,
                  FITIPOMARCAID  AS FITIPOMARCAID,
                  FICATEGORIA    AS CATEGORIA,
                  FIPROVEEDORID  AS FIPROVEEDORID,
                  FITIPOENTREGAID AS FITIPOENTREGAID,
                  FNPRECIOTDA    AS PRECIOTDA,
                  FCNORMAEMPAQUE AS NORMAEMPAQUE,
                  FNMINIMO       AS MINIMO,
                  FIDIASINVCONFIG AS DIASINVCONFIG,
                  FNDIASINVACTUAL AS DIASINVACTUAL,
                  FCNOMBRECORTO  AS NOMBRECORTO,
                  FCDESCRIPCION  AS DESCRIPCION,
                  FDFECHAACTUALIZA AS FDFECHAACTUALIZA,
                  FIEXISTEPROMO  AS EXISTEPROMO
             FROM TABLE (vlva_datosart)
         ORDER BY FIARTICULOID ASC;

      pa_DscError :=
         'Los datos del catálogo de artículos se obtuvieron correctamente';
      pa_CdgError := csg_Cero;
   EXCEPTION
      WHEN OTHERS
      THEN
         pa_CdgError := SQLCODE;
         vg_DscError := SQLERRM;
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              pa_CdgError,
                                              vg_DscError,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
         pa_DscError :=
            'Error al momento de consultar la información del catálogo de artículos. Error en el módulo de compras.';
   END SPCONSULTACATARTXTDA;

   PROCEDURE SPCONSULTAESTRATEGIAXART (
      ptab_PaisId       IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId     IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_ArticuloId   IN     VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      prec_Articulos       OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError        OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError        OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Consultar el catalogo de articulos disponibles en la tienda
   Parámetros de entrada: -ptab_PaisId     -> Id del pais
                          -ptab_TiendaId   -> Id de la tienda
                          -ptab_ArticuloId -> Id del articulo
   Parámetros de salida:  -prec_Articulos   -> Cursor de articulos
                          -ptab_CdgError   -> Código de error generado por ORACLE
                          -ptab_DscError   -> Descripción del error generado por ORACLE
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 27/10/2021
   ***********************************************************************************************/
   IS
   BEGIN
      OPEN prec_Articulos FOR
         WITH alcances
              AS (SELECT FIPAISID, FIARTICULOID, FIESTATUSASIGNACION
                    FROM VELIT.TAARTICULOSXTIENDA
                   WHERE     FITIENDAID = ptab_TiendaId
                         AND FIARTICULOID = ptab_ArticuloId)
         SELECT EPP.FIPAISID,
                EPP.FIESTRATEGIAPRCID,
                TIP.FCDESCRIPCION                AS ESTRATEGIA,
                TO_CHAR (EPP.FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY')
                   AS FDFECHAINICIOVIGENCIA,
                TO_CHAR (EPP.FDFECHAFINVIGENCIA, 'DD/MM/YYYY')
                   AS FDFECHAFINVIGENCIA,
                EST.FCDESCRIPCION                AS ESTATUS,
                EXA.FIARTICULOID,
                   ART.FCNOMBREARTICULO
                || ' '
                || FCMARCA
                || ' '
                || FCESPECIFICACION
                || ' '
                || FCPRESENTACION
                   AS ARTICULO,
                NVL (ALC.FIESTATUSASIGNACION, 0) AS ALCANCE,
                CASE EXA.FIESTATUSAPLICA
                   WHEN csg_Dos THEN csg_Uno
                   ELSE csg_Cero
                END
                   AS SINCRONIZADO,
                EPP.FCURLPROMOCIONAL
           FROM VELIT.TAESTRATGPRECIO EPP
                INNER JOIN VELIT.TATIPOSESTRTG TIP
                   ON TIP.FITIPOESTRATGID = EPP.FITIPOESTRATGID
                INNER JOIN VELIT.TAESTATUSESTRTG EST
                   ON EST.FIESTATUSESTRTGID = EPP.FIESTATUSESTRTGID
                INNER JOIN VELIT.TAESTRTXARTXTDA EXA
                   ON     EPP.FIPAISID = EXA.FIPAISID
                      AND EPP.FIESTRATEGIAPRCID = EXA.FIESTRATEGIAPRCID
                INNER JOIN VELIT.TAARTICULOS ART
                   ON ART.FIARTICULOID = EXA.FIARTICULOID
                LEFT JOIN alcances ALC
                   ON     ALC.FIPAISID = EXA.FIPAISID
                      AND ALC.FIARTICULOID = EXA.FIARTICULOID
          WHERE     EPP.FIPAISID = ptab_PaisId
                AND EXA.FITIENDAID = ptab_TiendaId
                AND EXA.FIARTICULOID = ptab_ArticuloId
                AND EXA.FIESTATUS = csg_Uno
                AND EXA.FIESTATUSAPLICA != csg_Cuatro;

      ptab_DscError := 'Consulta exitosa';
      ptab_CdgError := csg_Cero;
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := SQLCODE;
         vg_DscError := SQLERRM;
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              ptab_CdgError,
                                              vg_DscError,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
         ptab_DscError := 'Error al momento de consultar las estrategias.';
   END SPCONSULTAESTRATEGIAXART;

   FUNCTION FNEXISTEPROMOART (
      ptab_PaisId       IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId     IN VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_ArticuloId   IN VELIT.TAARTICULOS.FIARTICULOID%TYPE)
      RETURN NUMBER
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Valida si un artículo tiene una estrategia/promocion
   Parámetros de entrada: -ptab_PaisId     -> Id del pais
                          -ptab_TiendaId   -> Id de la tienda
                          -ptab_ArticuloId -> Id del articulo
   Parámetros de salida:  NUMBER
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 27/10/2021
   ***********************************************************************************************/
   IS
      vl_Salida   NUMBER (9) := 0;
   BEGIN
      SELECT COUNT (1)
        INTO vl_Salida
        FROM VELIT.TAESTRATGPRECIO EPP
             INNER JOIN VELIT.TAESTRTXARTXTDA EXA
                ON     EPP.FIPAISID = EXA.FIPAISID
                   AND EPP.FIESTRATEGIAPRCID = EXA.FIESTRATEGIAPRCID
       WHERE     EPP.FIPAISID = ptab_PaisId
             AND EXA.FITIENDAID = ptab_TiendaId
             AND EXA.FIARTICULOID = ptab_ArticuloId
             AND EXA.FIESTATUS = csg_Uno
             AND EXA.FIESTATUSAPLICA != csg_Cuatro;

      RETURN vl_Salida;
   END FNEXISTEPROMOART;

   PROCEDURE SPCONSULTAESTRATEGIAXTDA (
      ptab_PaisId      IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId    IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      prec_Articulos      OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError       OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError       OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Consulta las estrategias por tienda
   Parámetros de entrada: -ptab_PaisId     -> Id del pais
                          -ptab_TiendaId   -> Id de la tienda
   Parámetros de salida:  -prec_Estrategias -> Cursor de estrategias
                          -ptab_CdgError   -> Código de error generado por ORACLE
                          -ptab_DscError   -> Descripción del error generado por ORACLE
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 27/10/2021
   ***********************************************************************************************/
   IS
   BEGIN
      OPEN prec_Articulos FOR
         WITH alcances
              AS (  SELECT EPP.FIESTRATEGIAPRCID,
                           SUM (
                              CASE EXA.FIESTATUSAPLICA
                                 WHEN csg_Dos THEN csg_Cero
                                 ELSE csg_Uno
                              END)
                              AS SINCRONIZADO
                      FROM VELIT.TAESTRATGPRECIO EPP
                           INNER JOIN VELIT.TAESTRTXARTXTDA EXA
                              ON     EPP.FIPAISID = EXA.FIPAISID
                                 AND EPP.FIESTRATEGIAPRCID =
                                        EXA.FIESTRATEGIAPRCID
                     WHERE     EPP.FIPAISID = csg_Uno
                           AND EXA.FITIENDAID = ptab_TiendaId
                           AND EXA.FIESTATUS = csg_Uno
                           AND EXA.FIESTATUSAPLICA != csg_Cuatro
                  GROUP BY EPP.FIESTRATEGIAPRCID)
           SELECT EPP.FIPAISID,
                  EPP.FIESTRATEGIAPRCID,
                  TIP.FCDESCRIPCION             AS ESTRATEGIA,
                  TO_CHAR (EPP.FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY')
                     AS FDFECHAINICIOVIGENCIA,
                  TO_CHAR (EPP.FDFECHAFINVIGENCIA, 'DD/MM/YYYY')
                     AS FDFECHAFINVIGENCIA,
                  EST.FCDESCRIPCION             AS ESTATUS,
                  csg_Cero                      AS ALCANCE,
                  CASE
                     WHEN ALC.SINCRONIZADO > csg_Cero THEN csg_Cero
                     ELSE csg_Uno
                  END
                     AS SINCRONIZADO,
                  NVL (EPP.FCURLPROMOCIONAL, '-') AS FCURLPROMOCIONAL
             FROM VELIT.TAESTRATGPRECIO EPP
                  INNER JOIN VELIT.TATIPOSESTRTG TIP
                     ON TIP.FITIPOESTRATGID = EPP.FITIPOESTRATGID
                  INNER JOIN VELIT.TAESTATUSESTRTG EST
                     ON EST.FIESTATUSESTRTGID = EPP.FIESTATUSESTRTGID
                  INNER JOIN VELIT.TAESTRTXARTXTDA EXA
                     ON     EPP.FIPAISID = EXA.FIPAISID
                        AND EPP.FIESTRATEGIAPRCID = EXA.FIESTRATEGIAPRCID
                  INNER JOIN VELIT.TAARTICULOS ART
                     ON ART.FIARTICULOID = EXA.FIARTICULOID
                  LEFT JOIN alcances ALC
                     ON ALC.FIESTRATEGIAPRCID = EPP.FIESTRATEGIAPRCID
            WHERE     EPP.FIPAISID = ptab_PaisId
                  AND EXA.FITIENDAID = ptab_TiendaId
                  AND EXA.FIESTATUS = csg_Uno
                  AND EXA.FIESTATUSAPLICA != csg_Cuatro
         GROUP BY EPP.FIPAISID,
                  EPP.FIESTRATEGIAPRCID,
                  TIP.FCDESCRIPCION,
                  TO_CHAR (EPP.FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY'),
                  TO_CHAR (EPP.FDFECHAFINVIGENCIA, 'DD/MM/YYYY'),
                  EST.FCDESCRIPCION,
                  CASE
                     WHEN ALC.SINCRONIZADO > csg_Cero THEN csg_Cero
                     ELSE csg_Uno
                  END,
                  EPP.FCURLPROMOCIONAL;

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := SQLCODE;
         vg_DscError := SQLERRM;
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              ptab_CdgError,
                                              vg_DscError,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
         ptab_DscError := 'Error al momento de consultar las estrategias.';
   END SPCONSULTAESTRATEGIAXTDA;

   FUNCTION FNCAMBIOARTICULO (
      ptab_ArticuloId   IN VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      pa_TipoMovto      IN NUMBER)
      RETURN VARCHAR2
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Consultar si existe un cambio de precio por articulo
   Parámetros de entrada: -ptab_ArticuloId -> Id del articulo
   Parámetros de salida:  -VARCHAR2
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 27/10/2021
   ***********************************************************************************************/
   IS
      csl_Uno   CONSTANT VARCHAR2 (1 CHAR) := '1';
      vl_FechaCambio     VARCHAR2 (100 CHAR) := '01/01/1901';
   BEGIN
      SELECT TO_CHAR (FDFECHAACTUALIZA)
        INTO vl_FechaCambio
        FROM (SELECT FIARTICULOID,
                     FDFECHAACTUALIZA,
                     MAX (FDFECHAACTUALIZA) OVER (PARTITION BY FIARTICULOID)
                        MAX_FDFECHAACTUALIZA
                FROM VELIT.TASOLICITUDESMOVTOSARTICULOS SOL
               WHERE     FIESTATUSMOVTOID IN (csg_Cero, csg_Dos)
                     AND SUBSTR (FCARREGLOCAMBIOS, pa_TipoMovto, csg_Uno) =
                            csl_Uno
                     AND FIARTICULOID = ptab_ArticuloId
                     AND FDFECHAACTUALIZA >= TRUNC (SYSDATE - 180))
       WHERE FDFECHAACTUALIZA = MAX_FDFECHAACTUALIZA;

      RETURN vl_FechaCambio;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN vl_FechaCambio;
   END FNCAMBIOARTICULO;
END PAWSMOTORPROMTDA01;
/

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA01 TO USRVELITWS;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA01 TO USRVELITWSIND;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA01 TO USRVELITWSGENERICOODIN;