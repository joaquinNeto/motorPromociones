CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA07
AS

    csg_Uno                    CONSTANT NUMBER (1) := 1;             -- cte uno
    csg_Seis                   CONSTANT NUMBER (1) := 6;            -- cte seis
    csg_Cero                   CONSTANT NUMBER (1) := 0;            -- cte cero
    

 --Variables para manejo de errores --
    vg_Proceso                          VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                           := 'VELIT.PAWEBTRANSFERENCIAS'; -- Proceso
    vg_Tabla                            VELIT.TABITACORAERRORES.FCTABLA%TYPE
                                           := '';                     -- Tabla
    vg_ExecSql                          VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
                                           := ''; -- Logueo de parametros a la bitacora de errores
    vg_DescError                        VELIT.TABITACORAERRORES.FCDESERROR%TYPE
        := '';                 -- Almacena la descripción del error de oracle.
    vg_CdgError                         VELIT.TABITACORAERRORES.FINUMERROR%TYPE
        := 0;                        -- Almacena el numero de error de oracle.
    csg_IndicadorError         CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
        := 1 ; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
    
    vg_NombreSP                         VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                           := ''; -- nombre del proceso que se ejecuta
    vg_Parametros                       VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
                                           := ''; -- parámetros de entra del sp
    
    vg_DescripcionError                 VELIT.TABITACORAERRORES.FCDESERROR%TYPE
        := '';                            -- descripción del error
    vg_BitacoraId                       VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
        := 0;                -- identificador de la bitácora de errores.
    

PROCEDURE SPGENERARMENUMOTOR (
        pa_UsuarioId   IN     VELIT.TAUSUARIOS.FIUSUARIOID%TYPE,
        pa_MenuRow        OUT VELIT.TYPES.CURSORTYP);
    /**********************************************************************************
    Proyecto: Sistema ODIN
    Descripción: SP QUE GENERA EL MENU DE SIOC PARA EL ABASTO 3.0
    Parámetros de entrada: -pa_UsuarioId          ---- Identificador del Usuario
    Valor de retorno:      -pa_MenuRow            ---- Cursor Menu
    Creador: Oscar Gustavo Moreno Bautista
    Fecha de creación: 13/08/2020
    ************************************************************************************/
	

END PAWSMOTORPROMTDA07;
/

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA07
AS

PROCEDURE SPGENERARMENUMOTOR (
        pa_UsuarioId   IN     VELIT.TAUSUARIOS.FIUSUARIOID%TYPE,
        pa_MenuRow        OUT VELIT.TYPES.CURSORTYP)
    /**********************************************************************************
    Proyecto: Sistema ODIN
    Descripción: SP QUE GENERA EL MENU DE SIOC PARA EL ABASTO 3.0
    Parámetros de entrada: -pa_UsuarioId          ---- Identificador del Usuario
    Valor de retorno:      -pa_MenuRow            ---- Cursor Menu
    Creador: Oscar Gustavo Moreno Bautista
    Fecha de creación: 13/08/2020
    ************************************************************************************/
    IS
    BEGIN
        OPEN pa_MenuRow FOR
              SELECT M.FCDESCRIPCION     AS DESCMOD,
                     SM.FINIVEL,
                     SM.FCDESCRIPCION,
                     SM.FCREFERENCIA,
                     SM.FIMODULOID,
                     SM.FISUBMODULOID,
                     SM.FCPATH,
                     SM.FCNOMBREMODULO
                FROM VELIT.TAMODULOS M
                     JOIN VELIT.TASUBMODULOS SM
                         ON     M.FISISTEMAID = SM.FISISTEMAID
                            AND M.FIMODULOID = SM.FIMODULOID
                            AND SM.FIESTATUS = csg_Uno
                     LEFT JOIN VELIT.TAPERMISOSXPERFIL PE
                         ON     PE.FISISTEMAID = SM.FISISTEMAID
                            AND PE.FIMODULOID = SM.FIMODULOID
                            AND PE.FISUBMODULOID = SM.FISUBMODULOID
                     JOIN VELIT.TAPERFILESXUSUARIO PU
                         ON PE.FIPERFILID = PU.FIPERFILID
                     JOIN VELIT.TAPERFILES PR ON PR.FIPERFILID = PE.FIPERFILID
                     JOIN VELIT.TAOPCIONESSISTEMA OP
                         ON     OP.FISISTEMAID = SM.FISISTEMAID
                            AND OP.FIMODULOID = SM.FIMODULOID
                            AND OP.FISUBMODULOID = SM.FISUBMODULOID
                     JOIN VELIT.TAUSUARIOS US
                         ON US.FIUSUARIOID = PU.FIUSUARIOID
               WHERE     M.FISISTEMAID = csg_Seis
                     AND M.FIESTATUS = csg_Uno
                     AND OP.FIOPCIONID = csg_Uno
                     AND PU.FIUSUARIOID = pa_UsuarioId
                     AND OP.FIESTATUS = csg_Uno
                     AND PE.FIESTATUS = csg_Uno
                     AND PU.FIESTATUS = csg_Uno
                     AND PR.FIESTATUS = csg_Uno
                     AND US.FIESTATUS = csg_Uno
            GROUP BY M.FCDESCRIPCION,
                     SM.FINIVEL,
                     SM.FCDESCRIPCION,
                     SM.FCREFERENCIA,
                     SM.FIMODULOID,
                     SM.FISUBMODULOID,
                     SM.FCPATH,
                     SM.FCNOMBREMODULO
            ORDER BY SM.FINIVEL, SM.FIMODULOID, SM.FISUBMODULOID;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            vg_ExecSql := vg_Parametros;
            vg_CdgError := SQLCODE;
            vg_DescError := SQLERRM;
            VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                 csg_Cero,
                                                 vg_Proceso,
                                                 vg_Tabla,
                                                 csg_Cero,
                                                 vg_CdgError,
                                                 vg_DescError,
                                                 vg_ExecSql,
                                                 csg_Cero,
                                                 vg_BitacoraId);
    END SPGENERARMENUMOTOR;
 
 
END PAWSMOTORPROMTDA07;
/

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWS;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWSIND;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWSGENERICOODIN;