--PROCEDURE SPGENERARMENUMOTOR 

--OPEN paMenuRow FOR
  

    SELECT M.FCDESCRIPCION     AS DESCMOD,
                     SM.FINIVEL,
                     SM.FCDESCRIPCION,
                     SM.FCREFERENCIA,
                     SM.FIMODULOID,
                     SM.FISUBMODULOID,
                     SM.FCPATH,
                     SM.FCNOMBREMODULO
                FROM VELIT.TAMODULOS M
                     JOIN VELIT.TASUBMODULOS SM
                         ON     M.FISISTEMAID = SM.FISISTEMAID
                            AND M.FIMODULOID = SM.FIMODULOID
                            AND SM.FIESTATUS = 1
                     LEFT JOIN VELIT.TAPERMISOSXPERFIL PE
                         ON     PE.FISISTEMAID = SM.FISISTEMAID
                            AND PE.FIMODULOID = SM.FIMODULOID
                            AND PE.FISUBMODULOID = SM.FISUBMODULOID
                     JOIN VELIT.TAPERFILESXUSUARIO PU
                         ON PE.FIPERFILID = PU.FIPERFILID
                     JOIN VELIT.TAPERFILES PR ON PR.FIPERFILID = PE.FIPERFILID
                     JOIN VELIT.TAOPCIONESSISTEMA OP
                         ON     OP.FISISTEMAID = SM.FISISTEMAID
                            AND OP.FIMODULOID = SM.FIMODULOID
                            AND OP.FISUBMODULOID = SM.FISUBMODULOID
                     JOIN VELIT.TAUSUARIOS US
                         ON US.FIUSUARIOID = PU.FIUSUARIOID
               WHERE     M.FISISTEMAID = 6
                     AND M.FIESTATUS = 1
                     AND OP.FIOPCIONID = 1
                     AND PU.FIUSUARIOID = 815311
                     AND OP.FIESTATUS = 1
                     AND PE.FIESTATUS = 1
                     AND PU.FIESTATUS = 1
                     AND PR.FIESTATUS = 1
                     AND US.FIESTATUS = 1
            GROUP BY M.FCDESCRIPCION,
                     SM.FINIVEL,
                     SM.FCDESCRIPCION,
                     SM.FCREFERENCIA,
                     SM.FIMODULOID,
                     SM.FISUBMODULOID,
                     SM.FCPATH,
                     SM.FCNOMBREMODULO
            ORDER BY SM.FINIVEL, SM.FIMODULOID, SM.FISUBMODULOID;