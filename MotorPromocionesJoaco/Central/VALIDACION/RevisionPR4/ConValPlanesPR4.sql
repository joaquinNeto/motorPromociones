--PACKAGE VELIT.PAWSMOTORPROMTDA04


--FUNCTION FNCALCULADIASAPLICACION (


SELECT EPE.FIPERIODOID,
               FITIPOPERIODO,
               FCDIASAPLICACION,
               FDINICOPERIODO,
               FDFINPERIODO
          FROM VELIT.TAESTRATGPRXPER EPE
               INNER JOIN VELIT.TAPERIODOESTRTG PES
                  ON PES.FIPERIODOID = EPE.FIPERIODOID
         WHERE     FIPAISID = 1
               AND FIESTARTEGIAPRCID = 44
               AND EPE.FIESTATUS = 1
               AND PES.FIESTATUS = 1
      GROUP BY EPE.FIPERIODOID,
               FITIPOPERIODO,
               FCDIASAPLICACION,
               FDINICOPERIODO,
               FDFINPERIODO;
			   
			   
SELECT SUBSTR (NVL (DIAS, -11),
                        1,
                        LENGTH (NVL (DIAS, -11)) - 1)
           FROM (SELECT    CASE WHEN LUNES = 1 THEN 'Lu-' END
                        || CASE WHEN MARTES = 2 THEN 'Ma-' END
                        || CASE WHEN MIERCOLES = 3 THEN 'Mi-' END
                        || CASE WHEN JUEVES = 4 THEN 'Ju-' END
                        || CASE WHEN VIERNES = 5 THEN 'Vi-' END
                        || CASE WHEN SABADO = 6 THEN 'Sa-' END
                        || CASE WHEN DOMINGO = 7 THEN 'Do-' END
                           AS DIAS
                   FROM (SELECT FCREGISTROS, ORDEN
                           FROM (    SELECT REGEXP_SUBSTR ('1,1,1,1,1,1,1',
                                                           '[^,]+',
                                                           1,
                                                           LEVEL)
                                               AS FCREGISTROS,
                                            ROWNUM AS ORDEN
                                       FROM DUAL
                                 CONNECT BY REGEXP_SUBSTR ('1,1,1,1,1,1,1',
                                                           '[^,]+',
                                                           1,
                                                           LEVEL)
                                               IS NOT NULL))
                        PIVOT
                           (MAX (FCREGISTROS)
                           FOR ORDEN
                           IN ('44' AS LUNES,
                              '2' AS MARTES,
                              '3' AS MIERCOLES,
                              '4' AS JUEVES,
                              '5' AS VIERNES,
                              '6' AS SABADO,
                              '7' AS DOMINGO)));


 SELECT SUBSTR (NVL (DIAS, -11),
                        1,
                        LENGTH (NVL (DIAS, -11)) - 1)
           FROM (SELECT    CASE WHEN LUNES = 1 THEN 'Lu-' END
                        || CASE WHEN MARTES = 2 THEN 'Ma-' END
                        || CASE WHEN MIERCOLES = 3 THEN 'Mi-' END
                        || CASE WHEN JUEVES = 4 THEN 'Ju-' END
                        || CASE WHEN VIERNES = 5 THEN 'Vi-' END
                        || CASE WHEN SABADO = 6 THEN 'Sa-' END
                        || CASE WHEN DOMINGO = 7 THEN 'Do-' END
                           AS DIAS
                   FROM (SELECT CASE
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'LUNES'
                                   THEN
                                      1
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'MARTES'
                                   THEN
                                      2
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'MIERCOLES'
                                   THEN
                                      3
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'JUEVES'
                                   THEN
                                      4
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'VIERNES'
                                   THEN
                                      5
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'SABADO'
                                   THEN
                                      6
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - 1)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           '����������',
                                           'AEIOUAEIOU') = 'DOMINGO'
                                   THEN
                                      7
                                END
                                   AS DIAS,
                                FCREGISTROS
                           FROM (    SELECT TO_DATE ('09/12/2021', 'DD/MM/YYYY')
                                               AS FECHAINICIAL,
                                            REGEXP_SUBSTR ('1,1,1,1,1,1,1',
                                                           '[^,]+',
                                                           1,
                                                           LEVEL)
                                               AS FCREGISTROS,
                                            ROWNUM AS ORDEN
                                       FROM DUAL
                                 CONNECT BY REGEXP_SUBSTR ('1,1,1,1,1,1,1',
                                                           '[^,]+',
                                                           1,
                                                           LEVEL)
                                               IS NOT NULL)
                          WHERE FCREGISTROS = 1)
                        PIVOT
                           (MAX (FCREGISTROS)
                           FOR DIAS
                           IN ('44' AS LUNES,
                              '2' AS MARTES,
                              '3' AS MIERCOLES,
                              '4' AS JUEVES,
                              '5' AS VIERNES,
                              '6' AS SABADO,
                              '7' AS DOMINGO)));


--PROCEDURE SPCONSULTAPROMOCIONES (



         WITH articulos
              AS (  SELECT FIPAISID,
                           FIESTRATEGIAPRCID,
                           COUNT (FIARTICULOID) AS ARTICULOS
                      FROM VELIT.TAESTRPRECXART
                  GROUP BY FIPAISID, FIESTRATEGIAPRCID),
              tiendas
              AS (  SELECT FIPAISID,
                           FIESTRATEGIAPRCID,
                           COUNT (FITIENDAID) AS TIENDAS,
                           SUM (FIESTATUS)  AS ALCANCES
                      FROM VELIT.TADETAPLICCNTDA
                     WHERE FIPAISID = 1
                  GROUP BY FIPAISID, FIESTRATEGIAPRCID),
              pendientes
              AS (  SELECT FIPAISID,
                           FIESTRATEGIAPRCID,
                           SUM (
                              CASE
                                 WHEN PENDIENTE > 0 THEN 0
                                 ELSE 1
                              END)
                              AS APLICADOS
                      FROM (  SELECT FIPAISID,
                                     FIESTRATEGIAPRCID,
                                     FITIENDAID,
                                     SUM (
                                        CASE
                                           WHEN FIESTATUSAPLICA = 2
                                           THEN
                                              0
                                           ELSE
                                              1
                                        END)
                                        AS PENDIENTE
                                FROM VELIT.TAESTRTXARTXTDA
                            GROUP BY FIPAISID, FIESTRATEGIAPRCID, FITIENDAID)
                  GROUP BY FIPAISID, FIESTRATEGIAPRCID)
           SELECT EST.FIPAISID,
                  EST.FIESTRATEGIAPRCID,
                  EST.FCDESCRIPCION           AS "Concepto",
                  TIP.FCDESCRIPCION           AS "Tipo Promocion",
                  NVL (ART.ARTICULOS, 0) AS "No. Articulos",
                  NVL (TDA.TIENDAS, 0) AS "No. Tiendas Agregadas",
                  NVL (TDA.ALCANCES, 0) AS "No. Tiendas con Alcance",
                  NVL (PND.APLICADOS, 0) AS "No. Tiendas Aplicadas",
                  0                    AS "Margen maquina"
             FROM VELIT.TAESTRATGPRECIO EST
                  INNER JOIN VELIT.TATIPOSESTRTG TIP
                     ON TIP.FITIPOESTRATGID = EST.FITIPOESTRATGID
                  LEFT JOIN articulos ART
                     ON     ART.FIPAISID = EST.FIPAISID
                        AND ART.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                  LEFT JOIN tiendas TDA
                     ON     TDA.FIPAISID = EST.FIPAISID
                        AND TDA.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                  LEFT JOIN pendientes PND
                     ON     PND.FIPAISID = EST.FIPAISID
                        AND PND.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
            WHERE     EST.FIPAISID = 1
                  AND EST.FIESTATUSESTRTGID NOT IN (0, 1)
				  AND EST.FIESTRATEGIAPRCID = 44
         ORDER BY EST.FIESTRATEGIAPRCID ASC;


           SELECT EST.FIPAISID,
                  EST.FIESTRATEGIAPRCID,
                  EST.FCDESCRIPCION               AS "Concepto",
                  TIP.FCDESCRIPCION               AS "Tipo Promocion",
                  EAT.FIARTICULOID                AS "Id Articulo",
                     ART.FCNOMBREARTICULO
                  || ' '
                  || FCMARCA
                  || ' '
                  || FCESPECIFICACION
                  || ' '
                  || FCPRESENTACION
                     AS "Articulo",
                  UPPER (CAT.FCDESCRIPCION)       AS "Categoria Articulo",
                  UPPER (SUB.FCDESCRIPCION)       AS "Subcategoria Articulo",
                  UPPER (PRV.FCNOMBREPROVEEDOR)   AS "Nombre de Proveedor",
                  UPPER (PRV.FCNOMBRECORTO)       AS "Nombre Corto de Proveedor",
                  ZON.FCDESCRIPCION               AS "Region",
                  EAT.FITIENDAID                  AS "Id Tienda",
                  TDA.FCNOMBRE                    AS "Nombre de Tienda",
                  CASE EAT.FIESTATUSAPLICA
                     WHEN 1 THEN 'REGISTRADO'
                     WHEN 2 THEN 'APLICADO'
                     WHEN 3 THEN 'ENVIADO CON ERROR'
                     WHEN 4 THEN 'CANCELADO'
                  END
                     AS "Aplicado en Tienda",
                  0                        AS "Margen Maquina",
                  NVL (EPA.FNMONTOAPLICA, 0) AS "Precio",
                  0                        AS "Costo"
             FROM VELIT.TAESTRATGPRECIO EST
                  INNER JOIN VELIT.TATIPOSESTRTG TIP
                     ON TIP.FITIPOESTRATGID = EST.FITIPOESTRATGID
                  INNER JOIN VELIT.TAESTRTXARTXTDA EAT
                     ON     EAT.FIPAISID = EST.FIPAISID
                        AND EAT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                  INNER JOIN VELIT.TAESTRPRECXART EPA
                     ON     EAT.FIPAISID = EPA.FIPAISID
                        AND EAT.FIESTRATEGIAPRCID = EPA.FIESTRATEGIAPRCID
                        AND EAT.FIARTICULOID = EPA.FIARTICULOID
                  INNER JOIN VELIT.TAARTICULOSXTIENDA AXT
                     ON     AXT.FIPAISID = EAT.FIPAISID
                        AND AXT.FIARTICULOID = EAT.FIARTICULOID
                        AND AXT.FITIENDAID = EAT.FITIENDAID
                  INNER JOIN VELIT.TAPROVEEDORES PRV
                     ON PRV.FIPROVEEDORID = AXT.FIPROVEEDORID
                  INNER JOIN VELIT.TAARTICULOS ART
                     ON ART.FIARTICULOID = EAT.FIARTICULOID
                  INNER JOIN VELIT.TASUBCATEGORIAS SUB
                     ON     SUB.FIDIVISIONID = ART.FIDIVISIONID
                        AND SUB.FIAGRUPACIONID = ART.FIAGRUPACIONID
                        AND SUB.FICATEGORIAID = ART.FICATEGORIAID
                        AND SUB.FISUBCATEGORIAID = ART.FISUBCATEGORIAID
                  INNER JOIN VELIT.TACATEGORIAS CAT
                     ON     CAT.FIDIVISIONID = SUB.FIDIVISIONID
                        AND CAT.FIAGRUPACIONID = SUB.FIAGRUPACIONID
                        AND CAT.FICATEGORIAID = SUB.FICATEGORIAID
                  INNER JOIN VELIT.TATIENDAS TDA
                     ON     TDA.FIPAISID = EAT.FIPAISID
                        AND TDA.FITIENDAID = EAT.FITIENDAID
                  INNER JOIN VELIT.TAREGIONES REG
                     ON     REG.FIPAISID = TDA.FIPAISID
                        AND REG.FIZONAID = TDA.FIZONAID
                        AND REG.FIREGIONID = TDA.FIREGIONID
                  INNER JOIN VELIT.TAZONAS ZON
                     ON     ZON.FIPAISID = REG.FIPAISID
                        AND ZON.FIZONAID = REG.FIZONAID
            WHERE     EST.FIPAISID = 1
                  AND EST.FIESTATUSESTRTGID NOT IN (0, 1)
				  AND EST.FIESTRATEGIAPRCID = 44
         ORDER BY EST.FIESTRATEGIAPRCID ASC, EAT.FIARTICULOID ASC;
		 
		 

      
         SELECT FIPAISID, FCCORREOELECTRONICO
           FROM VELIT.TAUSUARIOS TU
                INNER JOIN VELIT.TAPERFILESXUSUARIO PXU
                   ON TU.FIUSUARIOID = PXU.FIUSUARIOID
          WHERE     PXU.FIPERFILID = 432
                AND PXU.FIESTATUS = 1
                AND TU.FIESTATUS = 1
                AND FCCORREOELECTRONICO IS NOT NULL;		 