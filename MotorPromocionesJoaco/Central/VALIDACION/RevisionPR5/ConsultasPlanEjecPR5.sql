
--PACKAGE VELIT.PAWSMOTORPROMTDA05

--PROCEDURE SPACTUALIZAENVTDA
      
   
--      CURSOR cur_Registros
--      IS

         SELECT FITIPO,
                FITIENDAID,
                FCREGISTROS,
                FIBITACORAID
           FROM (SELECT TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                  '[^}]+',
                                                  1,
                                                  1))
                           AS FITIPO,
                        TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                  '[^}]+',
                                                  1,
                                                  2))
                           AS FITIENDAID,
                        TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                  '[^}]+',
                                                  1,
                                                  3))
                           AS FIBITACORAID,
                        REGEXP_SUBSTR (FCREGISTROS,
                                       '[^}]+',
                                       1,
                                       4)
                           AS FCREGISTROS
                   FROM (SELECT '1}145}20211208163910}1<!2<!2021061001419<!4<!2<!Folleto mensual Diciembre 2021<!3400000<!1<!01/12/2021 00:00:00<!31/12/2021 00:00:00<!8<!1<!1<!28/11/2021 15:04:06<!655735<!189961<!28/11/2021 19:40:43<!1<!Folleto mensual Diciembre 2021<!0<!0<!-}' FCREGISTROS FROM DUAL))
          WHERE FITIPO = 2;


   UPDATE VELIT.TAESTRTXARTXTDA
            SET FIESTATUSAPLICA = 2
          WHERE     FIPAISID = 1
                AND FIESTRATEGIAPRCID = 444
                AND FITIENDAID = 1256
                AND FIARTICULOID = 3050001358;

         UPDATE VELIT.TABITESTARTXTDA
            SET FIESTATUSAPLICA = 2
          WHERE     FIPAISID = 1
                AND FIESTRATEGIAPRCID = 44
                AND FITIENDAID = 1256
                AND FIARTICULOID = 3050001358
                AND FIFECHAID = 20211128150429;		 
				

           SELECT SUM (
                     CASE
                        WHEN FIESTATUSAPLICA = 2 THEN 0
                        ELSE 1
                     END)
                     AS PENDIENTES
             FROM VELIT.TAESTRTXARTXTDA
            WHERE     FIPAISID = 1
			 AND FITIENDAID = 1256
                  AND FIESTRATEGIAPRCID = 1
         GROUP BY FIESTRATEGIAPRCID;
		 


    UPDATE VELIT.TAESTRATGPRECIO
               SET FIENVIOPENDTDA = 1
             WHERE     FIPAISID = 1
                   AND FIESTRATEGIAPRCID = 44;