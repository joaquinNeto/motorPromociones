/* Formatted on 30/11/2021 12:09:20 p. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA04
AS
   csg_PaisId   CONSTANT NUMBER (1) := 1;
   csg_MUno     CONSTANT NUMBER (2) := -1;
   csg_Cero     CONSTANT NUMBER (1) := 0;
   csg_Uno      CONSTANT NUMBER (1) := 1;
   csg_Dos      CONSTANT NUMBER (1) := 2;
   csg_Tres     CONSTANT NUMBER (1) := 3;
   csg_cuatro   CONSTANT NUMBER (1) := 4;
   csg_Cinco    CONSTANT NUMBER (1) := 5;
   csg_Seis     CONSTANT NUMBER (1) := 6;
   csg_Siete    CONSTANT NUMBER (1) := 7;

   FUNCTION FNCALCULADIASAPLICACION (
      ptab_PaisId         IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN VELIT.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE)
      RETURN VARCHAR2
/**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso que obtiene los dias de aplicacion
Parámetros de entrada: - ptab_PaisId       -> Identificador del pais
                       - ptab_EstrategiaId -> Identificador de la estrategia
Parámetros de salida:  - VARCHAR
Valor de retorno:
Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 30/11/2021
************************************************************************************/;

   PROCEDURE SPCONSULTAPROMOCIONES (
      ptab_configuracionId   IN     VELIT.TACONFXPUESTO.FICONFIGURACIONID%TYPE,
      ptab_MonitoreoId       IN     VELIT.TACONFXPUESTO.FITIPOMONITOREO%TYPE,
      prec_CurCorreos           OUT VELIT.TYPES.CURSORTYP,
      prec_CurCabecero          OUT VELIT.TYPES.CURSORTYP,
      prec_CurDetalle           OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError             OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError             OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);
/**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso que obtiene las diferencias en la conciliacion de pagos de servicios
Parámetros de entrada: - paconfiguracionId      -> Identificador del parametro de configuración
                       - paMonitoreoId          -> Identificador del tipo de monitoreo
Parámetros de salida:  - paCurCorreos           -> Cursor con los correos a los que se les enviara los archivos
                       - paCurDetalle           -> Cursor con los detalles que se mostraran en la tabla
                       - paCurCambiosRechazadas -> Cursor con los cambios de puesto rechazados
                       - paCdgError             -> Identificador del Error.
                       - paDescError            -> Descripcion del error en caso de que exista.
Valor de retorno:
Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 30/11/2021
************************************************************************************/
END PAWSMOTORPROMTDA04;
/

/* Formatted on 30/11/2021 04:39:02 p. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA04
AS
   FUNCTION FNCALCULADIASAPLICACION (
      ptab_PaisId         IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN VELIT.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE)
      RETURN VARCHAR2
/**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso que obtiene los dias de aplicacion
Parámetros de entrada: - ptab_PaisId       -> Identificador del pais
                       - ptab_EstrategiaId -> Identificador de la estrategia
Parámetros de salida:  - VARCHAR
Valor de retorno:
Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 30/11/2021
************************************************************************************/
   IS
      vl_PeriodoId        VELIT.TAPERIODOESTRTG.FIPERIODOID%TYPE;
      vl_TipoPeriodoId    VELIT.TAPERIODOESTRTG.FITIPOPERIODO%TYPE;
      vl_DiasAplicacion   VELIT.TAPERIODOESTRTG.FCDIASAPLICACION%TYPE;
      vl_FechaIni         VELIT.TAPERIODOESTRTG.FDINICOPERIODO%TYPE;
      vl_FechaFin         VELIT.TAPERIODOESTRTG.FDFINPERIODO%TYPE;
      vl_periodo          VARCHAR2 (200 CHAR) := '';
	  csl_MOnce CONSTANT VARCHAR2(3 CHAR) := '-11';
   BEGIN
        SELECT EPE.FIPERIODOID,
               FITIPOPERIODO,
               FCDIASAPLICACION,
               FDINICOPERIODO,
               FDFINPERIODO
          INTO vl_PeriodoId,
               vl_TipoPeriodoId,
               vl_DiasAplicacion,
               vl_FechaIni,
               vl_FechaFin
          FROM VELIT.TAESTRATGPRXPER EPE
               INNER JOIN VELIT.TAPERIODOESTRTG PES
                  ON PES.FIPERIODOID = EPE.FIPERIODOID
         WHERE     FIPAISID = ptab_PaisId
               AND FIESTARTEGIAPRCID = ptab_EstrategiaId
               AND EPE.FIESTATUS = csg_Uno
               AND PES.FIESTATUS = csg_Uno
      GROUP BY EPE.FIPERIODOID,
               FITIPOPERIODO,
               FCDIASAPLICACION,
               FDINICOPERIODO,
               FDFINPERIODO;

      IF vl_TipoPeriodoId = csg_Uno OR vl_TipoPeriodoId = csg_Dos
      THEN                                                            --DIARIO
         SELECT SUBSTR (NVL (DIAS, csl_MOnce),
                        csg_Uno,
                        LENGTH (NVL (DIAS, csl_MOnce)) - csg_Uno)
           INTO vl_periodo
           FROM (SELECT    CASE WHEN LUNES = csg_Uno THEN 'Lu-' END
                        || CASE WHEN MARTES = csg_Dos THEN 'Ma-' END
                        || CASE WHEN MIERCOLES = csg_Tres THEN 'Mi-' END
                        || CASE WHEN JUEVES = csg_Cuatro THEN 'Ju-' END
                        || CASE WHEN VIERNES = csg_Cinco THEN 'Vi-' END
                        || CASE WHEN SABADO = csg_Seis THEN 'Sa-' END
                        || CASE WHEN DOMINGO = csg_Siete THEN 'Do-' END
                           AS DIAS
                   FROM (SELECT FCREGISTROS, ORDEN
                           FROM (    SELECT REGEXP_SUBSTR (vl_DiasAplicacion,
                                                           '[^,]+',
                                                           csg_Uno,
                                                           LEVEL)
                                               AS FCREGISTROS,
                                            ROWNUM AS ORDEN
                                       FROM DUAL
                                 CONNECT BY REGEXP_SUBSTR (vl_DiasAplicacion,
                                                           '[^,]+',
                                                           csg_Uno,
                                                           LEVEL)
                                               IS NOT NULL))
                        PIVOT
                           (MAX (FCREGISTROS)
                           FOR ORDEN
                           IN ('1' AS LUNES,
                              '2' AS MARTES,
                              '3' AS MIERCOLES,
                              '4' AS JUEVES,
                              '5' AS VIERNES,
                              '6' AS SABADO,
                              '7' AS DOMINGO)));
      ELSIF vl_TipoPeriodoId = csg_Tres
      THEN                                                               --MES
         SELECT SUBSTR (NVL (DIAS, csl_MOnce),
                        csg_Uno,
                        LENGTH (NVL (DIAS, csl_MOnce)) - csg_Uno)
           INTO vl_periodo
           FROM (SELECT    CASE WHEN LUNES = csg_Uno THEN 'Lu-' END
                        || CASE WHEN MARTES = csg_Dos THEN 'Ma-' END
                        || CASE WHEN MIERCOLES = csg_Tres THEN 'Mi-' END
                        || CASE WHEN JUEVES = csg_Cuatro THEN 'Ju-' END
                        || CASE WHEN VIERNES = csg_Cinco THEN 'Vi-' END
                        || CASE WHEN SABADO = csg_Seis THEN 'Sa-' END
                        || CASE WHEN DOMINGO = csg_Siete THEN 'Do-' END
                           AS DIAS
                   FROM (SELECT CASE
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'LUNES'
                                   THEN
                                      csg_Uno
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'MARTES'
                                   THEN
                                      csg_Dos
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'MIERCOLES'
                                   THEN
                                      csg_Tres
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'JUEVES'
                                   THEN
                                      csg_Cuatro
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'VIERNES'
                                   THEN
                                      csg_Cinco
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'SABADO'
                                   THEN
                                      csg_Seis
                                   WHEN TRANSLATE (
                                           UPPER (
                                              TRIM (
                                                 TO_CHAR (
                                                    TRUNC (
                                                         FECHAINICIAL
                                                       + (ORDEN - csg_Uno)),
                                                    'DAY',
                                                    'NLS_DATE_LANGUAGE = SPANISH'))),
                                           'ÁÉÍÓÚÁÉÍÓÚ',
                                           'AEIOUAEIOU') = 'DOMINGO'
                                   THEN
                                      csg_Siete
                                END
                                   AS DIAS,
                                FCREGISTROS
                           FROM (    SELECT TO_DATE (vl_FechaIni, 'DD/MM/YYYY')
                                               AS FECHAINICIAL,
                                            REGEXP_SUBSTR (vl_DiasAplicacion,
                                                           '[^,]+',
                                                           csg_Uno,
                                                           LEVEL)
                                               AS FCREGISTROS,
                                            ROWNUM AS ORDEN
                                       FROM DUAL
                                 CONNECT BY REGEXP_SUBSTR (vl_DiasAplicacion,
                                                           '[^,]+',
                                                           csg_Uno,
                                                           LEVEL)
                                               IS NOT NULL)
                          WHERE FCREGISTROS = csg_Uno)
                        PIVOT
                           (MAX (FCREGISTROS)
                           FOR DIAS
                           IN ('1' AS LUNES,
                              '2' AS MARTES,
                              '3' AS MIERCOLES,
                              '4' AS JUEVES,
                              '5' AS VIERNES,
                              '6' AS SABADO,
                              '7' AS DOMINGO)));
      END IF;

      RETURN vl_periodo;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN vl_periodo;
   END FNCALCULADIASAPLICACION;

   PROCEDURE SPCONSULTAPROMOCIONES (
      ptab_configuracionId   IN     VELIT.TACONFXPUESTO.FICONFIGURACIONID%TYPE,
      ptab_MonitoreoId       IN     VELIT.TACONFXPUESTO.FITIPOMONITOREO%TYPE,
      prec_CurCorreos           OUT VELIT.TYPES.CURSORTYP,
      prec_CurCabecero          OUT VELIT.TYPES.CURSORTYP,
      prec_CurDetalle           OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError             OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError             OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso que obtiene las diferencias en la conciliacion de pagos de servicios
   Parámetros de entrada: - paconfiguracionId      -> Identificador del parametro de configuración
                          - paMonitoreoId          -> Identificador del tipo de monitoreo
   Parámetros de salida:  - paCurCorreos           -> Cursor con los correos a los que se les enviara los archivos
                          - paCurDetalle           -> Cursor con los detalles que se mostraran en la tabla
                          - paCurCambiosRechazadas -> Cursor con los cambios de puesto rechazados
                          - paCdgError             -> Identificador del Error.
                          - paDescError            -> Descripcion del error en caso de que exista.
   Valor de retorno:
   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 30/11/2021
   ************************************************************************************/
   IS
      csl_Proceso             CONSTANT VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                          := 'SPCONSULTAPROMOCIONES' ; -- Nombre Proceso
      csl_Modulo              CONSTANT VARCHAR2 (50)
                                          := 'Error en el módulo de correos' ; -- Mensage Módulo
      csl_Numtransac          CONSTANT VELIT.TABITACORAERRORES.FINUMTRANSACCION%TYPE
                                          := 0 ;      -- Número de transacción
      csl_TdaCd               CONSTANT NUMBER (1) := 1; -- Consntante para designar que no es una sucursal en la bitácora de errores.
      csl_ConfigModuloId      CONSTANT NUMBER (2) := 18; -- Constante indica configuracion id de modulo
      csl_ConfigSubmoduloId   CONSTANT NUMBER (1) := 1; -- Constante indica condiguracion id de submodulo
      csl_PerfilId            CONSTANT NUMBER (3) := 432; -- Perfil de correo promociones
      vl_ConfigHorasBaja               NUMBER (2) := 0; -- Constante para obtener los dobles turnos
      vl_CdgError                      VELIT.TABITACORAERRORES.FINUMERROR%TYPE
         := 0;                                       -- Código de Error Oracle
      vl_DescError                     VELIT.TABITACORAERRORES.FCDESERROR%TYPE
         := ' ';                                -- Descripción de error Oracle
      vl_Tabla                         VELIT.TABITACORAERRORES.FCTABLA%TYPE
                                          := ' ';    -- Nombre de tabla en uso
      vl_ExecSql                       VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
                                          := ' '; -- Descripción del query en ejecución
      vl_Parametros                    VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
                                          := ' '; -- Parametros de entrada del SP.
      vl_IndicaError                   VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
         := 1;                                        -- Indica que hubo error
      vl_Bitacoraid                    VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
         := 0;                                   -- ID de registro en bitácora
      vl_ArrayTiendas                  VELIT.TYPARRAYTIENDASXREGION
                                          := VELIT.TYPARRAYTIENDASXREGION (); -- Arreglo con las tiendas a enviar
      vlArr_TiendasEnv                 VELIT.TYPARRAYUSRXTIENDAXASIS
                                          := VELIT.TYPARRAYUSRXTIENDAXASIS (); -- Arreglo con las tiendas a enviar
      vlArr_regresar                   VELIT.TYPARRAYTIENDASXREGION
                                          := VELIT.TYPARRAYTIENDASXREGION (); -- Arreglo con las tiendas y correos a recuperar
      vl_regionActual                  VELIT.TATIENDAS.FIZONAID%TYPE := 0; --Zona actual en el recorrido
      vl_correosElectronico            VELIT.TAUSUARIOS.FCCORREOELECTRONICO%TYPE
         := 0;                               -- correo electronico del usuario
      vl_HorasBusqueda                 VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE
         := 0;                     -- Numero de dias para realizar la busqueda
	  vlEstrategiaId   VELIT.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE := 0;  
   BEGIN
      -- cursor con las zonas por
      OPEN prec_CurCabecero FOR
         WITH articulos
              AS (  SELECT FIPAISID,
                           FIESTRATEGIAPRCID,
                           COUNT (FIARTICULOID) AS ARTICULOS
                      FROM VELIT.TAESTRPRECXART
                  GROUP BY FIPAISID, FIESTRATEGIAPRCID),
              tiendas
              AS (  SELECT FIPAISID,
                           FIESTRATEGIAPRCID,
                           COUNT (FITIENDAID) AS TIENDAS,
                           SUM (FIESTATUS)  AS ALCANCES
                      FROM VELIT.TADETAPLICCNTDA
                     WHERE FIPAISID = csg_Uno
                  GROUP BY FIPAISID, FIESTRATEGIAPRCID),
              pendientes
              AS (  SELECT FIPAISID,
                           FIESTRATEGIAPRCID,
                           SUM (
                              CASE
                                 WHEN PENDIENTE > csg_Cero THEN csg_Cero
                                 ELSE csg_Uno
                              END)
                              AS APLICADOS
                      FROM (  SELECT FIPAISID,
                                     FIESTRATEGIAPRCID,
                                     FITIENDAID,
                                     SUM (
                                        CASE
                                           WHEN FIESTATUSAPLICA = csg_Dos
                                           THEN
                                              csg_Cero
                                           ELSE
                                              csg_Uno
                                        END)
                                        AS PENDIENTE
                                FROM VELIT.TAESTRTXARTXTDA
                            GROUP BY FIPAISID, FIESTRATEGIAPRCID, FITIENDAID)
                  GROUP BY FIPAISID, FIESTRATEGIAPRCID)
           SELECT EST.FIPAISID,
                  EST.FIESTRATEGIAPRCID,
                  EST.FCDESCRIPCION           AS "Concepto",
                  TIP.FCDESCRIPCION           AS "Tipo Promocion",
                  NVL (ART.ARTICULOS, csg_Cero) AS "No. Articulos",
                  NVL (TDA.TIENDAS, csg_Cero) AS "No. Tiendas Agregadas",
                  NVL (TDA.ALCANCES, csg_Cero) AS "No. Tiendas con Alcance",
                  NVL (PND.APLICADOS, csg_Cero) AS "No. Tiendas Aplicadas",
                  csg_Cero                    AS "Margen maquina"
             FROM VELIT.TAESTRATGPRECIO EST
                  INNER JOIN VELIT.TATIPOSESTRTG TIP
                     ON TIP.FITIPOESTRATGID = EST.FITIPOESTRATGID
                  LEFT JOIN articulos ART
                     ON     ART.FIPAISID = EST.FIPAISID
                        AND ART.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                  LEFT JOIN tiendas TDA
                     ON     TDA.FIPAISID = EST.FIPAISID
                        AND TDA.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                  LEFT JOIN pendientes PND
                     ON     PND.FIPAISID = EST.FIPAISID
                        AND PND.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
            WHERE     EST.FIPAISID = csg_Uno
                  AND EST.FIESTATUSESTRTGID NOT IN (csg_Cero, csg_Uno)
         ORDER BY EST.FIESTRATEGIAPRCID ASC;

      -- Cursor para obtener el detalle por Zona
      OPEN prec_CurDetalle FOR
           SELECT EST.FIPAISID,
                  EST.FIESTRATEGIAPRCID,
                  EST.FCDESCRIPCION               AS "Concepto",
                  TIP.FCDESCRIPCION               AS "Tipo Promocion",
                  EAT.FIARTICULOID                AS "Id Articulo",
                     ART.FCNOMBREARTICULO
                  || ' '
                  || FCMARCA
                  || ' '
                  || FCESPECIFICACION
                  || ' '
                  || FCPRESENTACION
                     AS "Articulo",
                  UPPER (CAT.FCDESCRIPCION)       AS "Categoria Articulo",
                  UPPER (SUB.FCDESCRIPCION)       AS "Subcategoria Articulo",
                  UPPER (PRV.FCNOMBREPROVEEDOR)   AS "Nombre de Proveedor",
                  UPPER (PRV.FCNOMBRECORTO)       AS "Nombre Corto de Proveedor",
                  ZON.FCDESCRIPCION               AS "Region",
                  EAT.FITIENDAID                  AS "Id Tienda",
                  TDA.FCNOMBRE                    AS "Nombre de Tienda",
                  CASE EAT.FIESTATUSAPLICA
                     WHEN csg_Uno THEN 'REGISTRADO'
                     WHEN csg_Dos THEN 'APLICADO'
                     WHEN csg_Tres THEN 'ENVIADO CON ERROR'
                     WHEN csg_Cuatro THEN 'CANCELADO'
                  END
                     AS "Aplicado en Tienda",
                  csg_Cero                        AS "Margen Maquina",
                  NVL (EPA.FNMONTOAPLICA, csg_Cero) AS "Precio",
                  csg_Cero                        AS "Costo"
             FROM VELIT.TAESTRATGPRECIO EST
                  INNER JOIN VELIT.TATIPOSESTRTG TIP
                     ON TIP.FITIPOESTRATGID = EST.FITIPOESTRATGID
                  INNER JOIN VELIT.TAESTRTXARTXTDA EAT
                     ON     EAT.FIPAISID = EST.FIPAISID
                        AND EAT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                  INNER JOIN VELIT.TAESTRPRECXART EPA
                     ON     EAT.FIPAISID = EPA.FIPAISID
                        AND EAT.FIESTRATEGIAPRCID = EPA.FIESTRATEGIAPRCID
                        AND EAT.FIARTICULOID = EPA.FIARTICULOID
                  INNER JOIN VELIT.TAARTICULOSXTIENDA AXT
                     ON     AXT.FIPAISID = EAT.FIPAISID
                        AND AXT.FIARTICULOID = EAT.FIARTICULOID
                        AND AXT.FITIENDAID = EAT.FITIENDAID
                  INNER JOIN VELIT.TAPROVEEDORES PRV
                     ON PRV.FIPROVEEDORID = AXT.FIPROVEEDORID
                  INNER JOIN VELIT.TAARTICULOS ART
                     ON ART.FIARTICULOID = EAT.FIARTICULOID
                  INNER JOIN VELIT.TASUBCATEGORIAS SUB
                     ON     SUB.FIDIVISIONID = ART.FIDIVISIONID
                        AND SUB.FIAGRUPACIONID = ART.FIAGRUPACIONID
                        AND SUB.FICATEGORIAID = ART.FICATEGORIAID
                        AND SUB.FISUBCATEGORIAID = ART.FISUBCATEGORIAID
                  INNER JOIN VELIT.TACATEGORIAS CAT
                     ON     CAT.FIDIVISIONID = SUB.FIDIVISIONID
                        AND CAT.FIAGRUPACIONID = SUB.FIAGRUPACIONID
                        AND CAT.FICATEGORIAID = SUB.FICATEGORIAID
                  INNER JOIN VELIT.TATIENDAS TDA
                     ON     TDA.FIPAISID = EAT.FIPAISID
                        AND TDA.FITIENDAID = EAT.FITIENDAID
                  INNER JOIN VELIT.TAREGIONES REG
                     ON     REG.FIPAISID = TDA.FIPAISID
                        AND REG.FIZONAID = TDA.FIZONAID
                        AND REG.FIREGIONID = TDA.FIREGIONID
                  INNER JOIN VELIT.TAZONAS ZON
                     ON     ZON.FIPAISID = REG.FIPAISID
                        AND ZON.FIZONAID = REG.FIZONAID
            WHERE     EST.FIPAISID = csg_Uno
                  AND EST.FIESTATUSESTRTGID NOT IN (csg_Cero, csg_Uno)
         ORDER BY EST.FIESTRATEGIAPRCID ASC, EAT.FIARTICULOID ASC;

      OPEN prec_CurCorreos FOR
         SELECT FIPAISID, FCCORREOELECTRONICO
           FROM VELIT.TAUSUARIOS TU
                INNER JOIN VELIT.TAPERFILESXUSUARIO PXU
                   ON TU.FIUSUARIOID = PXU.FIUSUARIOID
          WHERE     PXU.FIPERFILID = csl_PerfilId
                AND PXU.FIESTATUS = csg_Uno
                AND TU.FIESTATUS = csg_Uno
                AND FCCORREOELECTRONICO IS NOT NULL;

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'Error de base de datos.';
         VELIT.PAGENERICOS.SPBITACORAERRORES (csg_PaisId,
                                              csl_TdaCd,
                                              csl_Proceso,
                                              vl_Tabla,
                                              csl_Numtransac,
                                              SQLCODE,
                                              SQLERRM,
                                              vl_ExecSql,
                                              vl_IndicaError,
                                              vl_Bitacoraid);
         COMMIT;
         ptab_DscError :=
               TRIM (ptab_DscError)
            || ' '
            || TRIM (csl_Modulo)
            || '|'
            || TRIM (csl_Proceso)
            || '|'
            || TO_CHAR (vl_Bitacoraid);
   END SPCONSULTAPROMOCIONES;
END PAWSMOTORPROMTDA04;
/

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA04 TO USRVELITWS;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA04 TO USRVELITWSIND;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA04 TO USRVELITWSGENERICOODIN;


GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA04 TO USRWSESTRTGCENT;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA04 TO USRWSESTRTGTDA;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA04 TO USRSRVETGPRMCNT;