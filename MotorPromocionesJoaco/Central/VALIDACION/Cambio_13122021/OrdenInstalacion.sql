/*********************************************************************************************
Proyecto:      Sistema Integral de Operaciones Neto
Descripción:   Sistema WMS Neto
Elaborado por: Omar Perez Alavez
Fecha de Creacion: 23/03/2021
***********************************************************************************************/

1.-PAWSMOTORPROMTDA02.sql --Creacion de Paquete1
2.-PAWSMOTORPROMTDA04.sql --Creacion de Paquete2
3.-PAWSMOTORPROMTDA05.sql --Creacion de Paquete3
4.-GrantsMotorPromP1.sql --Permisos de ejecucion 1
5.-GrantsMotorPromP2.sql --Permisos de ejecucion 2
6.-ConfCorreo.sql --Configuraciones para envio correos
