CREATE OR REPLACE PACKAGE VELIT.PAWSESTRATEGIASPROMOCION
AS
    vgProceso                     VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                    := 'VELIT.PAWSCAMBIOSARTICULOS'; -- Proceso
    vgTabla                       VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
    vgExecSql                     VELIT.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
    vgBitacoraId                  VELIT.TABITACORAERRORES.FIBITACORAID%TYPE := 0; -- Id de la bitacora
    vgDescError                   VELIT.TABITACORAERRORES.FCDESERROR%TYPE := ''; -- Almacena la descripción del error de oracle.
    vgCdgError                    VELIT.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
    excExcepcion                  EXCEPTION;         -- Excepcion personalizada
    csgIndicadorError             VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.

    -- Valores 0 y 1
    csgCero              CONSTANT NUMBER (1) := 0;                    --Valor 0
    csgUno               CONSTANT NUMBER (1) := 1;                    --Valor 1
    csgActivado          CONSTANT NUMBER (1) := 1;                    --Valor 1
    csgDesactivado       CONSTANT NUMBER (1) := 0;                    --Valor 0


    -- Modo de ejecucion
    csgEjecCron          CONSTANT NUMBER (1) := 0; --Ejecucion por Cron Nocturno
    csgEjecSIAN          CONSTANT NUMBER (1) := 1; --Ejecucion por Cron de SIAN

    --Posiciones Cadena Envio
    csgPosDatosArt       CONSTANT NUMBER (1) := 1; --Posicion para datos de articulo
    csgPosDatosCodBar    CONSTANT NUMBER (1) := 2; --Posicion para datos de articulo

    --Posiciones Datos Articulo
    csgPosFechaEnv       CONSTANT NUMBER (2) := 1;              --- VALORES = 0
    csgPosArtId          CONSTANT NUMBER (1) := 2;              --- VALORES = 0
    csgPosNombre         CONSTANT NUMBER (1) := 3;              --- VALORES = 0
    csgPosNumBus         CONSTANT NUMBER (1) := 4;              --- VALORES = 0
    csgPosDiv            CONSTANT NUMBER (1) := 5;              --- VALORES = 0
    csgPosPrecio         CONSTANT NUMBER (1) := 6;              --- VALORES = 0
    csgPosDesc           CONSTANT NUMBER (1) := 7;              --- VALORES = 0
    csgPosGranel         CONSTANT NUMBER (1) := 8;              --- VALORES = 0
    csgPosEstatus        CONSTANT NUMBER (1) := 9;              --- VALORES = 0
    csgPosAgrup          CONSTANT NUMBER (2) := 10;             --- VALORES = 0
    csgPosCate           CONSTANT NUMBER (2) := 11;             --- VALORES = 0


    --Posiciones Cod Barras
    csgPosCodBar         CONSTANT NUMBER (1) := 1;              --- VALORES = 0
    csgPosUnidadMed      CONSTANT NUMBER (1) := 2;              --- VALORES = 0
    csgPosNormaEmp       CONSTANT NUMBER (1) := 3;              --- VALORES = 0
    csgPosCosto          CONSTANT NUMBER (1) := 4;              --- VALORES = 0
    csgPosIVA            CONSTANT NUMBER (1) := 5;              --- VALORES = 0
    csgPosIEPS           CONSTANT NUMBER (1) := 6;              --- VALORES = 0
    csgPosCodPadre       CONSTANT NUMBER (1) := 7;              --- VALORES = 0
    csgPosNormaPallet    CONSTANT NUMBER (1) := 8;
    csgPosIVAID          CONSTANT NUMBER (1) := 9;

    -- Constantes para Configuracion de Parametros
    csgPaisId            CONSTANT NUMBER (1) := 1;               -- Id del pais
    csgSistema           CONSTANT NUMBER (1) := 1;           -- Id del sisterma
    csgModulo            CONSTANT NUMBER (1) := 7;     -- Id del modulo compras
    csgSubmodulo         CONSTANT NUMBER (1) := 1; -- Id del submodulo articulos
    csgIdConfiCrontTda   CONSTANT NUMBER (2) := 10; -- Id de configuracion envio de costo por pieza
    csgIdConfLlaveEnc    CONSTANT NUMBER (2) := 11; --Id de Configuracion llave de encriptacion



    vg_Proceso                    VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                    := 'VELIT.PAWSESTRATEGIASPROMOCION'; -- Proceso
    vg_Tabla                      VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
    vg_ExecSql                    VELIT.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
    vg_BitacoraId                 VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
                                    := 0;                 -- Id de la bitacora
    vg_DscError                   VELIT.TABITACORAERRORES.FCDESERROR%TYPE
                                    := ''; -- Almacena la descripción del error de oracle.
    vg_CdgError                   VELIT.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
    exc_Excepcion                 EXCEPTION;         -- Excepcion personalizada
    csg_IndError         CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
      := 1 ; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
    csg_IndAlerta        CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
      := 2 ; -- Indica si es una alerta por lo tanto no se aplica commit dentro del store.
    csg_MUno             CONSTANT NUMBER (1) := -1;       -- Valor constante -1
    csg_Cero             CONSTANT NUMBER (1) := 0;         -- Valor constante 0
    csg_Uno              CONSTANT NUMBER (1) := 1;         -- Valor constante 1
    csg_Dos              CONSTANT NUMBER (1) := 2;         -- Valor constante 2
    csg_Tres             CONSTANT NUMBER (1) := 3;         -- Valor constante 3
    csg_Cuatro           CONSTANT NUMBER (1) := 4;         -- Valor constante 4
    vg_BitacoraErroresId          VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
                                    := csg_Cero;

   PROCEDURE SPINSERTAESTRATEGIASCRON (
        parr_ArregloCambios   IN     VELIT.TYPARRAYENVIO,
        prec_CurDatosSalida      OUT VELIT.TYPES.CURSORTYP,
        ptab_CdgError            OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
        ptab_DscError            OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

    /***********************************************************************************************
    Proyecto: Sistema Integral de Operaciones NETO
    Descripción: Procedimiento para actualizar las estrategias de venta
    Parámetros de entrada: -paArregloCambios  -> Array Con los Cambios a procesar
    Parámetros de salida:  -paCurDatosSalida  -> Curson con la respuesta generada por articulo
                          -paCdgError       -> Código de respuesta generado por ORACLE
                          -paDescError      -> Descripción de respuesta generado por ORACLE
    Creador: Joaquin Jaime Sánchez Salgado
    Fecha: 22/11/2021
    ***********************************************************************************************/
    PROCEDURE SPHABILITACRONTDA (
        paPaisId            IN     NUMBER,
        paHabilitaCronTda   IN     NUMBER,
        paCdgError             OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
        paDescError            OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);
    /***********************************************************************************************
    Proyecto: Sistema Integral de Operaciones NETO
    Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
    Parámetros de entrada: -paPaisId  -> Id del Pais

    Parámetros de salida:  -paHabilitaCronTda  -> Valor del Parametro
                       -paCdgError       -> Código de error generado por ORACLE, si no
                                            hay errores se manda un cero
                       -paDescError      -> Descripción del error generado por ORACLE
    Creador: Omar Perez Alavez
    Fecha de creación: 20/09/2016
    ***********************************************************************************************/

END PAWSESTRATEGIASPROMOCION;
/

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSESTRATEGIASPROMOCION
AS
    PROCEDURE SPINSERTAESTRATEGIASCRON (
        parr_ArregloCambios   IN     VELIT.TYPARRAYENVIO,
        prec_CurDatosSalida      OUT VELIT.TYPES.CURSORTYP,
        ptab_CdgError            OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
        ptab_DscError            OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
    /***********************************************************************************************
    Proyecto: Sistema Integral de Operaciones NETO
    Descripción: Procedimiento para actualizar las estrategias de venta
    Parámetros de entrada: -parr_ArregloCambios  -> Array Con los Cambios a procesar
    Parámetros de salida:  -prec_CurDatosSalida  -> Curson con la respuesta generada por articulo
                          -ptab_CdgError       -> Código de respuesta generado por ORACLE
                          -paDescError      -> Descripción de respuesta generado por ORACLE
    Creador: Joaquin Jaime Sánchez Salgado
    Fecha: 22/11/2021
    ***********************************************************************************************/
    IS
        vl_Cadena              VARCHAR2 (4000 CHAR) := '';
        vl_Cont                NUMBER (3) := 1; --- contador para recorrer cadena
        vl_PosI                NUMBER (4) := 0; ---- Validar posiciones de cadenas
        vl_PosF                NUMBER (4) := 0; ---- Validar posiciones de cadenas
        vl_Largo               NUMBER (4) := 0; ---- Validar posiciones de cadenas

        ---- Array cabecero --
        vl_PaisId              NUMBER (3);
        vl_EstrategiaprcId     NUMBER (18);
        vl_NumEstrategia       NUMBER (18);
        vl_TipoEstratgId       NUMBER (3);
        vl_CombinaOfrtId       NUMBER (2);
        vl_Descripcion         VARCHAR2 (50 CHAR);
        vl_VentaObjetivo       NUMBER (18, 2);
        vl_MonedaId            NUMBER (2);
        vl_FechaIniVigencia    VARCHAR2 (20 CHAR);
        vl_FechaFinVigencia    VARCHAR2 (20 CHAR);
        vl_EstatusEstrtgId     NUMBER (2);
        vl_RegAplicaTda        NUMBER (1);
        vl_ConfigReglasId      NUMBER (3);
        vl_FechaRegistro       VARCHAR2 (20 CHAR);
        vl_UsuarioRegistraId   NUMBER (12);
        vl_UsuarioModificaId   NUMBER (12);
        vl_EnvioaTda           NUMBER (1);
        vl_Observaciones       VARCHAR2 (500 CHAR);
        vl_MaxCombos           NUMBER (4);
        vl_EstrategiapadreId   NUMBER (18);
        vl_UrlPromocional      VARCHAR2 (200 CHAR);

        ---- Array articulos --
        vl_ArticuloId          NUMBER (12);
        vl_Preciobase          NUMBER (8, 2);
        vl_Montoaplica         NUMBER (8, 2);
        vl_Preciominimo        NUMBER (8, 2);
        vl_Estatus             NUMBER (1);
        vl_Fechamodificacion   VARCHAR2 (20 CHAR);
        vl_Tipodescuento       NUMBER (3);
        vl_Tipodescregalo      NUMBER (3);
        vl_Descripcionregla    VARCHAR2 (300 CHAR);
        vl_Totalartreqrd       NUMBER (18, 2);
        vl_Totalartregalo      NUMBER (18, 2);

        ---- Array periodos --
        vl_PeriodoId           NUMBER (8);
        vl_NumPeriodo          NUMBER (3);
        vl_FechaIniVig         VARCHAR2 (20 CHAR);
        vl_FechaFinVig         VARCHAR2 (20 CHAR);
        vl_NumPeriodos         NUMBER (3);
        vl_TipoPeriodo         NUMBER (1);
        vl_DiasAplicacion      VARCHAR2 (100 CHAR);
        vl_FechaIniPeriodo     VARCHAR2 (20 CHAR);
        vl_FechaFinPeriodo     VARCHAR2 (20 CHAR);
        vl_EstatusId           NUMBER (1);


        vl_DatosSalida         VELIT.TYPARRAYENVIO := VELIT.TYPARRAYENVIO (); --- registro con los datos que seran enviado a central
        vl_CadenaEnvio         VARCHAR2 (2000) := ''; ---- cadena con los datos de resultado

        CURSOR cur_CadenaCabecero
        IS
            SELECT FITIPO,
                  FITIENDAID,
                  FIFECHAID,
                  FCREGISTROS
             FROM (SELECT TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                    '[^}]+',
                                                    csg_Uno,
                                                    csg_Uno))
                             AS FITIPO,
                          TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                    '[^}]+',
                                                    csg_Uno,
                                                    csg_Dos))
                             AS FITIENDAID,
                          TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                    '[^}]+',
                                                    csg_Uno,
                                                    csg_Tres))
                             AS FIFECHAID,
                          REGEXP_SUBSTR (FCREGISTROS,
                                         '[^}]+',
                                         csg_Uno,
                                         csg_Cuatro)
                             AS FCREGISTROS
                     FROM TABLE (parr_ArregloCambios))
            ORDER BY FITIPO ASC;

    BEGIN

        vgProceso :=  'SPINSERTAESTRATEGIASCRON';
        vg_Tabla := 'cur_CadenaCabecero';

        vg_Tabla := 'INICIA_SEPARACION';
        FOR C IN cur_CadenaCabecero LOOP
            EXIT WHEN cur_CadenaCabecero%NOTFOUND;
            vg_Tabla := 'cur_CadenaCabecero_01';

            IF C.FITIPO = csg_Uno THEN
                
                BEGIN
                    vl_PosI := csg_Uno;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_PaisId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, csg_Cero, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EstrategiaprcId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vg_ExecSql := 'vl_NumEstrategia';
                    vl_NumEstrategia :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_TipoEstratgId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_CombinaOfrtId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Descripcion :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_VentaObjetivo :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_MonedaId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaIniVigencia :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaFinVigencia :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EstatusEstrtgId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_RegAplicaTda :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_ConfigReglasId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaRegistro :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UsuarioRegistraId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UsuarioModificaId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EnvioaTda :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Observaciones :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_MaxCombos :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EstrategiapadreId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF := LENGTH (C.FCREGISTROS);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UrlPromocional :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);

                    vg_Tabla := ' INTO VELIT.TAESTRATGPRECIO_TDA';

                    BEGIN
                        MERGE INTO VELIT.TAESTRATGPRECIO_TDA A
                            USING (SELECT vl_PaisId          AS FIPAISID,
                                         vl_EstrategiaprcId AS FIESTRATEGIAPRCID,
                                         vl_NumEstrategia   AS FINUMESTRATEGIA,
                                         vl_TipoEstratgId   AS FITIPOESTRATGID,
                                         vl_CombinaOfrtId   AS FICOMBINAOFRTID,
                                         vl_Descripcion     AS FCDESCRIPCION,
                                         vl_VentaObjetivo   AS FNVENTAOBJETIVO,
                                         vl_MonedaId        AS FIMONEDAID,
                                         TO_DATE (vl_FechaIniVigencia,
                                                  'DD/MM/YYYY HH24:MI:SS')
                                            AS FDFECHAINICIOVIGENCIA,
                                         TO_DATE (vl_FechaFinVigencia,
                                                  'DD/MM/YYYY HH24:MI:SS')
                                            AS FDFECHAFINVIGENCIA,
                                         vl_EstatusEstrtgId AS FIESTATUSESTRTGID,
                                         vl_RegAplicaTda    AS FIREGAPLICATDA,
                                         vl_ConfigReglasId  AS FICONFIGREGLASID,
                                         TO_DATE (vl_FechaRegistro,
                                                  'DD/MM/YYYY HH24:MI:SS')
                                            AS FDFECHAREGISTRO,
                                         vl_UsuarioRegistraId
                                            AS FIUSUARIOREGISTRAID,
                                         vl_UsuarioModificaId
                                            AS FIUSUARIOMODIFICAID,
                                         SYSDATE
                                            AS FDFECHAMODIFICACION,
                                         vl_EnvioaTda       AS FIENVIOATDA,
                                         vl_Observaciones   AS FCOBSERVACIONES,
                                         vl_MaxCombos       AS FIMAXCOMBOS,
                                         vl_EstrategiapadreId
                                            AS FIESTRATEGIAPADREID,
                                         vl_UrlPromocional  AS FCURLPROMOCIONAL
                                    FROM DUAL) B
                              ON (    A.FIPAISID = B.FIPAISID
                                  AND A.FIESTRATEGIAPRCID = B.FIESTRATEGIAPRCID)
                        WHEN NOT MATCHED
                        THEN
                            INSERT    (FIPAISID,
                                     FIESTRATEGIAPRCID,
                                     FINUMESTRATEGIA,
                                     FITIPOESTRATGID,
                                     FICOMBINAOFRTID,
                                     FCDESCRIPCION,
                                     FNVENTAOBJETIVO,
                                     FIMONEDAID,
                                     FDFECHAINICIOVIGENCIA,
                                     FDFECHAFINVIGENCIA,
                                     FIESTATUSESTRTGID,
                                     FIREGAPLICATDA,
                                     FICONFIGREGLASID,
                                     FDFECHAREGISTRO,
                                     FIUSUARIOREGISTRAID,
                                     FIUSUARIOMODIFICAID,
                                     FDFECHAMODIFICACION,
                                     FIENVIOATDA,
                                     FCOBSERVACIONES,
                                     FIMAXCOMBOS,
                                     FIESTRATEGIAPADREID,
                                     FCURLPROMOCIONAL)
                             VALUES (B.FIPAISID,
                                     B.FIESTRATEGIAPRCID,
                                     B.FINUMESTRATEGIA,
                                     B.FITIPOESTRATGID,
                                     B.FICOMBINAOFRTID,
                                     B.FCDESCRIPCION,
                                     B.FNVENTAOBJETIVO,
                                     B.FIMONEDAID,
                                     B.FDFECHAINICIOVIGENCIA,
                                     B.FDFECHAFINVIGENCIA,
                                     B.FIESTATUSESTRTGID,
                                     B.FIREGAPLICATDA,
                                     B.FICONFIGREGLASID,
                                     B.FDFECHAREGISTRO,
                                     B.FIUSUARIOREGISTRAID,
                                     B.FIUSUARIOMODIFICAID,
                                     B.FDFECHAMODIFICACION,
                                     B.FIENVIOATDA,
                                     B.FCOBSERVACIONES,
                                     B.FIMAXCOMBOS,
                                     B.FIESTRATEGIAPADREID,
                                     B.FCURLPROMOCIONAL)
                        WHEN MATCHED
                        THEN
                         UPDATE SET
                            A.FINUMESTRATEGIA = B.FINUMESTRATEGIA,
                            A.FITIPOESTRATGID = B.FITIPOESTRATGID,
                            A.FICOMBINAOFRTID = B.FICOMBINAOFRTID,
                            A.FCDESCRIPCION = B.FCDESCRIPCION,
                            A.FNVENTAOBJETIVO = B.FNVENTAOBJETIVO,
                            A.FIMONEDAID = B.FIMONEDAID,
                            A.FDFECHAINICIOVIGENCIA = B.FDFECHAINICIOVIGENCIA,
                            A.FDFECHAFINVIGENCIA = B.FDFECHAFINVIGENCIA,
                            A.FIESTATUSESTRTGID = B.FIESTATUSESTRTGID,
                            A.FIREGAPLICATDA = B.FIREGAPLICATDA,
                            A.FICONFIGREGLASID = B.FICONFIGREGLASID,
                            A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                            A.FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                            A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                            A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                            A.FIENVIOATDA = B.FIENVIOATDA,
                            A.FCOBSERVACIONES = B.FCOBSERVACIONES,
                            A.FIMAXCOMBOS = B.FIMAXCOMBOS,
                            A.FIESTRATEGIAPADREID = B.FIESTRATEGIAPADREID,
                            A.FCURLPROMOCIONAL = B.FCURLPROMOCIONAL;

                        vl_CadenaEnvio :=
                            C.FITIPO
                         || '}'
                         || C.FITIENDAID
                         || '}'
                         || C.FIFECHAID
                         || '}'
                         || C.FCREGISTROS
                         || '}1}REGISTRO ACTUALIZADO CON EXITO};';

                    EXCEPTION
                        WHEN OTHERS THEN
                            vg_Tabla := 'ERR_INTO VELIT.TAESTRATGPRECIO_TDA';
                            vg_ExecSql :=
                               C.FITIPO
                            || '}'
                            || C.FITIENDAID
                            || '}'
                            || C.FIFECHAID
                            || '}'
                            || C.FCREGISTROS
                            || '}2}'
                            || SQLERRM
                            || '}';
                            VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                              csg_Cero,
                                                              vg_Proceso,
                                                              vg_Tabla,
                                                              csg_Cero,
                                                              SQLCODE,
                                                              SQLERRM,
                                                              vg_ExecSql,
                                                              csg_IndError,
                                                              vg_BitacoraId);
                    END;

                    vl_Cont := csg_Uno;
                    vl_PosI := csg_Cero;
                    vl_PosF := csg_Cero;
                    vl_Largo := csg_Cero;

                EXCEPTION
                   WHEN OTHERS
                   THEN
                      vl_CadenaEnvio := '<>';
                      VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                           csg_Cero,
                                                           vg_Proceso,
                                                           vg_Tabla,
                                                           csg_Cero,
                                                           SQLCODE,
                                                           SQLERRM,
                                                           vg_ExecSql,
                                                           csg_IndError,
                                                           vg_BitacoraId);
                END;

                IF (vl_CadenaEnvio <> '<>')
                THEN
                   vl_DatosSalida.EXTEND;
                   vl_DatosSalida (vl_DatosSalida.COUNT) :=
                      VELIT.TYPCADENAENVIO (vl_CadenaEnvio);
                END IF;

            ELSIF C.FITIPO = csg_Dos THEN
                vl_Cont := csg_Uno;
                vl_PosI := csg_Cero;
                vl_PosF := csg_Cero;
                vl_Largo := csg_Cero;

                BEGIN
                    vl_PosI := csg_Uno;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_PaisId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, csg_Cero, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EstrategiaprcId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_ArticuloId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Preciobase :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Montoaplica :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Preciominimo :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Estatus :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Fecharegistro :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UsuarioregistraId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UsuariomodificaId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Fechamodificacion :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Tipodescuento :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Tipodescregalo :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Descripcionregla :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Totalartreqrd :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF := LENGTH (C.FCREGISTROS);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Totalartregalo :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));

                     vg_Tabla := ' INTO VELIT.TAESTRPRECXART_TDA';
                     
                    BEGIN
                        MERGE INTO VELIT.TAESTRPRECXART_TDA A
                           USING (SELECT vl_PaisId          AS FIPAISID,
                                         vl_EstrategiaprcId AS FIESTRATEGIAPRCID,
                                         vl_ArticuloId      AS FIARTICULOID,
                                         vl_Preciobase      AS FNPRECIOBASE,
                                         vl_Montoaplica     AS FNMONTOAPLICA,
                                         vl_Preciominimo    AS FNPRECIOMINIMO,
                                         vl_Estatus         AS FIESTATUS,
                                         TO_DATE (vl_Fecharegistro,
                                                  'DD/MM/YYYY HH24:MI:SS')
                                            AS FDFECHAREGISTRO,
                                         vl_UsuarioregistraId
                                            AS FIUSUARIOREGISTRAID,
                                         vl_UsuariomodificaId
                                            AS FIUSUARIOMODIFICAID,
                                         SYSDATE
                                            AS FDFECHAMODIFICACION,
                                         vl_Tipodescuento   AS FITIPODESCUENTO,
                                         vl_Tipodescregalo  AS FITIPODESCREGALO,
                                         vl_Descripcionregla
                                            AS FCDESCRIPCIONREGLA,
                                         vl_Totalartreqrd   AS FITOTALARTREQRD,
                                         vl_Totalartregalo  AS FITOTALARTREGALO
                                    FROM DUAL) B
                              ON (    A.FIPAISID = B.FIPAISID
                                  AND A.FIESTRATEGIAPRCID = B.FIESTRATEGIAPRCID)
                        WHEN NOT MATCHED
                        THEN
                         INSERT     (FIPAISID,
                                     FIESTRATEGIAPRCID,
                                     FIARTICULOID,
                                     FNPRECIOBASE,
                                     FNMONTOAPLICA,
                                     FNPRECIOMINIMO,
                                     FIESTATUS,
                                     FDFECHAREGISTRO,
                                     FIUSUARIOREGISTRAID,
                                     FIUSUARIOMODIFICAID,
                                     FDFECHAMODIFICACION,
                                     FITIPODESCUENTO,
                                     FITIPODESCREGALO,
                                     FCDESCRIPCIONREGLA,
                                     FITOTALARTREQRD,
                                     FITOTALARTREGALO)
                             VALUES (B.FIPAISID,
                                     B.FIESTRATEGIAPRCID,
                                     B.FIARTICULOID,
                                     B.FNPRECIOBASE,
                                     B.FNMONTOAPLICA,
                                     B.FNPRECIOMINIMO,
                                     B.FIESTATUS,
                                     B.FDFECHAREGISTRO,
                                     B.FIUSUARIOREGISTRAID,
                                     B.FIUSUARIOMODIFICAID,
                                     B.FDFECHAMODIFICACION,
                                     B.FITIPODESCUENTO,
                                     B.FITIPODESCREGALO,
                                     B.FCDESCRIPCIONREGLA,
                                     B.FITOTALARTREQRD,
                                     B.FITOTALARTREGALO)
                        WHEN MATCHED
                        THEN
                         UPDATE SET
                            A.FNPRECIOBASE = B.FNPRECIOBASE,
                            A.FNMONTOAPLICA = B.FNMONTOAPLICA,
                            A.FNPRECIOMINIMO = B.FNPRECIOMINIMO,
                            A.FIESTATUS = B.FIESTATUS,
                            A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                            A.FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                            A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                            A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                            A.FITIPODESCUENTO = B.FITIPODESCUENTO,
                            A.FITIPODESCREGALO = B.FITIPODESCREGALO,
                            A.FCDESCRIPCIONREGLA = B.FCDESCRIPCIONREGLA,
                            A.FITOTALARTREQRD = B.FITOTALARTREQRD,
                            A.FITOTALARTREGALO = B.FITOTALARTREGALO;

                        vl_CadenaEnvio :=
                            C.FITIPO
                         || '}'
                         || C.FITIENDAID
                         || '}'
                         || C.FIFECHAID
                         || '}'
                         || C.FCREGISTROS
                         || '}1}REGISTRO ACTUALIZADO CON EXITO;';
                    EXCEPTION
                        WHEN OTHERS
                        THEN
                         vg_ExecSql :=
                               C.FITIPO
                            || '}'
                            || C.FITIENDAID
                            || '}'
                            || C.FIFECHAID
                            || '}'
                            || C.FCREGISTROS
                            || '}2}'
                            || SQLERRM
                            || '}';
                        VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                              csg_Cero,
                                                              vg_Proceso,
                                                              vg_Tabla,
                                                              csg_Cero,
                                                              SQLCODE,
                                                              SQLERRM,
                                                              vg_ExecSql,
                                                              csg_IndError,
                                                              vg_BitacoraId);
                    END;

                    vl_Cont := csg_Uno;
                    vl_PosI := csg_Cero;
                    vl_PosF := csg_Cero;
                    vl_Largo := csg_Cero;

                EXCEPTION
                    WHEN OTHERS
                    THEN
                        vl_CadenaEnvio := '<>';
                        VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                           csg_Cero,
                                                           vg_Proceso,
                                                           vg_Tabla,
                                                           csg_Cero,
                                                           SQLCODE,
                                                           SQLERRM,
                                                           vg_ExecSql,
                                                           csg_IndError,
                                                           vg_BitacoraId);
                END;

                IF (vl_CadenaEnvio <> '<>')
                THEN
                   vl_DatosSalida.EXTEND;
                   vl_DatosSalida (vl_DatosSalida.COUNT) :=
                      VELIT.TYPCADENAENVIO (vl_CadenaEnvio);
                END IF;
            
            ELSIF C.FITIPO = csg_Tres THEN
                vl_Cont := csg_Uno;
                vl_PosI := csg_Cero;
                vl_PosF := csg_Cero;
                vl_Largo := csg_Cero;

                BEGIN
                    vl_PosI := csg_Uno;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_PaisId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, csg_Cero, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EstrategiaprcId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_PeriodoId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_NumPeriodo :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaIniVig :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaFinVig :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UsuarioregistraId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_Fecharegistro :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_NumPeriodos :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_TipoPeriodo :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_DiasAplicacion :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaIniPeriodo :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaFinPeriodo :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_UsuarioModificaId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF :=
                      INSTR (C.FCREGISTROS,
                             '<!',
                             csg_Uno,
                             vl_Cont);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_FechaModificacion :=
                      SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
                    vl_Cont := vl_Cont + csg_Uno;
                    vl_PosI := vl_PosF + csg_Dos;
                    vl_PosF := LENGTH (C.FCREGISTROS);
                    vl_Largo := vl_PosF - vl_PosI;
                    vl_EstatusId :=
                      TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));

                    vg_Tabla := ' INTO VELIT.TAPERIODOESTRTG';
                    BEGIN
                        MERGE INTO VELIT.TAPERIODOESTRTG A
                           USING (SELECT vl_PeriodoId      AS FIPERIODOID,
                                         vl_NumPeriodos    AS FINUMPERIODOS,
                                         vl_TipoPeriodo    AS FITIPOPERIODO,
                                         vl_DiasAplicacion AS FCDIASAPLICACION,
                                         TO_DATE (vl_FechaIniPeriodo,
                                                  'DD/MM/YYYY')
                                            AS FDINICOPERIODO,
                                         TO_DATE (vl_FechaFinPeriodo,
                                                  'DD/MM/YYYY')
                                            AS FDFINPERIODO,
                                         TO_DATE (vl_Fecharegistro,
                                                  'DD/MM/YYYY HH24:MI:SS')
                                            AS FDFECHAREGISTRO,
                                         vl_UsuarioregistraId
                                            AS FIUSUARIOINSERTAID,
                                         vl_UsuarioModificaId
                                            AS FIUSUARIOMODIFICAID,
                                         SYSDATE           AS FDFECHAMODIFICACION,
                                         vl_EstatusId      AS FIESTATUS
                                    FROM DUAL) B
                              ON (A.FIPERIODOID = B.FIPERIODOID)
                        WHEN NOT MATCHED
                        THEN
                            INSERT     (FIPERIODOID,
                                     FINUMPERIODOS,
                                     FITIPOPERIODO,
                                     FCDIASAPLICACION,
                                     FDINICOPERIODO,
                                     FDFINPERIODO,
                                     FDFECHAREGISTRO,
                                     FIUSUARIOINSERTAID,
                                     FIUSUARIOMODIFICAID,
                                     FDFECHAMODIFICACION,
                                     FIESTATUS)
                             VALUES (B.FIPERIODOID,
                                     B.FINUMPERIODOS,
                                     B.FITIPOPERIODO,
                                     B.FCDIASAPLICACION,
                                     B.FDINICOPERIODO,
                                     B.FDFINPERIODO,
                                     B.FDFECHAREGISTRO,
                                     B.FIUSUARIOINSERTAID,
                                     B.FIUSUARIOMODIFICAID,
                                     B.FDFECHAMODIFICACION,
                                     B.FIESTATUS)
                        WHEN MATCHED
                        THEN
                         UPDATE SET
                            A.FINUMPERIODOS = B.FINUMPERIODOS,
                            A.FITIPOPERIODO = B.FITIPOPERIODO,
                            A.FCDIASAPLICACION = B.FCDIASAPLICACION,
                            A.FDINICOPERIODO = B.FDINICOPERIODO,
                            A.FDFINPERIODO = B.FDFINPERIODO,
                            A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                            A.FIUSUARIOINSERTAID = B.FIUSUARIOINSERTAID,
                            A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                            A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                            A.FIESTATUS = B.FIESTATUS;

                        vg_Tabla := ' INTO VELIT.TAESTRATGPRXPER';
                        MERGE INTO VELIT.TAESTRATGPRXPER A
                           USING (SELECT vl_PaisId          AS FIPAISID,
                                         vl_EstrategiaprcId AS FIESTARTEGIAPRCID,
                                         vl_PeriodoId       AS FIPERIODOID,
                                         vl_NumPeriodo      AS FINUMPERIODO,
                                         TO_DATE (vl_FechaIniVig, 'DD/MM/YYYY')
                                            AS FDFECHAINICIOVIGENCIA,
                                         TO_DATE (vl_FechaFinVig, 'DD/MM/YYYY')
                                            AS FDFECHAFINVIGENCIA,
                                         vl_UsuarioregistraId
                                            AS FIUSUARIOREGISTRAID,
                                         TO_DATE (vl_Fecharegistro,
                                                  'DD/MM/YYYY HH24:MI:SS')
                                            AS FDFECHAREGISTRO,
                                         vl_UsuarioModificaId
                                            AS FIUSUARIOMODIFICAID,
                                         SYSDATE
                                            AS FDFECHAMODIFICACION,
                                         vl_EstatusId       AS FIESTATUS
                                    FROM DUAL) B
                              ON (    A.FIPAISID = B.FIPAISID
                                  AND A.FIESTARTEGIAPRCID = B.FIESTARTEGIAPRCID
                                  AND A.FIPERIODOID = B.FIPERIODOID
                                  AND A.FINUMPERIODO = B.FINUMPERIODO)
                        WHEN NOT MATCHED
                        THEN
                         INSERT     (FIPAISID,
                                     FIESTARTEGIAPRCID,
                                     FIPERIODOID,
                                     FINUMPERIODO,
                                     FDFECHAINICIOVIGENCIA,
                                     FDFECHAFINVIGENCIA,
                                     FIUSUARIOREGISTRAID,
                                     FDFECHAREGISTRO,
                                     FIUSUARIOMODIFICAID,
                                     FDFECHAMODIFICACION,
                                     FIESTATUS)
                             VALUES (B.FIPAISID,
                                     B.FIESTARTEGIAPRCID,
                                     B.FIPERIODOID,
                                     B.FINUMPERIODO,
                                     B.FDFECHAINICIOVIGENCIA,
                                     B.FDFECHAFINVIGENCIA,
                                     B.FIUSUARIOREGISTRAID,
                                     B.FDFECHAREGISTRO,
                                     B.FIUSUARIOMODIFICAID,
                                     B.FDFECHAMODIFICACION,
                                     B.FIESTATUS)
                        WHEN MATCHED
                        THEN
                         UPDATE SET
                            A.FDFECHAINICIOVIGENCIA = B.FDFECHAINICIOVIGENCIA,
                            A.FDFECHAFINVIGENCIA = B.FDFECHAFINVIGENCIA,
                            A.FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                            A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                            A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                            A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                            A.FIESTATUS = B.FIESTATUS;

                        vl_CadenaEnvio :=
                            C.FITIPO
                         || '}'
                         || C.FITIENDAID
                         || '}'
                         || C.FIFECHAID
                         || '}'
                         || C.FCREGISTROS
                         || '}1}REGISTRO ACTUALIZADO CON EXITO;';
                    EXCEPTION
                        WHEN OTHERS
                        THEN
                         vg_ExecSql :=
                               C.FITIPO
                            || '}'
                            || C.FITIENDAID
                            || '}'
                            || C.FIFECHAID
                            || '}'
                            || C.FCREGISTROS
                            || '}2}'
                            || SQLERRM
                            || '}';
                        VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                              csg_Cero,
                                                              vg_Proceso,
                                                              vg_Tabla,
                                                              csg_Cero,
                                                              SQLCODE,
                                                              SQLERRM,
                                                              vg_ExecSql,
                                                              csg_IndError,
                                                              vg_BitacoraId);
                    END;

                    vl_Cont := csg_Uno;
                    vl_PosI := csg_Cero;
                    vl_PosF := csg_Cero;
                    vl_Largo := csg_Cero;
                
                EXCEPTION
                    WHEN OTHERS
                    THEN
                      vl_CadenaEnvio := '<>';
                      VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                                           csg_Cero,
                                                           vg_Proceso,
                                                           vg_Tabla,
                                                           csg_Cero,
                                                           SQLCODE,
                                                           SQLERRM,
                                                           vg_ExecSql,
                                                           csg_IndError,
                                                           vg_BitacoraId);
                END;

                IF (vl_CadenaEnvio <> '<>')
                THEN
                   vl_DatosSalida.EXTEND;
                   vl_DatosSalida (vl_DatosSalida.COUNT) :=
                      VELIT.TYPCADENAENVIO (vl_CadenaEnvio);
                END IF;
            END IF;

            COMMIT;

        END LOOP;

        OPEN prec_CurDatosSalida FOR
        SELECT FCREGISTROS FROM TABLE (vl_DatosSalida);

        ptab_CdgError := csg_Uno;
        ptab_DscError := 'Actualización exitosa';

    EXCEPTION
        WHEN OTHERS THEN
            ptab_CdgError := csg_MUno;
            ptab_DscError := 'Error al insertar las estrategias';
            VELIT.PAGENERICOS.SPBITACORAERRORES (csg_Uno,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              SQLCODE,
                                              SQLERRM,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
    END SPINSERTAESTRATEGIASCRON;

   PROCEDURE SPHABILITACRONTDA (
        paPaisId            IN     NUMBER,
        paHabilitaCronTda   IN     NUMBER,
        paCdgError             OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
        paDescError            OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
    IS
      /***********************************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO
      Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
      Parámetros de entrada: -paPaisId  -> Id del Pais

      Parámetros de salida:  -paHabilitaCronTda  -> Valor del Parametro
                             -paCdgError       -> Código de error generado por ORACLE, si no
                                                  hay errores se manda un cero
                             -paDescError      -> Descripción del error generado por ORACLE
      Creador: Omar Perez Alavez
      Fecha de creación: 20/09/2016
      ***********************************************************************************************/

        vlValorConfig                   VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE := 0; --Almacena el valor del parametro

        cslIdConfCronSiemAct   CONSTANT NUMBER (2) := 12; --Id de configuracion para nunca deshabilitar el Cron
    
    BEGIN
        vgProceso := 'SPCONSULTAPARAMCRON';
        vgTabla := 'TACONFIGURACIONPARAMETRO';

        IF (paHabilitaCronTda = csgDesactivado) THEN
            --Consulto el parametro que no permite el deshabilitar el Cron
            VELIT.PAGENERICOS.SPCONSULTAPARAMETROS (csgPaisId,
                                                 csgSistema,
                                                 csgModulo,
                                                 csgSubmodulo,
                                                 cslIdConfCronSiemAct,
                                                 vgCdgError,
                                                 vgDescError,
                                                 vlValorConfig);

            IF (vlValorConfig = csgDesactivado) THEN
               
                UPDATE VELIT.TACONFIGURACIONPARAMETRO
                   SET FNVALOR = paHabilitaCronTda, FDFECHAMODIFICACION = SYSDATE
                 WHERE     FIPAISID = csgPaisId
                       AND FISISTEMAID = csgSistema
                       AND FIMODULOID = csgModulo
                       AND FISUBMODULOID = csgSubModulo
                       AND FICONFIGURACIONID = csgIdConfiCrontTda;
            END IF;

        ELSE

            UPDATE VELIT.TACONFIGURACIONPARAMETRO
                SET FNVALOR = paHabilitaCronTda, FDFECHAMODIFICACION = SYSDATE
            WHERE     FIPAISID = csgPaisId
                AND FISISTEMAID = csgSistema
                AND FIMODULOID = csgModulo
                AND FISUBMODULOID = csgSubModulo
                AND FICONFIGURACIONID = csgIdConfiCrontTda;

            COMMIT;

        END IF;

        paCdgError := csgCero;
        paDescError := 'Se Modifico Parametro Exitosamente';

    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            paCdgError := -csgUno;
            paDescError := 'Se presento un error al ejecutar el proceso.';
            vgDescError := SQLERRM;
            vgCdgError := SQLCODE;
    END SPHABILITACRONTDA;

END PAWSESTRATEGIASPROMOCION;
/