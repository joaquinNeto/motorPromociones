CREATE OR REPLACE PACKAGE VELIT.PAWSPROMDESG08
AS
   csg_MUno        CONSTANT NUMBER (2) := -1;
   csg_Cero        CONSTANT NUMBER (1) := 0;
   csg_Uno         CONSTANT NUMBER (1) := 1;
   csg_Dos         CONSTANT NUMBER (1) := 2;
   csg_Tres        CONSTANT NUMBER (1) := 3;
   csg_Dieciocho   CONSTANT NUMBER (2) := 18;

   FUNCTION FNCAMBIOSARTICULO (
      ptab_ArticuloId   IN VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      pa_TipoConsulta   IN NUMBER)
      RETURN VARCHAR2;
/**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Consulta si el articulo tiene cambio de precio
   Parámetros de entrada: - ptab_ArticuloId        --- id del articulo
                          - pa_TipoConsulta  --- id de tipo consulta 1=Costo 2=Precio
   Parámetros de salida: VARCHAR2
   Parámetros de entrada-salida: N/A
   Valor de retorno:
   Precondiciones: N/A
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 26/11/2021
************************************************************************************/

END PAWSPROMDESG08;
/

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSPROMDESG08
AS
   FUNCTION FNCAMBIOSARTICULO (
      ptab_ArticuloId   IN VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      pa_TipoConsulta   IN NUMBER)
      RETURN VARCHAR2
   /**********************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO
      Descripción: Consulta si el articulo tiene cambio de precio
      Parámetros de entrada: - ptab_ArticuloId        --- id del articulo
                             - pa_TipoConsulta  --- id de tipo consulta 1=Costo 2=Precio
      Parámetros de salida: VARCHAR2
      Parámetros de entrada-salida: N/A
      Valor de retorno:
      Precondiciones: N/A
      Creador: Joaquin Jaime Sanchez Salgado
      Fecha de creación: 26/11/2021
   ************************************************************************************/
   IS
      vl_CambioCosto    VARCHAR2 (200 CHAR) := '';
      vl_CambioPrecio   VARCHAR2 (200 CHAR) := '';
      vl_Default        VARCHAR2 (200 CHAR) := '';
      vl_ExistePrecio   NUMBER (1) := 0;
      vl_ExisteCosto    NUMBER (1) := 0;
   BEGIN
      BEGIN
         SELECT    'Costo anterior: '
                || TRIM (TO_CHAR (FNCOSTOANTERIOR, 'L999G999G999D99MI'))
                || ', Fecha: '
                || FECHAAUTO
                   AS CAMBIOCOSTO
           INTO vl_CambioCosto
           FROM (SELECT FIARTICULOID,
                        TO_CHAR (FECHAAUTO, 'DD/MM/YYYY') AS FECHAAUTO,
                        CAMBIOCOSTO,
                        FNCOSTOANTERIOR,
                        ROW_NUMBER ()
                        OVER (PARTITION BY FIARTICULOID
                              ORDER BY FECHAAUTO DESC)
                           AS FIORDEN
                   FROM (SELECT SOL.FIARTICULOID,
                                TRUNC (SOL.FDFECHAACTUALIZA) AS FECHAAUTO,
                                CASE TO_NUMBER (
                                        SUBSTR (FCARREGLOCAMBIOS,
                                                csg_Tres,
                                                csg_Uno))
                                   WHEN csg_Uno
                                   THEN
                                      SOL.FNCOSTOXCAJA
                                   ELSE
                                      csg_MUno
                                END
                                   AS CAMBIOCOSTO,
                                CT.FNCOSTOXCAJA
                                   AS FNCOSTOANTERIOR,
                                ROW_NUMBER ()
                                OVER (
                                   PARTITION BY SOL.FDFECHAACTUALIZA,
                                                SOL.FIARTICULOID,
                                                SOL.FISOLICITUDMOVTOCDGBARRASID
                                   ORDER BY SOL.FDFECHAACTUALIZA DESC)
                                   AS FIORDEN
                           FROM VELIT.TASOLICITUDESMOVTOSCODBARRAS SOL
                                INNER JOIN
                                (  SELECT FIARTICULOID,
                                          FNCOSTOXCAJA,
                                          FCCDGBARRASID,
                                          FDFECHAACTUALIZA
                                     FROM VELIT.TASOLICITUDESMOVTOSCODBARRAS
                                    WHERE     FIESTATUSMOVTOID = csg_Dos
                                          AND FIARTICULOID = ptab_ArticuloId
                                 ORDER BY FDFECHAACTUALIZA DESC) CT
                                   ON     CT.FIARTICULOID = SOL.FIARTICULOID
                                      AND CT.FCCDGBARRASID =
                                             SOL.FCCDGBARRASID
                                      AND CT.FDFECHAACTUALIZA <
                                             SOL.FDFECHAACTUALIZA
                                      AND CT.FNCOSTOXCAJA <> SOL.FNCOSTOXCAJA
                          WHERE     SOL.FIESTATUSMOVTOID = csg_Dos
                                AND NOT EXISTS
                                       (  SELECT csg_Uno
                                            FROM VELIT.TASOLICITUDESMOVTOSARTICULOS
                                                 SA
                                           WHERE     SA.FISOLICITUDMOVTOARTID =
                                                        SOL.FISOLICITUDMOVTOCDGBARRASID
                                                 AND SA.FIARTICULOID =
                                                        SOL.FIARTICULOID
                                                 AND SA.FIESTATUSARTICULOID =
                                                        csg_Uno
                                                 AND SA.FIARTICULOID =
                                                        ptab_ArticuloId
                                        GROUP BY csg_Uno))
                  WHERE FIORDEN = csg_Uno AND CAMBIOCOSTO <> csg_MUno)
          WHERE FIORDEN = csg_Uno;

         vl_ExisteCosto := csg_Uno;
      EXCEPTION
         WHEN OTHERS
         THEN
            vl_CambioCosto := '';
            vl_ExisteCosto := csg_Cero;
      END;

      BEGIN
         SELECT    'Precio anterior: '
                || TRIM (TO_CHAR (FNPRECIOANTERIOR, 'L999G999G999D99MI'))
                || ', Fecha: '
                || FECHAAUTO
                   AS CAMBIOPRECIO
           INTO vl_CambioPrecio
           FROM (SELECT FIARTICULOID,
                        TO_CHAR (FECHAAUTO, 'DD/MM/YYYY') AS FECHAAUTO,
                        FNPRECIOVENTA,
                        FNPRECIOANTERIOR,
                        FDFECHAACTUALIZA,
                        ROW_NUMBER ()
                        OVER (PARTITION BY FIARTICULOID
                              ORDER BY FDFECHAACTUALIZA DESC)
                           AS FIORDEN
                   FROM (  SELECT SOL.FIARTICULOID,
                                  SOL.FDFECHAACTUALIZA,
                                  TRUNC (SOL.FDFECHAACTUALIZA) AS FECHAAUTO,
                                  SOL.FNPRECIOVENTA,
                                  AAR.FNPRECIOVENTA
                                     AS FNPRECIOANTERIOR,
                                  ROW_NUMBER ()
                                  OVER (
                                     PARTITION BY SOL.FDFECHAACTUALIZA,
                                                  SOL.FIARTICULOID,
                                                  SOL.FISOLICITUDMOVTOARTID
                                     ORDER BY SOL.FDFECHAACTUALIZA DESC)
                                     AS FIORDEN
                             FROM VELIT.TASOLICITUDESMOVTOSARTICULOS SOL
                                  INNER JOIN VELIT.TAARTICULOS AR
                                     ON SOL.FIARTICULOID = AR.FIARTICULOID
                                  INNER JOIN
                                  (  SELECT FIARTICULOID,
                                            FNPRECIOVENTA,
                                            FIESTATUSARTICULOID,
                                            FDFECHAACTUALIZA
                                       FROM VELIT.TASOLICITUDESMOVTOSARTICULOS
                                            SOL
                                      WHERE     FIESTATUSMOVTOID = csg_Dos
                                            AND FIARTICULOID = ptab_ArticuloId
                                   ORDER BY FDFECHAACTUALIZA DESC) AAR
                                     ON     AAR.FIARTICULOID = SOL.FIARTICULOID
                                        AND AAR.FDFECHAACTUALIZA <
                                               SOL.FDFECHAACTUALIZA
                                        AND AAR.FNPRECIOVENTA <>
                                               SOL.FNPRECIOVENTA
                            WHERE     TO_NUMBER (
                                         SUBSTR (FCARREGLOCAMBIOS,
                                                 csg_Dieciocho,
                                                 csg_Uno)) = csg_Uno
                                  AND SOL.FIESTATUSARTICULOID <> csg_Uno
                                  AND SOL.FIESTATUSMOVTOID = csg_Dos
                                  AND SOL.FIARTICULOID = ptab_ArticuloId
                         ORDER BY SOL.FDFECHAACTUALIZA DESC)
                  WHERE FIORDEN = csg_Uno)
          WHERE FIARTICULOID = ptab_ArticuloId AND FIORDEN = csg_Uno;

         vl_ExistePrecio := csg_Uno;
      EXCEPTION
         WHEN OTHERS
         THEN
            vl_CambioPrecio := '';
            vl_ExistePrecio := csg_Cero;
      END;

      IF pa_TipoConsulta = csg_Uno
      THEN
         IF vl_ExistePrecio = csg_Uno AND vl_ExisteCosto = csg_Uno
         THEN
            RETURN TRIM (vl_CambioPrecio) || ' | ' || TRIM (vl_CambioCosto);
         ELSIF vl_ExistePrecio = csg_Uno AND vl_ExisteCosto = csg_Cero
         THEN
            RETURN TRIM (vl_CambioPrecio);
         ELSIF vl_ExistePrecio = csg_Cero AND vl_ExisteCosto = csg_Uno
         THEN
            RETURN TRIM (vl_CambioCosto);
         ELSE
            RETURN '-';
         END IF;
      ELSIF pa_TipoConsulta = csg_Dos
      THEN
         IF vl_ExistePrecio = csg_Uno AND vl_ExisteCosto = csg_Uno
         THEN
            vl_Default := '2';
         ELSIF    (vl_ExistePrecio = csg_Uno AND vl_ExisteCosto = csg_Cero)
               OR (vl_ExistePrecio = csg_Cero AND vl_ExisteCosto = csg_Uno)
         THEN
            vl_Default := '1';
         ELSE
            vl_Default := '0';
         END IF;

         RETURN vl_Default;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN vl_Default;
   END FNCAMBIOSARTICULO;
END PAWSPROMDESG08;
/