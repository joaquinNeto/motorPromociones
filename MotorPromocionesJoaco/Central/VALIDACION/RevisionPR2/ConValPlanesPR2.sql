--PACKAGE VELIT.PAWSMOTORPROMTDA02


--FUNCTION FNCALCULAPERIODOESTR (


SELECT TO_CHAR (FDINICOPERIODO, 'DD/MM/YYYY'),
             TO_CHAR (FDFINPERIODO, 'DD/MM/YYYY'),
             TIPOPERIODO,
             SUBSTR (NVL (DIAS, '-11'),
                     1,
                     LENGTH (NVL (DIAS, '-11')) - 1)
                AS DIAS
--        INTO vl_IniPeriodo,
--             vl_FinPeriodo,
--             vl_TipoPeriodo,
--             vl_DiasPeriodo
        FROM (SELECT FIESTARTEGIAPRCID,
                     FDINICOPERIODO,
                     FDFINPERIODO,
                     TIPOPERIODO,
                        CASE FILUNES
                           WHEN 1 THEN 'LU-'
                           ELSE ''
                        END
                     || CASE FIMARTES
                           WHEN 1 THEN 'MA-'
                           ELSE ''
                        END
                     || CASE FIMIERCOLES
                           WHEN 1 THEN 'MI-'
                           ELSE ''
                        END
                     || CASE FIJUEVES
                           WHEN 1 THEN 'JU-'
                           ELSE ''
                        END
                     || CASE FIVIERNES
                           WHEN 1 THEN 'VI-'
                           ELSE ''
                        END
                     || CASE FISABADO
                           WHEN 1 THEN 'SA-'
                           ELSE ''
                        END
                     || CASE FIDOMINGO
                           WHEN 1 THEN 'DO-'
                           ELSE ''
                        END
                        DIAS
                FROM (SELECT FIESTARTEGIAPRCID,
                             FDINICOPERIODO,
                             FDFINPERIODO,
                             TIPOPERIODO,
                             TO_NUMBER (FILUNES)     AS FILUNES,
                             TO_NUMBER (FIMARTES)    AS FIMARTES,
                             TO_NUMBER (FIMIERCOLES) AS FIMIERCOLES,
                             TO_NUMBER (FIJUEVES)    AS FIJUEVES,
                             TO_NUMBER (FIVIERNES)   AS FIVIERNES,
                             TO_NUMBER (FISABADO)    AS FISABADO,
                             TO_NUMBER (FIDOMINGO)   AS FIDOMINGO
                        FROM (SELECT FIESTARTEGIAPRCID,
                                     FDINICOPERIODO,
                                     FDFINPERIODO,
                                     TIPOPERIODO,
                                     DIAS,
                                     ROWNUM AS ORDEN
                                FROM (    SELECT FIESTARTEGIAPRCID,
                                                 FDINICOPERIODO,
                                                 FDFINPERIODO,
                                                 TIPOPERIODO,
                                                 REGEXP_SUBSTR (
                                                    UPPER (FCDIASAPLICACION),
                                                    '[^,]+',
                                                    1,
                                                    LEVEL)
                                                    AS DIAS
                                            FROM (  SELECT FIESTARTEGIAPRCID,
                                                           FDINICOPERIODO,
                                                           FDFINPERIODO,
                                                           CASE FITIPOPERIODO
                                                              WHEN 1
                                                              THEN
                                                                 'DIARIO'
                                                              WHEN 2
                                                              THEN
                                                                 'SEMANA'
                                                              WHEN 3
                                                              THEN
                                                                 'MES'
                                                           END
                                                              AS TIPOPERIODO,
                                                           FCDIASAPLICACION
                                                      FROM VELIT.TAESTRATGPRXPER
                                                           SXP
                                                           INNER JOIN
                                                           VELIT.TAPERIODOESTRTG
                                                           PES
                                                              ON SXP.FIPERIODOID =
                                                                    PES.FIPERIODOID
                                                     WHERE     SXP.FIPAISID =
                                                                  1
                                                           AND FIESTARTEGIAPRCID =
                                                                  40
                                                           AND SXP.FIESTATUS =
                                                                  1
                                                           AND PES.FIESTATUS =
                                                                  1
                                                  GROUP BY FIESTARTEGIAPRCID,
                                                           FDINICOPERIODO,
                                                           FDFINPERIODO,
                                                           CASE FITIPOPERIODO
                                                              WHEN 1
                                                              THEN
                                                                 'DIARIO'
                                                              WHEN 2
                                                              THEN
                                                                 'SEMANA'
                                                              WHEN 3
                                                              THEN
                                                                 'MES'
                                                           END,
                                                           FCDIASAPLICACION)
                                      CONNECT BY REGEXP_SUBSTR (
                                                    UPPER (FCDIASAPLICACION),
                                                    '[^,]+',
                                                    1,
                                                    LEVEL)
                                                    IS NOT NULL))
                             PIVOT
                                (MAX (DIAS)
                                FOR ORDEN
                                IN ('1' AS FILUNES,
                                   '2' AS FIMARTES,
                                   '3' AS FIMIERCOLES,
                                   '4' AS FIJUEVES,
                                   '5' AS FIVIERNES,
                                   '6' AS FISABADO,
                                   '7' AS FIDOMINGO))));

--PROCEDURE SPACTUALIZAMOVENVTDA (


SELECT FIPAISID,
                FITIENDAID,
                FIESTRATEGIAPRCID,
                FIARTICULOID,
                FIESTATUS
           FROM 
		   (
		   SELECT 1 AS FIPAISID, 1621 AS FITIENDAID, 44 AS FIESTRATEGIAPRCID,3050001358 AS FIARTICULOID, 1 AS FIESTATUS FROM DUAL UNION
		   SELECT 1 AS FIPAISID, 1256 AS FITIENDAID, 45 AS FIESTRATEGIAPRCID,3050001358 AS FIARTICULOID, 1 AS FIESTATUS FROM DUAL
		   );


       UPDATE VELIT.TAESTRTXARTXTDA
            SET FIESTATUSAPLICA = 1,
                FIFLUJOAPLICACION = 3,
                FIUSUARIOMODIFICAID = 815311,
                FDFECHAMODIFICACION = SYSDATE
          WHERE     FIPAISID = 1
                AND FIESTRATEGIAPRCID = 44
                AND FITIENDAID = 1256
                AND FIARTICULOID = 3050001358;		   