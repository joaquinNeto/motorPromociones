/*********************************************************************************************
Proyecto:      Sistema Integral de Operaciones Neto
Descripción:   Sistema WMS Neto
Elaborado por: Omar Perez Alavez
Fecha de Creacion: 23/03/2021
***********************************************************************************************/

1.-CreaUserWSMotorProm.sql --Creacion de Usuarios
2.-CargaOpcMotorProm.sql --Carga de Opciones sistema
3.-ActPrimRecepArtsNvos.sql  --Activa articulos nuevos
