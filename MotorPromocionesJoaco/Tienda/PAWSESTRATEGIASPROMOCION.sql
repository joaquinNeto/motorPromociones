/* Formatted on 06/12/2021 10:06:15 p. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE USRVELIT.PAWSESTRATEGIASPROMOCION
AS
   vg_Proceso                     VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE
                                     := 'USRVELIT.PAWSESTRATEGIASPROMOCION'; -- Proceso
   vg_Tabla                       VELITTDA.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                     VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId                  VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE := 0; -- Id de la bitacora
   vg_DscError                    VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE := ''; -- Almacena la descripción del error de oracle.
   vg_CdgError                    VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion                  EXCEPTION;        -- Excepcion personalizada
   csg_IndError          CONSTANT VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                     := 1 ; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_IndAlerta         CONSTANT VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                     := 2 ; -- Indica si es una alerta por lo tanto no se aplica commit dentro del store.
   csg_Desactivado       CONSTANT NUMBER (1) := 0;                   --Valor 0
   csg_PaisId            CONSTANT NUMBER (1) := 1;              -- Id del pais
   csg_Sistema           CONSTANT NUMBER (1) := 1;          -- Id del sisterma
   csg_Modulo            CONSTANT NUMBER (1) := 7;    -- Id del modulo compras
   csg_Submodulo         CONSTANT NUMBER (1) := 1; -- Id del submodulo articulos
   csg_IdConfiCrontTda   CONSTANT NUMBER (2) := 14; -- Id de configuracion cron tienda
   csg_IdConfLlaveEnc    CONSTANT NUMBER (2) := 11; --Id de Configuracion llave de encriptacion
   csg_MUno              CONSTANT NUMBER (2) := -1;      -- Valor constante -1
   csg_Cero              CONSTANT NUMBER (1) := 0;        -- Valor constante 0
   csg_Uno               CONSTANT NUMBER (1) := 1;        -- Valor constante 1
   csg_Dos               CONSTANT NUMBER (1) := 2;        -- Valor constante 2
   csg_Tres              CONSTANT NUMBER (1) := 3;        -- Valor constante 3
   csg_Cuatro            CONSTANT NUMBER (1) := 4;        -- Valor constante 4
   vg_BitacoraErroresId           VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE
      := csg_Cero;                                       -- Indicador bitácora

   PROCEDURE SPREGISTRAESTRATEGIAS (
      rec_Articulos   IN     USRVELIT.TYPARRMOVENVTDA,
      rec_Respuesta      OUT USRVELIT.TYPES.CURSORTYP,
      ptab_CdgError      OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError      OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso para registrar las promociones
   Parámetros de entrada: -rec_Articulos -> Array con las promociones
   Parámetros de salida:  -rec_Respuesta -> Array con respuesta de actualización
                          -ptab_CdgError -> Codigo de respuesta
                          -ptab_DscError -> Descripcion de respuesta

   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 09/11/2021

   ************************************************************************************/
   PROCEDURE SPINSERTACABESTRATEGIA (
      ptab_PaisId         IN     VELITTDA.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE,
      rec_Articulos       IN     USRVELIT.TYPARRMOVENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso para registrar las promociones
   Parámetros de entrada: -ptab_PaisId   -> Id de pais
                          -ptab_TiendaId -> Id de tienda
                          -pa_CadenaGen  -> Cadena de cambios
   Parámetros de salida:  -ptab_CdgError -> Codigo de respuesta
                          -ptab_DscError -> Descripcion de respuesta

   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 09/11/2021

   ************************************************************************************/
   PROCEDURE SPREGISTRAESTRATEGIAXART (
      ptab_PaisId         IN     VELITTDA.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRPRECXART.FIESTRATEGIAPRCID%TYPE,
      rec_ArtEstrategia   IN     USRVELIT.TYPARRARTENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso para registrar las promociones
Parámetros de entrada: -ptab_PaisId       -> Id de pais
                       -ptab_EstrategiaId -> Id de la estrategia
                       -rec_ArtEstrategia -> arreglo de articulos
Parámetros de salida:  -ptab_CdgError     -> Codigo de respuesta
                       -ptab_DscError     -> Descripcion de respuesta

Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 09/11/2021

************************************************************************************/
   PROCEDURE SPREGISTRAESTRATEGIAXPER (
      ptab_PaisId         IN     VELITTDA.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE,
      rec_PerEstrategia   IN     USRVELIT.TYPARRPERENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

         /**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso para registrar las promociones
Parámetros de entrada: -ptab_PaisId       -> Id de pais
                       -ptab_EstrategiaId -> Id de la estrategia
                       -rec_PerEstrategia -> arreglo de articulos
Parámetros de salida:  -ptab_CdgError     -> Codigo de respuesta
                       -ptab_DscError     -> Descripcion de respuesta

Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 09/11/2021

************************************************************************************/
   PROCEDURE SPINSERTAESTRATEGIASCRON (
      parr_ArregloCambios   IN     USRVELIT.TYPARRAYENVIO,
      prec_CurDatosSalida      OUT USRVELIT.TYPES.CURSORTYP,
      ptab_CdgError            OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError            OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para actualizar las estrategias de venta
   Parámetros de entrada: -paArregloCambios  -> Array Con los Cambios a procesar
   Parámetros de salida:  -paCurDatosSalida  -> Curson con la respuesta generada por articulo
                          -paCdgError       -> Código de respuesta generado por ORACLE
                          -paDescError      -> Descripción de respuesta generado por ORACLE
   Creador: Joaquin Jaime Sánchez Salgado
   Fecha: 22/11/2021
   ***********************************************************************************************/
   PROCEDURE SPHABILITACRONTDA (
      pa_PaisId            IN     NUMBER,
      pa_HabilitaCronTda   IN     NUMBER,
      ptab_CdgError           OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
   Parámetros de entrada: -paPaisId          -> Id del Pais
   Parámetros de salida:  -paHabilitaCronTda -> Valor del Parametro
                          -paCdgError        -> Código de error generado por ORACLE
                          -paDescError       -> Descripción del error generado por ORACLE
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha: 24/11/2021
   ***********************************************************************************************/
   PROCEDURE SPCONSULTAPARAMCRON (
      pa_PaisId            IN     NUMBER,
      pa_HabilitaCronTda      OUT NUMBER,
      ptab_CdgError           OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);
/***********************************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
Parámetros de entrada: -paPaisId  -> Id del Pais

Parámetros de salida: -paHabilitaCronTda -> Valor del Parametro
                      -paCdgError        -> Código de error generado por ORACLE
                      -paDescError       -> Descripción del error generado por ORACLE
Creador: Joaquin Jaime Sanchez Salgado
Fecha: 24/11/2021
***********************************************************************************************/
END PAWSESTRATEGIASPROMOCION;
/

CREATE OR REPLACE PACKAGE BODY USRVELIT.PAWSESTRATEGIASPROMOCION
AS
   PROCEDURE SPREGISTRAESTRATEGIAS (
      rec_Articulos   IN     USRVELIT.TYPARRMOVENVTDA,
      rec_Respuesta      OUT USRVELIT.TYPES.CURSORTYP,
      ptab_CdgError      OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError      OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso para registrar las promociones
   Parámetros de entrada: -rec_Articulos -> Array con las promociones
   Parámetros de salida:  -rec_Respuesta -> Array con respuesta de actualización
                          -ptab_CdgError -> Codigo de respuesta
                          -ptab_DscError -> Descripcion de respuesta
   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 09/11/2021

   ************************************************************************************/
   IS
      vl_CdgError       VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE; -- Codigo error
      vl_DscError       VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE; -- Descripcion error
      vlrec_Respuesta   USRVELIT.TYPARRRESPESTRG
                           := USRVELIT.TYPARRRESPESTRG ();  -- Array respuesta

      CURSOR cur_Estrategias
      IS
         SELECT FIPAISID,
                FIESTRATEGIAPRCID,
                FINUMESTRATEGIA,
                FITIPOESTRATGID,
                FICOMBINAOFRTID,
                FCDESCRIPCION,
                FNVENTAOBJETIVO,
                FIMONEDAID,
                FDFECHAINICIOVIGENCIA,
                FDFECHAFINVIGENCIA,
                FIESTATUSESTRTGID,
                FIREGAPLICATDA,
                FICONFIGREGLASID,
                FDFECHAREGISTRO,
                FIUSUARIOREGISTRAID,
                FIUSUARIOMODIFICAID,
                FDFECHAMODIFICACION,
                FIENVIOATDA AS FIENVIOPENDTDA,
                FCOBSERVACIONES,
                FIMAXCOMBOS,
                FIESTRATEGIAPADREID,
                FCURLPROMOCIONAL,
                TYPARRARTENVTDA,
                TYPARRPERENVTDA
           FROM TABLE (rec_Articulos);
   BEGIN
      USRVELIT.PAGENERICOS.SPBITACORAERRORES (
         vg_Proceso,
         'SPREGISTRAESTRATEGIAS',
         csg_Cero,
         csg_Uno,
         'rec_Articulos: ' || rec_Articulos.COUNT,
         '',
         csg_Uno,
         vg_BitacoraErroresId);

      FOR A IN cur_Estrategias
      LOOP
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (
            vg_Proceso,
            'SPREGISTRAESTRATEGIAS',
            csg_Cero,
            csg_Uno,
               'PAISID: '
            || A.FIPAISID
            || ',ESTRATEGIAID:'
            || A.FIESTRATEGIAPRCID,
            '',
            csg_Uno,
            vg_BitacoraErroresId);
         USRVELIT.PAWSESTRATEGIASPROMOCION.SPINSERTACABESTRATEGIA (
            A.FIPAISID,
            A.FIESTRATEGIAPRCID,
            rec_Articulos,
            vlrec_Respuesta,
            vl_CdgError,
            vl_DscError);

         COMMIT;
      END LOOP;

      OPEN rec_Respuesta FOR
         SELECT FIPAISID,
                FITIENDAID,
                FIESTRATEGIAPRCID,
                FIARTICULOID,
                FIESTATUS
           FROM TABLE (vlrec_Respuesta);

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := SQLCODE;
         ptab_DscError := SQLERRM;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (
            vg_Proceso,
            'SPREGISTRAESTRATEGIAS',
            csg_Cero,
            ptab_CdgError,
            ptab_DscError,
            'rec_Articulos: ' || rec_Articulos.COUNT,
            csg_Uno,
            vg_BitacoraErroresId);
   END SPREGISTRAESTRATEGIAS;

   PROCEDURE SPINSERTACABESTRATEGIA (
      ptab_PaisId         IN     VELITTDA.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE,
      rec_Articulos       IN     USRVELIT.TYPARRMOVENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso para registrar las promociones
   Parámetros de entrada: -ptab_PaisId   -> Id de pais
                          -ptab_TiendaId -> Id de tienda
                          -pa_CadenaGen  -> Cadena de cambios
   Parámetros de salida:  -ptab_CdgError -> Codigo de respuesta
                          -ptab_DscError -> Descripcion de respuesta
   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 09/11/2021

   ************************************************************************************/
   IS
      vlrec_RespArt   USRVELIT.TYPARRRESPESTRG := USRVELIT.TYPARRRESPESTRG (); --Array articulos
      vlrec_RespPer   USRVELIT.TYPARRRESPESTRG := USRVELIT.TYPARRRESPESTRG (); --Array periodo

      CURSOR cur_Respuesta
      IS
         SELECT FIPAISID,
                FITIENDAID,
                FIESTRATEGIAPRCID,
                FIARTICULOID,
                FIESTATUS
           FROM TABLE (vlrec_RespArt);

      CURSOR cur_Detalle
      IS
         SELECT TYPARRARTENVTDA, TYPARRPERENVTDA
           FROM TABLE (rec_Articulos)
          WHERE     FIPAISID = ptab_PaisId
                AND FIESTRATEGIAPRCID = ptab_EstrategiaId;
   BEGIN
      rec_Respuesta := USRVELIT.TYPARRRESPESTRG ();

      USRVELIT.PAGENERICOS.SPBITACORAERRORES (
         vg_Proceso,
         'SPINSERTACABESTRATEGIA_01',
         csg_Cero,
         csg_Uno,
         ptab_DscError,
         'PAISID: ' || ptab_PaisId || ',ESTRATEGIAID:' || ptab_EstrategiaId,
         csg_Uno,
         vg_BitacoraErroresId);

      MERGE INTO VELITTDA.TAESTRATGPRECIO A
           USING (SELECT FIPAISID,
                         FIESTRATEGIAPRCID,
                         FINUMESTRATEGIA,
                         FITIPOESTRATGID,
                         FICOMBINAOFRTID,
                         FCDESCRIPCION,
                         FNVENTAOBJETIVO,
                         FIMONEDAID,
                         TO_DATE (FDFECHAINICIOVIGENCIA,
                                  'DD/MM/YYYY HH24:MI:SS')
                            AS FDFECHAINICIOVIGENCIA,
                         TO_DATE (FDFECHAFINVIGENCIA,
                                  'DD/MM/YYYY HH24:MI:SS')
                            AS FDFECHAFINVIGENCIA,
                         FIESTATUSESTRTGID,
                         FIREGAPLICATDA,
                         FICONFIGREGLASID,
                         TO_DATE (FDFECHAREGISTRO, 'DD/MM/YYYY HH24:MI:SS')
                            AS FDFECHAREGISTRO,
                         FIUSUARIOREGISTRAID,
                         FIUSUARIOMODIFICAID,
                         SYSDATE     AS FDFECHAMODIFICACION,
                         FIENVIOATDA AS FIENVIOPENDTDA,
                         FCOBSERVACIONES,
                         FIMAXCOMBOS,
                         FIESTRATEGIAPADREID,
                         FCURLPROMOCIONAL
                    FROM TABLE (rec_Articulos)
                   WHERE     FIPAISID = ptab_PaisId
                         AND FIESTRATEGIAPRCID = ptab_EstrategiaId) B
              ON (    A.FIPAISID = B.FIPAISID
                  AND A.FIESTRATEGIAPRCID = B.FIESTRATEGIAPRCID)
      WHEN NOT MATCHED
      THEN
         INSERT     (FIPAISID,
                     FIESTRATEGIAPRCID,
                     FINUMESTRATEGIA,
                     FITIPOESTRATGID,
                     FICOMBINAOFRTID,
                     FCDESCRIPCION,
                     FNVENTAOBJETIVO,
                     FIMONEDAID,
                     FDFECHAINICIOVIGENCIA,
                     FDFECHAFINVIGENCIA,
                     FIESTATUSESTRTGID,
                     FIREGAPLICATDA,
                     FICONFIGREGLASID,
                     FCOBSERVACIONES,
                     FIMAXCOMBOS,
                     FIESTRATEGIAPADREID,
                     FCURLPROMOCIONAL,
                     FIENVIOPENDTDA,
                     FDFECHAREGISTRO,
                     FIUSUARIOREGISTRAID,
                     FIUSUARIOMODIFICAID,
                     FDFECHAMODIFICACION)
             VALUES (B.FIPAISID,
                     B.FIESTRATEGIAPRCID,
                     B.FINUMESTRATEGIA,
                     B.FITIPOESTRATGID,
                     B.FICOMBINAOFRTID,
                     B.FCDESCRIPCION,
                     B.FNVENTAOBJETIVO,
                     B.FIMONEDAID,
                     B.FDFECHAINICIOVIGENCIA,
                     B.FDFECHAFINVIGENCIA,
                     B.FIESTATUSESTRTGID,
                     B.FIREGAPLICATDA,
                     B.FICONFIGREGLASID,
                     B.FCOBSERVACIONES,
                     B.FIMAXCOMBOS,
                     B.FIESTRATEGIAPADREID,
                     B.FCURLPROMOCIONAL,
                     B.FIENVIOPENDTDA,
                     B.FDFECHAREGISTRO,
                     B.FIUSUARIOREGISTRAID,
                     B.FIUSUARIOMODIFICAID,
                     B.FDFECHAMODIFICACION)
      WHEN MATCHED
      THEN
         UPDATE SET A.FINUMESTRATEGIA = B.FINUMESTRATEGIA,
                    A.FITIPOESTRATGID = B.FITIPOESTRATGID,
                    A.FICOMBINAOFRTID = B.FICOMBINAOFRTID,
                    A.FCDESCRIPCION = B.FCDESCRIPCION,
                    A.FNVENTAOBJETIVO = B.FNVENTAOBJETIVO,
                    A.FIMONEDAID = B.FIMONEDAID,
                    A.FDFECHAINICIOVIGENCIA = B.FDFECHAINICIOVIGENCIA,
                    A.FDFECHAFINVIGENCIA = B.FDFECHAFINVIGENCIA,
                    A.FIESTATUSESTRTGID = B.FIESTATUSESTRTGID,
                    A.FIREGAPLICATDA = B.FIREGAPLICATDA,
                    A.FICONFIGREGLASID = B.FICONFIGREGLASID,
                    A.FCOBSERVACIONES = B.FCOBSERVACIONES,
                    A.FIMAXCOMBOS = B.FIMAXCOMBOS,
                    A.FIESTRATEGIAPADREID = B.FIESTRATEGIAPADREID,
                    A.FCURLPROMOCIONAL = B.FCURLPROMOCIONAL,
                    A.FIENVIOPENDTDA = B.FIENVIOPENDTDA,
                    A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                    A.FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                    A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                    A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION;

      COMMIT;

      FOR A IN cur_Detalle
      LOOP
         EXIT WHEN cur_Detalle%NOTFOUND;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (
            'PAWSESTRATEGIASPROMOCION',
            'SPREGISTRAESTRATEGIAXART',
            csg_Cero,
            csg_Uno,
            ptab_DscError,
               'PAISID: '
            || ptab_PaisId
            || ',ESTRATEGIAID:'
            || ptab_EstrategiaId
            || ',TYPARRARTENVTDA: '
            || A.TYPARRARTENVTDA.COUNT,
            csg_Uno,
            vg_BitacoraErroresId);

         USRVELIT.PAWSESTRATEGIASPROMOCION.SPREGISTRAESTRATEGIAXART (
            ptab_PaisId,
            ptab_EstrategiaId,
            A.TYPARRARTENVTDA,
            vlrec_RespArt,
            ptab_CdgError,
            ptab_DscError);

         USRVELIT.PAGENERICOS.SPBITACORAERRORES (
            vg_Proceso,
            'SPREGISTRAESTRATEGIAXPER',
            csg_Cero,
            csg_Uno,
            ptab_DscError,
               'PAISID: '
            || ptab_PaisId
            || ',ESTRATEGIAID:'
            || ptab_EstrategiaId
            || ',TYPARRPERENVTDA: '
            || A.TYPARRPERENVTDA.COUNT,
            csg_Uno,
            vg_BitacoraErroresId);
         USRVELIT.PAWSESTRATEGIASPROMOCION.SPREGISTRAESTRATEGIAXPER (
            ptab_PaisId,
            ptab_EstrategiaId,
            A.TYPARRPERENVTDA,
            vlrec_RespPer,
            ptab_CdgError,
            ptab_DscError);

         FOR D IN cur_Respuesta
         LOOP
            EXIT WHEN cur_Respuesta%NOTFOUND;

            rec_Respuesta.EXTEND;
            rec_Respuesta (rec_Respuesta.COUNT) :=
               USRVELIT.TYPOBJRESPESTRG (D.FIPAISID,
                                         D.FITIENDAID,
                                         D.FIESTRATEGIAPRCID,
                                         D.FIARTICULOID,
                                         D.FIESTATUS);
         END LOOP;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_Uno;
         ptab_DscError := SQLERRM;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                 'SPINSERTACABESTRATEGIA',
                                                 csg_Cero,
                                                 csg_Uno,
                                                 ptab_DscError,
                                                 '',
                                                 csg_Uno,
                                                 vg_BitacoraErroresId);
   END SPINSERTACABESTRATEGIA;

   PROCEDURE SPREGISTRAESTRATEGIAXART (
      ptab_PaisId         IN     VELITTDA.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRPRECXART.FIESTRATEGIAPRCID%TYPE,
      rec_ArtEstrategia   IN     USRVELIT.TYPARRARTENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
   /**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso para registrar las promociones
Parámetros de entrada: -ptab_PaisId     -> Id de pais
                       -ptab_ArticuloId -> Id de articulo
                       -pa_CadenaGen    -> Cadena de cambios
Parámetros de salida:  -ptab_CdgError   -> Codigo de respuesta
                       -ptab_DscError   -> Descripcion de respuesta

Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 09/11/2021

************************************************************************************/
   IS
      vl_ExisteAt   NUMBER (3);                   -- Indica si exite promocion
      vl_TiendaId   VELITTDA.TACONFIGURACIONTIENDA.FITIENDAID%TYPE; -- Id de tienda

      CURSOR cur_Estrategias
      IS
         SELECT FIPAISID,
                FIARTICULOID,
                FIESTRATEGIAPRCID,
                FNPRECIOBASE,
                FNMONTOAPLICA,
                FNPRECIOMINIMO,
                FIESTATUS,
                TO_DATE (FDFECHAREGISTRO, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAREGISTRO,
                FIUSUARIOREGISTRAID,
                FIUSUARIOMODIFICAID,
                SYSDATE AS FDFECHAMODIFICACION,
                FITIPODESCUENTO,
                FITIPODESCREGALO,
                FCDESCRIPCIONREGLA,
                FITOTALARTREQRD,
                FITOTALARTREGALO
           FROM TABLE (rec_ArtEstrategia);
   BEGIN
      rec_Respuesta := USRVELIT.TYPARRRESPESTRG ();

      SELECT FITIENDAID
        INTO vl_TiendaId
        FROM VELITTDA.TACONFIGURACIONTIENDA
       WHERE FIPAISID = ptab_PaisId;

      FOR B IN cur_Estrategias
      LOOP
         EXIT WHEN cur_Estrategias%NOTFOUND;

         USRVELIT.PAGENERICOS.SPBITACORAERRORES (
            vg_Proceso,
            'SPREGISTRAESTRATEGIAXART_01',
            csg_Cero,
            csg_Uno,
            ptab_DscError,
               'PAISID: '
            || ptab_PaisId
            || ',ESTRATEGIAID: '
            || ptab_EstrategiaId
            || ',ARTICULOID: '
            || B.FIARTICULOID,
            csg_Uno,
            vg_BitacoraErroresId);

         SELECT COUNT (csg_Uno)
           INTO vl_ExisteAt
           FROM VELITTDA.TAESTRPRECXART
          WHERE     FIPAISID = ptab_PaisId
                AND FIARTICULOID = B.FIARTICULOID
                AND FIESTRATEGIAPRCID = ptab_EstrategiaId;

         IF vl_ExisteAt > csg_Cero
         THEN
            UPDATE VELITTDA.TAESTRPRECXART
               SET FNPRECIOBASE = B.FNPRECIOBASE,
                   FNMONTOAPLICA = B.FNMONTOAPLICA,
                   FNPRECIOMINIMO = B.FNPRECIOMINIMO,
                   FIESTATUS = B.FIESTATUS,
                   FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                   FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                   FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                   FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                   FITIPODESCUENTO = B.FITIPODESCUENTO,
                   FITIPODESCREGALO = B.FITIPODESCREGALO,
                   FCDESCRIPCIONREGLA = B.FCDESCRIPCIONREGLA,
                   FITOTALARTREQRD = B.FITOTALARTREQRD,
                   FITOTALARTREGALO = B.FITOTALARTREGALO
             WHERE     FIPAISID = ptab_PaisId
                   AND FIARTICULOID = B.FIARTICULOID
                   AND FIESTRATEGIAPRCID = ptab_EstrategiaId;
         ELSE
            INSERT INTO VELITTDA.TAESTRPRECXART (FIPAISID,
                                                 FIESTRATEGIAPRCID,
                                                 FIARTICULOID,
                                                 FNPRECIOBASE,
                                                 FNMONTOAPLICA,
                                                 FNPRECIOMINIMO,
                                                 FIESTATUS,
                                                 FDFECHAREGISTRO,
                                                 FIUSUARIOREGISTRAID,
                                                 FIUSUARIOMODIFICAID,
                                                 FDFECHAMODIFICACION,
                                                 FITIPODESCUENTO,
                                                 FITIPODESCREGALO,
                                                 FCDESCRIPCIONREGLA,
                                                 FITOTALARTREQRD,
                                                 FITOTALARTREGALO)
                 VALUES (B.FIPAISID,
                         B.FIESTRATEGIAPRCID,
                         B.FIARTICULOID,
                         B.FNPRECIOBASE,
                         B.FNMONTOAPLICA,
                         B.FNPRECIOMINIMO,
                         B.FIESTATUS,
                         B.FDFECHAREGISTRO,
                         B.FIUSUARIOREGISTRAID,
                         B.FIUSUARIOMODIFICAID,
                         B.FDFECHAMODIFICACION,
                         B.FITIPODESCUENTO,
                         B.FITIPODESCREGALO,
                         B.FCDESCRIPCIONREGLA,
                         B.FITOTALARTREQRD,
                         B.FITOTALARTREGALO);
         END IF;

         IF SQL%FOUND
         THEN
            COMMIT;
            rec_Respuesta.EXTEND;
            rec_Respuesta (rec_Respuesta.COUNT) :=
               USRVELIT.TYPOBJRESPESTRG (ptab_PaisId,
                                         vl_TiendaId,
                                         ptab_EstrategiaId,
                                         B.FIARTICULOID,
                                         csg_Dos);
         ELSE
            ROLLBACK;
            rec_Respuesta.EXTEND;
            rec_Respuesta (rec_Respuesta.COUNT) :=
               USRVELIT.TYPOBJRESPESTRG (ptab_PaisId,
                                         vl_TiendaId,
                                         ptab_EstrategiaId,
                                         B.FIARTICULOID,
                                         csg_Tres);
         END IF;

         vl_ExisteAt := csg_Cero;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_Uno;
         ptab_DscError := SQLERRM;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                 'SPREGISTRAESTRATEGIAXART',
                                                 csg_Cero,
                                                 csg_Uno,
                                                 ptab_DscError,
                                                 '',
                                                 csg_Uno,
                                                 vg_BitacoraErroresId);
   END SPREGISTRAESTRATEGIAXART;

   PROCEDURE SPREGISTRAESTRATEGIAXPER (
      ptab_PaisId         IN     VELITTDA.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE,
      rec_PerEstrategia   IN     USRVELIT.TYPARRPERENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
         /**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso para registrar las promociones
Parámetros de entrada: -ptab_PaisId       -> Id de pais
                       -ptab_EstrategiaId -> Id de la estrategia
                       -rec_PerEstrategia -> arreglo de articulos
Parámetros de salida:  -ptab_CdgError     -> Codigo de respuesta
                       -ptab_DscError     -> Descripcion de respuesta

Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 09/11/2021

************************************************************************************/
   IS
      vl_TiendaId    VELITTDA.TACONFIGURACIONTIENDA.FITIENDAID%TYPE; -- Id de tienda
      vl_ExisteEPP   NUMBER (3);
      vl_ExistePST   NUMBER (3);
      vl_CdgError    VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE := 0;

      CURSOR cur_Periodos
      IS
         SELECT FIPAISID,
                FIESTARTEGIAPRCID,
                FIPERIODOID,
                FINUMPERIODO,
                TO_DATE (FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAINICIOVIGENCIA,
                TO_DATE (FDFECHAFINVIGENCIA, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAFINVIGENCIA,
                FIUSUARIOREGISTRAID,
                TO_DATE (FDFECHAREGISTRO, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAREGISTRO,
                FINUMPERIODOS,
                FITIPOPERIODO,
                FCDIASAPLICACION,
                FDINICOPERIODO,
                FDFINPERIODO,
                FIUSUARIOMODIFICAID,
                SYSDATE AS FDFECHAMODIFICACION,
                FIESTATUS
           FROM TABLE (rec_PerEstrategia);
   BEGIN
      rec_Respuesta := USRVELIT.TYPARRRESPESTRG ();

      SELECT FITIENDAID
        INTO vl_TiendaId
        FROM VELITTDA.TACONFIGURACIONTIENDA
       WHERE FIPAISID = ptab_PaisId;

      FOR B IN cur_Periodos
      LOOP
         EXIT WHEN cur_Periodos%NOTFOUND;

         USRVELIT.PAGENERICOS.SPBITACORAERRORES (
            'PAWSESTRATEGIASPROMOCION',
            'SPREGISTRAESTRATEGIAXPER_01',
            csg_Cero,
            csg_Uno,
            ptab_DscError,
               'PAISID: '
            || ptab_PaisId
            || ',ESTRATEGIAID: '
            || B.FIESTARTEGIAPRCID
            || ',FIPERIODOID: '
            || B.FIPERIODOID
            || ',FINUMPERIODO: '
            || B.FINUMPERIODO,
            csg_Uno,
            vg_BitacoraErroresId);

         SELECT COUNT (csg_Uno)
           INTO vl_ExisteEPP
           FROM VELITTDA.TAESTRATGPRXPER
          WHERE     FIPAISID = ptab_PaisId
                AND FIESTARTEGIAPRCID = ptab_EstrategiaId
                AND FIPERIODOID = B.FIPERIODOID
                AND FINUMPERIODO = B.FINUMPERIODO;

         SELECT COUNT (csg_Uno)
           INTO vl_ExistePST
           FROM VELITTDA.TAPERIODOESTRTG
          WHERE FIPERIODOID = B.FIPERIODOID;

         IF vl_ExistePST > csg_Cero
         THEN
            UPDATE VELITTDA.TAPERIODOESTRTG
               SET FINUMPERIODOS = B.FINUMPERIODOS,
                   FITIPOPERIODO = B.FITIPOPERIODO,
                   FCDIASAPLICACION = B.FCDIASAPLICACION,
                   FDINICOPERIODO = B.FDINICOPERIODO,
                   FDFINPERIODO = B.FDFINPERIODO,
                   FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                   FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                   FIESTATUS = B.FIESTATUS
             WHERE FIPERIODOID = B.FIPERIODOID;
         ELSE
            INSERT INTO VELITTDA.TAPERIODOESTRTG (FIPERIODOID,
                                                  FINUMPERIODOS,
                                                  FITIPOPERIODO,
                                                  FCDIASAPLICACION,
                                                  FDINICOPERIODO,
                                                  FDFINPERIODO,
                                                  FDFECHAREGISTRO,
                                                  FIUSUARIOINSERTAID,
                                                  FIUSUARIOMODIFICAID,
                                                  FDFECHAMODIFICACION,
                                                  FIESTATUS)
                 VALUES (B.FIPERIODOID,
                         B.FINUMPERIODOS,
                         B.FITIPOPERIODO,
                         B.FCDIASAPLICACION,
                         B.FDINICOPERIODO,
                         B.FDFINPERIODO,
                         B.FDFECHAREGISTRO,
                         B.FIUSUARIOMODIFICAID,
                         B.FIUSUARIOMODIFICAID,
                         B.FDFECHAMODIFICACION,
                         B.FIESTATUS);
         END IF;

         IF vl_ExisteEPP > csg_Cero
         THEN
            UPDATE VELITTDA.TAESTRATGPRXPER
               SET FDFECHAINICIOVIGENCIA = B.FDFECHAINICIOVIGENCIA,
                   FDFECHAFINVIGENCIA = B.FDFECHAFINVIGENCIA,
                   FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                   FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                   FIESTATUS = B.FIESTATUS
             WHERE     FIPAISID = B.FIPAISID
                   AND FIESTARTEGIAPRCID = B.FIESTARTEGIAPRCID
                   AND FIPERIODOID = B.FIPERIODOID
                   AND FINUMPERIODO = B.FINUMPERIODO;
         ELSE
            INSERT INTO VELITTDA.TAESTRATGPRXPER (FIPAISID,
                                                  FIESTARTEGIAPRCID,
                                                  FIPERIODOID,
                                                  FINUMPERIODO,
                                                  FDFECHAINICIOVIGENCIA,
                                                  FDFECHAFINVIGENCIA,
                                                  FIUSUARIOREGISTRAID,
                                                  FDFECHAREGISTRO,
                                                  FIUSUARIOMODIFICAID,
                                                  FDFECHAMODIFICACION,
                                                  FIESTATUS)
                 VALUES (B.FIPAISID,
                         B.FIESTARTEGIAPRCID,
                         B.FIPERIODOID,
                         B.FINUMPERIODO,
                         B.FDFECHAINICIOVIGENCIA,
                         B.FDFECHAFINVIGENCIA,
                         B.FIUSUARIOREGISTRAID,
                         B.FDFECHAREGISTRO,
                         B.FIUSUARIOMODIFICAID,
                         B.FDFECHAMODIFICACION,
                         B.FIESTATUS);
         END IF;

         IF SQL%FOUND
         THEN
            IF SQL%FOUND
            THEN                                 -- Periodo insertado correcto
               COMMIT;
               rec_Respuesta.EXTEND;
               rec_Respuesta (rec_Respuesta.COUNT) :=
                  USRVELIT.TYPOBJRESPESTRG (ptab_PaisId,
                                            vl_TiendaId,
                                            ptab_EstrategiaId,
                                            B.FIPERIODOID,
                                            csg_Cero);
            ELSE                          -- Error al insertar detalle periodo
               ROLLBACK;
               rec_Respuesta.EXTEND;
               rec_Respuesta (rec_Respuesta.COUNT) :=
                  USRVELIT.TYPOBJRESPESTRG (ptab_PaisId,
                                            vl_TiendaId,
                                            ptab_EstrategiaId,
                                            B.FIPERIODOID,
                                            csg_Uno);
            END IF;
         ELSE                            -- Error al insertar cabecero periodo
            ROLLBACK;
            rec_Respuesta.EXTEND;
            rec_Respuesta (rec_Respuesta.COUNT) :=
               USRVELIT.TYPOBJRESPESTRG (ptab_PaisId,
                                         vl_TiendaId,
                                         ptab_EstrategiaId,
                                         B.FIPERIODOID,
                                         csg_Uno);
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_Uno;
         ptab_DscError := SQLERRM;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                 'SPREGISTRAESTRATEGIAXPER',
                                                 csg_Cero,
                                                 csg_Uno,
                                                 ptab_DscError,
                                                 '',
                                                 csg_Uno,
                                                 vg_BitacoraErroresId);
   END SPREGISTRAESTRATEGIAXPER;

   PROCEDURE SPINSERTAESTRATEGIASCRON (
      parr_ArregloCambios   IN     USRVELIT.TYPARRAYENVIO,
      prec_CurDatosSalida      OUT USRVELIT.TYPES.CURSORTYP,
      ptab_CdgError            OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError            OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para actualizar las estrategias de venta
   Parámetros de entrada: -parr_ArregloCambios  -> Array Con los Cambios a procesar
   Parámetros de salida:  -prec_CurDatosSalida  -> Curson con la respuesta generada por articulo
                          -ptab_CdgError       -> Código de respuesta generado por ORACLE
                          -ptab_DscError      -> Descripción de respuesta generado por ORACLE
   Creador: Joaquin Jaime Sánchez Salgado
   Fecha: 22/11/2021
   ***********************************************************************************************/
   IS
      vl_Cadena              VARCHAR2 (4000 CHAR) := '';
      vl_Cont                NUMBER (3) := 1; --- contador para recorrer cadena
      vl_PosI                NUMBER (4) := 0; ---- Validar posiciones de cadenas
      vl_PosF                NUMBER (4) := 0; ---- Validar posiciones de cadenas
      vl_Largo               NUMBER (4) := 0; ---- Validar posiciones de cadenas

      ---- Array cabecero --
      vl_PaisId              NUMBER (3);
      vl_EstrategiaprcId     NUMBER (18);
      vl_NumEstrategia       NUMBER (18);
      vl_TipoEstratgId       NUMBER (3);
      vl_CombinaOfrtId       NUMBER (2);
      vl_Descripcion         VARCHAR2 (50 CHAR);
      vl_VentaObjetivo       NUMBER (18, 2);
      vl_MonedaId            NUMBER (2);
      vl_FechaIniVigencia    VARCHAR2 (20 CHAR);
      vl_FechaFinVigencia    VARCHAR2 (20 CHAR);
      vl_EstatusEstrtgId     NUMBER (2);
      vl_RegAplicaTda        NUMBER (1);
      vl_ConfigReglasId      NUMBER (3);
      vl_FechaRegistro       VARCHAR2 (20 CHAR);
      vl_UsuarioRegistraId   NUMBER (12);
      vl_UsuarioModificaId   NUMBER (12);
      vl_EnvioaTda           NUMBER (1);
      vl_Observaciones       VARCHAR2 (500 CHAR);
      vl_MaxCombos           NUMBER (4);
      vl_EstrategiapadreId   NUMBER (18);
      vl_UrlPromocional      VARCHAR2 (200 CHAR);

      ---- Array articulos --
      vl_ArticuloId          NUMBER (12);
      vl_Preciobase          NUMBER (8, 2);
      vl_Montoaplica         NUMBER (8, 2);
      vl_Preciominimo        NUMBER (8, 2);
      vl_Estatus             NUMBER (1);
      vl_Fechamodificacion   VARCHAR2 (20 CHAR);
      vl_Tipodescuento       NUMBER (3);
      vl_Tipodescregalo      NUMBER (3);
      vl_Descripcionregla    VARCHAR2 (300 CHAR);
      vl_Totalartreqrd       NUMBER (18, 2);
      vl_Totalartregalo      NUMBER (18, 2);
      vl_DatosSalida         USRVELIT.TYPARRAYENVIO
                                := USRVELIT.TYPARRAYENVIO (); --- registro con los datos que seran enviado a central
      vl_CadenaEnvio         VARCHAR2 (2000) := ''; ---- cadena con los datos de resultado

      CURSOR cur_CadenaCabecero
      IS
           SELECT FITIPO, FCREGISTROS
             FROM (SELECT TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                    '[^}]+',
                                                    csg_Uno,
                                                    csg_Uno))
                             AS FITIPO,
                          REGEXP_SUBSTR (FCREGISTROS,
                                         '[^}]+',
                                         csg_Uno,
                                         csg_Tres)
                             AS FCREGISTROS
                     FROM TABLE (parr_ArregloCambios))
         ORDER BY FITIPO ASC;
   BEGIN
      vg_Proceso := vg_Proceso || '.SPINSERTAESTRATEGIASCRON';

      vg_Tabla := 'cur_CadenaCabecero';

      FOR C IN cur_CadenaCabecero
      LOOP
         EXIT WHEN cur_CadenaCabecero%NOTFOUND;
         vg_Tabla := 'cur_CadenaCabecero_01';

         IF C.FITIPO = csg_Uno
         THEN
            BEGIN
               vl_PosI := csg_Uno;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_PaisId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, csg_Cero, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_EstrategiaprcId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vg_ExecSql := 'vl_NumEstrategia';
               vl_NumEstrategia :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_TipoEstratgId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_CombinaOfrtId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Descripcion :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_VentaObjetivo :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_MonedaId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_FechaIniVigencia :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_FechaFinVigencia :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_EstatusEstrtgId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_RegAplicaTda :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_ConfigReglasId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Observaciones :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_MaxCombos :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_EstrategiapadreId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_UrlPromocional :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_EnvioaTda :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_FechaRegistro :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_UsuarioRegistraId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_UsuarioModificaId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));

               vg_Tabla := ' INTO VELIT.TAESTRATGPRECIO';

               BEGIN
                  MERGE INTO VELITTDA.TAESTRATGPRECIO A
                       USING (SELECT vl_PaisId          AS FIPAISID,
                                     vl_EstrategiaprcId AS FIESTRATEGIAPRCID,
                                     vl_NumEstrategia   AS FINUMESTRATEGIA,
                                     vl_TipoEstratgId   AS FITIPOESTRATGID,
                                     vl_CombinaOfrtId   AS FICOMBINAOFRTID,
                                     vl_Descripcion     AS FCDESCRIPCION,
                                     vl_VentaObjetivo   AS FNVENTAOBJETIVO,
                                     vl_MonedaId        AS FIMONEDAID,
                                     TO_DATE (vl_FechaIniVigencia,
                                              'DD/MM/YYYY HH24:MI:SS')
                                        AS FDFECHAINICIOVIGENCIA,
                                     TO_DATE (vl_FechaFinVigencia,
                                              'DD/MM/YYYY HH24:MI:SS')
                                        AS FDFECHAFINVIGENCIA,
                                     vl_EstatusEstrtgId AS FIESTATUSESTRTGID,
                                     vl_RegAplicaTda    AS FIREGAPLICATDA,
                                     vl_ConfigReglasId  AS FICONFIGREGLASID,
                                     vl_Observaciones   AS FCOBSERVACIONES,
                                     vl_MaxCombos       AS FIMAXCOMBOS,
                                     vl_EstrategiapadreId
                                        AS FIESTRATEGIAPADREID,
                                     vl_UrlPromocional  AS FCURLPROMOCIONAL,
                                     TO_DATE (vl_FechaRegistro,
                                              'DD/MM/YYYY HH24:MI:SS')
                                        AS FDFECHAREGISTRO,
                                     vl_UsuarioRegistraId
                                        AS FIUSUARIOREGISTRAID,
                                     vl_UsuarioModificaId
                                        AS FIUSUARIOMODIFICAID,
                                     SYSDATE
                                        AS FDFECHAMODIFICACION
                                FROM DUAL) B
                          ON (    A.FIPAISID = B.FIPAISID
                              AND A.FIESTRATEGIAPRCID = B.FIESTRATEGIAPRCID)
                  WHEN NOT MATCHED
                  THEN
                     INSERT     (FIPAISID,
                                 FIESTRATEGIAPRCID,
                                 FINUMESTRATEGIA,
                                 FITIPOESTRATGID,
                                 FICOMBINAOFRTID,
                                 FCDESCRIPCION,
                                 FNVENTAOBJETIVO,
                                 FIMONEDAID,
                                 FDFECHAINICIOVIGENCIA,
                                 FDFECHAFINVIGENCIA,
                                 FIESTATUSESTRTGID,
                                 FIREGAPLICATDA,
                                 FICONFIGREGLASID,
                                 FCOBSERVACIONES,
                                 FIMAXCOMBOS,
                                 FIESTRATEGIAPADREID,
                                 FCURLPROMOCIONAL,
                                 FDFECHAREGISTRO,
                                 FIUSUARIOREGISTRAID,
                                 FIUSUARIOMODIFICAID,
                                 FDFECHAMODIFICACION)
                         VALUES (B.FIPAISID,
                                 B.FIESTRATEGIAPRCID,
                                 B.FINUMESTRATEGIA,
                                 B.FITIPOESTRATGID,
                                 B.FICOMBINAOFRTID,
                                 B.FCDESCRIPCION,
                                 B.FNVENTAOBJETIVO,
                                 B.FIMONEDAID,
                                 B.FDFECHAINICIOVIGENCIA,
                                 B.FDFECHAFINVIGENCIA,
                                 B.FIESTATUSESTRTGID,
                                 B.FIREGAPLICATDA,
                                 B.FICONFIGREGLASID,
                                 B.FCOBSERVACIONES,
                                 B.FIMAXCOMBOS,
                                 B.FIESTRATEGIAPADREID,
                                 B.FCURLPROMOCIONAL,
                                 B.FDFECHAREGISTRO,
                                 B.FIUSUARIOREGISTRAID,
                                 B.FIUSUARIOMODIFICAID,
                                 B.FDFECHAMODIFICACION)
                  WHEN MATCHED
                  THEN
                     UPDATE SET
                        A.FINUMESTRATEGIA = B.FINUMESTRATEGIA,
                        A.FITIPOESTRATGID = B.FITIPOESTRATGID,
                        A.FICOMBINAOFRTID = B.FICOMBINAOFRTID,
                        A.FCDESCRIPCION = B.FCDESCRIPCION,
                        A.FNVENTAOBJETIVO = B.FNVENTAOBJETIVO,
                        A.FIMONEDAID = B.FIMONEDAID,
                        A.FDFECHAINICIOVIGENCIA = B.FDFECHAINICIOVIGENCIA,
                        A.FDFECHAFINVIGENCIA = B.FDFECHAFINVIGENCIA,
                        A.FIESTATUSESTRTGID = B.FIESTATUSESTRTGID,
                        A.FIREGAPLICATDA = B.FIREGAPLICATDA,
                        A.FICONFIGREGLASID = B.FICONFIGREGLASID,
                        A.FCOBSERVACIONES = B.FCOBSERVACIONES,
                        A.FIMAXCOMBOS = B.FIMAXCOMBOS,
                        A.FIESTRATEGIAPADREID = B.FIESTRATEGIAPADREID,
                        A.FCURLPROMOCIONAL = B.FCURLPROMOCIONAL,
                        A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                        A.FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                        A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                        A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION;

                  vl_CadenaEnvio :=
                        vl_PaisId
                     || ';'
                     || vl_EstrategiaprcId
                     || ';'
                     || vl_NumEstrategia
                     || ';'
                     || vl_TipoEstratgId
                     || ';1;REGISTRO ACTUALIZADO CON EXITO;';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vg_ExecSql :=
                           vl_PaisId
                        || ';'
                        || vl_EstrategiaprcId
                        || ';'
                        || vl_NumEstrategia
                        || ';'
                        || vl_TipoEstratgId
                        || ';2;'
                        || SQLERRM
                        || ';';
                     USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                             vg_Tabla,
                                                             csg_Cero,
                                                             SQLCODE,
                                                             SQLERRM,
                                                             vg_ExecSql,
                                                             csg_IndError,
                                                             vg_BitacoraId);
               END;

               vl_Cont := csg_Uno;
               vl_PosI := csg_Cero;
               vl_PosF := csg_Cero;
               vl_Largo := csg_Cero;
            EXCEPTION
               WHEN OTHERS
               THEN
                  vl_CadenaEnvio := '<>';
                  USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                          vg_Tabla,
                                                          csg_Cero,
                                                          SQLCODE,
                                                          SQLERRM,
                                                          vg_ExecSql,
                                                          csg_IndError,
                                                          vg_BitacoraId);
            END;

            IF (vl_CadenaEnvio <> '<>')
            THEN
               vl_DatosSalida.EXTEND;
               vl_DatosSalida (vl_DatosSalida.COUNT) :=
                  USRVELIT.TYPCADENAENVIO (vl_CadenaEnvio);
            END IF;
         ELSIF C.FITIPO = csg_Dos
         THEN
            vl_Cont := csg_Uno;
            vl_PosI := csg_Cero;
            vl_PosF := csg_Cero;
            vl_Largo := csg_Cero;

            BEGIN
               vl_PosI := csg_Uno;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_PaisId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, csg_Cero, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_EstrategiaprcId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_ArticuloId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Preciobase :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Montoaplica :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Preciominimo :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Estatus :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Fecharegistro :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_UsuarioregistraId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_UsuariomodificaId :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Fechamodificacion :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Tipodescuento :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Tipodescregalo :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Descripcionregla :=
                  SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo);
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF :=
                  INSTR (C.FCREGISTROS,
                         '<!',
                         csg_Uno,
                         vl_Cont);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Totalartreqrd :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
               vl_Cont := vl_Cont + csg_Uno;
               vl_PosI := vl_PosF + csg_Dos;
               vl_PosF := LENGTH (C.FCREGISTROS);
               vl_Largo := vl_PosF - vl_PosI;
               vl_Totalartregalo :=
                  TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));

               BEGIN
                  MERGE INTO VELITTDA.TAESTRPRECXART A
                       USING (SELECT vl_PaisId          AS FIPAISID,
                                     vl_EstrategiaprcId AS FIESTRATEGIAPRCID,
                                     vl_ArticuloId      AS FIARTICULOID,
                                     vl_Preciobase      AS FNPRECIOBASE,
                                     vl_Montoaplica     AS FNMONTOAPLICA,
                                     vl_Preciominimo    AS FNPRECIOMINIMO,
                                     vl_Estatus         AS FIESTATUS,
                                     TO_DATE (vl_Fecharegistro,
                                              'DD/MM/YYYY HH24:MI:SS')
                                        AS FDFECHAREGISTRO,
                                     vl_UsuarioregistraId
                                        AS FIUSUARIOREGISTRAID,
                                     vl_UsuariomodificaId
                                        AS FIUSUARIOMODIFICAID,
                                     SYSDATE
                                        AS FDFECHAMODIFICACION,
                                     vl_Tipodescuento   AS FITIPODESCUENTO,
                                     vl_Tipodescregalo  AS FITIPODESCREGALO,
                                     vl_Descripcionregla
                                        AS FCDESCRIPCIONREGLA,
                                     vl_Totalartreqrd   AS FITOTALARTREQRD,
                                     vl_Totalartregalo  AS FITOTALARTREGALO
                                FROM DUAL) B
                          ON (    A.FIPAISID = B.FIPAISID
                              AND A.FIESTRATEGIAPRCID = B.FIESTRATEGIAPRCID)
                  WHEN NOT MATCHED
                  THEN
                     INSERT     (FIPAISID,
                                 FIESTRATEGIAPRCID,
                                 FIARTICULOID,
                                 FNPRECIOBASE,
                                 FNMONTOAPLICA,
                                 FNPRECIOMINIMO,
                                 FIESTATUS,
                                 FDFECHAREGISTRO,
                                 FIUSUARIOREGISTRAID,
                                 FIUSUARIOMODIFICAID,
                                 FDFECHAMODIFICACION,
                                 FITIPODESCUENTO,
                                 FITIPODESCREGALO,
                                 FCDESCRIPCIONREGLA,
                                 FITOTALARTREQRD,
                                 FITOTALARTREGALO)
                         VALUES (B.FIPAISID,
                                 B.FIESTRATEGIAPRCID,
                                 B.FIARTICULOID,
                                 B.FNPRECIOBASE,
                                 B.FNMONTOAPLICA,
                                 B.FNPRECIOMINIMO,
                                 B.FIESTATUS,
                                 B.FDFECHAREGISTRO,
                                 B.FIUSUARIOREGISTRAID,
                                 B.FIUSUARIOMODIFICAID,
                                 B.FDFECHAMODIFICACION,
                                 B.FITIPODESCUENTO,
                                 B.FITIPODESCREGALO,
                                 B.FCDESCRIPCIONREGLA,
                                 B.FITOTALARTREQRD,
                                 B.FITOTALARTREGALO)
                  WHEN MATCHED
                  THEN
                     UPDATE SET
                        A.FNPRECIOBASE = B.FNPRECIOBASE,
                        A.FNMONTOAPLICA = B.FNMONTOAPLICA,
                        A.FNPRECIOMINIMO = B.FNPRECIOMINIMO,
                        A.FIESTATUS = B.FIESTATUS,
                        A.FDFECHAREGISTRO = B.FDFECHAREGISTRO,
                        A.FIUSUARIOREGISTRAID = B.FIUSUARIOREGISTRAID,
                        A.FIUSUARIOMODIFICAID = B.FIUSUARIOMODIFICAID,
                        A.FDFECHAMODIFICACION = B.FDFECHAMODIFICACION,
                        A.FITIPODESCUENTO = B.FITIPODESCUENTO,
                        A.FITIPODESCREGALO = B.FITIPODESCREGALO,
                        A.FCDESCRIPCIONREGLA = B.FCDESCRIPCIONREGLA,
                        A.FITOTALARTREQRD = B.FITOTALARTREQRD,
                        A.FITOTALARTREGALO = B.FITOTALARTREGALO;

                  vl_CadenaEnvio :=
                        vl_PaisId
                     || ';'
                     || vl_EstrategiaprcId
                     || ';'
                     || vl_ArticuloId
                     || ';'
                     || vl_Preciobase
                     || ';1;REGISTRO ACTUALIZADO CON EXITO;';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vg_ExecSql :=
                           vl_PaisId
                        || ';'
                        || vl_EstrategiaprcId
                        || ';'
                        || vl_ArticuloId
                        || ';'
                        || vl_Preciobase
                        || ';2;'
                        || SQLERRM
                        || ';';
                     USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                             vg_Tabla,
                                                             csg_Cero,
                                                             SQLCODE,
                                                             SQLERRM,
                                                             vg_ExecSql,
                                                             csg_IndError,
                                                             vg_BitacoraId);
               END;

               vl_Cont := csg_Uno;
               vl_PosI := csg_Cero;
               vl_PosF := csg_Cero;
               vl_Largo := csg_Cero;
            EXCEPTION
               WHEN OTHERS
               THEN
                  vl_CadenaEnvio := '<>';
                  USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                          vg_Tabla,
                                                          csg_Cero,
                                                          SQLCODE,
                                                          SQLERRM,
                                                          vg_ExecSql,
                                                          csg_IndError,
                                                          vg_BitacoraId);
            END;

            IF (vl_CadenaEnvio <> '<>')
            THEN
               vl_DatosSalida.EXTEND;
               vl_DatosSalida (vl_DatosSalida.COUNT) :=
                  USRVELIT.TYPCADENAENVIO (vl_CadenaEnvio);
            END IF;
         END IF;

         COMMIT;
      END LOOP;

      OPEN prec_CurDatosSalida FOR SELECT * FROM TABLE (vl_DatosSalida);

      ptab_CdgError := csg_Uno;
      ptab_DscError := 'Actualización exitosa';
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'Error al insertar las estrategias';
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                 vg_Tabla,
                                                 csg_Cero,
                                                 SQLCODE,
                                                 SQLERRM,
                                                 vg_ExecSql,
                                                 csg_IndError,
                                                 vg_BitacoraId);
   END SPINSERTAESTRATEGIASCRON;

   PROCEDURE SPHABILITACRONTDA (
      pa_PaisId            IN     NUMBER,
      pa_HabilitaCronTda   IN     NUMBER,
      ptab_CdgError           OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
   Parámetros de entrada: -pa_PaisId          -> Id del Pais
   Parámetros de salida:  -pa_HabilitaCronTda -> Valor del Parametro
                          -ptab_CdgError        -> Código de error generado por ORACLE
                          -ptab_DscError       -> Descripción del error generado por ORACLE
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha: 24/11/2021
   ***********************************************************************************************/
   IS
      vl_ValorConfig                  VELITTDA.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE := 0; --Almacena el valor del parametro
      cslIdConfCronSiemAct   CONSTANT NUMBER (2) := 14; --Id de configuracion para nunca deshabilitar el Cron
   BEGIN
      vg_Proceso := vg_Proceso || '.SPHABILITACRONTDA';
      vg_Tabla := 'TACONFIGURACIONPARAMETRO';

      IF (pa_HabilitaCronTda = csg_Desactivado)
      THEN
         --Consulto el parametro que no permite el deshabilitar el Cron
         USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS (csg_PaisId,
                                                    csg_Sistema,
                                                    csg_Modulo,
                                                    csg_SubModulo,
                                                    cslIdConfCronSiemAct,
                                                    vg_CdgError,
                                                    vg_DscError,
                                                    vl_ValorConfig);

         IF (vl_ValorConfig = csg_Desactivado)
         THEN
            UPDATE VELITTDA.TACONFIGURACIONPARAMETRO
               SET FNVALOR = pa_HabilitaCronTda,
                   FDFECHAMODIFICACION = SYSDATE
             WHERE     FIPAISID = csg_PaisId
                   AND FISISTEMAID = csg_Sistema
                   AND FIMODULOID = csg_Modulo
                   AND FISUBMODULOID = csg_SubModulo
                   AND FICONFIGURACIONID = csg_IdConfiCrontTda;
         END IF;
      ELSE
         UPDATE VELITTDA.TACONFIGURACIONPARAMETRO
            SET FNVALOR = pa_HabilitaCronTda, FDFECHAMODIFICACION = SYSDATE
          WHERE     FIPAISID = csg_PaisId
                AND FISISTEMAID = csg_Sistema
                AND FIMODULOID = csg_Modulo
                AND FISUBMODULOID = csg_SubModulo
                AND FICONFIGURACIONID = csg_IdConfiCrontTda;

         COMMIT;
      END IF;

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Se Modifico Parametro Exitosamente';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'Se presento un error al ejecutar el proceso.';
         vg_DscError := SQLERRM;
         vg_CdgError := SQLCODE;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                 vg_Tabla,
                                                 csg_Cero,
                                                 vg_CdgError,
                                                 vg_DscError,
                                                 vg_ExecSql,
                                                 csg_Uno,
                                                 vg_BitacoraId);
   END SPHABILITACRONTDA;

   PROCEDURE SPCONSULTAPARAMCRON (
      pa_PaisId            IN     NUMBER,
      pa_HabilitaCronTda      OUT NUMBER,
      ptab_CdgError           OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
   Parámetros de entrada: -pa_PaisId  -> Id del Pais

   Parámetros de salida: -pa_HabilitaCronTda -> Valor del Parametro
                         -ptab_CdgError        -> Código de error generado por ORACLE
                         -ptab_DscError       -> Descripción del error generado por ORACLE
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha: 24/11/2021
   ***********************************************************************************************/
   IS
      vl_ValorConfig   VELITTDA.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE := 0; --Almacena el valor del parametro
   BEGIN
      vg_Proceso := 'SPCONSULTAPARAMCRON';
      vg_Tabla := 'TACONFIGURACIONPARAMETRO';

      USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS (csg_PaisId,
                                                 csg_Sistema,
                                                 csg_Modulo,
                                                 csg_Submodulo,
                                                 csg_IdConfiCrontTda,
                                                 vg_CdgError,
                                                 vg_DscError,
                                                 vl_ValorConfig);
      pa_HabilitaCronTda := vl_ValorConfig;

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Se Consulto Parametro Exitosamente';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'Se presento un error al ejecutar el proceso.';
         vg_DscError := SQLERRM;
         vg_CdgError := SQLCODE;
         USRVELIT.PAGENERICOS.SPBITACORAERRORES (vg_Proceso,
                                                 vg_Tabla,
                                                 csg_Cero,
                                                 vg_CdgError,
                                                 vg_DscError,
                                                 vg_ExecSql,
                                                 csg_Uno,
                                                 vg_BitacoraId);
   END SPCONSULTAPARAMCRON;
END PAWSESTRATEGIASPROMOCION;
/