/* Formatted on 19/11/2021 11:33:35 (QP5 v5.294) */
CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA03
AS
   csg_SinAutorizacion   CONSTANT NUMBER (1) := 0; -- Estatus de solicitud autorizacion no requerida
   csg_Rechazado         CONSTANT NUMBER (1) := 3; -- Estatus de solicitud rechazado
   csg_Autorizado        CONSTANT NUMBER (1) := 2; -- Estatus de Solicitd autorizado
   vg_Proceso                     VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                     := 'VELIT.PAWSMOTORPROMTDA03'; -- Proceso
   vg_Tabla                       VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                     VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
                                     := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId                  VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
                                     := 0;                -- Id de la bitacora
   vg_DescError                   VELIT.TABITACORAERRORES.FCDESERROR%TYPE
                                     := ''; -- Almacena la descripción del error de oracle.
   vg_CdgError                    VELIT.TABITACORAERRORES.FINUMERROR%TYPE
                                     := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion                  EXCEPTION;        -- Excepcion personalizada
   csg_IndicadorError    CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
      := 1 ; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_Activado          CONSTANT NUMBER (1) := 1;         -- Estatus activado
   csg_Desactivado       CONSTANT NUMBER (1) := 0;      -- Estatus desactivado
   csg_Cero              CONSTANT NUMBER (1) := 0;            -- Valor de cero
   csg_Uno               CONSTANT NUMBER (1) := 1;              -- Valores = 1
   csg_PaisId            CONSTANT NUMBER (1) := 1;              -- Id del pais
   csg_Sistema           CONSTANT NUMBER (1) := 1;          -- Id del sisterma
   csg_Modulo            CONSTANT NUMBER (1) := 3;    -- Id del modulo compras
   csg_Submodulo         CONSTANT NUMBER (1) := 2; -- Id del submodulo articulos
   csg_IdConfiguracion   CONSTANT NUMBER (1) := 5; -- Id de configuracion envio de costo por pieza
   csg_EnvioCostoVta     CONSTANT NUMBER (2) := 17; -- Id de configuracion envio de costo de venta

   PROCEDURE SPGENERACAMBIOSARTICULOSTIENDA (
      ptab_TiendaId   IN VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_PaisId     IN VELIT.TAPAISES.FIPAISID%TYPE);
/**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Procedimiento para insertar los cambios de articulos del dia para su envio a tienda
Parámetros de entrada: - ptab_PaisId -> Id del pais
Parametros de Salida : N/A
Parámetros de entrada-salida: N/A
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación    : 14/11/2021
************************************************************************************/

END PAWSMOTORPROMTDA03;

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA03
AS
   /* Paquete que tiene todas los procedimientos que se generan
      en el área central de compras especificamente para el alta de articulos version 1.0
   */
   PROCEDURE SPGENERACAMBIOSARTICULOSTIENDA (
      ptab_TiendaId   IN VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_PaisId     IN VELIT.TAPAISES.FIPAISID%TYPE)
   IS
      /**********************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO
      Descripción: Procedimiento para insertar los cambios de articulos del dia para su envio a tienda
      Parámetros de entrada: - ptab_PaisId -> Id del pais
      Parametros de Salida : N/A
      Parámetros de entrada-salida: N/A
      Precondiciones:
      Creador: Joaquin Jaime Sanchez Salgado
      Fecha de creación    : 14/11/2021
      ************************************************************************************/
      csl_Pendiente         CONSTANT NUMBER (1) := 0; -- Estatus de aplicacion pendiente
      csl_ModoEjecucion     CONSTANT NUMBER (1) := 0; -- Indica que se ejecuta con el proceso automatico
      csl_EstatusApl        CONSTANT VARCHAR2 (60 CHAR)
         :=    'Pendiente de enviar :'
            || TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS AM') ; -- Descripcion de estatus de aplicacion
      csl_FechaAplicacion   CONSTANT DATE
         := TO_DATE ('01/01/1900', 'DD/MM/YYYY') ; -- Fecha de aplicacion defaulr
      vl_SolicitudId                 VELIT.TASOLICITUDESMOVTOSARTICULOS.FISOLICITUDMOVTOARTID%TYPE
         := 0;                                  -- Almacena el Id de solicitud
      vl_FechaMovto                  VELIT.TASOLICITUDESMOVTOSARTICULOS.FDFECHAMOVTOID%TYPE
         := SYSDATE;                        -- Almacena la fecha de movimiento
      vl_ArticuloId                  VELIT.TAARTICULOS.FIARTICULOID%TYPE := 0; -- Almacena el Id del articulo
      vl_ArrayTiendas                VELIT.TYPARRAYTDAID
                                        := VELIT.TYPARRAYTDAID (); --Arreglo con los Ids de las tiendas a enviar el articulo
      vl_RegistroEnvio               VARCHAR2 (4000) := ''; -- Almacena el registro de envio
      vl_FechaEnvio                  DATE := SYSDATE; -- Fecha de envio de los articulos a tienda
      vl_AplicaEnvio                 NUMBER (1) := 1; -- Indica si  el articulo aplica para enviar su actualizacion a tienda
      vl_HabilitaPiezas              NUMBER (1) := 0; -- Indica si se envia el costo por pieza
      vl_EnvioCostoVta               NUMBER (1) := 0; -- Indica si se envian los costos de venta.

      CURSOR cur_ArticulosxEnviar
      IS -- Cursor con los Ids de articulos a enviar a las tiendas (movimientos del dia)
           SELECT FISOLICITUDID,
                  FIARTICULOID,
                  FDFECHAMOVTOID,
                  FIESTATUSMOVTOID
             FROM (SELECT FISOLICITUDMOVTOARTID AS FISOLICITUDID,
                          FIARTICULOID,
                          FDFECHAMOVTOID,
                          FIESTATUSMOVTOID
                     FROM VELIT.TASOLICITUDESMOVTOSARTICULOS
                    WHERE     FIESTATUSMOVTOID IN
                                 (csg_SinAutorizacion,
                                  csg_Autorizado,
                                  csg_Rechazado)
                          AND FIINDICADORENVIO = csg_Desactivado
                   UNION ALL
                   SELECT FISOLICITUDMOVTOCDGBARRASID AS FISOLICITUDID,
                          FIARTICULOID,
                          FDFECHAMOVTOID,
                          FIESTATUSMOVTOID
                     FROM VELIT.TASOLICITUDESMOVTOSCODBARRAS
                    WHERE     FIESTATUSMOVTOID IN
                                 (csg_SinAutorizacion,
                                  csg_Autorizado,
                                  csg_Rechazado)
                          AND FIINDICADORENVIO = csg_Desactivado
                          AND VELIT.PAWSENVIOARTICULOS.FNVALIDACAMBIOSPARACOSTO (
                                 FISOLICITUDMOVTOCDGBARRASID,
                                 FIARTICULOID,
                                 FDFECHAMOVTOID) = csg_Desactivado)
         GROUP BY FISOLICITUDID,
                  FIARTICULOID,
                  FDFECHAMOVTOID,
                  FIESTATUSMOVTOID;

      CURSOR cur_SolArticuloROWID
      IS -- Cursor con los ROWID de las solicitudes de movimientos de articulos
         SELECT ROWID
           FROM VELIT.TASOLICITUDESMOVTOSARTICULOS
          WHERE     FISOLICITUDMOVTOARTID = vl_SolicitudId
                AND FIARTICULOID = vl_ArticuloId
                AND FDFECHAMOVTOID = vl_FechaMovto;

      CURSOR cur_SolCodBarrasROWID
      IS -- Cursor con los ROWID  de las solicitudes de movimientos de codigo de barras
         SELECT ROWID
           FROM VELIT.TASOLICITUDESMOVTOSCODBARRAS
          WHERE     FISOLICITUDMOVTOCDGBARRASID = vl_SolicitudId
                AND FIARTICULOID = vl_ArticuloId
                AND FDFECHAMOVTOID = vl_FechaMovto;
   BEGIN
      vg_Proceso := 'SPGENERACAMBIOSARTICULOSTIENDA';
      vg_Tabla := 'TAMOVTOSENVIADOSXTDA';

      vl_FechaEnvio := TRUNC (SYSDATE);
      -- Consulto si el envio de costos es por pieza o por la unidad de medida del codigo de barras.
      VELIT.PAGENERICOS.SPCONSULTAPARAMETROS (ptab_PaisId,
                                              csg_Sistema,
                                              csg_Modulo,
                                              csg_SubModulo,
                                              csg_IdConfiguracion,
                                              vg_CdgError,
                                              vg_DescError,
                                              vl_HabilitaPiezas);

      -- Consulto si el envio de costos  de venta
      VELIT.PAGENERICOS.SPCONSULTAPARAMETROS (ptab_PaisId,
                                              csg_Sistema,
                                              csg_Modulo,
                                              csg_SubModulo,
                                              csg_EnvioCostoVta,
                                              vg_CdgError,
                                              vg_DescError,
                                              vl_EnvioCostoVta);

      FOR I IN cur_ArticulosxEnviar
      LOOP
         vl_AplicaEnvio := csg_Activado;

         -- Si el estatus es rechazado se valida la cadena de cambios para ver si existio alguno que no requiere autorizacion
         IF (I.FIESTATUSMOVTOID = csg_Rechazado)
         THEN
            vl_AplicaEnvio := csg_Desactivado;

            -- Validamos si  cumple las condiciones para envio
            vl_AplicaEnvio :=
               VELIT.PAWSENVIOARTICULOS.FNVALIDASOLICITUDRECHAZADA (
                  ptab_PaisId,
                  I.FISOLICITUDID,
                  I.FIARTICULOID,
                  I.FDFECHAMOVTOID);

            IF (vl_AplicaEnvio = csg_Desactivado)
            THEN
               -- Actualizar el indicador  de envio en la solicitud
               vl_SolicitudId := I.FISOLICITUDID;
               vl_ArticuloId := I.FIARTICULOID;
               vl_FechaMovto := I.FDFECHAMOVTOID;

               FOR L IN cur_SolArticuloROWID
               LOOP
                  UPDATE VELIT.TASOLICITUDESMOVTOSARTICULOS
                     SET FIINDICADORENVIO = csg_Activado
                   WHERE ROWID = L.ROWID;

                  COMMIT;
               END LOOP;

               FOR L IN cur_SolCodBarrasROWID
               LOOP
                  UPDATE VELIT.TASOLICITUDESMOVTOSCODBARRAS
                     SET FIINDICADORENVIO = csg_Activado
                   WHERE ROWID = L.ROWID;

                  COMMIT;
               END LOOP;
            END IF;
         END IF;

         IF (vl_AplicaEnvio = csg_Activado)
         THEN
            -- Obtener las tiendas a las que les corresponde el articulo
            vl_ArrayTiendas :=
               VELIT.PAWSENVIOARTICULOS.FNOBTIENETIENDASXARTICULO (
                  ptab_PaisId,
                  I.FIARTICULOID);

            IF (vl_ArrayTiendas.COUNT > csg_Cero)
            THEN
               FOR J IN vl_ArrayTiendas.FIRST .. vl_ArrayTiendas.LAST
               LOOP
                  vg_ExecSql :=
                        'TiendaId |'
                     || vl_ArrayTiendas (J).FITIENDAID
                     || '|ArticuloId| '
                     || I.FIARTICULOID
                     || ' |FechaEnvio| '
                     || vl_FechaEnvio;

                  -- Generar la cadena de cambio del articulo para la tienda
                  vl_RegistroEnvio :=
                     VELIT.PAWSMOTORPROMTDA04.FNGENERAREGISTROENVIOARTICULO (
                        ptab_PaisId,
                        vl_ArrayTiendas (J).FITIENDAID,
                        I.FIARTICULOID,
                        vl_FechaEnvio,
                        vl_HabilitaPiezas,
                        csl_ModoEjecucion,
                        vl_EnvioCostoVta);

                  -- Insertar/Actualizar en la tabla TAMOVTOSENVIADOSXTDA a las tiendas que les corresponda
                  IF (vl_RegistroEnvio IS NOT NULL)
                  THEN
                     BEGIN
                        DELETE FROM VELIT.TAMOVTOSENVIADOSXTDA
                              WHERE     FIPAISID = ptab_PaisId
                                    AND FITIENDAID =
                                           vl_ArrayTiendas (J).FITIENDAID
                                    AND FIARTICULOID = I.FIARTICULOID
                                    AND FDFECHAENVIOID < vl_FechaEnvio
                                    AND FIESTATUSAPLICACION = csl_Pendiente
                                    AND FITIPOEJECUCION = csl_ModoEjecucion;

                        IF (SQL%ROWCOUNT > csg_Cero)
                        THEN
                           COMMIT;
                        END IF;

                        MERGE INTO VELIT.TAMOVTOSENVIADOSXTDA MOV
                             USING (SELECT ptab_PaisId    AS FIPAISID,
                                           vl_ArrayTiendas (J).FITIENDAID
                                              AS FITIENDAID,
                                           I.FIARTICULOID AS FIARTICULOID,
                                           vl_FechaEnvio  AS FDFECHAENVIOID
                                      FROM DUAL) B
                                ON (    MOV.FIPAISID = B.FIPAISID
                                    AND MOV.FITIENDAID = B.FITIENDAID
                                    AND MOV.FIARTICULOID = B.FIARTICULOID
                                    AND MOV.FDFECHAENVIOID = B.FDFECHAENVIOID)
                        WHEN NOT MATCHED
                        THEN
                           INSERT     (FIPAISID,
                                       FITIENDAID,
                                       FIARTICULOID,
                                       FDFECHAENVIOID,
                                       FIESTATUSAPLICACION,
                                       FCRESULTADOAPLICACION,
                                       FINUMEROINTENTOS,
                                       FIESPROMOCION,
                                       FCREGISTROENVIO,
                                       FDFECHAAPLICACION,
                                       FISISTEMAID,
                                       FIMODULOID,
                                       FISUBMODULOID)
                               VALUES (ptab_PaisId,
                                       vl_ArrayTiendas (J).FITIENDAID,
                                       I.FIARTICULOID,
                                       vl_FechaEnvio,
                                       csl_Pendiente,
                                       csl_EstatusApl,
                                       csg_Cero,
                                       csg_Cero,
                                       vl_RegistroEnvio,
                                       csl_FechaAplicacion,
                                       csg_Sistema,
                                       csg_Modulo,
                                       csg_SubModulo)
                        WHEN MATCHED
                        THEN
                           UPDATE SET
                              FINUMEROINTENTOS = csg_Cero,
                              FIESTATUSAPLICACION = csl_Pendiente,
                              FCREGISTROENVIO = vl_RegistroEnvio,
                              FCRESULTADOAPLICACION = csl_EstatusApl
                                   WHERE     MOV.FIPAISID = ptab_PaisId
                                         AND MOV.FITIENDAID =
                                                vl_ArrayTiendas (J).FITIENDAID
                                         AND MOV.FIARTICULOID =
                                                I.FIARTICULOID
                                         AND MOV.FDFECHAENVIOID =
                                                vl_FechaEnvio;

                        COMMIT;
                     EXCEPTION
                        WHEN DUP_VAL_ON_INDEX
                        THEN
                           vg_CdgError := SQLCODE;
                           vg_DescError := SQLERRM;
                           VELIT.PAGENERICOS.SPBITACORAERRORES (
                              ptab_PaisId,
                              NULL,
                              vg_Proceso,
                              vg_Tabla,
                              csg_Desactivado,
                              vg_CdgError,
                              vg_DescError,
                              vg_ExecSql,
                              csg_IndicadorError,
                              vg_BitacoraId);
                        WHEN OTHERS
                        THEN
                           vg_CdgError := SQLCODE;
                           vg_DescError := SQLERRM;
                           RAISE exc_Excepcion;
                     END;
                  END IF;
               END LOOP;

               -- Actualizar el indicador  de envio en la solicitud
               vl_SolicitudId := I.FISOLICITUDID;
               vl_ArticuloId := I.FIARTICULOID;
               vl_FechaMovto := I.FDFECHAMOVTOID;

               FOR L IN cur_SolArticuloROWID
               LOOP
                  UPDATE VELIT.TASOLICITUDESMOVTOSARTICULOS
                     SET FIINDICADORENVIO = csg_Activado
                   WHERE ROWID = L.ROWID;

                  COMMIT;
               END LOOP;

               FOR L IN cur_SolCodBarrasROWID
               LOOP
                  UPDATE VELIT.TASOLICITUDESMOVTOSCODBARRAS
                     SET FIINDICADORENVIO = csg_Activado
                   WHERE ROWID = L.ROWID;

                  COMMIT;
               END LOOP;
            END IF;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN exc_Excepcion
      THEN
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              NULL,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Desactivado,
                                              vg_CdgError,
                                              vg_DescError,
                                              vg_ExecSql,
                                              csg_IndicadorError,
                                              vg_BitacoraId);
      WHEN OTHERS
      THEN
         ROLLBACK;
         vg_CdgError := SQLCODE;
         vg_DescError := SQLERRM;
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              NULL,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Desactivado,
                                              vg_CdgError,
                                              vg_DescError,
                                              vg_ExecSql,
                                              csg_IndicadorError,
                                              vg_BitacoraId);
         vg_DescError :=
               'Error al dar de generar los cambios de articulos'
            || '|'
            || vg_DescError
            || '|'
            || vg_Proceso
            || '|'
            || vg_BitacoraId;
   END SPGENERACAMBIOSARTICULOSTIENDA;
END PAWSMOTORPROMTDA03;