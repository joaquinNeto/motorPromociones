BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
      ,start_date      => TO_TIMESTAMP_TZ('2020/01/01 00:01:00.000000 America/Mexico_City','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=MINUTELY;interval=5;'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'BEGIN
                             USRVELIT.PAWSESTRATEGIASPROMOCION.SPSCHPROMOCIONES;
                           END;'
      ,comments        => 'Actualiza cambio de precio promociones'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'USRVELIT.SCH_VEL_ACTPRECIOMOTORPROMO');
END;
/
