CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA04
AS
    CSG_PAISID   CONSTANT NUMBER (1) := 1;
    CSG_MUNO     CONSTANT NUMBER (2) := -1;
    CSG_CERO     CONSTANT NUMBER (1) := 0;
    CSG_UNO      CONSTANT NUMBER (1) := 1;
    CSG_DOS      CONSTANT NUMBER (1) := 2;
    CSG_TRES     CONSTANT NUMBER (1) := 3;
    CSG_CUATRO   CONSTANT NUMBER (1) := 4;
    CSG_CINCO    CONSTANT NUMBER (1) := 5;
    CSG_SEIS     CONSTANT NUMBER (1) := 6;
    CSG_SIETE    CONSTANT NUMBER (1) := 7;

    FUNCTION FNCALCULADIASAPLICACION (
        PTAB_PAISID         IN VELIT.TAPAISES.FIPAISID%TYPE,
        PTAB_ESTRATEGIAID   IN VELIT.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE)
        RETURN VARCHAR2/**********************************************************************************
                       Proyecto: Sistema Integral de Operaciones NETO
                       Descripci�n: Proceso que obtiene los dias de aplicacion
                       Par�metros de entrada: - ptab_PaisId       -> Identificador del pais
                                              - ptab_EstrategiaId -> Identificador de la estrategia
                       Par�metros de salida:  - VARCHAR
                       Valor de retorno:
                       Par�metros de entrada-salida:
                       Precondiciones:
                       Creador: Joaquin Jaime Sanchez Salgado
                       Fecha de creaci�n: 30/11/2021
                       ************************************************************************************/
                       ;

    PROCEDURE SPCONSULTAPROMOCIONES (
        PTAB_CONFIGURACIONID   IN     VELIT.TACONFXPUESTO.FICONFIGURACIONID%TYPE,
        PTAB_MONITOREOID       IN     VELIT.TACONFXPUESTO.FITIPOMONITOREO%TYPE,
        PREC_CURCORREOS           OUT VELIT.TYPES.CURSORTYP,
        PREC_CURCABECERO          OUT VELIT.TYPES.CURSORTYP,
        PREC_CURDETALLE           OUT VELIT.TYPES.CURSORTYP,
        PTAB_CDGERROR             OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
        PTAB_DSCERROR             OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);
/**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripci�n: Proceso que obtiene las diferencias en la conciliacion de pagos de servicios
Par�metros de entrada: - paconfiguracionId      -> Identificador del parametro de configuraci�n
                       - paMonitoreoId          -> Identificador del tipo de monitoreo
Par�metros de salida:  - paCurCorreos           -> Cursor con los correos a los que se les enviara los archivos
                       - paCurDetalle           -> Cursor con los detalles que se mostraran en la tabla
                       - paCurCambiosRechazadas -> Cursor con los cambios de puesto rechazados
                       - paCdgError             -> Identificador del Error.
                       - paDescError            -> Descripcion del error en caso de que exista.
Valor de retorno:
Par�metros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creaci�n: 30/11/2021
************************************************************************************/
END PAWSMOTORPROMTDA04;
/

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA04
AS
    FUNCTION FNCALCULADIASAPLICACION (
        PTAB_PAISID         IN VELIT.TAPAISES.FIPAISID%TYPE,
        PTAB_ESTRATEGIAID   IN VELIT.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE)
        RETURN VARCHAR2
    /**********************************************************************************
    Proyecto: Sistema Integral de Operaciones NETO
    Descripci�n: Proceso que obtiene los dias de aplicacion
    Par�metros de entrada: - ptab_PaisId       -> Identificador del pais
                           - ptab_EstrategiaId -> Identificador de la estrategia
    Par�metros de salida:  - VARCHAR
    Valor de retorno:
    Par�metros de entrada-salida:
    Precondiciones:
    Creador: Joaquin Jaime Sanchez Salgado
    Fecha de creaci�n: 30/11/2021
    ************************************************************************************/
    IS
        VL_PERIODOID         VELIT.TAPERIODOESTRTG.FIPERIODOID%TYPE;
        VL_TIPOPERIODOID     VELIT.TAPERIODOESTRTG.FITIPOPERIODO%TYPE;
        VL_DIASAPLICACION    VELIT.TAPERIODOESTRTG.FCDIASAPLICACION%TYPE;
        VL_FECHAINI          VELIT.TAPERIODOESTRTG.FDINICOPERIODO%TYPE;
        VL_FECHAFIN          VELIT.TAPERIODOESTRTG.FDFINPERIODO%TYPE;
        VL_PERIODO           VARCHAR2 (200 CHAR) := '';
        CSL_MONCE   CONSTANT VARCHAR2 (3 CHAR) := '-11';
    BEGIN
          SELECT EPE.FIPERIODOID,
                 FITIPOPERIODO,
                 FCDIASAPLICACION,
                 FDINICOPERIODO,
                 FDFINPERIODO
            INTO VL_PERIODOID,
                 VL_TIPOPERIODOID,
                 VL_DIASAPLICACION,
                 VL_FECHAINI,
                 VL_FECHAFIN
            FROM VELIT.TAESTRATGPRXPER EPE
                 INNER JOIN VELIT.TAPERIODOESTRTG PES
                     ON PES.FIPERIODOID = EPE.FIPERIODOID
           WHERE     FIPAISID = PTAB_PAISID
                 AND FIESTARTEGIAPRCID = PTAB_ESTRATEGIAID
                 AND EPE.FIESTATUS = CSG_UNO
                 AND PES.FIESTATUS = CSG_UNO
        GROUP BY EPE.FIPERIODOID,
                 FITIPOPERIODO,
                 FCDIASAPLICACION,
                 FDINICOPERIODO,
                 FDFINPERIODO;

        IF VL_TIPOPERIODOID = CSG_UNO OR VL_TIPOPERIODOID = CSG_DOS
        THEN                                                          --DIARIO
            SELECT SUBSTR (NVL (DIAS, CSL_MONCE),
                           CSG_UNO,
                           LENGTH (NVL (DIAS, CSL_MONCE)) - CSG_UNO)
              INTO VL_PERIODO
              FROM (SELECT    CASE WHEN LUNES = CSG_UNO THEN 'Lu-' END
                           || CASE WHEN MARTES = CSG_DOS THEN 'Ma-' END
                           || CASE WHEN MIERCOLES = CSG_TRES THEN 'Mi-' END
                           || CASE WHEN JUEVES = CSG_CUATRO THEN 'Ju-' END
                           || CASE WHEN VIERNES = CSG_CINCO THEN 'Vi-' END
                           || CASE WHEN SABADO = CSG_SEIS THEN 'Sa-' END
                           || CASE WHEN DOMINGO = CSG_SIETE THEN 'Do-' END    AS DIAS
                      FROM (SELECT FCREGISTROS, ORDEN
                              FROM (    SELECT REGEXP_SUBSTR (VL_DIASAPLICACION,
                                                              '[^,]+',
                                                              CSG_UNO,
                                                              LEVEL)
                                                   AS FCREGISTROS,
                                               ROWNUM
                                                   AS ORDEN
                                          FROM DUAL
                                    CONNECT BY REGEXP_SUBSTR (
                                                   VL_DIASAPLICACION,
                                                   '[^,]+',
                                                   CSG_UNO,
                                                   LEVEL)
                                                   IS NOT NULL))
                           PIVOT (MAX (FCREGISTROS)
                                 FOR ORDEN
                                 IN ('1' AS LUNES,
                                    '2' AS MARTES,
                                    '3' AS MIERCOLES,
                                    '4' AS JUEVES,
                                    '5' AS VIERNES,
                                    '6' AS SABADO,
                                    '7' AS DOMINGO)));
        ELSIF VL_TIPOPERIODOID = CSG_TRES
        THEN                                                             --MES
            SELECT SUBSTR (NVL (DIAS, CSL_MONCE),
                           CSG_UNO,
                           LENGTH (NVL (DIAS, CSL_MONCE)) - CSG_UNO)
              INTO VL_PERIODO
              FROM (SELECT    CASE WHEN LUNES = CSG_UNO THEN 'Lu-' END
                           || CASE WHEN MARTES = CSG_DOS THEN 'Ma-' END
                           || CASE WHEN MIERCOLES = CSG_TRES THEN 'Mi-' END
                           || CASE WHEN JUEVES = CSG_CUATRO THEN 'Ju-' END
                           || CASE WHEN VIERNES = CSG_CINCO THEN 'Vi-' END
                           || CASE WHEN SABADO = CSG_SEIS THEN 'Sa-' END
                           || CASE WHEN DOMINGO = CSG_SIETE THEN 'Do-' END    AS DIAS
                      FROM (SELECT CASE
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'LUNES'
                                       THEN
                                           CSG_UNO
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'MARTES'
                                       THEN
                                           CSG_DOS
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'MIERCOLES'
                                       THEN
                                           CSG_TRES
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'JUEVES'
                                       THEN
                                           CSG_CUATRO
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'VIERNES'
                                       THEN
                                           CSG_CINCO
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'SABADO'
                                       THEN
                                           CSG_SEIS
                                       WHEN TRANSLATE (
                                                UPPER (
                                                    TRIM (
                                                        TO_CHAR (
                                                            TRUNC (
                                                                  FECHAINICIAL
                                                                + (  ORDEN
                                                                   - CSG_UNO)),
                                                            'DAY',
                                                            'NLS_DATE_LANGUAGE = SPANISH'))),
                                                '����������',
                                                'AEIOUAEIOU') =
                                            'DOMINGO'
                                       THEN
                                           CSG_SIETE
                                   END    AS DIAS,
                                   FCREGISTROS
                              FROM (    SELECT TO_DATE (VL_FECHAINI,
                                                        'DD/MM/YYYY')
                                                   AS FECHAINICIAL,
                                               REGEXP_SUBSTR (VL_DIASAPLICACION,
                                                              '[^,]+',
                                                              CSG_UNO,
                                                              LEVEL)
                                                   AS FCREGISTROS,
                                               ROWNUM
                                                   AS ORDEN
                                          FROM DUAL
                                    CONNECT BY REGEXP_SUBSTR (
                                                   VL_DIASAPLICACION,
                                                   '[^,]+',
                                                   CSG_UNO,
                                                   LEVEL)
                                                   IS NOT NULL)
                             WHERE FCREGISTROS = CSG_UNO)
                           PIVOT (MAX (FCREGISTROS)
                                 FOR DIAS
                                 IN ('1' AS LUNES,
                                    '2' AS MARTES,
                                    '3' AS MIERCOLES,
                                    '4' AS JUEVES,
                                    '5' AS VIERNES,
                                    '6' AS SABADO,
                                    '7' AS DOMINGO)));
        END IF;

        RETURN VL_PERIODO;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN VL_PERIODO;
    END FNCALCULADIASAPLICACION;

    PROCEDURE SPCONSULTAPROMOCIONES (
        PTAB_CONFIGURACIONID   IN     VELIT.TACONFXPUESTO.FICONFIGURACIONID%TYPE,
        PTAB_MONITOREOID       IN     VELIT.TACONFXPUESTO.FITIPOMONITOREO%TYPE,
        PREC_CURCORREOS           OUT VELIT.TYPES.CURSORTYP,
        PREC_CURCABECERO          OUT VELIT.TYPES.CURSORTYP,
        PREC_CURDETALLE           OUT VELIT.TYPES.CURSORTYP,
        PTAB_CDGERROR             OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
        PTAB_DSCERROR             OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
    /**********************************************************************************
    Proyecto: Sistema Integral de Operaciones NETO
    Descripci�n: Proceso que obtiene las diferencias en la conciliacion de pagos de servicios
    Par�metros de entrada: - paconfiguracionId      -> Identificador del parametro de configuraci�n
                           - paMonitoreoId          -> Identificador del tipo de monitoreo
    Par�metros de salida:  - paCurCorreos           -> Cursor con los correos a los que se les enviara los archivos
                           - paCurDetalle           -> Cursor con los detalles que se mostraran en la tabla
                           - paCurCambiosRechazadas -> Cursor con los cambios de puesto rechazados
                           - paCdgError             -> Identificador del Error.
                           - paDescError            -> Descripcion del error en caso de que exista.
    Valor de retorno:
    Par�metros de entrada-salida:
    Precondiciones:
    Creador: Joaquin Jaime Sanchez Salgado
    Fecha de creaci�n: 30/11/2021
    ************************************************************************************/
    IS
        CSL_PROCESO             CONSTANT VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                             := 'SPCONSULTAPROMOCIONES' ; -- Nombre Proceso
        CSL_MODULO              CONSTANT VARCHAR2 (50)
                                             := 'Error en el m�dulo de correos' ; -- Mensage M�dulo
        CSL_NUMTRANSAC          CONSTANT VELIT.TABITACORAERRORES.FINUMTRANSACCION%TYPE
            := 0 ;                                    -- N�mero de transacci�n
        CSL_TDACD               CONSTANT NUMBER (1) := 1; -- Consntante para designar que no es una sucursal en la bit�cora de errores.
        CSL_CONFIGMODULOID      CONSTANT NUMBER (2) := 18; -- Constante indica configuracion id de modulo
        CSL_CONFIGSUBMODULOID   CONSTANT NUMBER (1) := 1; -- Constante indica condiguracion id de submodulo
        CSL_PERFILID            CONSTANT NUMBER (3) := 432; -- Perfil de correo promociones
        VL_CONFIGHORASBAJA               NUMBER (2) := 0; -- Constante para obtener los dobles turnos
        VL_CDGERROR                      VELIT.TABITACORAERRORES.FINUMERROR%TYPE
            := 0;                                    -- C�digo de Error Oracle
        VL_DESCERROR                     VELIT.TABITACORAERRORES.FCDESERROR%TYPE
            := ' ';                             -- Descripci�n de error Oracle
        VL_TABLA                         VELIT.TABITACORAERRORES.FCTABLA%TYPE
                                             := ' '; -- Nombre de tabla en uso
        VL_EXECSQL                       VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
            := ' ';                      -- Descripci�n del query en ejecuci�n
        VL_PARAMETROS                    VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
            := ' ';                           -- Parametros de entrada del SP.
        VL_INDICAERROR                   VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
            := 1;                                     -- Indica que hubo error
        VL_BITACORAID                    VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
            := 0;                                -- ID de registro en bit�cora
        VL_ARRAYTIENDAS                  VELIT.TYPARRAYTIENDASXREGION
            := VELIT.TYPARRAYTIENDASXREGION (); -- Arreglo con las tiendas a enviar
        VLARR_TIENDASENV                 VELIT.TYPARRAYUSRXTIENDAXASIS
            := VELIT.TYPARRAYUSRXTIENDAXASIS (); -- Arreglo con las tiendas a enviar
        VLARR_REGRESAR                   VELIT.TYPARRAYTIENDASXREGION
            := VELIT.TYPARRAYTIENDASXREGION (); -- Arreglo con las tiendas y correos a recuperar
        VL_REGIONACTUAL                  VELIT.TATIENDAS.FIZONAID%TYPE := 0; --Zona actual en el recorrido
        VL_CORREOSELECTRONICO            VELIT.TAUSUARIOS.FCCORREOELECTRONICO%TYPE
            := 0;                            -- correo electronico del usuario
        VL_HORASBUSQUEDA                 VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE
            := 0;                  -- Numero de dias para realizar la busqueda
        VLESTRATEGIAID                   VELIT.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE
            := 0;
    BEGIN
        -- cursor con las zonas por
        OPEN PREC_CURCABECERO FOR
            WITH
                ARTICULOS
                AS
                    (  SELECT FIPAISID,
                              FIESTRATEGIAPRCID,
                              COUNT (FIARTICULOID)     AS ARTICULOS
                         FROM VELIT.TAESTRPRECXART
                     GROUP BY FIPAISID, FIESTRATEGIAPRCID),
                TIENDAS
                AS
                    (  SELECT FIPAISID,
                              FIESTRATEGIAPRCID,
                              COUNT (FITIENDAID)     AS TIENDAS,
                              SUM (FIESTATUS)        AS ALCANCES
                         FROM VELIT.TADETAPLICCNTDA
                        WHERE FIPAISID = CSG_UNO
                     GROUP BY FIPAISID, FIESTRATEGIAPRCID),
                PENDIENTES
                AS
                    (  SELECT FIPAISID,
                              FIESTRATEGIAPRCID,
                              SUM (
                                  CASE
                                      WHEN PENDIENTE > CSG_CERO THEN CSG_CERO
                                      ELSE CSG_UNO
                                  END)    AS APLICADOS
                         FROM (  SELECT FIPAISID,
                                        FIESTRATEGIAPRCID,
                                        FITIENDAID,
                                        SUM (
                                            CASE
                                                WHEN FIESTATUSAPLICA = CSG_DOS
                                                THEN
                                                    CSG_CERO
                                                ELSE
                                                    CSG_UNO
                                            END)    AS PENDIENTE
                                   FROM VELIT.TAESTRTXARTXTDA
                               GROUP BY FIPAISID, FIESTRATEGIAPRCID, FITIENDAID)
                     GROUP BY FIPAISID, FIESTRATEGIAPRCID)
              SELECT EST.FIPAISID,
                     EST.FIESTRATEGIAPRCID,
                     EST.FCDESCRIPCION
                         AS "Concepto",
                     TIP.FCDESCRIPCION
                         AS "Tipo Promocion",
                     NVL (ART.ARTICULOS, CSG_CERO)
                         AS "No. Articulos",
                     NVL (TDA.TIENDAS, CSG_CERO)
                         AS "No. Tiendas Agregadas",
                     NVL (TDA.ALCANCES, CSG_CERO)
                         AS "No. Tiendas con Alcance",
                     NVL (PND.APLICADOS, CSG_CERO)
                         AS "No. Tiendas Aplicadas",
                     CSG_CERO
                         AS "Margen maquina"
                FROM VELIT.TAESTRATGPRECIO EST
                     INNER JOIN VELIT.TATIPOSESTRTG TIP
                         ON TIP.FITIPOESTRATGID = EST.FITIPOESTRATGID
                     LEFT JOIN ARTICULOS ART
                         ON     ART.FIPAISID = EST.FIPAISID
                            AND ART.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                     LEFT JOIN TIENDAS TDA
                         ON     TDA.FIPAISID = EST.FIPAISID
                            AND TDA.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                     LEFT JOIN PENDIENTES PND
                         ON     PND.FIPAISID = EST.FIPAISID
                            AND PND.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
               WHERE     EST.FIPAISID = CSG_UNO
                     AND EST.FIESTATUSESTRTGID NOT IN (CSG_CERO, CSG_UNO)
            ORDER BY EST.FIESTRATEGIAPRCID ASC;

        -- Cursor para obtener el detalle por Zona
        OPEN PREC_CURDETALLE FOR
              SELECT EST.FIPAISID,
                     EST.FIESTRATEGIAPRCID,
                     EST.FCDESCRIPCION
                         AS "Concepto",
                     TIP.FCDESCRIPCION
                         AS "Tipo Promocion",
                     EAT.FIARTICULOID
                         AS "Id Articulo",
                        ART.FCNOMBREARTICULO
                     || ' '
                     || FCMARCA
                     || ' '
                     || FCESPECIFICACION
                     || ' '
                     || FCPRESENTACION
                         AS "Articulo",
                     UPPER (CAT.FCDESCRIPCION)
                         AS "Categoria Articulo",
                     UPPER (SUB.FCDESCRIPCION)
                         AS "Subcategoria Articulo",
                     UPPER (PRV.FCNOMBREPROVEEDOR)
                         AS "Nombre de Proveedor",
                     UPPER (PRV.FCNOMBRECORTO)
                         AS "Nombre Corto de Proveedor",
                     ZON.FCDESCRIPCION
                         AS "Region",
                     EAT.FITIENDAID
                         AS "Id Tienda",
                     TDA.FCNOMBRE
                         AS "Nombre de Tienda",
                     CASE EAT.FIESTATUSAPLICA
                         WHEN CSG_UNO THEN 'REGISTRADO'
                         WHEN CSG_DOS THEN 'APLICADO'
                         WHEN CSG_TRES THEN 'ENVIADO CON ERROR'
                         WHEN CSG_CUATRO THEN 'CANCELADO'
                     END
                         AS "Aplicado en Tienda",
                     CSG_CERO
                         AS "Margen Maquina",
                     NVL (EPA.FNMONTOAPLICA, CSG_CERO)
                         AS "Precio",
                     CSG_CERO
                         AS "Costo"
                FROM VELIT.TAESTRATGPRECIO EST
                     INNER JOIN VELIT.TATIPOSESTRTG TIP
                         ON TIP.FITIPOESTRATGID = EST.FITIPOESTRATGID
                     INNER JOIN VELIT.TAESTRTXARTXTDA EAT
                         ON     EAT.FIPAISID = EST.FIPAISID
                            AND EAT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                     INNER JOIN VELIT.TAESTRPRECXART EPA
                         ON     EAT.FIPAISID = EPA.FIPAISID
                            AND EAT.FIESTRATEGIAPRCID = EPA.FIESTRATEGIAPRCID
                            AND EAT.FIARTICULOID = EPA.FIARTICULOID
                     INNER JOIN VELIT.TAARTICULOSXTIENDA AXT
                         ON     AXT.FIPAISID = EAT.FIPAISID
                            AND AXT.FIARTICULOID = EAT.FIARTICULOID
                            AND AXT.FITIENDAID = EAT.FITIENDAID
                     INNER JOIN VELIT.TAPROVEEDORES PRV
                         ON PRV.FIPROVEEDORID = AXT.FIPROVEEDORID
                     INNER JOIN VELIT.TAARTICULOS ART
                         ON ART.FIARTICULOID = EAT.FIARTICULOID
                     INNER JOIN VELIT.TASUBCATEGORIAS SUB
                         ON     SUB.FIDIVISIONID = ART.FIDIVISIONID
                            AND SUB.FIAGRUPACIONID = ART.FIAGRUPACIONID
                            AND SUB.FICATEGORIAID = ART.FICATEGORIAID
                            AND SUB.FISUBCATEGORIAID = ART.FISUBCATEGORIAID
                     INNER JOIN VELIT.TACATEGORIAS CAT
                         ON     CAT.FIDIVISIONID = SUB.FIDIVISIONID
                            AND CAT.FIAGRUPACIONID = SUB.FIAGRUPACIONID
                            AND CAT.FICATEGORIAID = SUB.FICATEGORIAID
                     INNER JOIN VELIT.TATIENDAS TDA
                         ON     TDA.FIPAISID = EAT.FIPAISID
                            AND TDA.FITIENDAID = EAT.FITIENDAID
                     INNER JOIN VELIT.TAREGIONES REG
                         ON     REG.FIPAISID = TDA.FIPAISID
                            AND REG.FIZONAID = TDA.FIZONAID
                            AND REG.FIREGIONID = TDA.FIREGIONID
                     INNER JOIN VELIT.TAZONAS ZON
                         ON     ZON.FIPAISID = REG.FIPAISID
                            AND ZON.FIZONAID = REG.FIZONAID
               WHERE     EST.FIPAISID = CSG_UNO
                     AND EST.FIESTATUSESTRTGID NOT IN (CSG_CERO, CSG_UNO)
            ORDER BY EST.FIESTRATEGIAPRCID ASC, EAT.FIARTICULOID ASC;

        OPEN PREC_CURCORREOS FOR
            SELECT FIPAISID, FCCORREOELECTRONICO
              FROM VELIT.TAUSUARIOS  TU
                   INNER JOIN VELIT.TAPERFILESXUSUARIO PXU
                       ON TU.FIUSUARIOID = PXU.FIUSUARIOID
             WHERE     PXU.FIPERFILID = CSL_PERFILID
                   AND PXU.FIESTATUS = CSG_UNO
                   AND TU.FIESTATUS = CSG_UNO
                   AND FCCORREOELECTRONICO IS NOT NULL;

        PTAB_CDGERROR := CSG_CERO;
        PTAB_DSCERROR := 'Consulta exitosa';
    EXCEPTION
        WHEN OTHERS
        THEN
            PTAB_CDGERROR := CSG_MUNO;
            PTAB_DSCERROR := 'Error de base de datos.';
            VELIT.PAGENERICOS.SPBITACORAERRORES (CSG_PAISID,
                                                 CSL_TDACD,
                                                 CSL_PROCESO,
                                                 VL_TABLA,
                                                 CSL_NUMTRANSAC,
                                                 SQLCODE,
                                                 SQLERRM,
                                                 VL_EXECSQL,
                                                 VL_INDICAERROR,
                                                 VL_BITACORAID);
            COMMIT;
            PTAB_DSCERROR :=
                   TRIM (PTAB_DSCERROR)
                || ' '
                || TRIM (CSL_MODULO)
                || '|'
                || TRIM (CSL_PROCESO)
                || '|'
                || TO_CHAR (VL_BITACORAID);
    END SPCONSULTAPROMOCIONES;
END PAWSMOTORPROMTDA04;
/
