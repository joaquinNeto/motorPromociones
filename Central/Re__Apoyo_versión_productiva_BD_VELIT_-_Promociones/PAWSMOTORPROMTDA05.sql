CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA05
AS
   csg_MUno     CONSTANT NUMBER (2) := -1;
   csg_Cero     CONSTANT NUMBER (1) := 0;
   csg_Uno      CONSTANT NUMBER (1) := 1;
   csg_Dos      CONSTANT NUMBER (1) := 2;
   csg_Tres     CONSTANT NUMBER (1) := 3;
   csg_Cuatro   CONSTANT NUMBER (1) := 4;

   PROCEDURE SPACTUALIZAENVTDA (
      prec_Regreso     IN     VELIT.TYPARRAYRESPUESTAENVIO,
      ptab_Ejecucion   IN     VELIT.TAMOVTOSENVIADOSXTDA.FITIPOEJECUCION%TYPE,
      ptab_CdgError       OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError       OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);
/***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: actualizacion de los registros en la tabla de envio que fueron procesados por tienda
   Parámetros de entrada: - prec_Regreso   -> arreglo de cadenas con los resultados de ejecucion en tienda
                          - ptab_Ejecucion -> 0=Cron , 1= SIAN
   Parámetros de salida:  - ptab_CdgError  -> 1= exito, -1= error
                          - ptab_DscError  -> mensaje de salida en base al estatus.
   Parámetros de entrada-salida: No aplica
   Valor de retorno:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 30/11/2021
   ***********************************************************************************************/
END PAWSMOTORPROMTDA05;
/


GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRSRVETGPRMCNT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWS;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWSGENERICOODIN;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWSIND;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRWSESTRTGCENT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRWSESTRTGTDA;

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA05
AS
   PROCEDURE SPACTUALIZAENVTDA (
      prec_Regreso     IN     VELIT.TYPARRAYRESPUESTAENVIO,
      ptab_Ejecucion   IN     VELIT.TAMOVTOSENVIADOSXTDA.FITIPOEJECUCION%TYPE,
      ptab_CdgError       OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError       OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO
      Descripción: actualizacion de los registros en la tabla de envio que fueron procesados por tienda
      Parámetros de entrada: - prec_Regreso   -> arreglo de cadenas con los resultados de ejecucion en tienda
                             - ptab_Ejecucion -> 0=Cron , 1= SIAN
      Parámetros de salida:  - ptab_CdgError  -> 1= exito, -1= error
                             - ptab_DscError  -> mensaje de salida en base al estatus.
      Parámetros de entrada-salida: No aplica
      Valor de retorno:
      Precondiciones:
      Creador: Joaquin Jaime Sanchez Salgado
      Fecha de creación: 30/11/2021
      ***********************************************************************************************/
   IS
      vl_Cont              NUMBER (3) := 1;  --- contador para recorrer cadena
      vl_PosI              NUMBER (4) := 0; ---- Validar posiciones de cadenas
      vl_PosF              NUMBER (4) := 0; ---- Validar posiciones de cadenas
      vl_Largo             NUMBER (4) := 0; ---- Validar posiciones de cadenas
      vl_PaisId            NUMBER (3) := 0;
      vl_EstrategiaprcId   NUMBER (18) := 0;
      vl_ArticuloId        NUMBER (12) := 0;
      vl_EnvioTda          NUMBER (12) := 0;
	  

      CURSOR cur_Registros
      IS
         SELECT FITIPO,
                FITIENDAID,
                FCREGISTROS,
                FIBITACORAID
           FROM (SELECT TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                  '[^}]+',
                                                  csg_Uno,
                                                  csg_Uno))
                           AS FITIPO,
                        TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                  '[^}]+',
                                                  csg_Uno,
                                                  csg_Dos))
                           AS FITIENDAID,
                        TO_NUMBER (REGEXP_SUBSTR (FCREGISTROS,
                                                  '[^}]+',
                                                  csg_Uno,
                                                  csg_Tres))
                           AS FIBITACORAID,
                        REGEXP_SUBSTR (FCREGISTROS,
                                       '[^}]+',
                                       csg_Uno,
                                       csg_cuatro)
                           AS FCREGISTROS
                   FROM TABLE (prec_Regreso))
          WHERE FITIPO = csg_Dos;
   BEGIN
      FOR C IN cur_Registros
      LOOP
         EXIT WHEN cur_Registros%NOTFOUND;

         vl_PosI := csg_Uno;
         vl_PosF :=
            INSTR (C.FCREGISTROS,
                   '<!',
                   csg_Uno,
                   vl_Cont);
         vl_Largo := vl_PosF - vl_PosI;
         vl_PaisId := TO_NUMBER (SUBSTR (C.FCREGISTROS, csg_Cero, vl_Largo));
         vl_Cont := vl_Cont + csg_Uno;
         vl_PosI := vl_PosF + csg_Dos;
         vl_PosF :=
            INSTR (C.FCREGISTROS,
                   '<!',
                   csg_Uno,
                   vl_Cont);
         vl_Largo := vl_PosF - vl_PosI;
         vl_EstrategiaprcId :=
            TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));
         vl_Cont := vl_Cont + csg_Uno;
         vl_PosI := vl_PosF + csg_Dos;
         vl_PosF :=
            INSTR (C.FCREGISTROS,
                   '<!',
                   csg_Uno,
                   vl_Cont);
         vl_Largo := vl_PosF - vl_PosI;
         vl_ArticuloId :=
            TO_NUMBER (SUBSTR (C.FCREGISTROS, vl_PosI, vl_Largo));

         vl_Cont := csg_Uno;
         vl_PosI := csg_Cero;
         vl_PosF := csg_Cero;
         vl_Largo := csg_Cero;

         UPDATE VELIT.TAESTRTXARTXTDA
            SET FIESTATUSAPLICA = csg_Dos
          WHERE     FIPAISID = vl_PaisId
                AND FIESTRATEGIAPRCID = vl_EstrategiaprcId
                AND FITIENDAID = C.FITIENDAID
                AND FIARTICULOID = vl_ArticuloId;

         UPDATE VELIT.TABITESTARTXTDA
            SET FIESTATUSAPLICA = csg_Dos
          WHERE     FIPAISID = vl_PaisId
                AND FIESTRATEGIAPRCID = vl_EstrategiaprcId
                AND FITIENDAID = C.FITIENDAID
                AND FIARTICULOID = vl_ArticuloId
                AND FIFECHAID = C.FIBITACORAID;


           SELECT SUM (
                     CASE
                        WHEN FIESTATUSAPLICA = csg_Dos THEN csg_Cero
                        ELSE csg_Uno
                     END)
                     AS PENDIENTES
             INTO vl_EnvioTda
             FROM VELIT.TAESTRTXARTXTDA
            WHERE     FIPAISID = vl_PaisId
			  AND FITIENDAID = C.FITIENDAID
         GROUP BY FIESTRATEGIAPRCID;


         IF vl_EnvioTda = csg_Cero
         THEN
            UPDATE VELIT.TAESTRATGPRECIO
               SET FIENVIOPENDTDA = csg_Uno
             WHERE     FIPAISID = vl_PaisId
                   AND FIESTRATEGIAPRCID = vl_EstrategiaprcId;
         END IF;
      END LOOP;

      COMMIT;
      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Actualización exitosa';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         ptab_CdgError := csg_Uno;
         ptab_DscError := 'Error al actualizar';
   END SPACTUALIZAENVTDA;
END PAWSMOTORPROMTDA05;
/


GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRSRVETGPRMCNT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWS;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWSGENERICOODIN;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRVELITWSIND;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRWSESTRTGCENT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA05 TO USRWSESTRTGTDA;
