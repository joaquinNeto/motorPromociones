/* Formatted on 23/12/2021 01:02:49 p. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA03
AS
   vg_Proceso                 VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                 := 'VELIT.PAWSMOTORPROMTDA03';     -- Proceso
   vg_Tabla                   VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                 VELIT.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId              VELIT.TABITACORAERRORES.FIBITACORAID%TYPE := 0; -- Id de la bitacora
   vg_DscError                VELIT.TABITACORAERRORES.FCDESERROR%TYPE := ''; -- Almacena la descripci�n del error de oracle.
   vg_CdgError                VELIT.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion              EXCEPTION;            -- Excepcion personalizada
   csg_IndError      CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_IndAlerta     CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                 := 2 ; -- Indica si es una alerta por lo tanto no se aplica commit dentro del store.

   -- Constantes  de estatus
   csl_UsrSistemas   CONSTANT NUMBER (6) := 100001;        -- USuario sistemas
   csg_Activado      CONSTANT NUMBER (1) := 1;             -- Estatus activado
   csg_Desactivado   CONSTANT NUMBER (1) := 0;          -- Estatus desactivado
   csg_Cero          CONSTANT NUMBER (1) := 0;                -- Valor de cero
   csg_Uno           CONSTANT NUMBER (1) := 1;                  -- Valores = 1
   csg_Dos           CONSTANT NUMBER (1) := 2;                  -- Valores = 2
   csg_Tres          CONSTANT NUMBER (1) := 3;                  -- Valores = 3
   csg_Cuatro        CONSTANT NUMBER (1) := 4;                  -- Valores = 4

   PROCEDURE SPENVIOXTIPOEJECUCION (
      parr_ArrayTdas       IN     VELIT.TYPARRAYTDAID,
      ptab_tipoEjecucion   IN     VELIT.TABITESTARTXTDA.FIFLUJOAPLICACION%TYPE,
      prec_CursorEnvio        OUT VELIT.TYPES.CURSORTYP,
      prec_CursorUsuario      OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError           OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripci�n: Invoca los procedimientos para envio de cambios a tiendas dependiendo del tipo de ejecucion
   Par�metros de entrada: - parr_ArrayTdas     -> arreglo con las tiendas solicitadas: [paisId, tiendaId]
                          - ptab_tipoEjecucion -> 0=CRON, 1= SIAN
   Par�metros de salida:  - prec_CursorEnvio   -> cursor con las cadenas a insertar en tienda,contiene:
                          - prec_CursorUsuario -> Cursor con los usuarios (aplica solo para envio SIAN)
                          - pa_EstatusSalida   -> 1= exito, -1= error
                          - pa_MensajeSalida   -> mensaje de salida en base al estatus.
   Par�metros de entrada-salida: No aplica
   Valor de retorno:
   Precondiciones:
   Creador: Joaquin Jaime S�nchez Salgado
   Fecha de creaci�n: 19/11/2021
   ***********************************************************************************************/
   FUNCTION FNTIENDASPORENVIAR (
      ptab_PaisId       IN     VELIT.TATIENDAS.FIPAISID%TYPE,
      ptab_Ejecucion    IN     VELIT.TAMOVTOSENVIADOSXTDA.FITIPOEJECUCION%TYPE,
      ptab_TdasXPedir      OUT VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE)
      RETURN VELIT.TYPES.CURSORTYP;
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripci�n: Regresa las tiendas donde deben ser enviados los cambios
   Par�metros de entrada: -ptab_PaisId     -> numero de pais
                          -ptab_Ejecucion  -> 0=Cron , 1= SIAN
   Par�metros de salida:  -ptab_TdasXPedir -> numero de tiendas en cada peticion
   Par�metros de entrada-salida: No aplica
   Valor de retorno: CURSORTYP
   Precondiciones:
Creador: Joaquin Jaime S�nchez Salgado
Fecha de creaci�n: 19/11/2021
***********************************************************************************************/
END PAWSMOTORPROMTDA03;
/


GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRSRVETGPRMCNT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRVELITWS;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRVELITWSGENERICOODIN;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRVELITWSIND;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRWSESTRTGCENT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRWSESTRTGTDA;

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA03
AS
   PROCEDURE SPENVIOXTIPOEJECUCION (
      parr_ArrayTdas       IN     VELIT.TYPARRAYTDAID,
      ptab_tipoEjecucion   IN     VELIT.TABITESTARTXTDA.FIFLUJOAPLICACION%TYPE,
      prec_CursorEnvio        OUT VELIT.TYPES.CURSORTYP,
      prec_CursorUsuario      OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError           OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   IS
      /***********************************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO
      Descripci�n: Invoca los procedimientos para envio de cambios a tiendas dependiendo del tipo de ejecucion
      Par�metros de entrada: - parr_ArrayTdas  --- arreglo con las tiendas solicitadas: [paisId, tiendaId]
                             - ptab_tipoEjecucion --- 0=CRON, 1= SIAN
      Par�metros de salida:  - prec_CursorEnvio   ---- cursor con las cadenas a insertar en tienda,contiene:
                             - prec_CursorUsuario --- Cursor con los usuarios (aplica solo para envio SIAN)
                             - ptab_CdgError --- 1= exito, -1= error
                             - ptab_DscError --- mensaje de salida en base al estatus.
      Par�metros de entrada-salida: No aplica
      Valor de retorno:
      Precondiciones:
      Creador: Joaquin Jaime S�nchez Salgado
      Fecha de creaci�n: 19/11/2021
      ***********************************************************************************************/
      csl_MUno   CONSTANT NUMBER (1) := -1;                   --- Valores = -1
      vl_TotalCambios     NUMBER (9) := 0;                -- Total estrategias
      cur_Estrategias     VELIT.TYPES.CURSORTYP;
      vl_FechaId          VELIT.TABITESTARTXTDA.FIFECHAID%TYPE;
      vl_EstrategiaId     VELIT.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE;
      vlrec_CadenaEnv     VELIT.TYPARRCADENVIOS := VELIT.TYPARRCADENVIOS ();
   BEGIN
      vl_FechaId := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS'));
      vg_Tabla := 'parr_ArrayTdas';

      FOR ART IN parr_ArrayTdas.FIRST .. parr_ArrayTdas.LAST    -- ESTRATEGIAS
      LOOP
         OPEN cur_Estrategias FOR
              SELECT EAT.FIESTRATEGIAPRCID
                FROM VELIT.TAESTRATGPRECIO EST
                     INNER JOIN VELIT.TAESTRTXARTXTDA EAT
                        ON     EAT.FIPAISID = EST.FIPAISID
                           AND EAT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
               WHERE     (   FIINDMODIFESTRTG = csg_Uno
                          OR FIINDMODIFESTATUS = csg_Uno
                          OR FIENVIOPENDTDA = csg_Cero)
                     AND EAT.FITIENDAID = parr_ArrayTdas (ART).FITIENDAID
                     AND EST.FIESTATUSESTRTGID NOT IN (csg_Cero, csg_Uno)
            GROUP BY EAT.FIESTRATEGIAPRCID;

         vg_Tabla := 'cur_Estrategias';

         LOOP
            FETCH cur_Estrategias INTO vl_EstrategiaId;

            EXIT WHEN cur_Estrategias%NOTFOUND;

            -- Genera registro bit�cora
            INSERT INTO VELIT.TABITESTARTXTDA
               SELECT FIPAISID,
                      FIESTRATEGIAPRCID,
                      FITIENDAID,
                      FIARTICULOID,
                      csg_Uno            AS FIESTATUSAPLICA,
                      vl_FechaId         AS FIFECHAID,
                      ptab_tipoEjecucion AS FIFLUJOAPLICACION,
                      csl_UsrSistemas    AS FIUSUARIOMODIFICAID,
                      SYSDATE            AS FDFECHAMODIFICACION
                 FROM VELIT.TAESTRTXARTXTDA
                WHERE     FIPAISID = parr_ArrayTdas (ART).FIPAISID
                      AND FIESTRATEGIAPRCID = vl_EstrategiaId
                      AND FIESTATUS = csg_Uno
                      AND FIALCANCETDA = csg_Uno
                      AND FIESTATUSAPLICA = csg_Uno
                      AND FITIENDAID = parr_ArrayTdas (ART).FITIENDAID;

            COMMIT;
            vg_Tabla := 'UPDATE VELIT.TABITESTARTXTDA';

            -- Cancela pendientes bit�cora
            UPDATE VELIT.TABITESTARTXTDA
               SET FIESTATUSAPLICA = csg_Cuatro
             WHERE     FIPAISID = parr_ArrayTdas (ART).FIPAISID
                   AND FIESTRATEGIAPRCID = vl_EstrategiaId
                   AND FIESTATUSAPLICA = csg_Uno
                   AND FITIENDAID = parr_ArrayTdas (ART).FITIENDAID
                   AND FIFLUJOAPLICACION = ptab_tipoEjecucion
                   AND FIFECHAID != vl_FechaId;

            COMMIT;
            vg_Tabla := 'vlrec_CadenaEnv';

            SELECT VELIT.TYPOBJCADENVIOS (parr_ArrayTdas (ART).FIPAISID,
                                          parr_ArrayTdas (ART).FITIENDAID,
                                          FCCADENAENVIO)
              BULK COLLECT INTO vlrec_CadenaEnv
              FROM (SELECT    csg_Uno
                           || '}'
                           || parr_ArrayTdas (ART).FITIENDAID
                           || '}'
                           || EST.FIPAISID
                           || '<!'
                           || EST.FIESTRATEGIAPRCID
                           || '<!'
                           || EST.FINUMESTRATEGIA
                           || '<!'
                           || EST.FITIPOESTRATGID
                           || '<!'
                           || EST.FICOMBINAOFRTID
                           || '<!'
                           || EST.FCDESCRIPCION
                           || '<!'
                           || EST.FNVENTAOBJETIVO
                           || '<!'
                           || EST.FIMONEDAID
                           || '<!'
                           || TO_CHAR (EST.FDFECHAINICIOVIGENCIA,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || TO_CHAR (EST.FDFECHAFINVIGENCIA,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || EST.FIESTATUSESTRTGID
                           || '<!'
                           || EST.FIREGAPLICATDA
                           || '<!'
                           || EST.FICONFIGREGLASID
                           || '<!'
                           || NVL (EST.FCOBSERVACIONES, '-')
                           || '<!'
                           || NVL (EST.FIMAXCOMBOS, csg_Cero)
                           || '<!'
                           || NVL (EST.FIESTRATEGIAPADREID, csg_Cero)
                           || '<!'
                           || NVL (EST.FCURLPROMOCIONAL, '-')
                           || '<!'
                           || EST.FIENVIOPENDTDA
                           || '<!'
                           || TO_CHAR (EST.FDFECHAREGISTRO,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || EST.FIUSUARIOREGISTRAID
                           || '<!'
                           || EST.FIUSUARIOMODIFICAID
                           || '<!'
                           || TO_CHAR (EST.FDFECHAMODIFICACION,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '}'
                              AS FCCADENAENVIO
                      FROM VELIT.TAESTRATGPRECIO EST
                     WHERE     FIPAISID = parr_ArrayTdas (ART).FIPAISID
                           AND FIESTRATEGIAPRCID = vl_EstrategiaId
                    UNION ALL
                    SELECT    csg_Dos
                           || '}'
                           || parr_ArrayTdas (ART).FITIENDAID
                           || '}'
                           || EST.FIPAISID
                           || '<!'
                           || EST.FIESTRATEGIAPRCID
                           || '<!'
                           || EST.FIARTICULOID
                           || '<!'
                           || EST.FNPRECIOBASE
                           || '<!'
                           || EST.FNMONTOAPLICA
                           || '<!'
                           || EST.FNPRECIOMINIMO
                           || '<!'
                           || EST.FIESTATUS
                           || '<!'
                           || TO_CHAR (EST.FDFECHAREGISTRO,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || EST.FIUSUARIOREGISTRAID
                           || '<!'
                           || EST.FIUSUARIOMODIFICAID
                           || '<!'
                           || TO_CHAR (EST.FDFECHAMODIFICACION,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || EST.FITIPODESCUENTO
                           || '<!'
                           || EST.FITIPODESCREGALO
                           || '<!'
                           || NVL (EST.FCDESCRIPCIONREGLA, '-')
                           || '<!'
                           || EST.FITOTALARTREQRD
                           || '<!'
                           || EST.FITOTALARTREGALO
                           || '}'
                              AS FCCADENAENVIO
                      FROM VELIT.TAESTRPRECXART EST
                           INNER JOIN VELIT.TABITESTARTXTDA BIT
                              ON     BIT.FIPAISID = EST.FIPAISID
                                 AND BIT.FIESTRATEGIAPRCID =
                                        EST.FIESTRATEGIAPRCID
                                 AND BIT.FIARTICULOID = EST.FIARTICULOID
                     WHERE     EST.FIPAISID = parr_ArrayTdas (ART).FIPAISID
                           AND EST.FIESTRATEGIAPRCID = vl_EstrategiaId
                           AND BIT.FITIENDAID =
                                  parr_ArrayTdas (ART).FITIENDAID
                           AND BIT.FIESTATUSAPLICA = csg_Uno
                    UNION ALL
                    SELECT    csg_Tres
                           || '}'
                           || parr_ArrayTdas (ART).FITIENDAID
                           || '}'
                           || EPP.FIPAISID
                           || '<!'
                           || EPP.FIESTARTEGIAPRCID
                           || '<!'
                           || EPP.FIPERIODOID
                           || '<!'
                           || EPP.FINUMPERIODO
                           || '<!'
                           || TO_CHAR (EPP.FDFECHAINICIOVIGENCIA,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || TO_CHAR (EPP.FDFECHAFINVIGENCIA,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || EPP.FIUSUARIOREGISTRAID
                           || '<!'
                           || TO_CHAR (EPP.FDFECHAREGISTRO,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || PXE.FINUMPERIODOS
                           || '<!'
                           || PXE.FITIPOPERIODO
                           || '<!'
                           || PXE.FCDIASAPLICACION
                           || '<!'
                           || PXE.FDINICOPERIODO
                           || '<!'
                           || PXE.FDFINPERIODO
                           || '<!'
                           || PXE.FIUSUARIOMODIFICAID
                           || '<!'
                           || TO_CHAR (PXE.FDFECHAMODIFICACION,
                                       'DD/MM/YYYY HH24:MI:SS')
                           || '<!'
                           || PXE.FIESTATUS
                           || '}'
                              AS FCCADENAENVIO
                      FROM VELIT.TAESTRATGPRXPER EPP
                           INNER JOIN VELIT.TAPERIODOESTRTG PXE
                              ON PXE.FIPERIODOID = EPP.FIPERIODOID
                     WHERE     FIPAISID = parr_ArrayTdas (ART).FIPAISID
                           AND FIESTARTEGIAPRCID = vl_EstrategiaId
                           AND EPP.FIESTATUS = csg_Uno
                           AND PXE.FIESTATUS = csg_Uno);
         END LOOP;
      END LOOP;

      OPEN prec_CursorEnvio FOR
         SELECT FIPAISID, FITIENDAID, FCCADENAENVIO
           FROM TABLE (vlrec_CadenaEnv);

      OPEN prec_CursorUsuario FOR
         SELECT    TU.FIUSUARIOID
                || ' - '
                || TU.FCNOMBRE
                || ' '
                || TU.FCAPELLIDOPATERNO
                || ' '
                || TU.FCAPELLIDOMATERNO
                   AS USUARIO
           FROM VELIT.TAUSUARIOS TU
          WHERE FIUSUARIOID = csl_UsrSistemas;

      ptab_CdgError := csg_Uno;
      ptab_DscError := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csl_MUno;
         ptab_DscError := SQLERRM;
         VELIT.PAGENERICOS.SPBITACORAERRORES (
            csg_Uno,
            csg_Cero,
            vg_Proceso,
            vg_Tabla,
            csg_Cero,
            SQLCODE,
            SQLERRM,
            'Tipo Ejecucion:' || ptab_tipoEjecucion,
            csg_IndError,
            vg_BitacoraId);
   END SPENVIOXTIPOEJECUCION;

   FUNCTION FNTIENDASPORENVIAR (
      ptab_PaisId       IN     VELIT.TATIENDAS.FIPAISID%TYPE,
      ptab_Ejecucion    IN     VELIT.TAMOVTOSENVIADOSXTDA.FITIPOEJECUCION%TYPE,
      ptab_TdasXPedir      OUT VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE)
      RETURN VELIT.TYPES.CURSORTYP
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripci�n: Regresa las tiendas donde deben ser enviados los cambios
   Par�metros de entrada: -ptab_PaisId     -> numero de pais
                          -ptab_Ejecucion  -> 0=Cron , 1= SIAN
   Par�metros de salida:  -ptab_TdasXPedir -> numero de tiendas en cada peticion
   Par�metros de entrada-salida: No aplica
   Valor de retorno: CURSORTYP
   Precondiciones:
Creador: Joaquin Jaime S�nchez Salgado
Fecha de creaci�n: 19/11/2021
***********************************************************************************************/
   AS
      vl_CurSalida    VELIT.TYPES.CURSORTYP; --- cursosr para regresar las tiendas que tienen cambios
      vl_Reintentos   VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE := 0; ----- Numero de reintentos para actualizar la informacion
   BEGIN
      ---- obtener el numero de tiendas que deben enviarse
      SELECT FNVALOR
        INTO ptab_TdasXPedir
        FROM VELIT.TACONFIGURACIONPARAMETRO
       WHERE     FIPAISID = ptab_PaisId
             AND FISISTEMAID = csg_Uno
             AND FIMODULOID = csg_Tres
             AND FISUBMODULOID = csg_Dos
             AND FICONFIGURACIONID = csg_Tres;

      OPEN vl_CurSalida FOR
           SELECT EAT.FIPAISID,
                  EAT.FITIENDAID,
                  VELIT.PAWEBTRANSFERENCIAS.FNNOMBRETIENDA (EAT.FIPAISID,
                                                            EAT.FITIENDAID)
                     AS NOMBRETIENDA
             FROM VELIT.TAESTRATGPRECIO EST
                  INNER JOIN VELIT.TAESTRTXARTXTDA EAT
                     ON     EAT.FIPAISID = EST.FIPAISID
                        AND EAT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
            WHERE (   FIINDMODIFESTRTG = csg_Uno
                   OR FIINDMODIFESTATUS = csg_Uno
                   OR FIENVIOPENDTDA = csg_Cero)
         GROUP BY EAT.FIPAISID, EAT.FITIENDAID
         ORDER BY EAT.FIPAISID, EAT.FITIENDAID;

      RETURN vl_CurSalida;
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_TdasXPedir := csg_Cero;
         RETURN NULL;
         VELIT.PAGENERICOS.SPBITACORAERRORES (
            ptab_PaisId,
            csg_Cero,
            vg_Proceso,
            vg_Tabla,
            csg_Cero,
            SQLCODE,
            SQLERRM,
            'Tipo Ejecucion:' || ptab_Ejecucion,
            csg_IndError,
            vg_BitacoraId);
   END FNTIENDASPORENVIAR;
END PAWSMOTORPROMTDA03;
/


GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRSRVETGPRMCNT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRVELITWS;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRVELITWSGENERICOODIN;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRVELITWSIND;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRWSESTRTGCENT;

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA03 TO USRWSESTRTGTDA;