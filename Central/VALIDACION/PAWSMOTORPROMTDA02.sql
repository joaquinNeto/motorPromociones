﻿/* Formatted on 01/12/2021 09:06:55 a. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA02
IS
   -- Variables para manejo de errores --
   vg_Proceso                 VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                 := 'VELIT.PAWSMOTORPROMTDA02';     -- Proceso
   vg_Tabla                   VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                 VELIT.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId              VELIT.TABITACORAERRORES.FIBITACORAID%TYPE := 0; -- Id de la bitacora
   vg_DscError                VELIT.TABITACORAERRORES.FCDESERROR%TYPE := ''; -- Almacena la descripción del error de oracle.
   vg_CdgError                VELIT.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion              EXCEPTION;            -- Excepcion personalizada
   csg_IndError      CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_IndAlerta     CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                 := 2 ; -- Indica si es una alerta por lo tanto no se aplica commit dentro del store.
   csg_MUno          CONSTANT NUMBER (2) := -1;                -- Valores = -1
   csg_Cero          CONSTANT NUMBER (1) := 0;                  -- Valores = 0
   csg_Uno           CONSTANT NUMBER (1) := 1;                  -- Valores = 1
   csg_Dos           CONSTANT NUMBER (1) := 2;                  -- Valores = 2
   csg_Tres          CONSTANT NUMBER (1) := 3;                  -- Valores = 3
   csg_Cuatro        CONSTANT NUMBER (1) := 4;                  -- Valores = 4
   csg_Cinco         CONSTANT NUMBER (1) := 5;                  -- Valores = 5
   csg_Autorizado    CONSTANT VELIT.TAESTATUSESTRTG.FIESTATUSESTRTGID%TYPE
                                 := 3 ;                  -- Estatus autorizado
   csg_Modificado    CONSTANT VELIT.TAESTATUSESTRTG.FIESTATUSESTRTGID%TYPE
                                 := 4 ;                  -- Estatus modificado
   csg_Vigente       CONSTANT VELIT.TAESTATUSESTRTG.FIESTATUSESTRTGID%TYPE
                                 := 5 ;                     -- Estatus vigente
   csg_SistemasUsr   CONSTANT VELIT.TAUSUARIOS.FIUSUARIOID%TYPE := 100001; -- Usuario de sistemas

   FUNCTION FNCALCULAPERIODOESTR (
      ptab_PaisId         IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN VELIT.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE,
      pa_TipoConsulta     IN NUMBER)
      RETURN VARCHAR2;

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: consulta el periodo de una promoción
   Parámetros de entrada: -ptab_PaisId     -> Id del pais
                          -ptab_EstrategiaId   -> Id de la estrategia
                          -pa_TipoConsulta -> Id del tipo de consulta
   Parámetros de salida:  VARCHAR2
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 08/11/2021
   ***********************************************************************************************/
   PROCEDURE SPSINCRONIZAESTRXARTXTDA (
      ptab_PaisId         IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId       IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_EstrategiaId   IN     VELIT.TAESTRTXARTXTDA.FIESTRATEGIAPRCID%TYPE,
      ptab_UsuarioId      IN     VELIT.TAUSUARIOS.FIUSUARIOID%TYPE,
      ptab_FlujoAplica    IN     VELIT.TABITESTARTXTDA.FIFLUJOAPLICACION%TYPE,
      prec_Articulos         OUT VELIT.TYPES.CURSORTYP,
      pa_TotalCambios        OUT NUMBER,
      ptab_CdgError          OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: consulta el periodo de una promoción
   Parámetros de entrada: -ptab_PaisId       -> Id del pais
                          -ptab_TiendaId     -> Id de la Tienda
                          -ptab_ArticuloId   -> Id del artículo
                          -ptab_EstrategiaId -> Id de la estrategia
                          -ptab_UsuarioId    -> Id usuario actualiza
                          -ptab_FlujoAplica  -> Id tipo aplicacion 1= AUTOMATICO, 2=MANUAL CENTRAL, 3=MANUAL TIENDA
   Parámetros de salida:  -prec_Articulos    -> Array cambios
                          -ptab_CdgError     -> Codigo Respuesta
                          -ptab_DscError     -> Descripcion respuesta
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 08/11/2021
   ***********************************************************************************************/
   PROCEDURE SPCONSULTADETESTRATEGIA (
      ptab_PaisId          IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId        IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_EstrategiaId    IN     VELIT.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE,
      prec_CabEstrategia      OUT VELIT.TYPES.CURSORTYP,
      prec_DetEstrategia      OUT VELIT.TYPES.CURSORTYP,
      prec_PerEstrategia      OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError           OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: consulta el periodo de una promoción
   Parámetros de entrada: -ptab_PaisId        -> Id del pais
                          -ptab_TiendaId      -> Id de la Tienda
                          -ptab_EstrategiaId  -> Id de la estrategia
   Parámetros de salida:  -prec_CabEstrategia -> Cursor estrategias
                          -prec_DetEstrategia -> Cursor articulos
                          -prec_PerEstrategia -> Cursor periodos
                          -ptab_CdgError      -> Codigo Respuesta
                          -ptab_DscError      -> Descripcion respuesta
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 08/11/2021
   ***********************************************************************************************/

   PROCEDURE SPACTUALIZAMOVENVTDA (
      ptab_PaisId      IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId    IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      Ptab_UsuarioId   IN     VELIT.TAUSUARIOS.FIUSUARIOID%TYPE,
      prec_Respuesta   IN     VELIT.TYPARRRESPESTRG,
      ptab_CdgError       OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError       OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE);
/***********************************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Actualiza estatus sincronizacion
Parámetros de entrada: -ptab_PaisId    -> Id del pais
                       -ptab_TiendaId  -> Id de la Tienda
                       -Ptab_UsuarioId -> Id del usuario
                       -prec_Respuesta -> array respuesta
Parámetros de salida:  -ptab_CdgError  -> Codigo Respuesta
                       -ptab_DscError  -> Descripcion respuesta
Parámetros de entrada-salida:N/A
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 08/11/2021
***********************************************************************************************/
END PAWSMOTORPROMTDA02;
/

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA02
IS
   FUNCTION FNCALCULAPERIODOESTR (
      ptab_PaisId         IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_EstrategiaId   IN VELIT.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE,
      pa_TipoConsulta     IN NUMBER)
      RETURN VARCHAR2
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: consulta el periodo de una promoción
   Parámetros de entrada: -ptab_PaisId     -> Id del pais
                          -ptab_EstrategiaId   -> Id de la estrategia
                          -pa_TipoConsulta -> Id del tipo de consulta
   Parámetros de salida:  VARCHAR2
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 08/11/2021
   ***********************************************************************************************/

   IS
      vl_IniPeriodo    VARCHAR2 (10 CHAR) := '';
      vl_FinPeriodo    VARCHAR2 (10 CHAR) := '';
      vl_DiasPeriodo   VARCHAR2 (100 CHAR) := '';
      vl_TipoPeriodo   VARCHAR2 (20 CHAR) := '';
      csl_Lunes        VARCHAR2 (3 CHAR) := 'LU-';
      csl_Martes       VARCHAR2 (3 CHAR) := 'MA-';
      csl_Miercoles    VARCHAR2 (3 CHAR) := 'MI-';
      csl_Jueves       VARCHAR2 (3 CHAR) := 'JU-';
      csl_Viernes      VARCHAR2 (3 CHAR) := 'VI-';
      csl_Sabado       VARCHAR2 (3 CHAR) := 'SA-';
      csl_Domingo      VARCHAR2 (3 CHAR) := 'DO-';
      csl_Vacio        VARCHAR2 (2 CHAR) := '';
   BEGIN
      SELECT TO_CHAR (FDINICOPERIODO, 'DD/MM/YYYY'),
             TO_CHAR (FDFINPERIODO, 'DD/MM/YYYY'),
             TIPOPERIODO,
             SUBSTR (NVL (DIAS, '-11'),
                     csg_Uno,
                     LENGTH (NVL (DIAS, '-11')) - csg_Uno)
                AS DIAS
        INTO vl_IniPeriodo,
             vl_FinPeriodo,
             vl_TipoPeriodo,
             vl_DiasPeriodo
        FROM (SELECT FIESTARTEGIAPRCID,
                     FDINICOPERIODO,
                     FDFINPERIODO,
                     TIPOPERIODO,
                        CASE FILUNES
                           WHEN csg_Uno THEN csl_Lunes
                           ELSE csl_Vacio
                        END
                     || CASE FIMARTES
                           WHEN csg_Uno THEN csl_Martes
                           ELSE csl_Vacio
                        END
                     || CASE FIMIERCOLES
                           WHEN csg_Uno THEN csl_Miercoles
                           ELSE csl_Vacio
                        END
                     || CASE FIJUEVES
                           WHEN csg_Uno THEN csl_Jueves
                           ELSE csl_Vacio
                        END
                     || CASE FIVIERNES
                           WHEN csg_Uno THEN csl_Viernes
                           ELSE csl_Vacio
                        END
                     || CASE FISABADO
                           WHEN csg_Uno THEN csl_Sabado
                           ELSE csl_Vacio
                        END
                     || CASE FIDOMINGO
                           WHEN csg_Uno THEN csl_Domingo
                           ELSE csl_Vacio
                        END
                        DIAS
                FROM (SELECT FIESTARTEGIAPRCID,
                             FDINICOPERIODO,
                             FDFINPERIODO,
                             TIPOPERIODO,
                             TO_NUMBER (FILUNES)     AS FILUNES,
                             TO_NUMBER (FIMARTES)    AS FIMARTES,
                             TO_NUMBER (FIMIERCOLES) AS FIMIERCOLES,
                             TO_NUMBER (FIJUEVES)    AS FIJUEVES,
                             TO_NUMBER (FIVIERNES)   AS FIVIERNES,
                             TO_NUMBER (FISABADO)    AS FISABADO,
                             TO_NUMBER (FIDOMINGO)   AS FIDOMINGO
                        FROM (SELECT FIESTARTEGIAPRCID,
                                     FDINICOPERIODO,
                                     FDFINPERIODO,
                                     TIPOPERIODO,
                                     DIAS,
                                     ROWNUM AS ORDEN
                                FROM (    SELECT FIESTARTEGIAPRCID,
                                                 FDINICOPERIODO,
                                                 FDFINPERIODO,
                                                 TIPOPERIODO,
                                                 REGEXP_SUBSTR (
                                                    UPPER (FCDIASAPLICACION),
                                                    '[^,]+',
                                                    csg_Uno,
                                                    LEVEL)
                                                    AS DIAS
                                            FROM (  SELECT FIESTARTEGIAPRCID,
                                                           FDINICOPERIODO,
                                                           FDFINPERIODO,
                                                           CASE FITIPOPERIODO
                                                              WHEN csg_Uno
                                                              THEN
                                                                 'DIARIO'
                                                              WHEN csg_Dos
                                                              THEN
                                                                 'SEMANA'
                                                              WHEN csg_Tres
                                                              THEN
                                                                 'MES'
                                                           END
                                                              AS TIPOPERIODO,
                                                           FCDIASAPLICACION
                                                      FROM VELIT.TAESTRATGPRXPER
                                                           SXP
                                                           INNER JOIN
                                                           VELIT.TAPERIODOESTRTG
                                                           PES
                                                              ON SXP.FIPERIODOID =
                                                                    PES.FIPERIODOID
                                                     WHERE     SXP.FIPAISID =
                                                                  ptab_PaisId
                                                           AND FIESTARTEGIAPRCID =
                                                                  ptab_EstrategiaId
                                                           AND SXP.FIESTATUS =
                                                                  csg_Uno
                                                           AND PES.FIESTATUS =
                                                                  csg_Uno
                                                  GROUP BY FIESTARTEGIAPRCID,
                                                           FDINICOPERIODO,
                                                           FDFINPERIODO,
                                                           CASE FITIPOPERIODO
                                                              WHEN csg_Uno
                                                              THEN
                                                                 'DIARIO'
                                                              WHEN csg_Dos
                                                              THEN
                                                                 'SEMANA'
                                                              WHEN csg_Tres
                                                              THEN
                                                                 'MES'
                                                           END,
                                                           FCDIASAPLICACION)
                                      CONNECT BY REGEXP_SUBSTR (
                                                    UPPER (FCDIASAPLICACION),
                                                    '[^,]+',
                                                    csg_Uno,
                                                    LEVEL)
                                                    IS NOT NULL))
                             PIVOT
                                (MAX (DIAS)
                                FOR ORDEN
                                IN ('1' AS FILUNES,
                                   '2' AS FIMARTES,
                                   '3' AS FIMIERCOLES,
                                   '4' AS FIJUEVES,
                                   '5' AS FIVIERNES,
                                   '6' AS FISABADO,
                                   '7' AS FIDOMINGO))));

      IF pa_TipoConsulta = csg_Uno
      THEN
         RETURN vl_IniPeriodo;
      ELSIF pa_TipoConsulta = csg_Dos
      THEN
         RETURN vl_FinPeriodo;
      ELSIF pa_TipoConsulta = csg_Tres
      THEN
         RETURN vl_DiasPeriodo;
      ELSIF pa_TipoConsulta = csg_Cuatro
      THEN
         RETURN vl_TipoPeriodo;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '';
   END FNCALCULAPERIODOESTR;

   PROCEDURE SPSINCRONIZAESTRXARTXTDA (
      ptab_PaisId         IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId       IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_EstrategiaId   IN     VELIT.TAESTRTXARTXTDA.FIESTRATEGIAPRCID%TYPE,
      ptab_UsuarioId      IN     VELIT.TAUSUARIOS.FIUSUARIOID%TYPE,
      ptab_FlujoAplica    IN     VELIT.TABITESTARTXTDA.FIFLUJOAPLICACION%TYPE,
      prec_Articulos         OUT VELIT.TYPES.CURSORTYP,
      pa_TotalCambios        OUT NUMBER,
      ptab_CdgError          OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: consulta el periodo de una promoción
   Parámetros de entrada: -ptab_PaisId       -> Id del pais
                          -ptab_TiendaId     -> Id de la Tienda
                          -ptab_ArticuloId   -> Id del artículo
                          -ptab_EstrategiaId -> Id de la estrategia
                          -ptab_UsuarioId    -> Id usuario actualiza
                          -ptab_FlujoAplica  -> Id tipo aplicacion 1= AUTOMATICO, 2=MANUAL CENTRAL, 3=MANUAL TIENDA
   Parámetros de salida:  -prec_Articulos    -> Array cambios
                          -ptab_CdgError     -> Codigo Respuesta
                          -ptab_DscError     -> Descripcion respuesta
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 08/11/2021
   ***********************************************************************************************/
   IS
      vl_PaisId          VELIT.TAPAISES.FIPAISID%TYPE;
      vl_FechaId         VELIT.TABITESTARTXTDA.FIFECHAID%TYPE;
      vl_TiendaId        VELIT.TABITESTARTXTDA.FITIENDAID%TYPE;
      vl_EstrategiaId    VELIT.TAESTRTXARTXTDA.FIESTRATEGIAPRCID%TYPE;
      cur_Estrategias    VELIT.TYPES.CURSORTYP;
      rec_MovtoEnvTda    VELIT.TYPARRMOVENVTDA := VELIT.TYPARRMOVENVTDA ();
      rec_ArtEnvTda      VELIT.TYPARRARTENVTDA := VELIT.TYPARRARTENVTDA ();
      rec_PerEnvTda      VELIT.TYPARRPERENVTDA := VELIT.TYPARRPERENVTDA ();
      rec_Estrategias    VELIT.TYPARRESTRATGPR := VELIT.TYPARRESTRATGPR ();

      vl_FechaInicial    DATE := TRUNC (SYSDATE);  --Fecha Inicial a consultar
      vl_FechaFinal      DATE := TRUNC (SYSDATE);    --Fecha final a consultar
      vl_Reintentos      VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE := 0; ----- Numero de reintentos para actualizar la informacion
      vl_ArticuloId      VELIT.TAARTICULOS.FIARTICULOID%TYPE;
      vl_RegistroEnvio   VARCHAR2 (4000 CHAR) := '';
      vl_Resultado       VARCHAR2 (50 CHAR)
                            := 'Estrategia Pendiente de enviar : ' || SYSDATE;

      CURSOR cur_Articulos
      IS
         SELECT EST.FIPAISID,
                EST.FIESTRATEGIAPRCID,
                EST.FINUMESTRATEGIA,
                EST.FITIPOESTRATGID,
                EST.FICOMBINAOFRTID,
                EST.FCDESCRIPCION,
                EST.FNVENTAOBJETIVO,
                EST.FIMONEDAID,
                TO_CHAR (EST.FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAINICIOVIGENCIA,
                TO_CHAR (EST.FDFECHAFINVIGENCIA, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAFINVIGENCIA,
                EST.FIESTATUSESTRTGID,
                EST.FIREGAPLICATDA,
                EST.FICONFIGREGLASID,
                TO_CHAR (EST.FDFECHAREGISTRO, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAREGISTRO,
                EST.FIUSUARIOREGISTRAID,
                EST.FIUSUARIOMODIFICAID,
                TO_CHAR (EST.FDFECHAMODIFICACION, 'DD/MM/YYYY HH24:MI:SS')
                   AS FDFECHAMODIFICACION,
                EST.FIENVIOPENDTDA,
                NVL (TRIM (EST.FCOBSERVACIONES), '-') AS FCOBSERVACIONES,
                NVL (EST.FIMAXCOMBOS, csg_Cero)       AS FIMAXCOMBOS,
                NVL (EST.FIESTRATEGIAPADREID, csg_Cero)
                   AS FIESTRATEGIAPADREID,
                NVL (EST.FCURLPROMOCIONAL, '-')       AS FCURLPROMOCIONAL
           FROM VELIT.TAESTRATGPRECIO EST
          WHERE     EST.FIPAISID = ptab_PaisId
                AND EST.FIESTRATEGIAPRCID = vl_EstrategiaId;
   BEGIN
      vl_FechaId := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS'));
      vg_Proceso := vg_Proceso || '.SPSINCRONIZAESTRXARTXTDA';
      vg_Tabla := ' INSERT TABITESTARTXTDA';

        SELECT VELIT.TYPOBJESTRATGPR (FIPAISID, FIESTRATEGIAPRCID)
          BULK COLLECT INTO rec_Estrategias
          FROM VELIT.TAESTRTXARTXTDA
         WHERE     FIPAISID = ptab_PaisId
               AND (   FIESTRATEGIAPRCID = ptab_EstrategiaId
                    OR ptab_EstrategiaId = csg_Cero)
               AND FIESTATUS = csg_Uno
               AND FIALCANCETDA = csg_Uno
               AND FIESTATUSAPLICA = csg_Uno
               AND FITIENDAID = ptab_TiendaId
      GROUP BY FIPAISID, FIESTRATEGIAPRCID;

      IF (rec_Estrategias.COUNT) = csg_Cero
      THEN
         RAISE Exc_Excepcion;
      END IF;

      INSERT INTO VELIT.TABITESTARTXTDA
         SELECT EAT.FIPAISID,
                EAT.FIESTRATEGIAPRCID,
                EAT.FITIENDAID,
                EAT.FIARTICULOID,
                csg_Uno          AS FIESTATUSAPLICA,
                vl_FechaId       AS FIFECHAID,
                ptab_FlujoAplica AS FIFLUJOAPLICACION,
                CASE
                   WHEN ptab_UsuarioId = csg_Cero THEN csg_SistemasUsr
                   ELSE ptab_UsuarioId
                END
                   AS FIUSUARIOMODIFICAID,
                SYSDATE          AS FDFECHAMODIFICACION
           FROM VELIT.TAESTRTXARTXTDA EAT
                INNER JOIN TABLE (rec_Estrategias) EST
                   ON     EAT.FIPAISID = EST.FIPAISID
                      AND EAT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
          WHERE     EAT.FIESTATUS = csg_Uno
                AND EAT.FIALCANCETDA = csg_Uno
                AND EAT.FIESTATUSAPLICA = csg_Uno
                AND EAT.FITIENDAID = ptab_TiendaId;

      COMMIT;
      vg_Tabla := 'UPDATE TABITESTARTXTDA';

      FOR vlrec IN rec_Estrategias.FIRST .. rec_Estrategias.LAST
      LOOP
         vl_EstrategiaId := rec_Estrategias (vlrec).FIESTRATEGIAPRCID;

         UPDATE VELIT.TABITESTARTXTDA EAT
            SET FIESTATUSAPLICA = csg_Cuatro
          WHERE     FIPAISID = rec_Estrategias (vlrec).FIPAISID
                AND FIESTRATEGIAPRCID = vl_EstrategiaId
                AND FIESTATUSAPLICA = csg_Uno
                AND FITIENDAID = ptab_TiendaId
                AND FIFLUJOAPLICACION = ptab_FlujoAplica
                AND FIFECHAID != vl_FechaId;

         vg_Tabla := 'BULK COLLECT rec_ArtEnvTda';

         SELECT VELIT.TYPOBJARTENVTDA (
                   EST.FIPAISID,
                   EST.FIARTICULOID,
                   EST.FIESTRATEGIAPRCID,
                   EST.FNPRECIOBASE,
                   EST.FNMONTOAPLICA,
                   EST.FNPRECIOMINIMO,
                   EST.FIESTATUS,
                   TO_CHAR (EST.FDFECHAREGISTRO, 'DD/MM/YYYY HH24:MI:SS'),
                   EST.FIUSUARIOREGISTRAID,
                   EST.FIUSUARIOMODIFICAID,
                   TO_CHAR (EST.FDFECHAMODIFICACION, 'DD/MM/YYYY HH24:MI:SS'),
                   EST.FITIPODESCUENTO,
                   EST.FITIPODESCREGALO,
                   NVL (EST.FCDESCRIPCIONREGLA, '-'),
                   EST.FITOTALARTREQRD,
                   EST.FITOTALARTREGALO)
           BULK COLLECT INTO rec_ArtEnvTda
           FROM VELIT.TAESTRPRECXART EST
                INNER JOIN VELIT.TABITESTARTXTDA BIT
                   ON     BIT.FIPAISID = EST.FIPAISID
                      AND BIT.FIESTRATEGIAPRCID = EST.FIESTRATEGIAPRCID
                      AND BIT.FIARTICULOID = EST.FIARTICULOID
          WHERE     EST.FIPAISID = rec_Estrategias (vlrec).FIPAISID
                AND EST.FIESTRATEGIAPRCID = vl_EstrategiaId
                AND BIT.FITIENDAID = ptab_TiendaId
                AND BIT.FIFLUJOAPLICACION = ptab_FlujoAplica
                AND BIT.FIESTATUSAPLICA = csg_Uno;

         SELECT VELIT.TYPOBJPERENVTDA (EPP.FIPAISID,
                                       EPP.FIESTARTEGIAPRCID,
                                       EPP.FIPERIODOID,
                                       EPP.FINUMPERIODO,
                                       EPP.FDFECHAINICIOVIGENCIA,
                                       EPP.FDFECHAFINVIGENCIA,
                                       EPP.FIUSUARIOREGISTRAID,
                                       EPP.FDFECHAREGISTRO,
                                       PXE.FINUMPERIODOS,
                                       PXE.FITIPOPERIODO,
                                       PXE.FCDIASAPLICACION,
                                       PXE.FDINICOPERIODO,
                                       PXE.FDFINPERIODO,
                                       PXE.FIUSUARIOMODIFICAID,
                                       PXE.FDFECHAMODIFICACION,
                                       PXE.FIESTATUS)
           BULK COLLECT INTO rec_PerEnvTda
           FROM VELIT.TAESTRATGPRXPER EPP
                INNER JOIN VELIT.TAPERIODOESTRTG PXE
                   ON PXE.FIPERIODOID = EPP.FIPERIODOID
          WHERE     EPP.FIPAISID = rec_Estrategias (vlrec).FIPAISID
                AND EPP.FIESTARTEGIAPRCID = vl_EstrategiaId
                AND EPP.FIPAISID = ptab_PaisId
                AND EPP.FIESTATUS = csg_Uno
                AND PXE.FIESTATUS = csg_Uno;

         vg_Tabla := 'OPEN prec_Articulos';

         FOR D IN cur_Articulos
         LOOP
            rec_MovtoEnvTda.EXTEND;
            rec_MovtoEnvTda (rec_MovtoEnvTda.COUNT) :=
               VELIT.TYPOBJMOVENVTDA (D.FIPAISID,
                                      D.FIESTRATEGIAPRCID,
                                      D.FINUMESTRATEGIA,
                                      D.FITIPOESTRATGID,
                                      D.FICOMBINAOFRTID,
                                      D.FCDESCRIPCION,
                                      D.FNVENTAOBJETIVO,
                                      D.FIMONEDAID,
                                      D.FDFECHAINICIOVIGENCIA,
                                      D.FDFECHAFINVIGENCIA,
                                      D.FIESTATUSESTRTGID,
                                      D.FIREGAPLICATDA,
                                      D.FICONFIGREGLASID,
                                      D.FDFECHAREGISTRO,
                                      D.FIUSUARIOREGISTRAID,
                                      D.FIUSUARIOMODIFICAID,
                                      D.FDFECHAMODIFICACION,
                                      D.FIENVIOPENDTDA,
                                      D.FCOBSERVACIONES,
                                      D.FIMAXCOMBOS,
                                      D.FIESTRATEGIAPADREID,
                                      D.FCURLPROMOCIONAL,
                                      rec_ArtEnvTda,
                                      rec_PerEnvTda);
         END LOOP;
      END LOOP;

      COMMIT;

      OPEN prec_Articulos FOR
         SELECT FIPAISID,
                FIESTRATEGIAPRCID,
                FINUMESTRATEGIA,
                FITIPOESTRATGID,
                FICOMBINAOFRTID,
                FCDESCRIPCION,
                FNVENTAOBJETIVO,
                FIMONEDAID,
                FDFECHAINICIOVIGENCIA,
                FDFECHAFINVIGENCIA,
                FIESTATUSESTRTGID,
                FIREGAPLICATDA,
                FICONFIGREGLASID,
                FDFECHAREGISTRO,
                FIUSUARIOREGISTRAID,
                FIUSUARIOMODIFICAID,
                FDFECHAMODIFICACION,
                FIENVIOATDA,
                FCOBSERVACIONES,
                FIMAXCOMBOS,
                FIESTRATEGIAPADREID,
                FCURLPROMOCIONAL,
                TYPARRARTENVTDA,
                TYPARRPERENVTDA
           FROM TABLE (rec_MovtoEnvTda);

      pa_TotalCambios := rec_ArtEnvTda.COUNT;
      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Consulta exitosa';
   EXCEPTION
      WHEN Exc_Excepcion
      THEN
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'No existen estrategias por sincronizar';
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              SQLCODE,
                                              SQLERRM,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'Error al consultar';
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              SQLCODE,
                                              SQLERRM,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
   END SPSINCRONIZAESTRXARTXTDA;

   PROCEDURE SPCONSULTADETESTRATEGIA (
      ptab_PaisId          IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId        IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_EstrategiaId    IN     VELIT.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE,
      prec_CabEstrategia      OUT VELIT.TYPES.CURSORTYP,
      prec_DetEstrategia      OUT VELIT.TYPES.CURSORTYP,
      prec_PerEstrategia      OUT VELIT.TYPES.CURSORTYP,
      ptab_CdgError           OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   IS
   BEGIN
      OPEN prec_DetEstrategia FOR
         SELECT EXA.FIARTICULOID,
                   ART.FCNOMBREARTICULO
                || ' '
                || FCMARCA
                || ' '
                || FCESPECIFICACION
                || ' '
                || FCPRESENTACION
                   AS NOMBREART,
                FNPRECIOBASE,
                FNMONTOAPLICA,
                FNPRECIOMINIMO,
                CASE
                   WHEN FIESTATUSAPLICA = csg_Uno THEN 'PENDIENTE'
                   WHEN FIESTATUSAPLICA = csg_Dos THEN 'APLICADO'
                   WHEN FIESTATUSAPLICA = csg_Tres THEN 'ENVIADO CON ERROR'
                   WHEN FIESTATUSAPLICA = csg_Cuatro THEN 'CANCELADO'
                END
                   AS SINCRONIZACION,
                FIALCANCETDA ALCANCE,
                CASE EPA.FITIPODESCUENTO
                   WHEN csg_Uno THEN 'MONTO'
                   WHEN csg_Dos THEN 'PORCENTAJE'
                   WHEN csg_Tres THEN 'PIEZA'
                END
                   AS FITIPODESCUENTO,
                CASE EPA.FITIPODESCREGALO
                   WHEN csg_Uno THEN 'MONTO'
                   WHEN csg_Dos THEN 'PORCENTAJE'
                   WHEN csg_Tres THEN 'PIEZA'
                END
                   AS FITIPODESCREGALO
           FROM VELIT.TAESTRATGPRECIO EPP
                INNER JOIN VELIT.TAESTRTXARTXTDA EXA
                   ON     EPP.FIPAISID = EXA.FIPAISID
                      AND EPP.FIESTRATEGIAPRCID = EXA.FIESTRATEGIAPRCID
                INNER JOIN VELIT.TAESTRPRECXART EPA
                   ON     EXA.FIPAISID = EPA.FIPAISID
                      AND EXA.FIESTRATEGIAPRCID = EPA.FIESTRATEGIAPRCID
                      AND EXA.FIARTICULOID = EPA.FIARTICULOID
                INNER JOIN VELIT.TAARTICULOS ART
                   ON ART.FIARTICULOID = EXA.FIARTICULOID
          WHERE     EPP.FIPAISID = ptab_PaisId
                AND EXA.FITIENDAID = ptab_TiendaId
                AND EPP.FIESTRATEGIAPRCID = ptab_EstrategiaId
                AND EXA.FIESTATUS = csg_Uno;

      OPEN prec_CabEstrategia FOR
         SELECT EPP.FIPAISID,
                EPP.FIESTRATEGIAPRCID,
                EPP.FCDESCRIPCION      AS ESTRATEGIA,
                TIP.FCDESCRIPCION      AS TIPOESTRATEGIA,
                COM.FCDESCRIPCION      AS COMBINAOFERTA,
                EPP.FNVENTAOBJETIVO,
                TO_CHAR (EPP.FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY')
                   AS FDFECHAINICIOVIGENCIA,
                TO_CHAR (EPP.FDFECHAFINVIGENCIA, 'DD/MM/YYYY')
                   AS FDFECHAFINVIGENCIA,
                'Lu-Ma-Mi-Ju-Vi-Sa-Do' AS DIASAPLICACION,
                'DIARIO'               AS TIPOPERIODO
           FROM VELIT.TAESTRATGPRECIO EPP
                INNER JOIN VELIT.TATIPOSESTRTG TIP
                   ON TIP.FITIPOESTRATGID = EPP.FITIPOESTRATGID
                LEFT JOIN VELIT.TACOMBINAOFRT COM
                   ON COM.FICOMBINAOFRTID = EPP.FICOMBINAOFRTID
          WHERE     EPP.FIPAISID = ptab_PaisId
                AND EPP.FIESTRATEGIAPRCID = ptab_EstrategiaId;


      OPEN prec_PerEstrategia FOR
           SELECT EPE.FIESTARTEGIAPRCID,
                  EPE.FIPERIODOID,
                  FINUMPERIODO,
                  TO_CHAR (EPE.FDFECHAINICIOVIGENCIA, 'DD/MM/YYYY')
                     AS FDFECHAINICIOVIGENCIA,
                  TO_CHAR (EPE.FDFECHAFINVIGENCIA, 'DD/MM/YYYY')
                     AS FDFECHAFINVIGENCIA
             FROM VELIT.TAESTRATGPRXPER EPE
                  INNER JOIN VELIT.TAPERIODOESTRTG PES
                     ON PES.FIPERIODOID = EPE.FIPERIODOID
            WHERE     FIESTARTEGIAPRCID = ptab_EstrategiaId
                  AND EPE.FIESTATUS = csg_Uno
                  AND PES.FIESTATUS = csg_Uno
         ORDER BY FINUMPERIODO;

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Consulta Exitosa';
   END SPCONSULTADETESTRATEGIA;

   PROCEDURE SPACTUALIZAMOVENVTDA (
      ptab_PaisId      IN     VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId    IN     VELIT.TATIENDAS.FITIENDAID%TYPE,
      Ptab_UsuarioId   IN     VELIT.TAUSUARIOS.FIUSUARIOID%TYPE,
      prec_Respuesta   IN     VELIT.TYPARRRESPESTRG,
      ptab_CdgError       OUT VELIT.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError       OUT VELIT.TABITACORAERRORES.FCDESERROR%TYPE)
   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: consulta el periodo de una promoción
   Parámetros de entrada: -ptab_PaisId    -> Id del pais
                          -ptab_TiendaId  -> Id de la Tienda
                          -prec_Respuesta -> array respuesta
   Parámetros de salida:  -ptab_CdgError  -> Codigo Respuesta
                          -ptab_DscError  -> Descripcion respuesta
   Parámetros de entrada-salida:N/A
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 08/11/2021
   ***********************************************************************************************/
   IS
      CURSOR cur_Respuesta
      IS
         SELECT FIPAISID,
                FITIENDAID,
                FIESTRATEGIAPRCID,
                FIARTICULOID,
                FIESTATUS
           FROM TABLE (prec_Respuesta);
   BEGIN
      vg_Proceso := vg_Proceso || '.SPACTUALIZAMOVENVTDA';

      FOR A IN cur_Respuesta
      LOOP
         VELIT.PAGENERICOS.SPBITACORAERRORES (
            ptab_PaisId,
            csg_Cero,
            vg_Proceso,
            vg_Tabla,
            csg_Cero,
            SQLCODE,
            SQLERRM,
               'PAISID: '
            || A.FIPAISID
            || ' ESTRATEGIAID: '
            || A.FIESTRATEGIAPRCID
            || ' TIENDAID: '
            || A.FITIENDAID
            || ' ARTICULOID: '
            || A.FIARTICULOID,
            csg_IndError,
            vg_BitacoraId);

         UPDATE VELIT.TAESTRTXARTXTDA
            SET FIESTATUSAPLICA = A.FIESTATUS,
                FIFLUJOAPLICACION = csg_Tres,
                FIUSUARIOMODIFICAID = Ptab_UsuarioId,
                FDFECHAMODIFICACION = SYSDATE
          WHERE     FIPAISID = A.FIPAISID
                AND FIESTRATEGIAPRCID = A.FIESTRATEGIAPRCID
                AND FITIENDAID = A.FITIENDAID
                AND FIARTICULOID = A.FIARTICULOID;
      END LOOP;

      COMMIT;

      ptab_CdgError := csg_Cero;
      ptab_DscError := 'Actualización correcta';
   EXCEPTION
      WHEN OTHERS
      THEN
         ptab_CdgError := csg_MUno;
         ptab_DscError := 'Error al actualizar';
         VELIT.PAGENERICOS.SPBITACORAERRORES (ptab_PaisId,
                                              csg_Cero,
                                              vg_Proceso,
                                              vg_Tabla,
                                              csg_Cero,
                                              SQLCODE,
                                              SQLERRM,
                                              vg_ExecSql,
                                              csg_IndError,
                                              vg_BitacoraId);
   END SPACTUALIZAMOVENVTDA;
END PAWSMOTORPROMTDA02;
/

GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA02 TO USRVELITWS;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA02 TO USRVELITWSIND;
GRANT EXECUTE ON VELIT.PAWSMOTORPROMTDA02 TO USRVELITWSGENERICOODIN;