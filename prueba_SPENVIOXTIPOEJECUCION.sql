DECLARE
   -- Declarations
   var_PARR_ARRAYTDAS       VELIT.TYPARRAYTDAID := VELIT.TYPARRAYTDAID();
   var_PTAB_TIPOEJECUCION   NUMBER;
   var_PREC_CURSORENVIO     SYS_REFCURSOR;
   var_PREC_CURSORUSUARIO   SYS_REFCURSOR;
   var_PTAB_CDGERROR        NUMBER;
   var_PTAB_DSCERROR        VARCHAR2 (4000);

   PROCEDURE OUTPUT_LINE (a VARCHAR2)
   AS
   BEGIN
      IF LENGTHB (a) <= 32767
      THEN
         SYS.DBMS_OUTPUT.put_line (a);
      ELSE
         SYS.DBMS_OUTPUT.put_line (SUBSTRB (a, 1, 32767));
      END IF;
   END;
BEGIN
   -- Initialization
   var_PTAB_TIPOEJECUCION := 1;

   var_PARR_ARRAYTDAS.EXTEND;
   var_PARR_ARRAYTDAS(var_PARR_ARRAYTDAS.COUNT) := VELIT.TYPOBJTDAID(1,3815);

   -- Call
   VELIT.PAWSMOTORPROMTDA03.SPENVIOXTIPOEJECUCION (
      PARR_ARRAYTDAS       => var_PARR_ARRAYTDAS,
      PTAB_TIPOEJECUCION   => var_PTAB_TIPOEJECUCION,
      PREC_CURSORENVIO     => var_PREC_CURSORENVIO,
      PREC_CURSORUSUARIO   => var_PREC_CURSORUSUARIO,
      PTAB_CDGERROR        => var_PTAB_CDGERROR,
      PTAB_DSCERROR        => var_PTAB_DSCERROR);

   -- Outputs
   -- var_PREC_CURSORENVIO: Unable to resolve fields
   -- var_PREC_CURSORUSUARIO: Unable to resolve fields
   OUTPUT_LINE ('PTAB_CDGERROR = ' || var_PTAB_CDGERROR);
   OUTPUT_LINE ('PTAB_DSCERROR = ' || var_PTAB_DSCERROR);
   OUTPUT_LINE ('');

   -- Transaction Control
   COMMIT;

   -- Output values, do not modify
   :4 := var_PREC_CURSORENVIO;
   :5 := var_PREC_CURSORUSUARIO;
   :6 := var_PTAB_CDGERROR;
   :7 := var_PTAB_DSCERROR;
END;