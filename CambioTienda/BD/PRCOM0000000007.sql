/* Formatted on 15/12/2021 12:21:29 p. m. (QP5 v5.294) */
CREATE OR REPLACE PACKAGE USRVELIT.PAWSESTRATEGIASPROMOCION
AS
   vg_Proceso                     VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE
                                     := 'USRVELIT.PAWSESTRATEGIASPROMOCION'; -- Proceso
   vg_Tabla                       VELITTDA.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                     VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId                  VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE := 0; -- Id de la bitacora
   vg_DscError                    VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE := ''; -- Almacena la descripción del error de oracle.
   vg_CdgError                    VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion                  EXCEPTION;        -- Excepcion personalizada
   csg_IndError          CONSTANT VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                     := 1 ; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_IndAlerta         CONSTANT VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE
                                     := 2 ; -- Indica si es una alerta por lo tanto no se aplica commit dentro del store.
   csg_Desactivado       CONSTANT NUMBER (1) := 0;                   --Valor 0
   csg_PaisId            CONSTANT NUMBER (1) := 1;              -- Id del pais
   csg_Sistema           CONSTANT NUMBER (1) := 1;          -- Id del sisterma
   csg_Modulo            CONSTANT NUMBER (1) := 7;    -- Id del modulo compras
   csg_Submodulo         CONSTANT NUMBER (1) := 1; -- Id del submodulo articulos
   csg_IdConfiCrontTda   CONSTANT NUMBER (2) := 14; -- Id de configuracion cron tienda
   csg_IdConfLlaveEnc    CONSTANT NUMBER (2) := 11; --Id de Configuracion llave de encriptacion
   csg_MUno              CONSTANT NUMBER (2) := -1;      -- Valor constante -1
   csg_Cero              CONSTANT NUMBER (1) := 0;        -- Valor constante 0
   csg_Uno               CONSTANT NUMBER (1) := 1;        -- Valor constante 1
   csg_Dos               CONSTANT NUMBER (1) := 2;        -- Valor constante 2
   csg_Tres              CONSTANT NUMBER (1) := 3;        -- Valor constante 3
   csg_Cuatro            CONSTANT NUMBER (1) := 4;        -- Valor constante 4
   vg_BitacoraErroresId           VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE
      := csg_Cero;                                       -- Indicador bitácora

   PROCEDURE SPREGISTRAESTRATEGIAS (
      rec_Articulos   IN     USRVELIT.TYPARRMOVENVTDA,
      rec_Respuesta      OUT USRVELIT.TYPES.CURSORTYP,
      ptab_CdgError      OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError      OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso para registrar las promociones
   Parámetros de entrada: -rec_Articulos -> Array con las promociones
   Parámetros de salida:  -rec_Respuesta -> Array con respuesta de actualización
                          -ptab_CdgError -> Codigo de respuesta
                          -ptab_DscError -> Descripcion de respuesta

   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 09/11/2021

   ************************************************************************************/
   PROCEDURE SPINSERTACABESTRATEGIA (
      ptab_PaisId         IN     VELITTDA.TACONFIGURACIONTIENDA.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRATGPRECIO.FIESTRATEGIAPRCID%TYPE,
      rec_Articulos       IN     USRVELIT.TYPARRMOVENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /**********************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Proceso para registrar las promociones
   Parámetros de entrada: -ptab_PaisId   -> Id de pais
                          -ptab_TiendaId -> Id de tienda
                          -pa_CadenaGen  -> Cadena de cambios
   Parámetros de salida:  -ptab_CdgError -> Codigo de respuesta
                          -ptab_DscError -> Descripcion de respuesta

   Parámetros de entrada-salida:
   Precondiciones:
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha de creación: 09/11/2021

   ************************************************************************************/
   PROCEDURE SPREGISTRAESTRATEGIAXART (
      ptab_PaisId         IN     VELITTDA.TACONFIGURACIONTIENDA.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRPRECXART.FIESTRATEGIAPRCID%TYPE,
      rec_ArtEstrategia   IN     USRVELIT.TYPARRARTENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso para registrar las promociones
Parámetros de entrada: -ptab_PaisId       -> Id de pais
                       -ptab_EstrategiaId -> Id de la estrategia
                       -rec_ArtEstrategia -> arreglo de articulos
Parámetros de salida:  -ptab_CdgError     -> Codigo de respuesta
                       -ptab_DscError     -> Descripcion de respuesta

Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 09/11/2021

************************************************************************************/
   PROCEDURE SPREGISTRAESTRATEGIAXPER (
      ptab_PaisId         IN     VELITTDA.TACONFIGURACIONTIENDA.FIPAISID%TYPE,
      ptab_EstrategiaId   IN     VELITTDA.TAESTRATGPRXPER.FIESTARTEGIAPRCID%TYPE,
      rec_PerEstrategia   IN     USRVELIT.TYPARRPERENVTDA,
      rec_Respuesta          OUT USRVELIT.TYPARRRESPESTRG,
      ptab_CdgError          OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError          OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

         /**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Proceso para registrar las promociones
Parámetros de entrada: -ptab_PaisId       -> Id de pais
                       -ptab_EstrategiaId -> Id de la estrategia
                       -rec_PerEstrategia -> arreglo de articulos
Parámetros de salida:  -ptab_CdgError     -> Codigo de respuesta
                       -ptab_DscError     -> Descripcion de respuesta

Parámetros de entrada-salida:
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creación: 09/11/2021

************************************************************************************/
   PROCEDURE SPINSERTAESTRATEGIASCRON (
      parr_ArregloCambios   IN     USRVELIT.TYPARRAYENVIO,
      prec_CurDatosSalida      OUT USRVELIT.TYPES.CURSORTYP,
      ptab_CdgError            OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError            OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para actualizar las estrategias de venta
   Parámetros de entrada: -paArregloCambios  -> Array Con los Cambios a procesar
   Parámetros de salida:  -paCurDatosSalida  -> Curson con la respuesta generada por articulo
                          -paCdgError       -> Código de respuesta generado por ORACLE
                          -paDescError      -> Descripción de respuesta generado por ORACLE
   Creador: Joaquin Jaime Sánchez Salgado
   Fecha: 22/11/2021
   ***********************************************************************************************/
   PROCEDURE SPHABILITACRONTDA (
      pa_PaisId            IN     NUMBER,
      pa_HabilitaCronTda   IN     NUMBER,
      ptab_CdgError           OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);

   /***********************************************************************************************
   Proyecto: Sistema Integral de Operaciones NETO
   Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
   Parámetros de entrada: -paPaisId          -> Id del Pais
   Parámetros de salida:  -paHabilitaCronTda -> Valor del Parametro
                          -paCdgError        -> Código de error generado por ORACLE
                          -paDescError       -> Descripción del error generado por ORACLE
   Creador: Joaquin Jaime Sanchez Salgado
   Fecha: 24/11/2021
   ***********************************************************************************************/
   PROCEDURE SPCONSULTAPARAMCRON (
      pa_PaisId            IN     NUMBER,
      pa_HabilitaCronTda      OUT NUMBER,
      ptab_CdgError           OUT VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE,
      ptab_DscError           OUT VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);
/***********************************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripción: Procedimiento para consultar si el parametro del cron de Tda esta Activo
Parámetros de entrada: -paPaisId  -> Id del Pais

Parámetros de salida: -paHabilitaCronTda -> Valor del Parametro
                      -paCdgError        -> Código de error generado por ORACLE
                      -paDescError       -> Descripción del error generado por ORACLE
Creador: Joaquin Jaime Sanchez Salgado
Fecha: 24/11/2021
***********************************************************************************************/
END PAWSESTRATEGIASPROMOCION;