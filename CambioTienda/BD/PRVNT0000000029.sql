CREATE OR REPLACE PACKAGE USRVELIT.PAWSCUPONES 
AS   
/**** 
   PROCESOS PARA LA PANTALLA DE CLARIDAD DE PAGO DE BONOS DE CEDIS    
*****/    
   csgCero                  CONSTANT NUMBER(1):=0;         -- Constante numerica cero 
   csgUno                   CONSTANT NUMBER(1):=1;         -- Constante numerica uno 
   csgDos                   CONSTANT NUMBER(1):=2;         -- Constante numerica dos
   csgTres                  CONSTANT NUMBER(1):=3;         -- Constante numerica tres
   csgCuatro                CONSTANT NUMBER(1):=4;         -- Constante numerica cuatro
   csgCinco                 CONSTANT NUMBER(1):=5;         -- Constante numerica cinco
   csgSeis                  CONSTANT NUMBER(1):=6;         -- Constante numerica seis
   csgSiete                 CONSTANT NUMBER(1):=7;         -- Constante numerica siete
   csgOcho                  CONSTANT NUMBER(1):=8;         -- Constante numerica ocho
   csgNueve                 CONSTANT NUMBER(1):=9;         -- Constante valor 9
   csgDiez                  CONSTANT NUMBER(2):=10;         -- Constante valor 10
   csgOnce                  CONSTANT NUMBER(2):=11;        -- Constante numerica once
   csgDoce                  CONSTANT NUMBER(2):=12;        -- Constante numerica doce
   csgTrece                 CONSTANT NUMBER(2):=13;        -- Constante valor trece
   csgCatorce               CONSTANT NUMBER(2):=14;        -- Constante valor catorce
   csgQuince                CONSTANT NUMBER(2):=15;        -- Constante valor quince
   csgCien                  CONSTANT NUMBER(3):=100;       -- Constante valor cien
   csgMUno                  CONSTANT NUMBER(1):=-1;        -- Constante valor -1 
   csgSistema               CONSTANT NUMBER(1):=1;         -- Constante para indicar sistema
   csgModuloVentas          CONSTANT NUMBER(1):=2;         -- Constante para indicar el modulo
   csgSubmoduloCupones      CONSTANT NUMBER(2):=17;        -- Constante para indicar el submodulo
   csgArticuloDescuento     CONSTANT NUMBER(7):=1000001;   -- Constante para indicar el id articulo
   csgCdgArticuloDesc       CONSTANT VARCHAR2(7 CHAR):='1000001'; -- Constante para indicar el codigo barras articulo
   csgEspacioVacio          CONSTANT VARCHAR2(2 CHAR):=' '; -- Constante de caracter con espacio en blanco

PROCEDURE SPGESTIONCOMBOS(
   paArrArticulos            IN USRVELIT.TYPARRAYDETARTICULOOFERTAS          
   ,paArrArticulosDescuento OUT USRVELIT.TYPARRAYCUPONXARTICULO 
   ,paCdgError              OUT NUMBER 
   ,paDescError             OUT VARCHAR2 );
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Proceso que gestiona los descuentos por combos en articulos de venta 
Parámetros de entrada: - paPaisId            ---- IDENTIFICADOR DEL PAIS 
                       - paTiendaId          ---- IDENTIFICADOR DE LA TIENDA 
                       - paArrArticulos      ---- Arreglo con los articulos en la venta
                       
Parámetros de salida:  - paArrArticulosDescuento ---- arreglo de salida con los descuentos que apliquen por articulo 
                       - paCdgError              ---- 0= Exito, 1= Error
                       - paDescError             ---- mensaje de salida 
Parámetros de entrada-salida: - NA
Valores de retorno: - na 
Precondiciones: 
Creador: Alberto Huesca Agüero.
Fecha de creación: 01/09/2020 
***********************************************************************************************************************/

PROCEDURE SPINSERTAOFERTASARR(
   paArrayTipoOfertas     IN USRVELIT.TYPARRAYTIPOOFERTA 
  ,paArrayOfertas         IN USRVELIT.TYPARRAYOFERTASPUNTOVENTA
  ,paArrayCatalogacionArt IN USRVELIT.TYPARRAYCLASIFICACIONARTICULOS
  ,paCdgError            OUT NUMBER
  ,paDescError           OUT VARCHAR2);
/*************************************************************************************************************   
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Este proceso inserta la información de las ofertas y sus tipos asi como de la catalogacion de los articulos            
Parámetros de entrada: - paArrayTipoOfertas     -> Informacion de los tipos de oferta
                       - paArrayOfertas         -> Informacion de las ofertas asignadas a las tiendas
                       - paArrayCatalogacionArt -> Informacion de los articulos a actualizar por division
					                               agrupacion, categoria y subcategoria.
Parámetros de salida:  - paCdgError  -> Código de error generado por ORACLE, si no 
                                        hay errores se manda un cero 
                       - paDescError -> Descripción del error generado por ORACLE                    
Parámetros de entrada-salida: N/A.
Precondiciones: 
Creador: Hector Alfonso Cruz Carmona
Fecha de creación: 09/09/2020
**************************************************************************************************************/ 

PROCEDURE SPFINALIZAVENTACONCUPONES(
  paPaisId                  IN      VELITTDA.TADETALLECUPONESXVENTA.FIPAISID%TYPE
  ,paTiendaId               IN      VELITTDA.TADETALLECUPONESXVENTA.FITIENDAID%TYPE
  ,paConciliacionId         IN      VELITTDA.TADETALLECUPONESXVENTA.FICONCILIACIONID%TYPE
  ,paDetalleCuponesNeto     IN      USRVELIT.TYPARRAYCUPONESVENTANETO
  ,paCdgError               OUT     VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE
  ,paDescError              OUT     VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE);
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Procedimiento que registra los detalles de los cupones aplicados en una venta de Neto asi .
Parámetros de entrada: - paPaisId               -> Identificador del pais
                       - paTiendaId             -> Identificador de la tienda
                       - paConciliacionId       -> Identificador de la conciliacion
                       - paDetalleCuponesNeto  -> Arreglo con los detalles de los cupones de una venta
Parámetros de salida:  - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Hector Alfonso Cruz Carmona
Fecha de creación: 02/10/2020
***********************************************************************************************************************/   

PROCEDURE SPARTICULOSOFERTA(
   paArrDetOfertas OUT USRVELIT.TYPARRAYDETCOMBOS  
  ,paCdgError      OUT NUMBER 
  ,paDescError     OUT VARCHAR2);
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Extraccion de los datos de la cadena de reglas en arreglos para trabajarlos
Parámetros de entrada: - NA
Parámetros de salida:  - paArrDetOfertas -> Detalle de ofertas.
                       - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Alberto Huesca Agüero
Fecha de creación: 24/05/2021
***********************************************************************************************************************/   

PROCEDURE SPOFERTAMISMOART(
    paArrDetOfertas       IN USRVELIT.TYPARRAYDETCOMBOS  
   ,paIndicador           IN NUMBER 
   ,paArrArticulosVnt     IN USRVELIT.TYPARRAYARTSVENTA  
   ,paMaxCmb              IN NUMBER
   ,paArrArtVntSalida    OUT USRVELIT.TYPARRAYARTSVENTA  
   ,paCdgError           OUT NUMBER  
   ,paDescError          OUT VARCHAR2);
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Extraccion de los datos de la cadena de reglas en arreglos para trabajarlos
Parámetros de entrada: - paArrDetOfertas     ------ arreglo de ofertas 
                       - paIndicador         ------ registro en el arreglo de ofertas 
                       - paArrArticulosVnt   ------ arreglo de articulos de venta 
                       - paMaxCmb            ------ MAXIMO DE COMBOS 
Parámetros de salida:  - paArrArtVntSalida   ------ arreglo de articulos de venta 
                       - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Alberto Huesca Agüero
Fecha de creación: 24/05/2021
***********************************************************************************************************************/    

PROCEDURE SPOFERTASCOMBINADAS(
    paArrDetOfertas      IN USRVELIT.TYPARRAYDETCOMBOS 
   ,paIndicador          IN NUMBER 
   ,paArrArticulosVnt    IN USRVELIT.TYPARRAYARTSVENTA   
   ,paCombRequerido      IN NUMBER 
   ,paCombRegalo         IN NUMBER 
   ,paMaxCmb             IN NUMBER
   ,paArrArtVntSalida   OUT USRVELIT.TYPARRAYARTSVENTA   
   ,paCdgError          OUT NUMBER 
   ,paDescError         OUT VARCHAR2);
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Calculo de descuento en ofertas mixtas 
Parámetros de entrada: - paArrDetOfertas     ------ arreglo de ofertas 
                       - paIndicador         ------ registro en el arreglo de ofertas 
                       - paArrArticulosVnt   ------ arreglo de articulos de venta 
                       - paCombRequerido     ------ tipo de requeridos 
                       - paCombRegalo        ------ tipo de regalos 
                       - paMaxCmb            ------ maximo de combos por oferta 
Parámetros de salida:  - paArrArtVntSalida   ------ arreglo de articulos de venta 
                       - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Alberto Huesca Agüero
Fecha de creación: 25/05/2021
***********************************************************************************************************************/ 


END PAWSCUPONES;
