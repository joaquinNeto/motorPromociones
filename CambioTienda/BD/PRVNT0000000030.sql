CREATE OR REPLACE PACKAGE BODY USRVELIT.PAWSCUPONES 
AS 
-----------------------------------------
PROCEDURE SPGESTIONCOMBOS(
    paArrArticulos           IN USRVELIT.TYPARRAYDETARTICULOOFERTAS  
   ,paArrArticulosDescuento OUT USRVELIT.TYPARRAYCUPONXARTICULO 
   ,paCdgError              OUT NUMBER 
   ,paDescError             OUT VARCHAR2 )
IS   
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Proceso que gestiona los descuentos por combos en articulos de venta 
Parámetros de entrada: - paPaisId            ---- IDENTIFICADOR DEL PAIS 
                       - paTiendaId          ---- IDENTIFICADOR DE LA TIENDA 
                       - paArrArticulos      ---- Arreglo con los articulos en la venta
                       
Parámetros de salida:  - paArrArticulosDescuento ---- arreglo de salida con los descuentos que apliquen por articulo 
                       - paCdgError              ---- 0= Exito, 1= Error
                       - paDescError             ---- mensaje de salida 
Parámetros de entrada-salida: - NA
Valores de retorno: - na 
Precondiciones: 
Creador: Alberto Huesca Agüero.
Fecha de creación: 01/09/2020 
***********************************************************************************************************************/
   vlProceso              VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE  := 'SPGESTIONCOMBOS'; -- Logueo del nombre del procedimiento principal que se esta ejecutando
   vlTabla                VELITTDA.TABITACORAERRORES.FCTABLA%TYPE := ' ';    -- Logueo del nombre de la tabla o proceso que se esta ejecutando.
   vlExecSql              VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE :=' ' ; -- Logueo de los valores al momento del error.
   vlParametros           VARCHAR2(4000):=  ' ';-- Logueo de parametros de entrada del store
   vlIndicaError          VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1;  --Variable que indica que se trata de un error.
   vlBitacoraid           VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE     := 0;  --Identificador del error generado por la bitacora de errores
   -----
   vlPaisId             VELITTDA.TACONFIGURACIONTIENDA.FIPAISID%TYPE := 0; --- NUMERO DE PAIS
   vlTiendaId           VELITTDA.TACONFIGURACIONTIENDA.FITIENDAID%TYPE := 0; --- NUMERO DE TIENDA 
   vlArrDetOfertas      USRVELIT.TYPARRAYDETCOMBOS := USRVELIT.TYPARRAYDETCOMBOS();  ----- arreglo de ofertas 
   vlArrArticulosVnt    USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlArrayReqAux        USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo para validar requeridos
   vlArrayDescAux       USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo para validar regalos
   vlZumRequeridos      NUMBER(3) := 0; ---- numero de articulos requeridos 
   vlaplica             NUMBER(12,2) := 0; ---- VALIDAR EXISTENCIAS 
   vlDescuento          NUMBER(18,2) := 0; ---- MONTO DESC 
   vlxArticulo          NUMBER(1) := 0; ---- indicador de que la oferta es por un solo articulo
   vlmismoregalo        NUMBER(1) := 0; ---- indicador de que la oferta lleva el mismo regalo 
   vlRequeridos         NUMBER(12,2) := 0; ---- Articulos requeridos 
   vlRegalos            NUMBER(12,2) := 0; ---- Articulos regalo
   vlNumCombos          NUMBER(3) := 0; ---- Articulos regalo
   vlArticulo           NUMBER(12) := 0; ---- numero de articulo 
   vlZumtotal           NUMBER(12) := 0; ---- TOTAL DE CANTIDADES DE VENTA 
   vlProcesado          NUMBER(12,2) := 0; --- PZAS PROCESADAS  
   vlxProcesar          NUMBER(12,2) := 0; --- PZAS POR PROCESAR 
   vlRegaloEntregado    NUMBER(12,2) := 0; --- PZAS POR PROCESAR  
   vlAgrupacion         NUMBER(2) := 0; --- AGRUPACION DE ARTICULOS 
   vlAgrupacionAux      NUMBER(2) := 0; --- AGRUPACION DE ARTICULOS 
   vlmontoxArt          NUMBER(12,2) := 0; --- monto para descuentos 
   vlMontoRegalo        NUMBER(12,2) := 0; --- monto para descuentos 
   vlDescAplica         NUMBER(12,2) := 0; --- monto para descuentos 
   vltotalReg           NUMBER(12,2) := 0; --- monto para descuentos 
   vlRequeProc          NUMBER(12,2) := 0; --- monto para descuentos 
   vlCombRequerido      NUMBER(3) := 0; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
   vlCombRegalo         NUMBER(3) := 0; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
   vlCumpleReq          NUMBER(1) := 0; --- VALIDAR REUQRIDOS
   vlDescOferta         VELITTDA.TAOFERTAS.FCDESCRIPCION%TYPE := ''; ---- DESCRIPCION DE LA OFERTA 
   vlArrArticulosVntII  USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlArrArticulosNA     USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlAlcanceDesc        NUMBER(2) := 0; ----- tipo de alcance en el descuento 
   vlActivoCombos       NUMBER(1) := 0; ----- validacion de combos en tienda 
   vlCdgError           VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE := 0;  ---- codigo error 
   vlDescError          VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE := ''; ---- msj error 
   vlAgregaLinea        NUMBER(1) := 0;
   vlNumArticulos       NUMBER(12,2) := 0; ---- cantidad de articulos 18/11/2020
   vlProcesadoAplica    NUMBER(12,2) := 0; --- procesados segun tipo de promocion ------19/11/2020
   vlAgregaProc         NUMBER(12,2) := 0; --- procesados previos  ------19/11/2020
   vlFiltro2021         NUMBER(1) := 0; ----- ajustes de filtro para regalos por $ y % 18/01/2021
   vlCantOrig           NUMBER(12,2) := 0; ------ cantidad
   clCantL1             NUMBER(12,2) := 0; ------ cantidad 
   vlaplica_nw          NUMBER(12,2):= 0;----- 25/03/2021
   vlRegTotal           NUMBER(12,2) := 0; ------- registros de regalos 20/04/2021
   vlProcTotal          NUMBER(12,2) := 0; ------- registros de regalos 20/04/2021
   vl_filtroAux         NUMBER(2) := 0; ----- 20/04/2021 
   vlArtRequ            NUMBER(12) := 0; ---- art ID 21/04/2021 
   vlMaxCmb             VELITTDA.TAOFERTAS.FIMAXCOMBOS%TYPE := 0; ----- maximo de combos por oferta 
   vl_artRegs           NUMBER(12) := 0; ---- 26/julio/2021
   vl_arArtsAux         USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos requeridos --- 27/JULIO/2021
   vl_arArtsEnt         USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos requeridos --- 27/JULIO/2021
   vl_ReqValido         NUMBER(3) := 0; --- 27/JULIO/2021
   vl_RegaloValido      NUMBER(3) := 0; --- 27/JULIO/2021
   vlArrDetOfertasII    USRVELIT.TYPARRAYDETCOMBOS := USRVELIT.TYPARRAYDETCOMBOS();  ----- arreglo de ofertas --- 27/JULIO/2021
   vlArrReqTres         USRVELIT.TYPARRAYDETCOMBOS  := USRVELIT.TYPARRAYDETCOMBOS();  ------ arreglo para articulos requericos --- 27/JULIO/2021

BEGIN
------------------------------------------------
    paCdgError  := csgCero;
    paDescError := '';
    paArrArticulosDescuento := USRVELIT.TYPARRAYCUPONXARTICULO();   
    vlTabla := 'paArrArticulos';
    vlExecSql := '';
    ----- VALIDAR LA CONFIGURACION DE COMBOS EN TIENDA ---------
    USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS(csgUno,csgUno ,csgDos,csgUno,37 ,vlCdgError ,vlDescError ,vlActivoCombos);
    ------------------------------------------------------------
    IF vlActivoCombos = csgUno THEN
    IF paArrArticulos.COUNT > csgCero THEN 
       FOR J IN paArrArticulos.FIRST..paArrArticulos.LAST
       LOOP
          IF paArrArticulos(J).FITIPODESCUENTO IN (csgCero,csgCinco) THEN
             vlArrArticulosVntII.EXTEND;
             vlArrArticulosVntII(vlArrArticulosVntII.COUNT) := USRVELIT.TYPOBJARTSVENTA 
                                                       (paArrArticulos(J).FNCANTIDAD      
                                                       ,paArrArticulos(J).FIARTICULOID     
                                                       ,csgCero --paArrArticulos(J).FNMONTODESCUENTO   
                                                       ,paArrArticulos(J).FNPRECIO     
                                                       ,csgCero
                                                       ,csgCero
                                                       ,csgCero
                                                       ,csgCero
                                                       ,paArrArticulos(J).FIORDEN );
          ELSE
            vlArrArticulosNA.EXTEND;
            vlArrArticulosNA(vlArrArticulosNA.COUNT) := USRVELIT.TYPOBJARTSVENTA  
                                                      (paArrArticulos(J).FNCANTIDAD          
                                                      ,paArrArticulos(J).FIARTICULOID    
                                                      ,paArrArticulos(J).FNMONTODESCUENTO  
                                                      ,paArrArticulos(J).FNPRECIO     
                                                      ,csgCero   
                                                      ,csgCero
                                                      ,csgCero  
                                                      ,paArrArticulos(J).FITIPODESCUENTO  
                                                      ,paArrArticulos(J).FIORDEN );
          END IF;
       END LOOP;
       ------ validar si algun sku de los NA esta en los que se van a trabajar en combos quitarlo 
       IF vlArrArticulosVntII.COUNT > csgCero THEN
          FOR P IN vlArrArticulosVntII.FIRST..vlArrArticulosVntII.LAST
          LOOP
             BEGIN
                SELECT csgUno
                  INTO vlaplica
                  FROM TABLE(vlArrArticulosNA)
                 WHERE FIARTICULOID =  vlArrArticulosVntII(P).FIARTICULOID
                 GROUP BY FIARTICULOID;
             EXCEPTION
                WHEN OTHERS THEN
                   vlaplica := csgCero;
             END;
             
             IF vlaplica = csgCero THEN
                vlArrArticulosVnt.EXTEND;
                vlArrArticulosVnt(vlArrArticulosVnt.COUNT) := USRVELIT.TYPOBJARTSVENTA 
                                                             (vlArrArticulosVntII(P).FNCANTIDAD   
                                                             ,vlArrArticulosVntII(P).FIARTICULOID     
                                                             ,vlArrArticulosVntII(P).FNMONTODESCUENTO   
                                                             ,vlArrArticulosVntII(P).FNPRECIO          
                                                             ,vlArrArticulosVntII(P).FIPROCESADO      
                                                             ,vlArrArticulosVntII(P).FIREGALO          
                                                             ,vlArrArticulosVntII(P).FIOFERTAID    
                                                             ,vlArrArticulosVntII(P).FITIPODESCUENTO
                                                             ,vlArrArticulosVntII(P).FIORDEN );   
             ELSE
                vlArrArticulosNA.EXTEND;
                vlArrArticulosNA(vlArrArticulosNA.COUNT) := USRVELIT.TYPOBJARTSVENTA  
                                                             (vlArrArticulosVntII(P).FNCANTIDAD   
                                                             ,vlArrArticulosVntII(P).FIARTICULOID     
                                                             ,vlArrArticulosVntII(P).FNMONTODESCUENTO   
                                                             ,vlArrArticulosVntII(P).FNPRECIO          
                                                             ,vlArrArticulosVntII(P).FIPROCESADO      
                                                             ,vlArrArticulosVntII(P).FIREGALO          
                                                             ,vlArrArticulosVntII(P).FIOFERTAID    
                                                             ,vlArrArticulosVntII(P).FITIPODESCUENTO 
                                                             ,vlArrArticulosVntII(P).FIORDEN);   
             END IF;
             
          END LOOP;
          vlArrArticulosVntII := USRVELIT.TYPARRAYARTSVENTA();
          vlaplica := csgCero;
       END IF;
    END IF;
    
/******************************************************************************************************************************************/
/********** EXTRACCION DE LOS DATOS DE LA CADENA DE REGLAS EN ARREGLOS PARA TRABAJARLOS ***************************************************/
/******************************************************************************************************************************************/
   USRVELIT.PAWSCUPONES.SPARTICULOSOFERTA(vlArrDetOfertas ,vlCdgError ,vlDescError);  
----------------------------------------------------------------------   
   --------------------------------- 27/JULIO/2021
----------------------------------------------------------------------   
   SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(FIARTICULOID, SUM(FNCANTIDAD), 0)  --- 27/JULIO/2021
     BULK COLLECT INTO vl_arArtsEnt
     FROM TABLE(vlArrArticulosVnt)
    GROUP BY FIARTICULOID;
   IF vlArrDetOfertas.COUNT > 0 THEN 
   FOR G IN vlArrDetOfertas.FIRST..vlArrDetOfertas.LAST  --- 27/JULIO/2021
   LOOP
      ----- OBTENER TODOS LOS ARTICULOS REQUERIDOS 
      SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS  
            (FIARTICULOID ,FNCANTIDAD ,TIPOART   )
        BULK COLLECT INTO vl_arArtsAux
        FROM(
      SELECT FNCANTIDAD, FIARTICULOID, 1 AS TIPOART
        FROM TABLE(vlArrDetOfertas(G).ARRAYARTREQUERIDO) 
      UNION
      SELECT FNCANTIDAD, FIARTICULOID, 2 AS TIPOART
        FROM TABLE(vlArrDetOfertas(G).ARRAYARTREGALO));
       ----- VALIDAR SI LA OFERTA PUEDE APLICAR 
       FOR H IN vl_arArtsAux.FIRST..vl_arArtsAux.LAST
       LOOP
          IF vl_arArtsAux(H).FIAGRUPACION = 1 THEN --- REQUERIDOS  1000001
             IF vl_arArtsAux(H).FIARTICULOID = 1000001 THEN ---- 
                vl_ReqValido := 1;
             ELSE
                BEGIN
                   SELECT 1
                     INTO vl_ReqValido
                     FROM TABLE(vl_arArtsEnt)
                    WHERE FIARTICULOID = vl_arArtsAux(H).FIARTICULOID
                      AND FNCANTIDAD   >= vl_arArtsAux(H).FNCANTIDAD;
                EXCEPTION
                   WHEN OTHERS THEN
                      vl_ReqValido := 0;
                END;
             END IF; 
          ELSE --- REGALOS 
             BEGIN
                SELECT 1
                  INTO vl_RegaloValido
                  FROM TABLE(vl_arArtsEnt)
                 WHERE FIARTICULOID = vl_arArtsAux(H).FIARTICULOID
                   AND FNCANTIDAD   >= CASE WHEN vlArrDetOfertas(G).FITIPODESCREGALO <> 3 THEN 1 ELSE vl_arArtsAux(H).FNCANTIDAD END;  ---  
             EXCEPTION
                WHEN OTHERS THEN
                   IF vlArrDetOfertas(G).FIINCLUSIVO = 1 THEN  
                      vl_RegaloValido := 0;
                   ELSE
                      IF vl_RegaloValido = 0 THEN
                         vl_RegaloValido := 0;
                      END IF;
                   END IF;
             END;   
       
          END IF;
       END LOOP; 
   
       IF vl_ReqValido = 1 AND vl_RegaloValido = 1 THEN
          --DBMS_OUTPUT.PUT_LINE('FIOFERTAID: '||vlArrDetOfertas(G).FIOFERTAID||' ,vl_ReqValido: '||vl_ReqValido||' ,vl_RegaloValido: '||vl_RegaloValido);
          vlArrDetOfertasII.EXTEND;
          vlArrDetOfertasII(vlArrDetOfertasII.COUNT) := USRVELIT.TYPOBJDETCOMBOS  
                                                       (vlArrDetOfertas(G).FIOFERTAID         
                                                       ,vlArrDetOfertas(G).ARRAYARTREQUERIDO  
                                                       ,vlArrDetOfertas(G).FITIPODESCUENTO    
                                                       ,vlArrDetOfertas(G).FNDESCUENTO       
                                                       ,vlArrDetOfertas(G).FIUNIDADES       
                                                       ,vlArrDetOfertas(G).FITIPODESCREGALO   
                                                       ,vlArrDetOfertas(G).ARRAYARTREGALO    
                                                       ,vlArrDetOfertas(G).FICOMBOREQUERIDO   
                                                       ,vlArrDetOfertas(G).FICOMBOREGALO  
                                                       ,vlArrDetOfertas(G).FIINCLUSIVO  );
       END IF;
       vl_ReqValido := 0;
       vl_RegaloValido := 0;
   END LOOP;  ---- 27/JULIO/2021 
   END IF;
   vlArrDetOfertas := vlArrDetOfertasII;
   vl_arArtsEnt    := USRVELIT.TYPARRAYARTREQUIERECOMBOS();
   vl_arArtsAux    := USRVELIT.TYPARRAYARTREQUIERECOMBOS();
----------------------------------------------------------------------
--------------------------------- 27/JULIO/2021
----------------------------------------------------------------------

/******************************************************************************************************************************************/
/********************** VALIDACION DE LOS ARTICULOS EN EL ARREGLO DE VENTA VS LA OFERTA ***************************************************/
/******************************************************************************************************************************************/  
   ---------------------------------
   vlTabla := 'vlArrDetOfertas';
   IF vlArrDetOfertas.COUNT > csgCero THEN
      ---- RECORRER EL ARREGLO DE ofertas
      FOR Y IN vlArrDetOfertas.FIRST..vlArrDetOfertas.LAST
      LOOP
         vlCombRequerido := vlArrDetOfertas(Y).FICOMBOREQUERIDO;
         vlCombRegalo    := vlArrDetOfertas(Y).FICOMBOREGALO;
         vlAlcanceDesc   := vlArrDetOfertas(Y).FIINCLUSIVO;
         ----       
         SELECT FIMAXCOMBOS
           INTO vlMaxCmb
           FROM VELITTDA.TAESTRATGPRECIO
          WHERE FIESTRATEGIAPRCID = vlArrDetOfertas(Y).FIOFERTAID;
         ----
         vlTabla := 'vlmismoregalo';
         IF vlArrDetOfertas(Y).ARRAYARTREQUERIDO.COUNT = csgUno THEN
            vlxArticulo := csgUno;
            BEGIN
               SELECT COUNT(RQ.FIARTICULOID)
                 INTO vlmismoregalo
                 FROM TABLE(vlArrDetOfertas(Y).ARRAYARTREQUERIDO) RQ
                INNER JOIN TABLE(vlArrDetOfertas(Y).ARRAYARTREGALO) RG
                   ON RQ.FIARTICULOID = RG.FIARTICULOID
                GROUP BY RQ.FIARTICULOID;
               -------
               IF vlArrDetOfertas(Y).ARRAYARTREGALO.COUNT > csgUno THEN
                  vlmismoregalo := csgCero;   
               END IF;
            EXCEPTION
               WHEN OTHERS THEN 
                  vlmismoregalo := csgCero;   
            END;
             
         ELSE
            vlxArticulo := csgCero;
         END IF;
/******************************************************************************************************************************************/
/********************** PARA OFERTAS DEL MISMO ARTICULO EN REQUERIDO Y REGALO (2X1,3X2, ETC) **********************************************/
/******************************************************************************************************************************************/       
         vlTabla := 'vlmismoregalo_1';    
         IF vlxArticulo = csgUno AND vlmismoregalo = csgUno THEN--- SI ES EL MISMO ARTICULO Y EL MISMO REGALO 
            --- VALIDAR LA CANTIDAD DE ARTICULOS EN EL ARREGLO DE ENTRADA, SUMAR LOS REQUERIDOS + REGALOS LAS VECES QUE QUEPA ARMAR LOS COMBOS 
            --- total de articulos en el arreglo de entrada 
            USRVELIT.PAWSCUPONES.SPOFERTAMISMOART(vlArrDetOfertas ,Y ,vlArrArticulosVnt,vlMaxCmb ,vlArrArticulosVntII  ,vlCdgError ,vlDescError);
            vlArrArticulosVnt := USRVELIT.TYPARRAYARTSVENTA();-- LIMPIAR
            vlArrArticulosVnt := vlArrArticulosVntII;
            ------------------------------------------------------
         ELSE
/******************************************************************************************************************************************/
/***************************************** COMBINACION DE OFERTAS *************************************************************************/
/******************************************************************************************************************************************/     

            USRVELIT.PAWSCUPONES.SPOFERTASCOMBINADAS(vlArrDetOfertas ,Y ,vlArrArticulosVnt,vlCombRequerido,vlCombRegalo,vlMaxCmb,vlArrArticulosVntII,vlCdgError ,vlDescError);
            vlArrArticulosVnt := USRVELIT.TYPARRAYARTSVENTA();-- LIMPIAR
            vlArrArticulosVnt := vlArrArticulosVntII;        
-----------------------------------------------------------------------------         
         END IF;
         vlArrArticulosVntII := USRVELIT.TYPARRAYARTSVENTA();
      END LOOP;
      ----- recorrer los articulos de venta, por si se tiene algun descuento
      vlArrArticulosVntII := USRVELIT.TYPARRAYARTSVENTA(); 
      vlTabla := 'vlArrArticulosVnt_6';
      IF vlArrArticulosVnt.COUNT > csgCero THEN
         FOR Q IN vlArrArticulosVnt.FIRST..vlArrArticulosVnt.LAST
         LOOP
            IF vlArrArticulosVnt(Q).FIREGALO > csgCero THEN
               IF vlArrArticulosVnt(Q).FITIPODESCUENTO = csgTres THEN
                  vlDescuento := ROUND((vlArrArticulosVnt(Q).FNPRECIO - vlArrArticulosVnt(Q).FNMONTODESCUENTO) * vlArrArticulosVnt(Q).FIREGALO,csgDos);
               ELSE
                  vlDescuento := vlArrArticulosVnt(Q).FIREGALO - vlArrArticulosVnt(Q).FNMONTODESCUENTO;
               END IF;
            ELSE
               vlDescuento := csgCero;
            END IF;
            
            vlArrArticulosVntII.EXTEND;
            vlArrArticulosVntII(vlArrArticulosVntII.COUNT) := USRVELIT.TYPOBJARTSVENTA  
                                                             (vlArrArticulosVnt(Q).FNCANTIDAD    
                                                             ,vlArrArticulosVnt(Q).FIARTICULOID    
                                                             ,vlDescuento   
                                                             ,vlArrArticulosVnt(Q).FNPRECIO            
                                                             ,vlArrArticulosVnt(Q).FIPROCESADO   
                                                             ,vlArrArticulosVnt(Q).FIREGALO     
                                                             ,vlArrArticulosVnt(Q).FIOFERTAID  
                                                             ,csgCero
                                                             ,vlArrArticulosVnt(Q).FIORDEN );
         END LOOP;
      END IF;
      ----- AGREGAR LOS ARTICULOS QUE YA TENIAN OTRA OFERTA
      IF vlArrArticulosNA.COUNT > csgCero THEN
         FOR K IN vlArrArticulosNA.FIRST..vlArrArticulosNA.LAST
         LOOP
            vlArrArticulosVntII.EXTEND;
            vlArrArticulosVntII(vlArrArticulosVntII.COUNT) := USRVELIT.TYPOBJARTSVENTA  
                                                             (vlArrArticulosNA(K).FNCANTIDAD    
                                                             ,vlArrArticulosNA(K).FIARTICULOID    
                                                             ,vlArrArticulosNA(K).FNMONTODESCUENTO
                                                             ,vlArrArticulosNA(K).FNPRECIO            
                                                             ,vlArrArticulosNA(K).FIPROCESADO   
                                                             ,vlArrArticulosNA(K).FIREGALO     
                                                             ,csgCero  
                                                             ,csgCero
                                                             ,vlArrArticulosNA(K).FIORDEN );
         END LOOP;
      END IF;
      -----------------
      vlArrArticulosNA := USRVELIT.TYPARRAYARTSVENTA();
      SELECT USRVELIT.TYPOBJARTSVENTA 
            (FNCANTIDAD    
            ,FIARTICULOID    
            ,FNMONTODESCUENTO  
            ,FNPRECIO    
            ,FIPROCESADO    
            ,FIREGALO    
            ,NVL(FIOFERTAID,0) --- 19/11/2020       
            ,FITIPODESCUENTO  
            ,FIORDEN )
       BULK COLLECT INTO vlArrArticulosNA
       FROM TABLE(vlArrArticulosVntII)
      ORDER BY FIORDEN;
      IF vlArrArticulosNA.COUNT > csgCero THEN
         FOR Y IN vlArrArticulosNA.FIRST..vlArrArticulosNA.LAST
         LOOP
            ------ descripcion de la oferta ---
            BEGIN
               SELECT FCDESCRIPCION
                 INTO vlDescOferta
                 FROM VELITTDA.TAESTRATGPRECIO 
                WHERE FIESTRATEGIAPRCID = vlArrArticulosNA(Y).FIOFERTAID;
            EXCEPTION
               WHEN OTHERS THEN
                  vlDescOferta := ' ';
            END;
            -----------------------------------
            IF vlArrArticulosNA(Y).FNMONTODESCUENTO > csgCero THEN
               vlDescuento := CASE WHEN (abs(vlArrArticulosNA(Y).FNCANTIDAD) * vlArrArticulosNA(Y).FNPRECIO) - vlArrArticulosNA(Y).FNMONTODESCUENTO <= 0 THEN 
                                        (vlArrArticulosNA(Y).FNMONTODESCUENTO - 0.01) --- 11/11/2020
                                   ELSE vlArrArticulosNA(Y).FNMONTODESCUENTO END;
            ELSE
               vlDescuento := vlArrArticulosNA(Y).FNMONTODESCUENTO;
            END IF;
            -----------------------------------
            paArrArticulosDescuento.EXTEND;
            paArrArticulosDescuento(paArrArticulosDescuento.COUNT) := USRVELIT.TYPOBJCUPONXARTICULO 
                                                                     (vlArrArticulosNA(Y).FNCANTIDAD        
                                                                     ,vlArrArticulosNA(Y).FIARTICULOID            	  
                                                                     ,' '    
	                                                                 ,vlArrArticulosNA(Y).FIOFERTAID          
                                                                     ,vlDescOferta 
                                                                     ,vlDescuento 
                                                                     ,' '
                                                                     ,vlArrArticulosNA(Y).FIORDEN );
         END LOOP;
      END IF;
      ------------
   ELSE
      -- REGRESAR EL ARREGLO CON LOS MISMOS ARTICULOS SIN DESCUENTO 
      vlTabla := 'paArrArticulos_2';
      FOR R IN paArrArticulos.FIRST..paArrArticulos.LAST
      LOOP
         paArrArticulosDescuento.EXTEND;
         paArrArticulosDescuento(paArrArticulosDescuento.COUNT) := USRVELIT.TYPOBJCUPONXARTICULO
                                                                   (paArrArticulos(R).FNCANTIDAD           
                                                                   ,paArrArticulos(R).FIARTICULOID                 	  
                                                                   ,''      
	                                                               ,csgCero             
                                                                   ,''  
                                                                   ,csgCero
                                                                   ,'' 
                                                                   ,paArrArticulos(R).FIORDEN );   
      END LOOP;
   END IF;
   paCdgError  := csgCero;
   paDescError := 'Exito al realizar las operaciones';

   ELSE
      paCdgError  := csgCero;
      paDescError := 'Combos no activos';
   END IF;
   
EXCEPTION   
   WHEN OTHERS THEN
      paCdgError  := csgUno;
      paDescError := SQLERRM;
      paArrArticulosDescuento := USRVELIT.TYPARRAYCUPONXARTICULO();
      USRVELIT.PAGENERICOS.SPBITACORAERRORES(vlProceso,
                                          vlTabla,
                                          csgCero,
                                          paCdgError,
                                          paDescError,
                                          vlExecSql,
                                          csgUno,
                                          vlBitacoraid);   
END SPGESTIONCOMBOS;

PROCEDURE SPINSERTAOFERTASARR(
   paArrayTipoOfertas     IN USRVELIT.TYPARRAYTIPOOFERTA 
  ,paArrayOfertas         IN USRVELIT.TYPARRAYOFERTASPUNTOVENTA
  ,paArrayCatalogacionArt IN USRVELIT.TYPARRAYCLASIFICACIONARTICULOS
  ,paCdgError            OUT NUMBER
  ,paDescError           OUT VARCHAR2)
/*************************************************************************************************************   
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Este proceso inserta la información de las ofertas y sus tipos asi como de la catalogacion de los articulos            
Parámetros de entrada: - paArrayTipoOfertas     -> Informacion de los tipos de oferta
                       - paArrayOfertas         -> Informacion de las ofertas asignadas a las tiendas
                       - paArrayCatalogacionArt -> Informacion de los articulos a actualizar por division
					                               agrupacion, categoria y subcategoria.
Parámetros de salida:  - paCdgError  -> Código de error generado por ORACLE, si no 
                                        hay errores se manda un cero 
                       - paDescError -> Descripción del error generado por ORACLE                    
Parámetros de entrada-salida: N/A.
Precondiciones: 
Creador: Hector Alfonso Cruz Carmona
Fecha de creación: 09/09/2020
**************************************************************************************************************/ 
IS 
   vlProceso         VARCHAR(60):= 'SPINSERTAOFERTASARR';  -- Logueo del nombre del procedimiento principal que se esta ejecutando
   cslNumtransac     CONSTANT VELITTDA.TABITACORAERRORES.FINUMTRANSACCION%TYPE := 0; -- Numero de la transaccion
   vlIndicaError     VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE    := 1;                               -- Si sucede un error
   vlBitacoraId      VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE       := 0; -- Se almacena el consecutivo de la bitacora de errores.
   vlCodigoError     VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE;
   vlMensajeError    VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE;
   vlTabla           VELITTDA.TABITACORAERRORES.FCTABLA%TYPE;                             -- Logueo de la tabla o proceso que se esta ejecutando.
   vlExecSql         VARCHAR2(8000):=  ' ';     -- Logueo de parametros de entrada del store
  
BEGIN
   paCdgError  := 0;
   paDescError := 'Se registraron correctamente las ofertas';
   
   IF paArrayTipoOfertas.COUNT > 0 THEN

      vlTabla:= 'MRG_TATIPOSOFERTA_01';
      MERGE INTO VELITTDA.TATIPOSOFERTA TIP
        USING ( SELECT FITIPOOFERTAID
                       ,FCDESCRIPCION
                       ,FITIPODESCUENTO
                       ,FIINDICAALCANCEDESCUENTO
                       ,FIOFERTAACUMULABLE
                       ,FITIPOVALIDACIONDESC
                 FROM TABLE (paArrayTipoOfertas) 
        ) AUX
        ON (TIP.FITIPOOFERTAID= AUX.FITIPOOFERTAID)
      WHEN MATCHED THEN
              UPDATE 
                 SET TIP.FCDESCRIPCION= AUX.FCDESCRIPCION
                     ,TIP.FITIPODESCUENTO= AUX.FITIPODESCUENTO
                     ,TIP.FIINDICAALCANCEDESCUENTO= AUX.FIINDICAALCANCEDESCUENTO
                     ,TIP.FIOFERTAACUMULABLE = AUX.FIOFERTAACUMULABLE
                     ,TIP.FITIPOVALIDACIONDESC=AUX.FITIPOVALIDACIONDESC
      WHEN NOT MATCHED THEN
                    INSERT(TIP.FITIPOOFERTAID
                           ,TIP.FCDESCRIPCION
                           ,TIP.FITIPODESCUENTO
                           ,TIP.FIINDICAALCANCEDESCUENTO
                           ,TIP.FIOFERTAACUMULABLE
                           ,TIP.FITIPOVALIDACIONDESC)
                     VALUES(AUX.FITIPOOFERTAID
                           ,AUX.FCDESCRIPCION
                           ,AUX.FITIPODESCUENTO
                           ,AUX.FIINDICAALCANCEDESCUENTO
                           ,AUX.FIOFERTAACUMULABLE
                           ,AUX.FITIPOVALIDACIONDESC);
      COMMIT;
   END IF;

   IF paArrayOfertas.COUNT > 0 THEN
   
      vlTabla:= 'MRG_TAOFERTAS_01';
      MERGE INTO VELITTDA.TAOFERTAS O
        USING ( SELECT FIOFERTAID
                       ,FCDESCRIPCION
                       ,FCOBSERVACIONES
                       ,FITIPOOFERTAID
                       ,FIUSUARIOSOLICITAID
                       ,FDFECHASOLICITUD
                       ,FIUSUARIOAUTORIZAID
                       ,FDFECHAAUTORIZA
                       ,FDFECHAINICIOVIGENCIA
                       ,FDFECHAFINVIGENCIA
                       ,FIESTATUSOFERTA
                       ,FIINDICADORENVIO
                       ,FNDESCUENTO
                       ,FCDETIMPRESIONCUPON
                       ,FCIMAGENESCUPON
                       ,FINUMDIASVIGENCIATICKET
                       ,FINEGOCIOXOFERTAID
                       ,FIMAXCOMBOS
                       ,SYSDATE FDFECHASINCRONIZACION
                       ,1 FIESTATUSSINCRONIZACION
                 FROM TABLE (paArrayOfertas) 
        ) AUX
        ON (O.FIOFERTAID= AUX.FIOFERTAID)
      WHEN MATCHED THEN
              UPDATE 
                 SET O.FCDESCRIPCION = AUX.FCDESCRIPCION
                     ,O.FCOBSERVACIONES = AUX.FCOBSERVACIONES
                     ,O.FITIPOOFERTAID = AUX.FITIPOOFERTAID
                     ,O.FIUSUARIOSOLICITAID = AUX.FIUSUARIOSOLICITAID
                     ,O.FDFECHASOLICITUD = to_date(AUX.FDFECHASOLICITUD,'DD/MM/YYYY HH24:MI:SS')
                     ,O.FIUSUARIOAUTORIZAID = AUX.FIUSUARIOAUTORIZAID
                     ,O.FDFECHAAUTORIZA = to_date(AUX.FDFECHAAUTORIZA,'DD/MM/YYYY HH24:MI:SS')
                     ,O.FDFECHAINICIOVIGENCIA = to_date(AUX.FDFECHAINICIOVIGENCIA,'DD/MM/YYYY HH24:MI:SS')
                     ,O.FDFECHAFINVIGENCIA = to_date(AUX.FDFECHAFINVIGENCIA,'DD/MM/YYYY HH24:MI:SS')
                     ,O.FIESTATUSOFERTA = AUX.FIESTATUSOFERTA
                     ,O.FIINDICADORENVIO = AUX.FIINDICADORENVIO
                     ,O.FNDESCUENTO = AUX.FNDESCUENTO
                     ,O.FCDETIMPRESIONCUPON = AUX.FCDETIMPRESIONCUPON
                     ,O.FCIMAGENESCUPON = AUX.FCIMAGENESCUPON
                     ,O.FINUMDIASVIGENCIATICKET = AUX.FINUMDIASVIGENCIATICKET
                     ,O.FINEGOCIOXOFERTAID = AUX.FINEGOCIOXOFERTAID
                     ,O.FIMAXCOMBOS        = AUX.FIMAXCOMBOS
                     ,O.FDFECHASINCRONIZACION = AUX.FDFECHASINCRONIZACION
                     ,O.FIESTATUSSINCRONIZACION = AUX.FIESTATUSSINCRONIZACION
      WHEN NOT MATCHED THEN
                    INSERT(O.FIOFERTAID
                           ,O.FCDESCRIPCION
                           ,O.FCOBSERVACIONES
                           ,O.FITIPOOFERTAID
                           ,O.FIUSUARIOSOLICITAID
                           ,O.FDFECHASOLICITUD
                           ,O.FIUSUARIOAUTORIZAID
                           ,O.FDFECHAAUTORIZA
                           ,O.FDFECHAINICIOVIGENCIA
                           ,O.FDFECHAFINVIGENCIA
                           ,O.FIESTATUSOFERTA
                           ,O.FIINDICADORENVIO
                           ,O.FNDESCUENTO
                           ,O.FCDETIMPRESIONCUPON
                           ,O.FCIMAGENESCUPON
                           ,O.FINUMDIASVIGENCIATICKET
                           ,O.FINEGOCIOXOFERTAID
                           ,O.FIMAXCOMBOS
                           ,O.FDFECHASINCRONIZACION
                           ,O.FIESTATUSSINCRONIZACION)
                     VALUES(AUX.FIOFERTAID
                            ,AUX.FCDESCRIPCION
                            ,AUX.FCOBSERVACIONES
                            ,AUX.FITIPOOFERTAID
                            ,AUX.FIUSUARIOSOLICITAID
                            ,to_date(AUX.FDFECHASOLICITUD,'DD/MM/YYYY HH24:MI:SS')
                            ,AUX.FIUSUARIOAUTORIZAID
                            ,to_date(AUX.FDFECHAAUTORIZA,'DD/MM/YYYY HH24:MI:SS')
                            ,to_date(AUX.FDFECHAINICIOVIGENCIA,'DD/MM/YYYY HH24:MI:SS')
                            ,to_date(AUX.FDFECHAFINVIGENCIA,'DD/MM/YYYY HH24:MI:SS')
                            ,AUX.FIESTATUSOFERTA
                            ,AUX.FIINDICADORENVIO
                            ,AUX.FNDESCUENTO
                            ,AUX.FCDETIMPRESIONCUPON
                            ,AUX.FCIMAGENESCUPON
                            ,AUX.FINUMDIASVIGENCIATICKET
                            ,AUX.FINEGOCIOXOFERTAID
                            ,AUX.FIMAXCOMBOS
                            ,AUX.FDFECHASINCRONIZACION
                            ,AUX.FIESTATUSSINCRONIZACION);

      FOR i IN 1..paArrayOfertas.COUNT
      LOOP

         vlTabla:= 'MRG_TAOFERTASXARTICULO_01';
         MERGE INTO VELITTDA.TAOFERTASXARTICULO OXA
           USING ( SELECT FIOFERTAID
                          ,FIARTICULOID
                          ,FITIPODESCUENTO
                          ,FNDESCUENTO
                          ,FIUNIDADES
                          ,FIUSUARIOREGISTROID
                          ,FDFECHAREGISTRO
                          ,FIESTATUS
                          ,FIINDICADORDESCUENTO
                          ,FITIPODESCREGALO
                          ,FCDESCRIPCIONREGLA
                    FROM TABLE (paArrayOfertas(i).VLARRAYOFERTASXARTICULO) 
           ) AUX
           ON(OXA.FIOFERTAID= AUX.FIOFERTAID
               AND OXA.FIARTICULOID = AUX.FIARTICULOID)
         WHEN MATCHED THEN
                 UPDATE 
                    SET OXA.FITIPODESCUENTO = AUX.FITIPODESCUENTO
                        ,OXA.FNDESCUENTO = AUX.FNDESCUENTO
                        ,OXA.FIUNIDADES = AUX.FIUNIDADES
                        ,OXA.FIUSUARIOREGISTROID = AUX.FIUSUARIOREGISTROID
                        ,OXA.FDFECHAREGISTRO = TO_DATE(AUX.FDFECHAREGISTRO,'DD/MM/YYYY HH24:MI:SS')
                        ,OXA.FIESTATUS = AUX.FIESTATUS
                        ,OXA.FIINDICADORDESCUENTO = AUX.FIINDICADORDESCUENTO
                        ,OXA.FITIPODESCREGALO = AUX.FITIPODESCREGALO
                        ,OXA.FCDESCRIPCIONREGLA = AUX.FCDESCRIPCIONREGLA
         WHEN NOT MATCHED THEN
                       INSERT( OXA.FIOFERTAID
                               ,OXA.FIARTICULOID
                               ,OXA.FITIPODESCUENTO
                               ,OXA.FNDESCUENTO
                               ,OXA.FIUNIDADES
                               ,OXA.FIUSUARIOREGISTROID
                               ,OXA.FDFECHAREGISTRO
                               ,OXA.FIESTATUS
                               ,OXA.FIINDICADORDESCUENTO
                               ,OXA.FITIPODESCREGALO
                               ,OXA.FCDESCRIPCIONREGLA)
                        VALUES(AUX.FIOFERTAID
                               ,AUX.FIARTICULOID
                               ,AUX.FITIPODESCUENTO
                               ,AUX.FNDESCUENTO
                               ,AUX.FIUNIDADES
                               ,AUX.FIUSUARIOREGISTROID
                               ,TO_DATE(AUX.FDFECHAREGISTRO,'DD/MM/YYYY HH24:MI:SS')
                               ,AUX.FIESTATUS
                               ,AUX.FIINDICADORDESCUENTO
                               ,AUX.FITIPODESCREGALO
                               ,AUX.FCDESCRIPCIONREGLA);
      END LOOP;
      COMMIT;
   END IF;
   
   vlTabla:= 'MRG_TAARTICULOSTDA_01';
   IF paArrayCatalogacionArt(1).FIARTICULOID <> 0 THEN
       MERGE INTO VELITTDA.TAARTICULOSTDA AT
       USING (SELECT FIARTICULOID
                     ,FIDIVISIONID
                     ,FIAGRUPACIONID
                     ,FICATEGORIAID
                     ,FISUBCATEGORIAID
             FROM TABLE (paArrayCatalogacionArt))AUX
          ON (AT.FIARTICULOID = AUX.FIARTICULOID)
        WHEN MATCHED THEN
      UPDATE 
         SET AT.FIDIVISIONID = AUX.FIDIVISIONID
             ,AT.FIAGRUPACIONID = AUX.FIAGRUPACIONID
             ,AT.FICATEGORIAID = AUX.FICATEGORIAID
             ,AT.FISUBCATEGORIAID = AUX.FISUBCATEGORIAID;

   END IF;

   paCdgError  := csgCero;
   paDescError := 'Exito al realizar las operaciones';
   COMMIT;

EXCEPTION
   WHEN OTHERS THEN
     ROLLBACK;
     paCdgError:= -1;
      vlCodigoError  := SQLCODE;
     vlMensajeError := SQLERRM;
     paCdgError  := -1;
     USRVELIT.PAGENERICOS.SPBITACORAERRORES(vlProceso
                                          ,vlTabla
                                          ,csgCero
                                          ,vlCodigoError
                                          ,vlMensajeError
                                          ,vlExecSql
                                          ,vlIndicaError
                                          ,vlBitacoraId); 
     paDescError := '|Alerta en el modulo de venta.'||'|'||vlCodigoError||'|'||vlMensajeError||'|'||vlProceso||'|';                   

END SPINSERTAOFERTASARR;

PROCEDURE SPFINALIZAVENTACONCUPONES(
  paPaisId                  IN      VELITTDA.TADETALLECUPONESXVENTA.FIPAISID%TYPE
  ,paTiendaId               IN      VELITTDA.TADETALLECUPONESXVENTA.FITIENDAID%TYPE
  ,paConciliacionId         IN      VELITTDA.TADETALLECUPONESXVENTA.FICONCILIACIONID%TYPE
  ,paDetalleCuponesNeto     IN      USRVELIT.TYPARRAYCUPONESVENTANETO
  ,paCdgError               OUT     VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE
  ,paDescError              OUT     VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE)
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Procedimiento que registra los detalles de los cupones aplicados en una venta de Neto asi .
Parámetros de entrada: - paPaisId               -> Identificador del pais
                       - paTiendaId             -> Identificador de la tienda
                       - paConciliacionId       -> Identificador de la conciliacion
                       - paDetalleCuponesNeto   -> Arreglo con los detalles de los cupones de una venta
Parámetros de salida:  - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Hector Alfonso Cruz Carmona
Fecha de creación: 02/10/2020
***********************************************************************************************************************/   
IS

   cslProceso      CONSTANT VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE  := 'SPFINALIZAVENTACONCUPONES';-- Proceso donde se origina el error (si existe).
   cslModulo       CONSTANT VARCHAR2(50)                            := 'Registro Cupones Neto';-- Módulo
   cslExitoso      CONSTANT NUMBER(1)    := 0;                          -- Proceos exitoso
   cslNumtransac   CONSTANT NUMBER(1)    := 0;                          -- Número de Transacción
   vlTabla         VELITTDA.TABITACORAERRORES.FCTABLA%TYPE          := ' ';-- Variable que guarda el objeto a procesar
   vlExecSql       VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE        := ' ';-- Variable que guarda Valores en proceso
   vlIndicaError   VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1;  -- Indicador de existencia de error
   vlIndicaWarning VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE := 2;      -- Indicador de existencia de un error para loguear
   vlBitacoraid    VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE     := 0;  -- Identificador en Bitacora
   vlParametros    VARCHAR2(1000)  := ' ';                              -- Parámetros Procesados

   cslRedimido     CONSTANT NUMBER(1) := 3;
   cslAplicado     CONSTANT NUMBER(1) := 5;
   
   vlArregloAgrupado USRVELIT.TYPARRAYCUPONESVENTANETO := USRVELIT.TYPARRAYCUPONESVENTANETO();

   excSinInformacion        EXCEPTION;

BEGIN
   vlParametros := 'paPaisId: '||paPaisId||
                   ' paTiendaId: '||paTiendaId||
                   ' paConciliacionId: '||paConciliacionId||
                   ' paDetalleCuponesNeto.COUNT: '||paDetalleCuponesNeto.COUNT;

   vlTabla := 'SEL_vlArregloAgrupado_01';
      SELECT USRVELIT.TYPOBJCUPONESVENTANETO(FIARTICULOID
                             ,FITIPOCUPONVENTAID
                             ,FCCODIGOCUPONID
                             ,FCDESCRIPCIONCUPON
                             ,SUM(FNDESCUENTO)
                             ,FCSESION 
                             ,FIAPLICACUPON
                            )
BULK COLLECT INTO  vlArregloAgrupado
        FROM TABLE (paDetalleCuponesNeto)
    GROUP BY FIARTICULOID
             ,FITIPOCUPONVENTAID
             ,FCCODIGOCUPONID
             ,FCDESCRIPCIONCUPON
             ,FCSESION
             ,FIAPLICACUPON;

   IF (vlArregloAgrupado.COUNT > csgCero) THEN
      FOR I IN vlArregloAgrupado.FIRST .. vlArregloAgrupado.LAST
         LOOP
            vlExecSql := 'Articulo: '||vlArregloAgrupado(I).FIARTICULOID
                         ||', Tipo Cupon: '||vlArregloAgrupado(I).FITIPOCUPONVENTAID
                         ||', Codigo Cupon: '||vlArregloAgrupado(I).FCCODIGOCUPONID
                         ||', Descripcion Cupon: '||vlArregloAgrupado(I).FCDESCRIPCIONCUPON
                         ||', Monto Descuento: '||vlArregloAgrupado(I).FNDESCUENTO
                         ||', Sesion: '||vlArregloAgrupado(I).FCSESION
                         ||', Aplica: '||vlArregloAgrupado(I).FIAPLICACUPON;

            IF vlArregloAgrupado(I).FIAPLICACUPON = csgUno THEN

               vlTabla := 'INS_TADETALLECUPONESXVENTA_01';
               INSERT INTO VELITTDA.TADETALLECUPONESXVENTA
                  (FIPAISID
                   ,FITIENDAID
                   ,FICONCILIACIONID
                   ,FIARTICULOID
                   ,FITIPOCUPONVENTAID
                   ,FCCODIGOCUPONID
                   ,FCDESCRIPCIONCUPON
                   ,FNDESCUENTO
                  )
               VALUES
                  (paPaisId
                   ,paTiendaId
                   ,paConciliacionId
                   ,vlArregloAgrupado(I).FIARTICULOID
                   ,vlArregloAgrupado(I).FITIPOCUPONVENTAID
                   ,vlArregloAgrupado(I).FCCODIGOCUPONID
                   ,vlArregloAgrupado(I).FCDESCRIPCIONCUPON
                   ,vlArregloAgrupado(I).FNDESCUENTO
                  );
            END IF;
         END LOOP;

      paCdgError   := cslExitoso;          
      paDescError  := 'Proceso Exitoso';

   ELSE
      paCdgError      := csgUno;
      paDescError     := 'El arreglo recibido no cuenta con información ';
      RAISE excSinInformacion;
   END IF;

EXCEPTION
   WHEN excSinInformacion THEN
      vlExecSql       := 'PARAMETROS: '||vlParametros||', Valores: '||vlExecSql;
      USRVELIT.PAGENERICOS.SPBITACORAERRORES(cslProceso,
                                          vlTabla,
                                          paConciliacionId,
                                          paCdgError,
                                          paDescError,
                                          vlExecSql,
                                          vlIndicaWarning,
                                          vlBitacoraid); 
   paDescError := paDescError||' '||cslModulo||'|'||paDescError||'|'||cslProceso||'|'||vlBitacoraid;
  
   WHEN OTHERS THEN
      ROLLBACK;
      paCdgError  := SQLCODE;
      paDescError := SQLERRM;
      USRVELIT.PAGENERICOS.SPBITACORAERRORES(cslProceso,
                                          vlTabla,
                                          paConciliacionId,
                                          paCdgError,
                                          paDescError,
                                          vlExecSql,
                                          vlIndicaWarning,
                                          vlBitacoraid); 
   paDescError := paDescError||' '||cslModulo||'|'||SQLERRM||'|'||cslProceso||'|'||vlBitacoraid;
END SPFINALIZAVENTACONCUPONES;
-----------------------------------
PROCEDURE SPARTICULOSOFERTA(
   paArrDetOfertas OUT USRVELIT.TYPARRAYDETCOMBOS  
  ,paCdgError      OUT NUMBER 
  ,paDescError     OUT VARCHAR2)
IS
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Extraccion de los datos de la cadena de reglas en arreglos para trabajarlos
Parámetros de entrada: - NA
Parámetros de salida:  - paArrDetOfertas -> Detalle de ofertas.
                       - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Alberto Huesca Agüero
Fecha de creación: 24/05/2021
***********************************************************************************************************************/   
   vlProceso              VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE  := 'SPARTICULOSOFERTA'; -- Logueo del nombre del procedimiento principal que se esta ejecutando
   vlTabla                VELITTDA.TABITACORAERRORES.FCTABLA%TYPE := ' ';    -- Logueo del nombre de la tabla o proceso que se esta ejecutando.
   vlExecSql              VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE :=' ' ; -- Logueo de los valores al momento del error.
   vlParametros           VARCHAR2(4000):=  ' ';-- Logueo de parametros de entrada del store
   vlIndicaError          VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1;  --Variable que indica que se trata de un error.
   vlBitacoraid           VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE     := 0;  --Identificador del error generado por la bitacora de errores
   -----
   vlCadena             VARCHAR2(200 CHAR) := '';  ---- cadena auxiliar 
   vlCadenaDos          VARCHAR2(200 CHAR) := '';  ---- cadena auxiliar 
   vlCadenaTres         VARCHAR2(200 CHAR) := '';  ---- cadena auxiliar 
   vlTipoAplica         NUMBER(1) := 0; ---- tipo de agrupacion que aplica la regla 
   vlCombRequerido      NUMBER(3) := 0; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
   vlApariciones        NUMBER(3) := 0; ---- APARICIONES DE AGRUPADOR EN CADENAS 
   vlArrArtRequerido    USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos requeridos 
   vlArrArtRegalo       USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos de regalo 
   vlArrArtRequeridoAux USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos requeridos 
   vlArrArtRegaloAux    USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos de regalo 
   vlAlcanceDesc        NUMBER(2) := 0; ----- tipo de alcance en el descuento 
   vlCombRegalo         NUMBER(3) := 0; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
----------------------------------------
   
   
   ------ CURSOR PARA LLEVAR  LAS OFERTAS ACTIVAS 
   CURSOR curArticulosOferta IS 
   SELECT OT.FIESTRATEGIAPRCID AS FIOFERTA, AO.FIARTICULOID, AO.FITIPODESCUENTO, 0 AS FNDESCUENTO,
          AO.FNPRECIOMINIMO AS FIUNIDADES,AO.FITIPODESCREGALO, AO.FCDESCRIPCIONREGLA
     FROM VELITTDA.TAESTRATGPRECIO OT
    INNER JOIN VELITTDA.TAESTRPRECXART AO
       ON AO.FIPAISID = OT.FIPAISID
      AND AO.FIESTRATEGIAPRCID = OT.FIESTRATEGIAPRCID
      AND AO.FIESTATUS = 1
    INNER JOIN VELITTDA.TAESTRATGPRXPER PE
       ON PE.FIPAISID = OT.FIPAISID
      AND PE.FIESTARTEGIAPRCID = OT.FIESTRATEGIAPRCID
      AND TRUNC(SYSDATE) BETWEEN PE.FDFECHAINICIOVIGENCIA AND PE.FDFECHAFINVIGENCIA
    WHERE TRUNC(SYSDATE) BETWEEN OT.FDFECHAINICIOVIGENCIA AND OT.FDFECHAFINVIGENCIA
      AND OT.FIESTATUSESTRTGID = 4
      AND OT.FITIPOESTRATGID IN (5) 
    ORDER BY OT.FIESTRATEGIAPRCID;

BEGIN
/******************************************************************************************************************************************/
/********** EXTRACCION DE LOS DATOS DE LA CADENA DE REGLAS EN ARREGLOS PARA TRABAJARLOS ***************************************************/
/******************************************************************************************************************************************/
   -- verificar si hay ofertas activas en la tienda y que articulos tiene
   -- la tienda ya no se valida, puesto que la oferta solo bajara a las tienda indicadas y si esta en subase ya lo validó la sincronizacion 
   --- LLENAR el arreglo con el detalle de articulos 
   paArrDetOfertas := USRVELIT.TYPARRAYDETCOMBOS();
   vlTabla := 'curArticulosOferta';
   FOR vlReg IN curArticulosOferta
   LOOP
       -- VALIDAR SI EL ARTICULO ES DEFAULT
       IF vlReg.FIARTICULOID = 1000001 THEN
          ---- IR A LAS REGLAS PARA VALIDAR EL TIPO DE ARTICULOS REQUERIDOS Y REGALOS 
          -- DIVIDIR LA REGLA POR EL | 
          vlCadena    :=  REGEXP_SUBSTR(vlReg.FCDESCRIPCIONREGLA, '[^|]+', csgUno, csgUno);
          vlCadenaDos :=  REGEXP_SUBSTR(vlReg.FCDESCRIPCIONREGLA, '[^|]+', csgUno, csgDos);
      
          ------- validar si las cadenas son vacias 
          IF vlCadenaDos IS NULL THEN   ----- 21/04/2021
             vlCadenaDos := vlCadena;
             vlCadena := '(1000001,1)';
          END IF;----- 21/04/2021
       
          IF LENGTH(vlCadena) > csgCero AND LENGTH(vlCadenaDos) > csgCero THEN -- VALIDAR QUE LA REGLA tenga 2 partes 
             -- REQUERIDOS - identificar el tipo de agrupacion (sku),[division],{marca}
             vlTabla := 'cadenaRequeridos';
             SELECT CASE WHEN INSTR(vlCadena,'(') > csgCero THEN csgUno  --- SKU
                      WHEN INSTR(vlCadena,']') > csgCero THEN csgDos  --- DIVISION
                      WHEN INSTR(vlCadena,'{') > csgCero THEN csgTres  --- MARCA 
                 END
            INTO vlTipoAplica
            FROM DUAL; 
            --
            IF vlTipoAplica = csgUno THEN  --- SKU
               vlCombRequerido := csgDos; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               vlApariciones :=  REGEXP_COUNT(vlCadena, '[ ^( ]');
               FOR I IN csgUno..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^(]+', csgUno, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadena AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,')','');
                  vlArrArtRequerido.EXTEND;
                  vlArrArtRequerido(vlArrArtRequerido.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                     (TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', csgUno, csgUno))
                                                     ,TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', csgUno, csgDos))
                                                     ,I);   
               END LOOP;
                
            ELSIF vlTipoAplica = csgDos THEN ---- DIVISION 
               vlCombRequerido := 3; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               vlApariciones :=  REGEXP_COUNT(vlCadena, ']');
               FOR I IN 1..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^[]+', 1, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadena AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,']','');  
                  -----
                  SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(
                         FIARTICULOID, TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 5)),0)
                    BULK COLLECT INTO vlArrArtRequeridoAux
                    FROM VELITTDA.TAARTICULOSTDA
                   WHERE FIDIVISIONID     = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 1))
                     AND FIAGRUPACIONID   = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 2))
                     AND FICATEGORIAID    = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 3))
                     AND FISUBCATEGORIAID = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 4))
                     AND FIESTATUSID IN (1,5,6)
                     AND FIUNIDADMEDIDAID = 1;
                  ---------------
                  FOR X IN vlArrArtRequeridoAux.FIRST..vlArrArtRequeridoAux.LAST
                  LOOP
                     vlArrArtRequerido.EXTEND;
                     vlArrArtRequerido(vlArrArtRequerido.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                                  (vlArrArtRequeridoAux(X).FIARTICULOID
                                                                  ,vlArrArtRequeridoAux(X).FNCANTIDAD
                                                                  ,I );   
                  END LOOP;     
               END LOOP;   
            ELSIF vlTipoAplica = csgTres THEN ---- MARCA 
               vlCombRequerido := 4; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               vlApariciones :=  REGEXP_COUNT(vlCadena, '{');
               FOR I IN 1..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^{]+', 1, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadena AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,'}',''); 
                  SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(FIARTICULOID ,TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 2)),0)
                    BULK COLLECT INTO vlArrArtRequeridoAux
                    FROM VELITTDA.TAARTICULOSTDA
                   WHERE UPPER(FCNOMBREARTICULO) LIKE UPPER('%'||REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 1)||'%')
                     AND FIESTATUSID IN (1,5,6)
                     AND FIUNIDADMEDIDAID = 1
                   GROUP BY FIARTICULOID;     
                  -------------
                  FOR X IN vlArrArtRequeridoAux.FIRST..vlArrArtRequeridoAux.LAST
                  LOOP
                     vlArrArtRequerido.EXTEND;
                     vlArrArtRequerido(vlArrArtRequerido.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                                  (vlArrArtRequeridoAux(X).FIARTICULOID
                                                                  ,vlArrArtRequeridoAux(X).FNCANTIDAD
                                                                  ,I );   
                  END LOOP;   
               END LOOP; 
             END IF; 
             -- REGALOS 
             vlAlcanceDesc  :=  TO_NUMBER(REGEXP_SUBSTR(vlCadenaDos, '[^!]+', csgUno, csgDos));
             vlCadenaDos    :=  REGEXP_SUBSTR(vlCadenaDos, '[^!]+', csgUno, csgUno);
             
             vlTabla := 'cadenaRegalos';
             SELECT CASE WHEN INSTR(vlCadenaDos,'(') > csgCero THEN csgUno  --- SKU
                      WHEN INSTR(vlCadenaDos,'[') > csgCero THEN csgDos  --- DIVISION
                      WHEN INSTR(vlCadenaDos,'{') > csgCero THEN csgTres  --- MARCA 
                 END
            INTO vlTipoAplica
            FROM DUAL; 
            ------------
            IF vlTipoAplica = csgUno THEN  --- SKU
               vlApariciones :=  REGEXP_COUNT(vlCadenaDos, '[ ^( ]');
               IF vlApariciones = csgUno THEN
                  vlCombRegalo := csgUno; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               ELSE
                  vlCombRegalo := csgDos; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               END IF;
               -----------                  
               FOR I IN csgUno..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^(]+', csgUno, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadenaDos AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,')','');
                  vlArrArtRegalo.EXTEND;
                  vlArrArtRegalo(vlArrArtRegalo.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                     (TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', csgUno, csgUno))
                                                     ,TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', csgUno, csgDos))
                                                     ,I);   
               END LOOP;
            ELSIF vlTipoAplica = csgDos THEN  --- DIVISION
               vlCombRegalo := 3; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               vlApariciones :=  REGEXP_COUNT(vlCadenaDos, ']');
               FOR I IN 1..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^[]+', 1, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadenaDos AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,']','');  
                  SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(
                         FIARTICULOID, TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 5)),0)
                    BULK COLLECT INTO vlArrArtRegaloAux
                    FROM VELITTDA.TAARTICULOSTDA
                   WHERE FIDIVISIONID     = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 1))
                     AND FIAGRUPACIONID   = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 2))
                     AND FICATEGORIAID    = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 3))
                     AND FISUBCATEGORIAID = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 4))
                     AND FIESTATUSID IN (1,5,6)
                     AND FIUNIDADMEDIDAID = 1;
                   ------------
                  FOR X IN vlArrArtRegaloAux.FIRST..vlArrArtRegaloAux.LAST
                  LOOP
                     vlArrArtRegalo.EXTEND;
                     vlArrArtRegalo(vlArrArtRegalo.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                                  (vlArrArtRegaloAux(X).FIARTICULOID
                                                                  ,vlArrArtRegaloAux(X).FNCANTIDAD
                                                                  ,I);   
                  END LOOP;   
               END LOOP;
            ELSIF vlTipoAplica = csgTres THEN  --- MARCA 
               vlCombRegalo := 4; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
               vlApariciones :=  REGEXP_COUNT(vlCadenaDos, '{');
               FOR I IN 1..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^{]+', 1, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadenaDos AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,'}',''); 
                  -----

                  SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(FIARTICULOID ,TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 2)),0)
                    BULK COLLECT INTO vlArrArtRegaloAux
                    FROM VELITTDA.TAARTICULOSTDA
                   WHERE UPPER(FCNOMBREARTICULO) LIKE UPPER('%'||REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 1)||'%')
                     AND FIESTATUSID IN (1,5,6)
                     AND FIUNIDADMEDIDAID = 1
                   GROUP BY FIARTICULOID;  
                  ------------------   

                  IF vlArrArtRegaloAux.COUNT > 0 THEN
                     FOR X IN vlArrArtRegaloAux.FIRST..vlArrArtRegaloAux.LAST
                     LOOP
                        vlArrArtRegalo.EXTEND;
                        vlArrArtRegalo(vlArrArtRegalo.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                                  (vlArrArtRegaloAux(X).FIARTICULOID
                                                                  ,vlArrArtRegaloAux(X).FNCANTIDAD
                                                                  ,I );   
                     END LOOP;   
                  END IF;
               END LOOP;
            END IF;
             
             paArrDetOfertas.EXTEND;
             paArrDetOfertas(paArrDetOfertas.COUNT) := USRVELIT.TYPOBJDETCOMBOS 
                                                     (vlReg.FIOFERTA  
                                                     ,vlArrArtRequerido
                                                     ,vlReg.FITIPODESCUENTO  
                                                     ,vlReg.FNDESCUENTO  
                                                     ,vlReg.FIUNIDADES  
                                                     ,vlReg.FITIPODESCREGALO 
                                                     ,vlArrArtRegalo
                                                     ,vlCombRequerido
                                                     ,vlCombRegalo
                                                     ,vlAlcanceDesc);  
             
          ELSE
             ---- REGLA NO DEFINIDA CORRECTAMENTE 
             paArrDetOfertas := USRVELIT.TYPARRAYDETCOMBOS();
          END IF;
       ELSE
          vlTabla := 'cadenaRequeridos';
          vlCombRequerido := csgUno; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
          vlArrArtRequerido.EXTEND;
          vlArrArtRequerido(vlArrArtRequerido.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS(vlReg.FIARTICULOID,vlReg.FIUNIDADES,csgCero );
          --- no lleva division la cadena se identificar el tipo de agrupacion (sku,cant),[DIV,AGRUP,CAT,SUBCAT,cant],{marca,cant}
          -- trabajar con cadenas para llenar el arreglo de regalos 
          vlCadenaDos    :=  REGEXP_SUBSTR(vlReg.FCDESCRIPCIONREGLA, '[^!]+', csgUno, csgUno);
          vlAlcanceDesc  :=  TO_NUMBER(REGEXP_SUBSTR(vlReg.FCDESCRIPCIONREGLA, '[^!]+', csgUno, csgDos));
          -----
          vlTabla := 'cadenaRegalos';
          SELECT CASE WHEN INSTR(vlCadenaDos,')') > csgCero THEN csgUno  --- SKU
                      WHEN INSTR(vlCadenaDos,']') > csgCero THEN csgDos  --- DIVISION
                      WHEN INSTR(vlCadenaDos,'}') > csgCero THEN csgTres  --- MARCA 
                 END
            INTO vlTipoAplica
            FROM DUAL; 
            
          IF vlTipoAplica = csgUno THEN 
             --- Quitar los agrupadores y dividir en las comas 
             vlApariciones :=  REGEXP_COUNT(vlCadenaDos, '[ ^) ]' );
             IF vlApariciones = csgUno THEN
                vlCombRegalo := csgUno; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
             ELSE
                vlCombRegalo := csgDos; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
             END IF;

             FOR I IN csgUno..vlApariciones
             LOOP
                SELECT REGEXP_SUBSTR(A.REG, '[^( ]+',csgUno, I) 
                  INTO vlCadenaTres
                  FROM (SELECT vlCadenaDos AS REG FROM DUAL) A;    
                vlCadenaTres := REPLACE(vlCadenaTres,')','');
                vlArrArtRegalo.EXTEND;
                vlArrArtRegalo(vlArrArtRegalo.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS
                                                   (TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', csgUno, csgUno))
                                                   ,TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', csgUno, csgDos))
                                                   ,I);   
             END LOOP;
             
          ELSIF vlTipoAplica = csgDos THEN 
             vlCombRegalo := 3; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
             vlApariciones :=  REGEXP_COUNT(vlCadenaDos,  ']');
               FOR I IN 1..vlApariciones
               LOOP
                  SELECT REGEXP_SUBSTR(A.REG, '[^[ ]+', 1, I) 
                    INTO vlCadenaTres
                    FROM (SELECT vlCadenaDos AS REG FROM DUAL) A;    
                  vlCadenaTres := REPLACE(vlCadenaTres,']','');  
                  --------------
                  SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(
                         FIARTICULOID, TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 5)),0)
                    BULK COLLECT INTO vlArrArtRegaloAux
                    FROM VELITTDA.TAARTICULOSTDA
                   WHERE FIDIVISIONID     = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 1))
                     AND FIAGRUPACIONID   = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 2))
                     AND FICATEGORIAID    = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 3))
                     AND FISUBCATEGORIAID = TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 4))
                     AND FIESTATUSID IN (3,5,6)
                     AND FIUNIDADMEDIDAID = 1;
                     
                   FOR G IN vlArrArtRegaloAux.FIRST..vlArrArtRegaloAux.LAST
                   LOOP
                      vlArrArtRegalo.EXTEND;
                      vlArrArtRegalo(vlArrArtRegalo.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS 
                                                           (vlArrArtRegaloAux(G).FIARTICULOID 
                                                           ,vlArrArtRegaloAux(G).FNCANTIDAD
                                                           ,I);
                   END LOOP;
               END LOOP;
          ELSIF vlTipoAplica = csgTres THEN 
              vlCombRegalo := 4; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
             ---- marca se maneja por nombre o tipo de marca -- 
             vlApariciones := REGEXP_COUNT(vlCadenaDos,  '[ ^{ ]');
             FOR I IN 1..vlApariciones
             LOOP
                SELECT REGEXP_SUBSTR(A.REG, '[^{ ]+', 1, I) 
                  INTO vlCadenaTres
                  FROM (SELECT vlCadenaDos AS REG FROM DUAL) A;    
                vlCadenaTres := REPLACE(vlCadenaTres,'}',''); 
                -------
                SELECT USRVELIT.TYPOBJARTREQUIERECOMBOS(FIARTICULOID ,TO_NUMBER(REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 2)),0)
                  BULK COLLECT INTO vlArrArtRegaloAux
                  FROM VELITTDA.TAARTICULOSTDA
                 WHERE UPPER(FCNOMBREARTICULO) LIKE UPPER('%'||REGEXP_SUBSTR(vlCadenaTres, '[^,]+', 1, 1)||'%')
                   AND FIESTATUSID IN (3,5,6)
                   AND FIUNIDADMEDIDAID = 1
                 GROUP BY FIARTICULOID;   
                 
                FOR G IN vlArrArtRegaloAux.FIRST..vlArrArtRegaloAux.LAST
                LOOP
                   vlArrArtRegalo.EXTEND;
                   vlArrArtRegalo(vlArrArtRegalo.COUNT) := USRVELIT.TYPOBJARTREQUIERECOMBOS 
                                                           (vlArrArtRegaloAux(G).FIARTICULOID 
                                                           ,vlArrArtRegaloAux(G).FNCANTIDAD
                                                           ,I);
                END LOOP;
                  
               END LOOP; 
          END IF;
          
          paArrDetOfertas.EXTEND;
          paArrDetOfertas(paArrDetOfertas.COUNT) := USRVELIT.TYPOBJDETCOMBOS 
                                                     (vlReg.FIOFERTA  
                                                     ,vlArrArtRequerido
                                                     ,vlReg.FITIPODESCUENTO  
                                                     ,vlReg.FNDESCUENTO  
                                                     ,vlReg.FIUNIDADES  
                                                     ,vlReg.FITIPODESCREGALO 
                                                     ,vlArrArtRegalo
                                                     ,vlCombRequerido
                                                     ,vlCombRegalo
                                                     ,vlAlcanceDesc);  
       END IF;
       vlArrArtRequerido := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); 
       vlArrArtRegalo    := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); 
   END LOOP; 

   paCdgError  := csgCero;
   paDescError := 'Exito al realizar las operaciones';

EXCEPTION   
   WHEN OTHERS THEN
      paCdgError      := csgUno;
      paDescError     := SQLERRM;
      paArrDetOfertas := USRVELIT.TYPARRAYDETCOMBOS(); 
      USRVELIT.PAGENERICOS.SPBITACORAERRORES(vlProceso,
                                          vlTabla,
                                          csgCero,
                                          paCdgError,
                                          paDescError,
                                          vlExecSql,
                                          csgUno,
                                          vlBitacoraid);   
      
END SPARTICULOSOFERTA;
------------
PROCEDURE SPOFERTAMISMOART(
    paArrDetOfertas       IN USRVELIT.TYPARRAYDETCOMBOS  
   ,paIndicador           IN NUMBER 
   ,paArrArticulosVnt     IN USRVELIT.TYPARRAYARTSVENTA  
   ,paMaxCmb              IN NUMBER
   ,paArrArtVntSalida    OUT USRVELIT.TYPARRAYARTSVENTA  
   ,paCdgError           OUT NUMBER  
   ,paDescError          OUT VARCHAR2)
IS
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Extraccion de los datos de la cadena de reglas en arreglos para trabajarlos
Parámetros de entrada: - paArrDetOfertas     ------ arreglo de ofertas 
                       - paIndicador         ------ registro en el arreglo de ofertas 
                       - paArrArticulosVnt   ------ arreglo de articulos de venta 
                       - paMaxCmb            ------ MAXIMO DE COMBOS 
Parámetros de salida:  - paArrArtVntSalida   ------ arreglo de articulos de venta 
                       - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Alberto Huesca Agüero
Fecha de creación: 24/05/2021
***********************************************************************************************************************/   
   vlProceso              VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE  := 'SPGESTIONCOMBOS'; -- Logueo del nombre del procedimiento principal que se esta ejecutando
   vlTabla                VELITTDA.TABITACORAERRORES.FCTABLA%TYPE := ' ';    -- Logueo del nombre de la tabla o proceso que se esta ejecutando.
   vlExecSql              VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE :=' ' ; -- Logueo de los valores al momento del error.
   vlParametros           VARCHAR2(4000):=  ' ';-- Logueo de parametros de entrada del store
   vlIndicaError          VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1;  --Variable que indica que se trata de un error.
   vlBitacoraid           VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE     := 0;  --Identificador del error generado por la bitacora de errores
   -----
   vlArrArticulosVntII    USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlArticulo           NUMBER(12) := 0; ---- numero de articulo 
   vlZumtotal           NUMBER(12) := 0; ---- TOTAL DE CANTIDADES DE VENTA 
   vlProcesado          NUMBER(12,2) := 0; --- PZAS PROCESADAS  
   vlxProcesar          NUMBER(12,2) := 0; --- PZAS POR PROCESAR 
   vlRegaloEntregado    NUMBER(12,2) := 0; --- PZAS POR PROCESAR  
   vlRequeridos         NUMBER(12,2) := 0; ---- Articulos requeridos 
   vlRegalos            NUMBER(12,2) := 0; ---- Articulos regalo
   vlNumCombos          NUMBER(3) := 0; ---- Articulos regalo
   vlmontoxArt          NUMBER(12,2) := 0; --- monto para descuentos 
   vlMontoRegalo        NUMBER(12,2) := 0; --- monto para descuentos 
   vlDescAplica         NUMBER(12,2) := 0; --- monto para descuentos 
   vltotalReg           NUMBER(12,2) := 0; --- monto para descuentos 
   vlRequeProc          NUMBER(12,2) := 0; --- monto para descuentos
   vlMaxCmb             NUMBER(6) := 0; ----- maximo de combos 
   
BEGIN
   IF paMaxCmb = 0 THEN
      vlMaxCmb := 9999;
   ELSE
      vlMaxCmb := paMaxCmb;
   END IF;  
/******************************************************************************************************************************************/
/********************** PARA OFERTAS DEL MISMO ARTICULO EN REQUERIDO Y REGALO (2X1,3X2, ETC) **********************************************/
/******************************************************************************************************************************************/              
            
            --- VALIDAR LA CANTIDAD DE ARTICULOS EN EL ARREGLO DE ENTRADA, SUMAR LOS REQUERIDOS + REGALOS LAS VECES QUE QUEPA ARMAR LOS COMBOS 
            --- total de articulos en el arreglo de entrada 
            BEGIN
               SELECT RQ.FIARTICULOID, SUM(VN.FNCANTIDAD - VN.FIPROCESADO)
                 INTO vlArticulo, vlZumtotal
                 FROM TABLE(paArrArticulosVnt) VN
                INNER JOIN TABLE(paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO) RQ
                   ON VN.FIARTICULOID = RQ.FIARTICULOID
                GROUP BY RQ.FIARTICULOID;
            EXCEPTION
               WHEN OTHERS THEN
                  vlArticulo := csgCero;
                  vlZumtotal := csgCero;
            END;
            vlProcesado := csgCero; 
            vlxProcesar := -1;
            vlRegaloEntregado := csgCero;
            --DBMS_OUTPUT.PUT_LINE(vlArticulo||' '||vlZumtotal);
            -------------
            ----- ordenar el arreglo descendente segun el indice, para que tome los descuentos a partir de la ultima linea
            vlArrArticulosVntII := USRVELIT.TYPARRAYARTSVENTA();
            SELECT USRVELIT.TYPOBJARTSVENTA(
                   FNCANTIDAD         
                  ,FIARTICULOID      
                  ,FNMONTODESCUENTO  
                  ,FNPRECIO          
                  ,FIPROCESADO       
                  ,FIREGALO           
                  ,FIOFERTAID        
                  ,FITIPODESCUENTO  
                  ,FIORDEN  )
              BULK COLLECT INTO vlArrArticulosVntII
              FROM TABLE(paArrArticulosVnt)
             ORDER BY FIORDEN DESC;
            ------------------------------------
            vlTabla := 'vlArrArticulosVnt'; 
            FOR V IN vlArrArticulosVntII.FIRST..vlArrArticulosVntII.LAST
            LOOP
               IF vlArticulo = vlArrArticulosVntII(V).FIARTICULOID THEN
                  SELECT RQ.FNCANTIDAD, RG.FNCANTIDAD
                    INTO vlRequeridos, vlRegalos
                    FROM TABLE(paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO) RQ
                   INNER JOIN TABLE(paArrDetOfertas(paIndicador).ARRAYARTREGALO) RG
                      ON RQ.FIARTICULOID = RG.FIARTICULOID;
                  
                  IF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgTres THEN
                     vlNumCombos := TRUNC(vlZumtotal / (vlRequeridos + vlRegalos));
                     IF vlxProcesar = csgMUno THEN
                        vlxProcesar := ((vlRequeridos + vlRegalos) * vlNumCombos);
                     END IF;
                  ELSE
                     --vlNumCombos := TRUNC(vlZumtotal / (vlRequeridos * csgDos));
                     vlNumCombos := TRUNC(vlZumtotal / (vlRequeridos + csgUno)); -------- 14/07/2021 
                     IF vlxProcesar = csgMUno THEN
                        vlxProcesar := ((vlRequeridos * csgDos) * vlNumCombos);
                     END IF;
                  END IF;
                  --DBMS_OUTPUT.PUT_LINE('vlNumCombos '||vlNumCombos);
                  -----------
                  IF vlNumCombos > vlMaxCmb THEN
                     vlNumCombos := vlMaxCmb;
                  END IF;
                  ------------
                  IF vlNumCombos > csgCero THEN
                     ------ VALIDAR LOS TIPOS DE DESCUENTO QUE SE APLICARAN ---------     
                     IF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgUno THEN ---- $
                        vlmontoxArt   := (vlArrArticulosVntII(V).FNCANTIDAD * (vlArrArticulosVntII(V).FNPRECIO - vlArrArticulosVntII(V).FNMONTODESCUENTO));
                        vlMontoRegalo := vlRegalos * vlNumCombos;
                        vlDescAplica  := CASE WHEN vlmontoxArt >= vlMontoRegalo THEN
                                                           CASE WHEN vlRegaloEntregado >= vlMontoRegalo THEN 
                                                                      vlArrArticulosVntII(V).FIREGALO
                                                                ELSE (vlMontoRegalo - vlRegaloEntregado) END
                                         ELSE CASE WHEN vlmontoxArt > 0 THEN vlmontoxArt ELSE 0 END --- 19/11/2020  
                                         END;
                     ELSIF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgDos THEN ---- %
                        vlmontoxArt   := (vlArrArticulosVntII(V).FNCANTIDAD * (vlArrArticulosVntII(V).FNPRECIO - vlArrArticulosVntII(V).FNMONTODESCUENTO));
                        vlMontoRegalo := (vlArrArticulosVntII(V).FNPRECIO - vlArrArticulosVntII(V).FNMONTODESCUENTO) *  vlRegalos;
                        vltotalReg    := vlMontoRegalo * vlNumCombos;
                        vlDescAplica  := CASE WHEN vlmontoxArt >= vltotalReg THEN
                                             CASE WHEN vlRegaloEntregado >= vltotalReg THEN 
                                                       vlArrArticulosVntII(V).FIREGALO
                                                  ELSE vltotalReg - vlRegaloEntregado END
                                             ELSE vlmontoxArt END;
                                             
                     ELSIF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgTres THEN ---- Pzas
                        vlDescAplica := CASE WHEN vlArrArticulosVntII(V).FNCANTIDAD >= ((vlRegalos * vlNumCombos) - vlRegaloEntregado) THEN  ---- AGOSTO/2021
                                                           CASE WHEN vlRegaloEntregado >= ((vlRegalos) * vlNumCombos) THEN 
                                                                      vlArrArticulosVntII(V).FIREGALO
                                                                ELSE 
                                                                   CASE WHEN vlArrArticulosVntII(V).FNCANTIDAD >= ((vlRegalos * vlNumCombos) - vlRegaloEntregado) THEN
                                                                      vlArrArticulosVntII(V).FIREGALO + ((vlRegalos * vlNumCombos) - vlRegaloEntregado)
                                                                   ELSE
                                                                      vlArrArticulosVntII(V).FNCANTIDAD
                                                                   END
                                                           END
                                                           ELSE 
                                                                CASE WHEN vlRegaloEntregado >= ((vlRegalos) * vlNumCombos) THEN 
                                                                      vlArrArticulosVntII(V).FIREGALO
                                                                ELSE 
                                                                  CASE WHEN vlArrArticulosVntII(V).FNCANTIDAD - vlRegaloEntregado > 0 THEN ---- AGOSTO/2021
                                                                       vlArrArticulosVntII(V).FNCANTIDAD - vlRegaloEntregado
                                                                  ELSE vlArrArticulosVntII(V).FNCANTIDAD
                                                                  END ---- AGOSTO/2021
                                                                END
                                                           END;
                        -----
                     END IF;
                     --- MARCAR EL REQUERIDO Y EL REGALO Y VALIDAR SI AUN SE PUEDE ARMAR OTRO COMBO, MARCARLOS 
                     vlProcesado := CASE WHEN (vlArrArticulosVntII(V).FNCANTIDAD - vlArrArticulosVntII(V).FIPROCESADO) >= vlxProcesar THEN   
                                              (vlArrArticulosVntII(V).FIPROCESADO + vlxProcesar)
                                         ELSE (vlArrArticulosVntII(V).FNCANTIDAD - vlArrArticulosVntII(V).FIPROCESADO) END;
                     vlArrArticulosVntII(V).FIPROCESADO :=  vlProcesado;
                     vlxProcesar                      := vlxProcesar - vlProcesado;
                     
                     ----------------------------------------------------------------
                     vlArrArticulosVntII(V).FIREGALO := vlDescAplica;
                     vlRegaloEntregado := vlRegaloEntregado + vlDescAplica;
                     vlArrArticulosVntII(V).FIOFERTAID := CASE WHEN vlArrArticulosVntII(V).FIREGALO > csgCero THEN
                                                              CASE WHEN LENGTH(vlArrArticulosVntII(V).FIOFERTAID) > csgUno THEN 
                                                                     vlArrArticulosVntII(V).FIOFERTAID ||' ,'|| TO_CHAR(paArrDetOfertas(paIndicador).FIOFERTAID)
                                                                ELSE TO_CHAR(paArrDetOfertas(paIndicador).FIOFERTAID) END END;
                     
                     vlArrArticulosVntII(V).FITIPODESCUENTO := paArrDetOfertas(paIndicador).FITIPODESCREGALO;
                  END IF;
               END IF;
            END LOOP;
            ------ ordenar el arreglo ascendete en base al indice
            paArrArtVntSalida := USRVELIT.TYPARRAYARTSVENTA();
            SELECT USRVELIT.TYPOBJARTSVENTA(
                   FNCANTIDAD         
                  ,FIARTICULOID      
                  ,FNMONTODESCUENTO  
                  ,FNPRECIO          
                  ,FIPROCESADO       
                  ,FIREGALO           
                  ,FIOFERTAID        
                  ,FITIPODESCUENTO  
                  ,FIORDEN  )
              BULK COLLECT INTO paArrArtVntSalida
              FROM TABLE(vlArrArticulosVntII)
             ORDER BY FIORDEN;
            ------------------------------------------------------

   paCdgError  := csgCero;
   paDescError := 'Exito al realizar las operaciones';

EXCEPTION   
   WHEN OTHERS THEN
      paCdgError  := csgUno;
      paDescError := SQLERRM;
      paArrArtVntSalida := USRVELIT.TYPARRAYARTSVENTA();
      USRVELIT.PAGENERICOS.SPBITACORAERRORES(vlProceso,
                                          vlTabla,
                                          csgCero,
                                          paCdgError,
                                          paDescError,
                                          vlExecSql,
                                          csgUno,
                                          vlBitacoraid);   
      
END SPOFERTAMISMOART;

PROCEDURE SPOFERTASCOMBINADAS(
    paArrDetOfertas      IN USRVELIT.TYPARRAYDETCOMBOS 
   ,paIndicador          IN NUMBER 
   ,paArrArticulosVnt    IN USRVELIT.TYPARRAYARTSVENTA   
   ,paCombRequerido      IN NUMBER 
   ,paCombRegalo         IN NUMBER 
   ,paMaxCmb             IN NUMBER
   ,paArrArtVntSalida   OUT USRVELIT.TYPARRAYARTSVENTA   
   ,paCdgError          OUT NUMBER 
   ,paDescError         OUT VARCHAR2)
IS
/**********************************************************************************************************************  
Proyecto: Sistema Integral de Operaciones NETO  
Descripción: Calculo de descuento en ofertas mixtas 
Parámetros de entrada: - paArrDetOfertas     ------ arreglo de ofertas 
                       - paIndicador         ------ registro en el arreglo de ofertas 
                       - paArrArticulosVnt   ------ arreglo de articulos de venta 
                       - paCombRequerido     ------ tipo de requeridos 
                       - paCombRegalo        ------ tipo de regalos 
                       - paMaxCmb            ------ maximo de combos por oferta 
Parámetros de salida:  - paArrArtVntSalida   ------ arreglo de articulos de venta 
                       - paCdgError     -> Código de error generado si es que exixte
                       - paDescError    -> Descripción del error genererado si es que existe
Parámetros de entrada-salida: N/A
Precondiciones: 
Creador: Alberto Huesca Agüero
Fecha de creación: 25/05/2021
***********************************************************************************************************************/ 
   vlProceso              VELITTDA.TABITACORAERRORES.FCPROCESO%TYPE  := 'SPGESTIONCOMBOS'; -- Logueo del nombre del procedimiento principal que se esta ejecutando
   vlTabla                VELITTDA.TABITACORAERRORES.FCTABLA%TYPE := ' ';    -- Logueo del nombre de la tabla o proceso que se esta ejecutando.
   vlExecSql              VELITTDA.TABITACORAERRORES.FCEXECSQL%TYPE :=' ' ; -- Logueo de los valores al momento del error.
   vlParametros           VARCHAR2(4000):=  ' ';-- Logueo de parametros de entrada del store
   vlIndicaError          VELITTDA.TABITACORAERRORES.FIINDICADORERROR%TYPE := 1;  --Variable que indica que se trata de un error.
   vlBitacoraid           VELITTDA.TABITACORAERRORES.FIBITACORAID%TYPE     := 0;  --Identificador del error generado por la bitacora de errores
   -----
   vlPaisId             VELITTDA.TACONFIGURACIONTIENDA.FIPAISID%TYPE := 0; --- NUMERO DE PAIS
   vlTiendaId           VELITTDA.TACONFIGURACIONTIENDA.FITIENDAID%TYPE := 0; --- NUMERO DE TIENDA 
   vlArrDetOfertas      USRVELIT.TYPARRAYDETCOMBOS := USRVELIT.TYPARRAYDETCOMBOS();  ----- arreglo de ofertas 
   vlArrArticulosVnt    USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlArrayReqAux        USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo para validar requeridos
   vlArrayDescAux       USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo para validar regalos
   vlZumRequeridos      NUMBER(3) := 0; ---- numero de articulos requeridos 
   vlaplica             NUMBER(12,2) := 0; ---- VALIDAR EXISTENCIAS 
   vlDescuento          NUMBER(18,2) := 0; ---- MONTO DESC 
   vlxArticulo          NUMBER(1) := 0; ---- indicador de que la oferta es por un solo articulo
   vlmismoregalo        NUMBER(1) := 0; ---- indicador de que la oferta lleva el mismo regalo 
   vlRequeridos         NUMBER(12,2) := 0; ---- Articulos requeridos 
   vlRegalos            NUMBER(12,2) := 0; ---- Articulos regalo
   vlNumCombos          NUMBER(3) := 0; ---- Articulos regalo
   vlArticulo           NUMBER(12) := 0; ---- numero de articulo 
   vlZumtotal           NUMBER(12) := 0; ---- TOTAL DE CANTIDADES DE VENTA 
   vlProcesado          NUMBER(12,2) := 0; --- PZAS PROCESADAS  
   vlxProcesar          NUMBER(12,2) := 0; --- PZAS POR PROCESAR 
   vlRegaloEntregado    NUMBER(12,2) := 0; --- PZAS POR PROCESAR  
   vlAgrupacion         NUMBER(2) := 0; --- AGRUPACION DE ARTICULOS 
   vlAgrupacionAux      NUMBER(2) := 0; --- AGRUPACION DE ARTICULOS 
   vlmontoxArt          NUMBER(12,2) := 0; --- monto para descuentos 
   vlMontoRegalo        NUMBER(12,2) := 0; --- monto para descuentos 
   vlDescAplica         NUMBER(12,2) := 0; --- monto para descuentos 
   vltotalReg           NUMBER(12,2) := 0; --- monto para descuentos 
   vlRequeProc          NUMBER(12,2) := 0; --- monto para descuentos 
   vlCombRequerido      NUMBER(3) := paCombRequerido; ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
   vlCombRegalo         NUMBER(3) := paCombRegalo; ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
   vlCumpleReq          NUMBER(1) := 0; --- VALIDAR REUQRIDOS
   vlDescOferta         VELITTDA.TAOFERTAS.FCDESCRIPCION%TYPE := ''; ---- DESCRIPCION DE LA OFERTA 
   vlArrArticulosVntII  USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlArrArticulosNA     USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo de articulos de venta 
   vlAlcanceDesc        NUMBER(2) := 0; ----- tipo de alcance en el descuento 
   vlActivoCombos       NUMBER(1) := 0; ----- validacion de combos en tienda 
   vlCdgError           VELITTDA.TABITACORAERRORES.FINUMERROR%TYPE := 0;  ---- codigo error 
   vlDescError          VELITTDA.TABITACORAERRORES.FCDESERROR%TYPE := ''; ---- msj error 
   vlAgregaLinea        NUMBER(1) := 0;
   vlNumArticulos       NUMBER(12,2) := 0; ---- cantidad de articulos 18/11/2020
   vlProcesadoAplica    NUMBER(12,2) := 0; --- procesados segun tipo de promocion ------19/11/2020
   vlAgregaProc         NUMBER(12,2) := 0; --- procesados previos  ------19/11/2020
   vlFiltro2021         NUMBER(1) := 0; ----- ajustes de filtro para regalos por $ y % 18/01/2021
   vlCantOrig           NUMBER(12,2) := 0; ------ cantidad
   clCantL1             NUMBER(12,2) := 0; ------ cantidad 
   vlaplica_nw          NUMBER(12,2):= 0;----- 25/03/2021
   vlRegTotal           NUMBER(12,2) := 0; ------- registros de regalos 20/04/2021
   vlProcTotal          NUMBER(12,2) := 0; ------- registros de regalos 20/04/2021
   vl_filtroAux         NUMBER(2) := 0; ----- 20/04/2021 
   vlArtRequ            NUMBER(12) := 0; ---- art ID 21/04/2021 
   vlMaxCmb             NUMBER(6) := 0; ----- maximo de combos 
   vlArrReqDos          USRVELIT.TYPARRAYARTSVENTA  := USRVELIT.TYPARRAYARTSVENTA();  ------ arreglo para articulos requericos en una ofertas de combo de sku
   vlArtCmbDos          NUMBER(2) := 0; ------ existe articulo del combo  26/05/2021
   vlRegDisp            NUMBER(12,2) := 0; ----- numero requerido al final ---- 27/05/2021
   vlProcAnt            NUMBER(12,2) := 0; ----- procesado anterior   --- 27/05/2021
   vl_artRegs           NUMBER(12) := 0; ---- 26/julio/2021
   vl_arArtsAux         USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos requeridos --- 27/JULIO/2021
   vl_arArtsEnt         USRVELIT.TYPARRAYARTREQUIERECOMBOS := USRVELIT.TYPARRAYARTREQUIERECOMBOS(); --- arreglo para articulos requeridos --- 27/JULIO/2021
   vl_ReqValido         NUMBER(3) := 0; --- 27/JULIO/2021
   vl_RegaloValido      NUMBER(3) := 0; --- 27/JULIO/2021
   vlArrDetOfertasII    USRVELIT.TYPARRAYDETCOMBOS := USRVELIT.TYPARRAYDETCOMBOS();  ----- arreglo de ofertas --- 27/JULIO/2021
   vlArrReqTres         USRVELIT.TYPARRAYDETCOMBOS  := USRVELIT.TYPARRAYDETCOMBOS();  ------ arreglo para articulos requericos --- 27/JULIO/2021
   vlaplicaAnt          NUMBER(12,2) := 0; ---- VALIDAR EXISTENCIAS 15/12/2021
   vl_resto             NUMBER(12,2) := 0; ---- VALIDAR EXISTENCIAS 15/12/2021
   vl_cantrq            NUMBER(12,2) := 0; ---- VALIDAR EXISTENCIAS 15/12/2021
   vl_CantTotal         NUMBER(12,2) := 0; ---- VALIDAR EXISTENCIAS 15/12/2021
   
BEGIN
   IF paMaxCmb = 0 THEN
      vlMaxCmb := 9999;
   ELSE
      vlMaxCmb := paMaxCmb;
   END IF;
   --------------
   paArrArtVntSalida := USRVELIT.TYPARRAYARTSVENTA();
   paArrArtVntSalida := paArrArticulosVnt; 
   
   /*************** ARTICULOS REQUERIDOS ***********************************************/    
         ----- VALIDAR  EL ARREGLO DE REQUERIDOS 
         IF paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO.COUNT = 1 THEN   ---- 21/04/2021 
             SELECT FIARTICULOID
               INTO vlArtRequ
               FROM TABLE(paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO);
         ELSE
            vlArtRequ := 0;
         END IF;
         
         vlTabla := 'vlArrayReqAux';  
         ---- SKU * DIVISION O MARCA 
         IF vlArtRequ = 1000001 THEN   --- 21/04/2021
            vlArrayReqAux := USRVELIT.TYPARRAYARTSVENTA();
         ELSE
            SELECT USRVELIT.TYPOBJARTSVENTA  
                  (RE.FNCANTIDAD    
                  ,RE.FIARTICULOID        
                  ,CASE WHEN paArrDetOfertas(paIndicador).FITIPODESCUENTO = csgTres THEN ---- si es a piezas lo requerido
                             SUM(VT.FNCANTIDAD- VT.FIPROCESADO)    ---- 14/12/2021 
                             --SUM(RE.FNCANTIDAD- VT.FIPROCESADO)  
                        WHEN paArrDetOfertas(paIndicador).FITIPODESCUENTO = csgUno THEN --- MONTOS 
                             --SUM((VT.FNCANTIDAD - VT.FIPROCESADO)*(VT.FNPRECIO - VT.FNMONTODESCUENTO)) 
                             SUM((RE.FNCANTIDAD - VT.FIPROCESADO)*(VT.FNPRECIO - VT.FNMONTODESCUENTO)) --- 21/07/2021
                   END  
                  ,CASE WHEN paArrDetOfertas(paIndicador).FITIPODESCUENTO = csgTres THEN ---- si es a piezas lo requerido
                             CASE WHEN SUM((VT.FNCANTIDAD - VT.FIPROCESADO)) >=  RE.FNCANTIDAD THEN csgUno ELSE csgCero END  
                        WHEN paArrDetOfertas(paIndicador).FITIPODESCUENTO = csgUno THEN --- MONTOS 
                             CASE WHEN SUM((VT.FNCANTIDAD - VT.FIPROCESADO)*(VT.FNPRECIO - VT.FNMONTODESCUENTO)) >=  RE.FNCANTIDAD THEN csgUno ELSE csgCero END  
                   END --- INDICA QUE VOY A APLICAR UN DESCUENTO 
                  ,RE.FIAGRUPACION
                  ,csgCero
                  ,''
                  ,csgCero
                  ,csgCero ) 
              BULK COLLECT INTO vlArrayReqAux
              FROM TABLE(paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO) RE
             INNER JOIN TABLE(paArrArticulosVnt) VT
                ON VT.FIARTICULOID = RE.FIARTICULOID
             WHERE VT.FIPROCESADO < VT.FNCANTIDAD
             GROUP BY RE.FIARTICULOID,RE.FNCANTIDAD,RE.FIAGRUPACION;
        END IF; 
         
         -----------
         ---- validar los requeridos, si son combinacion de sku, divisiones o marca 
         vlTabla := 'vlCumpleReq';  
         IF vlCombRequerido = csgUno THEN --- =SKU 
            vlCumpleReq := CASE WHEN vlArrayReqAux.COUNT > csgCero THEN csgUno ELSE csgCero END;
         ELSIF vlCombRequerido = csgDos THEN --- COMBINACION SKU
            vlCumpleReq := CASE WHEN vlArrayReqAux.COUNT = paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO.COUNT THEN csgUno ELSE csgCero END;
         ELSIF  vlCombRequerido IN (csgTres,csgCuatro) THEN --- 3=DIVISION, 4= MARCAS
            -- OBTENER EL NUMERO DE AGRUPACIONES QUE TIENE EL ARREGLO  
            SELECT COUNT(FIPROCESADO)
              INTO vlAgrupacion
              FROM( SELECT FIPROCESADO FROM TABLE(vlArrayReqAux) GROUP BY FIPROCESADO);    
            ---- validar si el requerido es a monto o pzas 
            FOR F IN csgUno..vlAgrupacion
            LOOP
               BEGIN
               SELECT CASE WHEN SUM(FNMONTODESCUENTO) >= FNCANTIDAD THEN csgUno ELSE csgCero END
                 INTO vlCumpleReq
                 FROM TABLE(vlArrayReqAux)  
                WHERE FIPROCESADO = F
                GROUP BY FNCANTIDAD;
                
               EXCEPTION
                  WHEN OTHERS THEN
                     vlCumpleReq := csgCero;
               END;
            END LOOP;
         END IF;
         ---- SI EL requerido es el articulo default y el arreglo es = 0
         IF vlArtRequ = 1000001 THEN   --- 21/04/2021
            vlCumpleReq := csgUno;
         END IF;
               
         IF vlCumpleReq = csgUno THEN  	
    
           vlTabla := 'vlZumRequeridos';  	
           IF vlArtRequ <> 1000001 THEN   --- 21/04/2021
            -- SI ES >= LA CANTIDAD, SUMAR A LOS REQUERIDOS	
            IF  vlCombRequerido IN (csgUno,csgDos) THEN --- 1=SKUS, 2= COMBINACION DE SKUS
               SELECT SUM(FNPRECIO)
                 INTO vlZumRequeridos --- SE APLICARA UN DESCUENTO 
                 FROM TABLE(vlArrayReqAux);
               -------
            ELSIF  vlCombRequerido IN (csgTres,csgCuatro) THEN --- 3=DIVISION, 4= MARCAS
               -- OBTENER EL NUMERO DE AGRUPACIONES QUE TIENE EL ARREGLO  
               SELECT COUNT(FIPROCESADO)
                 INTO vlAgrupacion
                 FROM( SELECT FIPROCESADO FROM TABLE(vlArrayReqAux) GROUP BY FIPROCESADO);    
            
               FOR F IN csgUno..vlAgrupacion
               LOOP
                  BEGIN
                     SELECT CASE WHEN SUM(FNMONTODESCUENTO) >= FNCANTIDAD THEN csgUno ELSE csgCero END
                       INTO vlZumRequeridos
                       FROM TABLE(vlArrayReqAux)  
                      WHERE FIPROCESADO = F
                      GROUP BY FNCANTIDAD;
                
                  EXCEPTION
                     WHEN OTHERS THEN
                        vlZumRequeridos := csgCero;
                  END;
               END LOOP;
            END IF;
            ELSE   ----- 21/04/2021
               vlZumRequeridos := 1;   
            END IF; --- IF vlArtRequ <> 1000001  ---- 21/04/2021  
            
            vlTabla := 'vlZumRequeridos_2'; 
            IF vlZumRequeridos > csgCero THEN	
               IF vlArtRequ <> 1000001 THEN ------ 21/04/2021 	
               --- MARCAR LOS ARTICULOS QUE YA FUERON TOMADOS PARA REQUERIDO
               vlProcesado := csgCero; 
               -------------
               vlTabla := 'vlArrArticulosVnt_2'; 
               -- OBTENER EL NUMERO DE ARTICULOS DE REGALO DE LA OFERTA 
               -- obtener los regalos que estan en el arreglo de ventas
               BEGIN
                  SELECT CASE WHEN paArrDetOfertas(paIndicador).FITIPODESCREGALO = 1 THEN -- MONTO
                               TRUNC((VN.FNCANTIDAD - VN.FIPROCESADO)/TRUNC(VN.FNPRECIO/ RE.FNCANTIDAD))
                           WHEN paArrDetOfertas(paIndicador).FITIPODESCREGALO = 3 THEN -- PZA
                               TRUNC((VN.FNCANTIDAD - VN.FIPROCESADO)/TRUNC(RE.FNCANTIDAD))
                           ELSE (VN.FNCANTIDAD - VN.FIPROCESADO) END
                     ,VN.FIARTICULOID
                 INTO vlRegDisp ,vl_artRegs  ---- 26/julio/2021
                 FROM TABLE(paArrDetOfertas(paIndicador).ARRAYARTREGALO) RE
                INNER JOIN TABLE(vlArrArticulosVnt) VN
                   ON VN.FIARTICULOID = RE.FIARTICULOID;
                EXCEPTION
                   WHEN OTHERS THEN
                      vlRegDisp := 0;
                END;
               ---------------
               FOR M IN paArrArtVntSalida.FIRST..paArrArtVntSalida.LAST
               LOOP
                  IF paArrArtVntSalida(M).FNCANTIDAD > csgCero THEN
                  BEGIN
                     IF vlCombRequerido IN (csgUno,csgDos) THEN ---- SKU/ COMBINACION SKU
                     
                        ---
                        IF vlCombRequerido = csgDos THEN
                        
                           BEGIN
                              SELECT csgUno
                                INTO vlaplica 
                                FROM TABLE(vlArrayReqAux)
                               WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID;
                           
                              SELECT MIN(CNT)
                                INTO vlaplica
                                FROM(
                                     SELECT --SUM((TRUNC(FNMONTODESCUENTO / FNCANTIDAD) *FNCANTIDAD)) AS CNT
                                            CASE WHEN MOD(SUM(FNMONTODESCUENTO),FNCANTIDAD) = 0 THEN
                                                      SUM(FNMONTODESCUENTO)
                                                 ELSE TRUNC(SUM(FNMONTODESCUENTO)/FNCANTIDAD) * FNCANTIDAD END AS CNT ----- 15/12/2021
                                       FROM TABLE(vlArrayReqAux)
                                      WHERE FNPRECIO = csgUno
                                      AND FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID
                                    GROUP BY FNCANTIDAD); ---- 26/05/2021
                                      
                              --------------------- 26/05/2021 
                              IF vlArrReqDos.COUNT > 0 THEN
                                 BEGIN
                                 SELECT 1
                                   INTO vlArtCmbDos
                                   FROM TABLE(vlArrReqDos)
                                  WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID
                                  GROUP BY FIARTICULOID;
                                 EXCEPTION
                                    WHEN OTHERS THEN
                                       vlArtCmbDos := 0;
                                 END;
                                  
                                  IF vlArtCmbDos = 0 THEN
                                     vlArrReqDos.EXTEND; 
                                     vlArrReqDos(vlArrReqDos.COUNT) := USRVELIT.TYPOBJARTSVENTA  
                                                                   (vlaplica --FNCANTIDAD  --- TOTAL APLICA   
                                                                   ,paArrArtVntSalida(M).FIARTICULOID    
                                                                   ,0  ,0  ,0  ,0  ,''  ,0  ,0 );
                                  END IF;
                              ELSE
                                 vlArrReqDos.EXTEND; 
                                 vlArrReqDos(vlArrReqDos.COUNT) := USRVELIT.TYPOBJARTSVENTA  
                                                                   (vlaplica --FNCANTIDAD  --- TOTAL APLICA   
                                                                   ,paArrArtVntSalida(M).FIARTICULOID    
                                                                   ,0  ,0  ,0  ,0  ,''  ,0  ,0 );
                              END IF;
                           EXCEPTION
                              WHEN OTHERS THEN 
                                 vlaplica := csgCero;
                           END;
                        ELSE
                        
                           SELECT TRUNC(FNMONTODESCUENTO / FNCANTIDAD), MOD(FNMONTODESCUENTO,FNCANTIDAD) ,FNCANTIDAD, FNMONTODESCUENTO
                             INTO vlaplicaAnt, vl_resto, vl_cantrq, vl_CantTotal  --- 15/12/2021
                             FROM TABLE(vlArrayReqAux)
                            WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID
                              AND FNPRECIO = csgUno;
                           
                           IF vl_resto = csgCero THEN --- 15/12/2021
                              vlaplica := vl_CantTotal;
                           ELSE 
                              vlaplica := vlaplicaAnt * vl_cantrq;
                           END IF;
                        END IF;
                     ELSIF vlCombRequerido IN (csgTres,csgCuatro) THEN --- DIVISION/MARCA 
                        ---- CANTIDAD * AGRUPACION 
                        SELECT FIPROCESADO
                          INTO vlAgrupacion
                          FROM TABLE(vlArrayReqAux)
                         WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID
                         GROUP BY FIPROCESADO;
                        ----------
                        SELECT SUM(FNCANTIDAD)
                          INTO vlaplica
                         FROM(SELECT FNCANTIDAD
                          FROM TABLE(vlArrayReqAux)
                         WHERE FIPROCESADO = vlAgrupacion
                         GROUP BY FNCANTIDAD);
                        ------- VALIDAR EL TOTAL DE REQUERIDOS Y EL TOTAL DE ARTICULOS DE EN LA VENTA PARA VER CUANTAS VECES APLICA PARA REGALO
                        SELECT SUM(FNMONTODESCUENTO)
                          INTO vltotalReg
                          FROM TABLE(vlArrayReqAux);

                         vlaplica := TRUNC(vltotalReg / vlaplica) * vlaplica;
                         
                     END IF;
                  EXCEPTION
                     WHEN OTHERS THEN 
                        vlaplica := csgCero;
                  END;
                  --
                  IF vlCombRequerido IN (csgUno,csgTres,csgCuatro) THEN ---- TIPO DE COMBINACION | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
                     vlaplica := vlaplica - vlProcesado;
                  END IF;

                  IF vlaplica > csgCero THEN
                     vlProcAnt := paArrArtVntSalida(M).FIPROCESADO; ---- 27/05/2021
                     --- validar el tipo de requerido 
                     IF paArrDetOfertas(paIndicador).FITIPODESCUENTO = csgUno THEN  ---- MONTO 
                        paArrArtVntSalida(M).FIPROCESADO := CASE WHEN (paArrArtVntSalida(M).FNCANTIDAD * (paArrArtVntSalida(M).FNPRECIO - paArrArtVntSalida(M).FNMONTODESCUENTO)) >= vlaplica THEN 
                                                                    paArrArtVntSalida(M).FIPROCESADO + vlaplica
                                                                 ELSE  
                                                                 CASE WHEN (paArrArtVntSalida(M).FNCANTIDAD * (paArrArtVntSalida(M).FNPRECIO - paArrArtVntSalida(M).FNMONTODESCUENTO))  >= (vlaplica - vlProcesado) THEN (vlaplica - vlProcesado) 
                                                                      ELSE (paArrArtVntSalida(M).FNCANTIDAD * (paArrArtVntSalida(M).FNPRECIO - paArrArtVntSalida(M).FNMONTODESCUENTO))  END 
                                                                 END;
                     ELSIF paArrDetOfertas(paIndicador).FITIPODESCUENTO = csgTres THEN --- PIEZAS
                        IF vlCombRequerido IN (csgUno,csgTres,csgCuatro) THEN----- 25/03/2021
                           vlaplica_nw := vlaplica;----- 25/03/2021
                        ELSE 
                           --vlaplica_nw := (vlaplica - vlProcesado);
                           SELECT FNCANTIDAD   ----------- 26/05/2021 
                             INTO vlaplica_nw
                             FROM TABLE(vlArrReqDos)
                            WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID;
                        END IF; ----- 25/03/2021
                     
                        paArrArtVntSalida(M).FIPROCESADO := CASE WHEN (paArrArtVntSalida(M).FNCANTIDAD - paArrArtVntSalida(M).FIPROCESADO) >= vlaplica THEN ----- 25/03/2021
                                                                    paArrArtVntSalida(M).FIPROCESADO + vlaplica
                                                                 ELSE 
                                                                      CASE WHEN paArrArtVntSalida(M).FNCANTIDAD  >= vlaplica_nw THEN  ----- 25/03/2021
                                                                          vlaplica_nw  ----- 25/03/2021
                                                                      ELSE paArrArtVntSalida(M).FNCANTIDAD END
                                                                 END;
                                                                 
                        
                     END IF;
                     -------------------
                     IF vlCombRequerido IN (csgDos) THEN
                        --IF paArrArtVntSalida(M).FIPROCESADO > (vlProcAnt + vlRegDisp) THEN ---- 27/05/2021 
                        IF (paArrArtVntSalida(M).FIPROCESADO > (vlProcAnt + vlRegDisp)) AND (paArrArtVntSalida(M).FIARTICULOID = vl_artRegs) THEN ---- 26/JULIO/2021 
                           paArrArtVntSalida(M).FIPROCESADO := vlProcAnt + vlRegDisp;
                        END IF;
                     END IF;
                     -------------------
                     vlProcesado := vlProcesado + paArrArtVntSalida(M).FIPROCESADO;
                     ----------
                      IF vlCombRequerido IN (csgDos) THEN----- 26/05/2021
                         --(vlaplica - vlProcesado)
                         FOR G IN vlArrReqDos.FIRST..vlArrReqDos.LAST
                         LOOP
                            IF vlArrReqDos(G).FIARTICULOID  = paArrArtVntSalida(M).FIARTICULOID THEN
                               vlArrReqDos(G).FNCANTIDAD :=  vlArrReqDos(G).FNCANTIDAD - paArrArtVntSalida(M).FIPROCESADO;
                            END IF; 
                         END LOOP;
                         
                      END IF; 
                     ----------
                  END IF;
                  ELSE
                     paArrArtVntSalida(M).FIPROCESADO := paArrArtVntSalida(M).FNCANTIDAD;
                  END IF;
               END LOOP;
               END IF;  --- IF vlArtRequ <> 1000001  --- 21/04/2021 
               
/****************************************************************************************/               
/*************** ARTICULOS CON DESCUENTO ***********************************************/
/****************************************************************************************/
      
               vlTabla := 'vlRequeProc'; 
               -- CUANDO SE TERMINE DE RECORRER LOS REQUERIDOS VER SI EL TOTAL DE REQUERIDOS FUE CUBIERTO	
               SELECT SUM(AV.FIPROCESADO), SUM(AV.FNCANTIDAD)
                 INTO vlRequeProc, vlProcTotal ----- 19/04/2021
                 FROM TABLE(paArrArtVntSalida) AV   ----- 20/10/2020 
                 INNER JOIN TABLE(vlArrayReqAux) RP ON RP.FIARTICULOID = AV.FIARTICULOID
                 WHERE AV.FIREGALO = 0; ---- 27/JULIO/2021
             vlTabla := 'vlRequeridos';   
               ---- obtener el total de queridos y compararlo vs lo que se obtuvo en el arreglo de entrada 
             IF vlCombRequerido IN (csgUno,csgDos) THEN ---- SKU/COMBINACION SKUS
                SELECT SUM(FNCANTIDAD)
                  INTO vlRequeridos
                  FROM TABLE(vlArrayReqAux);
             ELSIF vlCombRequerido IN (csgTres,csgCuatro) THEN ---- DIVISION/MARCA
             
                SELECT SUM(FNCANTIDAD)
                  INTO vlRequeridos
                  FROM(
                SELECT FIPROCESADO, FNCANTIDAD
                  FROM TABLE(vlArrayReqAux)
                 GROUP BY FIPROCESADO, FNCANTIDAD);
             END IF;

             IF vlArtRequ = 1000001 THEN --- 21/04/2021 
                vlRequeProc  := 1;
                vlRequeridos := 1;
             END IF;
             
             IF vlRequeProc >= vlRequeridos THEN ---- CUMPLIO CON TODOS LOS REQUERIDOS 
               vlTabla := 'vlArrayDescAux';   
               -- SI FUE CUBIERTO VALIDAR LOS REGALOS	
               IF vlCombRequerido IN (csgUno,csgDos) THEN
                  SELECT USRVELIT.TYPOBJARTSVENTA  
                        (RE.FNCANTIDAD     
                        ,RE.FIARTICULOID        
                        ,SUM(VT.FNCANTIDAD)     
                        ,CASE WHEN SUM((VT.FNCANTIDAD - VT.FIPROCESADO)) >=  csgUno THEN csgUno ELSE csgCero END    
                        ,RE.FIAGRUPACION
                        ,csgCero
                        ,'' 
                        ,csgCero
                        ,csgCero)
                   BULK COLLECT INTO vlArrayDescAux
                   FROM TABLE(paArrDetOfertas(paIndicador).ARRAYARTREGALO) RE
                  INNER JOIN TABLE(paArrArtVntSalida) VT
                     ON VT.FIARTICULOID = RE.FIARTICULOID
                  WHERE VT.FIPROCESADO < abs(VT.FNCANTIDAD) ---- 18/11/2020
                  GROUP BY RE.FIARTICULOID,RE.FNCANTIDAD,RE.FIAGRUPACION;	
                  	
               ELSIF vlCombRequerido IN (csgTres,csgCuatro) THEN
                  SELECT USRVELIT.TYPOBJARTSVENTA  
                        (RE.FNCANTIDAD     
                        ,RE.FIARTICULOID        
                        ,SUM(VT.FNCANTIDAD)     
                        ,CASE WHEN SUM((VT.FNCANTIDAD - ( CASE WHEN VT.FIPROCESADO > csgCero THEN VT.FIPROCESADO - RE.FNCANTIDAD ELSE VT.FIPROCESADO END ))) >=  csgUno THEN csgUno ELSE csgCero END    
                        ,RE.FIAGRUPACION
                        ,csgCero
                        ,'' 
                        ,csgCero
                        ,csgCero)
                   BULK COLLECT INTO vlArrayDescAux
                   FROM TABLE(paArrDetOfertas(paIndicador).ARRAYARTREGALO) RE
                  INNER JOIN TABLE(paArrArtVntSalida) VT
                     ON VT.FIARTICULOID = RE.FIARTICULOID
                  GROUP BY RE.FIARTICULOID,RE.FNCANTIDAD,RE.FIAGRUPACION;		
               END IF;
               
               -- SI HAY REGALOS, MARCAR LOS ARTICULOS COMO OCUPADOS YA TANTO LOS REQUERIDOS COMO LOS REGALOS Y REGRESAR EL DESCUENTO 
               IF vlArrayDescAux.COUNT > csgCero THEN
                  ---- VALIDAR SI LOS ARTICULOS REQUERIDOS ESTAN EN LOS REGALOS 
                  -- SI NO APLICA REGLA GENERAL, SI ESTAN APLICA PROCE / REQUERIDO + REGALO
                  vlTabla := 'vlNumCombos';   
                  IF vlCombRegalo IN (csgTres,csgCuatro) THEN
                     IF paArrDetOfertas(paIndicador).ARRAYARTREGALO.COUNT = paArrDetOfertas(paIndicador).ARRAYARTREQUERIDO.COUNT THEN
                        IF vlAlcanceDesc = csgUno THEN 
                           SELECT SUM(FNCANTIDAD)
                             INTO vlRegalos
                            FROM(SELECT FIPROCESADO, FNCANTIDAD
                                   FROM TABLE(vlArrayDescAux)
                                  GROUP BY FIPROCESADO, FNCANTIDAD);
                        ELSIF vlAlcanceDesc = csgCero THEN 
                           SELECT MIN(FNCANTIDAD)
                                 ,SUM(FNMONTODESCUENTO)
                             INTO vlRegalos
                                 ,vlRegTotal ------ 19/04/2021
                             FROM TABLE(vlArrayDescAux)
                            GROUP BY FNCANTIDAD;
                        END IF;
     
                        IF vlRequeProc = vlProcTotal THEN ----- 19/04/2021                       
                           vlNumCombos := TRUNC(vlRequeProc / (vlRequeridos+vlRegalos));

                        ELSE ----- 19/04/2021                       
                           vlNumCombos := TRUNC(vlRequeProc / vlRequeridos);
                           --- TOTAL DE REG 
                           IF vlRegTotal >= (vlNumCombos * (vlRequeridos+vlRegalos)) THEN ---- 19/04/2021 
                              vlNumCombos := vlNumCombos;
                           ELSE
                              vlNumCombos := vlNumCombos - TRUNC(ABS(vlRegTotal - (vlNumCombos * (vlRequeridos+vlRegalos))/vlRegalos)); ---- 19/04/2021
                           END IF; 
                        
                        END IF;
                        
                        IF TRUNC(vlRegTotal/vlRegalos) >= vlNumCombos THEN---- 19/04/2021
                          vlNumCombos := vlNumCombos;
                        ELSE 
                          vlNumCombos := TRUNC(vlRegTotal/vlRegalos); 
                        END IF;
                     ELSE
                        vlNumCombos := TRUNC(vlRequeProc / vlRequeridos);  
                     END IF;   
                                        
                     SELECT USRVELIT.TYPOBJARTSVENTA (
                               FNCANTIDAD   
                              ,FIARTICULOID  
                              ,FNMONTODESCUENTO   
                              ,FNPRECIO     
                              ,FIPROCESADO   
                              ,FIREGALO    
                              ,FIOFERTAID   
                              ,FITIPODESCUENTO
                              ,FIORDEN )
                          BULK COLLECT INTO vlArrArticulosVntII
                          FROM TABLE(paArrArtVntSalida)
                         ORDER BY FNPRECIO, FIORDEN DESC; ---- 01/10/2020
                  ELSE
                     --vlNumCombos := TRUNC(vlRequeProc / vlRequeridos);
                     IF (vlProcTotal - vlRequeProc) > 1 THEN  --- 26/JULIO/2021
                        vlNumCombos := TRUNC((vlProcTotal - vlRequeProc) / vlRequeridos); --- 26/JULIO/2021
                     ELSE --- 26/JULIO/2021
                        vlNumCombos := TRUNC(vlRequeProc / vlRequeridos);
                     END IF;  --- 26/JULIO/2021
                     
                     SELECT USRVELIT.TYPOBJARTSVENTA (
                               FNCANTIDAD   
                              ,FIARTICULOID  
                              ,FNMONTODESCUENTO   
                              ,FNPRECIO     
                              ,FIPROCESADO   
                              ,FIREGALO    
                              ,FIOFERTAID   
                              ,FITIPODESCUENTO
                              ,FIORDEN )
                          BULK COLLECT INTO vlArrArticulosVntII
                          FROM TABLE(paArrArtVntSalida)
                         ORDER BY FNPRECIO, FIORDEN DESC; 
                  END IF;
                  ---- marcar los regalos
                  vlRegaloEntregado := csgCero;
                  vlAgrupacion    := csgCero;
                  vlAgrupacionAux := csgCero;
                  vlTabla := 'vlArrArticulosVnt_3';

                  FOR M IN vlArrArticulosVntII.FIRST..vlArrArticulosVntII.LAST
                  LOOP
                     IF vlCombRequerido = 4 THEN  --------  20/04/2021 
                        IF vlArrArticulosVntII(M).FNCANTIDAD >= vlArrArticulosVntII(M).FIPROCESADO THEN   
                           vl_filtroAux := 1;
                        ELSE
                           vl_filtroAux := 0;
                        END IF;
                     ELSE
                        IF vlArrArticulosVntII(M).FNCANTIDAD > vlArrArticulosVntII(M).FIPROCESADO THEN   
                           vl_filtroAux := 1;
                        ELSE
                           vl_filtroAux := 0;
                        END IF;
                     END IF;
                     IF vl_filtroAux = 1 THEN  ----- 24/03/2021 
                     BEGIN
                        SELECT FNCANTIDAD, FIPROCESADO, FNMONTODESCUENTO
                          INTO vlaplica ,vlAgrupacion, vlNumArticulos  ---- 18/11/2020
                          FROM TABLE(vlArrayDescAux)
                         WHERE FIARTICULOID = vlArrArticulosVntII(M).FIARTICULOID
                           AND FNPRECIO = csgUno;
                                              
                        IF paArrDetOfertas(paIndicador).FITIPODESCREGALO = 3 THEN ------ si el desc de regalo es por pza ---- 18/01/2021
                           IF vlNumArticulos >= vlaplica AND (vlNumArticulos > csgCero) THEN
                              vlFiltro2021 := csgUno;
                           ELSE
                              vlFiltro2021 := csgCero;
                           END IF;   
  
                        ELSE
                           IF vlaplica > csgCero AND (vlNumArticulos > csgCero) THEN
                              vlFiltro2021 := csgUno;
                           ELSE
                              vlFiltro2021 := csgCero;
                           END IF;     
                        END IF;
                     EXCEPTION
                        WHEN OTHERS THEN 
                           vlaplica := csgCero;
                           vlNumArticulos := csgCero; ---- 18/11/2020
                           vlFiltro2021 := csgCero; ---- 18/01/2021 
                     END;
       
                     IF vlFiltro2021 = 1 THEN ----------- 18/11/2020 -- 19/11/2020 -- 18/01/2021
                           
                     IF vlAgrupacion <> vlAgrupacionAux THEN
                        IF vlCombRegalo <> csgDos THEN
                           IF vlCombRegalo = csgUno THEN 
                              vlRegaloEntregado := csgCero;
                           ELSE
                           IF vlAlcanceDesc = csgUno THEN 
                             vlRegaloEntregado := csgCero;
                           END IF; 
                           END IF;
                        ELSE
                          IF vlAlcanceDesc = csgUno THEN 
                             vlRegaloEntregado := csgCero;
                          END IF; 
                        END IF;
                     END IF;
                     --------------------- 19/11/2020 2
                     IF (vlNumCombos > vlNumArticulos) AND (vlAlcanceDesc = csgUno) THEN
                           vlNumCombos := vlNumArticulos; ---- 18/11/2020
                     END IF;
                     --
                     
                     vlTabla := 'vlaplica';
                     IF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgUno THEN --- $
                        vlRegalos := vlaplica;
                        vlaplica  := vlaplica * vlNumCombos;  
                     ELSIF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgDos THEN   ---- % 
                        vlRegalos :=  vlaplica;
                        vlaplica  := ((vlArrArticulosVntII(M).FNPRECIO - vlArrArticulosVntII(M).FNMONTODESCUENTO) * vlRegalos) * vlNumCombos;  
                     ELSE
                        vlaplica := vlaplica * vlNumCombos;
                     END IF;
                     
                     IF vlCombRegalo <> csgDos THEN ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
                        vlaplica := vlaplica - vlRegaloEntregado;
                     ELSE
                        IF vlCombRequerido IN (csgTres,csgCuatro) THEN
                           IF paArrDetOfertas(paIndicador).FITIPODESCREGALO IN (csgUno,csgTres) THEN
                              vlaplica := vlaplica - vlRegaloEntregado;
                           END IF;
                        END IF;
                        ---
                        IF vlCombRegalo = csgDos THEN
                           IF vlAlcanceDesc = csgCero THEN 
                              vlaplica := vlaplica - vlRegaloEntregado;
                           END IF;
                        END IF;
                     END IF;
                     -------
                     vlTabla := 'vlDescAplica';
                     IF vlaplica > csgCero THEN
                        IF vlArtRequ = 1000001 THEN --------- 21/04/2021 
                           vlNumCombos := vlNumArticulos;
                        END IF;
                        ------------------------
                        
                        IF vlNumCombos > vlMaxCmb THEN
                           vlNumCombos := vlMaxCmb;
                        END IF;
                        --DBMS_OUTPUT.PUT_LINE(vlMaxCmb||' > '||vlNumCombos);
                        --------------------------------------------------------
                        IF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgUno THEN ---- $
                           vlmontoxArt   := (vlArrArticulosVntII(M).FNCANTIDAD * (vlArrArticulosVntII(M).FNPRECIO - vlArrArticulosVntII(M).FNMONTODESCUENTO));
                           vlMontoRegalo := vlRegalos * vlNumCombos;
                           vlDescAplica  := CASE WHEN vlmontoxArt >= vlMontoRegalo THEN
                                                      CASE WHEN vlRegaloEntregado >= vlMontoRegalo THEN 
                                                                vlArrArticulosVntII(M).FIREGALO
                                                           ELSE (vlMontoRegalo - vlRegaloEntregado) END
                                                 ELSE vlmontoxArt END;

                        ELSIF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgDos THEN ---- %
                           vlmontoxArt   := (vlArrArticulosVntII(M).FNCANTIDAD * (vlArrArticulosVntII(M).FNPRECIO - vlArrArticulosVntII(M).FNMONTODESCUENTO));
                           vlMontoRegalo := (vlArrArticulosVntII(M).FNPRECIO - vlArrArticulosVntII(M).FNMONTODESCUENTO) *  vlRegalos;
                           vltotalReg    := vlMontoRegalo * vlNumCombos;

                           IF vlCombRegalo IN (csgUno,csgDos) THEN ---- TIPO DE COMBINACION  | 1=SKU, 2=COMBINACION SKU, 3=DIVISION, 4= MARCAS
                              ---- IR A RECORRER EL ARREGLO DE VENTAS Y OBTENER EL TOTAL ENTREGADO POR ARTICULO  
                              SELECT SUM(FIREGALO)
                                INTO vlRegaloEntregado
                                FROM TABLE(vlArrArticulosVntII)
                               WHERE FIARTICULOID = vlArrArticulosVntII(M).FIARTICULOID;
                           END IF;
                           vlDescAplica  := CASE WHEN vlmontoxArt >= vltotalReg THEN
                                                 CASE WHEN vlRegaloEntregado >= vltotalReg THEN 
                                                           vlArrArticulosVntII(M).FIREGALO
                                                 ELSE vltotalReg - vlRegaloEntregado END
                                            ELSE CASE WHEN vlmontoxArt >= (vltotalReg - vlRegaloEntregado) THEN (vltotalReg - vlRegaloEntregado) ELSE vlmontoxArt END  END;
                                             
                        ELSIF paArrDetOfertas(paIndicador).FITIPODESCREGALO = csgTres THEN ---- Pzas
                           vlDescAplica := ------- validar si la cantidad - regalo > aplica  ---- 22/10/2020
                                           CASE WHEN (vlArrArticulosVntII(M).FNCANTIDAD - vlArrArticulosVntII(M).FIREGALO) > vlaplica THEN
                                              vlaplica
                                           ELSE
                                              (vlArrArticulosVntII(M).FNCANTIDAD - vlArrArticulosVntII(M).FIREGALO)
                                           END;
                        -----
                        END IF;
                        --------------------------------------------------------
                        IF vlDescAplica > csgCero THEN ---- 22/10/2020
                        IF (LENGTH(vlArrArticulosVntII(M).FIOFERTAID) > csgUno) AND (paArrDetOfertas(paIndicador).FIOFERTAID <> vlArrArticulosVntII(M).FIOFERTAID) THEN
                           IF ((vlArrArticulosVntII(M).FNCANTIDAD - vlArrArticulosVntII(M).FIPROCESADO) >= vlNumCombos 
                              OR (vlArrArticulosVntII(M).FNCANTIDAD - vlArrArticulosVntII(M).FIPROCESADO) >= 1 )THEN  --- 13/11/2020
                              vlAgregaLinea := csgUno;
                           ELSE 
                              vlAgregaLinea := csgCero;
                           END IF;
                        ELSE
                           vlAgregaLinea := csgCero;
                        END IF;
                        
                        IF vlAgregaLinea = csgCero THEN
                        ---- VALIDAR si ya tiene descuento y es de la misma promocion a el combo no quitarle el procesado a menos que pase de la cantidad
                           IF (vlArrArticulosVntII(M).FIOFERTAID = TO_CHAR(paArrDetOfertas(paIndicador).FIOFERTAID)) AND (vlArrArticulosVntII(M).FIPROCESADO > 0) THEN  ---- 19/11/2020
                              vlAgregaProc := vlArrArticulosVntII(M).FIPROCESADO;
                              IF vlNumCombos <= (vlArrArticulosVntII(M).FNCANTIDAD - vlArrArticulosVntII(M).FIPROCESADO) THEN ---- 19/11/2020
                                 vlProcesadoAplica := 0;
                              ELSE
                                 vlProcesadoAplica := vlNumCombos - (vlArrArticulosVntII(M).FNCANTIDAD - vlArrArticulosVntII(M).FIPROCESADO);
                              END IF;
                           ELSE  ---- 19/11/2020
                              vlAgregaProc := 0;
                              vlProcesadoAplica := vlArrArticulosVntII(M).FIPROCESADO;
                           END IF; ---- 19/11/2020
                        
                        vlArrArticulosVntII(M).FIREGALO := vlAgregaProc + vlDescAplica; --- 19/11/2020
                        vlRegaloEntregado := vlRegaloEntregado + vlArrArticulosVntII(M).FIREGALO;
                        
                        vlArrArticulosVntII(M).FIPROCESADO := vlAgregaProc + CASE WHEN (vlNumCombos - vlArrArticulosVntII(M).FIPROCESADO) > vlArrArticulosVntII(M).FNCANTIDAD THEN 
                                                                       vlArrArticulosVntII(M).FNCANTIDAD
                                                                  ELSE 
                                                                       vlNumCombos - vlProcesadoAplica  ---- 19/11/2020
                                                                  END;
                                                                  
                        vlArrArticulosVntII(M).FIOFERTAID := CASE WHEN LENGTH(vlArrArticulosVntII(M).FIOFERTAID) > csgUno THEN 
                                                                     CASE WHEN LENGTH(vlArrArticulosVntII(M).FIOFERTAID) > csgCero THEN TO_CHAR(vlArrArticulosVntII(M).FIOFERTAID)
                                                                          ELSE TO_CHAR(paArrDetOfertas(paIndicador).FIOFERTAID) END
                                                                ELSE
                                                                     TO_CHAR(paArrDetOfertas(paIndicador).FIOFERTAID) END; ---- 20/10/2020
                        vlArrArticulosVntII(M).FITIPODESCUENTO := paArrDetOfertas(paIndicador).FITIPODESCREGALO;
                        vlAgrupacionAux := vlAgrupacion;
                        
                        ELSE --- 20/10/2020
                           IF (vlDescAplica > (vlNumCombos - vlArrArticulosVntII(M).FIREGALO)) THEN --- 13/11/2020
                              IF paArrDetOfertas(paIndicador).FIOFERTAID = vlArrArticulosVntII(M).FIOFERTAID THEN --- 13/11/2020
                                 --- aplica si el descuento es pieza 24/03/2021 
                                 IF vlArrArticulosVntII(M).FITIPODESCUENTO = 3 THEN --- PIEZA
                                    vlDescAplica := (vlNumCombos - vlArrArticulosVntII(M).FIREGALO);
                                 END IF;
                              ELSE
                                 --- aplica si el descuento es pieza 24/03/2021 
                                 IF vlArrArticulosVntII(M).FITIPODESCUENTO = 3 THEN --- PIEZA
                                    vlDescAplica := (vlNumCombos);
                                 END IF;
                              END IF;
                           END IF;
                           
                           vlCantOrig := vlArrArticulosVntII(M).FNCANTIDAD; ------ 24/03/2021+
                           vlArrArticulosVntII(M).FNCANTIDAD :=  CASE WHEN vlArrArticulosVntII(M).FNCANTIDAD >=  vlNumCombos THEN --- 13/11/2020 
                                                                            CASE WHEN vlArrArticulosVntII(M).FITIPODESCUENTO = 3 THEN --- PIEZA   24/03/2021
                                                                               vlArrArticulosVntII(M).FNCANTIDAD - (vlDescAplica)
                                                                            ELSE
                                                                               ---- VALIDAR si el descuento es menor o igual que precio 24/03/2021+
                                                                               CASE WHEN vlArrArticulosVntII(M).FNPRECIO > vlDescAplica THEN
                                                                                  csgUno
                                                                               ELSE
                                                                                   CASE WHEN CEIL(vlDescAplica/ vlArrArticulosVntII(M).FNPRECIO) <=  vlCantOrig THEN
                                                                                      CEIL(vlDescAplica/ vlArrArticulosVntII(M).FNPRECIO)
                                                                                   ELSE
                                                                                      vlCantOrig
                                                                                   END
                                                                               END
                                                                            END
                                                                      ELSE
                                                                            CASE WHEN vlArrArticulosVntII(M).FITIPODESCUENTO = 3 THEN --- PIEZA   24/03/2021
                                                                              vlArrArticulosVntII(M).FNCANTIDAD - (vlDescAplica)
                                                                           ELSE
                                                                               ---- VALIDAR si el descuento es menor o igual que precio 24/03/2021+
                                                                               CASE WHEN vlArrArticulosVntII(M).FNPRECIO > vlDescAplica THEN
                                                                                  csgUno
                                                                               ELSE
                                                                                   CASE WHEN CEIL(vlDescAplica/ vlArrArticulosVntII(M).FNPRECIO) <=  vlCantOrig THEN
                                                                                      CEIL(vlDescAplica/ vlArrArticulosVntII(M).FNPRECIO)
                                                                                   ELSE
                                                                                      vlCantOrig
                                                                                   END
                                                                               END
                                                                            END
                                                                      END;
                           
                           clCantL1 := vlCantOrig - vlArrArticulosVntII(M).FNCANTIDAD;  ---- 24/03/2021+
                           vlArrArticulosVntII.EXTEND;
                           vlArrArticulosVntII(vlArrArticulosVntII.COUNT) := USRVELIT.TYPOBJARTSVENTA 
                                                                            (clCantL1 * -1 ---- 24/03/2021+
                                                                            ,vlArrArticulosVntII(M).FIARTICULOID  
                                                                            ,vlArrArticulosVntII(M).FNMONTODESCUENTO  
                                                                            ,vlArrArticulosVntII(M).FNPRECIO   
                                                                            ,CASE WHEN vlArrArticulosVntII(M).FNCANTIDAD >=  vlNumCombos THEN --- 13/11/2020 
                                                                                        ((vlNumCombos - vlArrArticulosVntII(M).FIREGALO) )  ---- 30/10/2020 
                                                                              ELSE
                                                                                 vlDescAplica 
                                                                              END
                                                                            ,vlDescAplica
                                                                            ,paArrDetOfertas(paIndicador).FIOFERTAID
                                                                            ,paArrDetOfertas(paIndicador).FITIPODESCREGALO
                                                                            ,vlArrArticulosVntII(M).FIORDEN ); ---- 30/10/2020 
                            vlRegaloEntregado := vlRegaloEntregado + vlNumCombos + vlArrArticulosVntII(M).FIREGALO;
                            vlAgrupacionAux := vlAgrupacion;
                        END IF;
                        END IF; ----- 22/10/2020
                     END IF;
                     END IF;-------------------- 18/11/2020
                     
                    END IF;------- 24/03/2021 
                  END LOOP;
                  ---- salida 
                  paArrArtVntSalida := vlArrArticulosVntII; 
                     
               ELSE ---- SINO TIENE REGALOS, MARCAR LOS ARTICULOS COMO NO TOMADOS 
                  paArrArtVntSalida := paArrArticulosVnt; 
                  vlTabla := 'vlArrArticulosVnt_4';
                  FOR M IN paArrArtVntSalida.FIRST..paArrArtVntSalida.LAST
                  LOOP
                     BEGIN
                        SELECT FNCANTIDAD
                          INTO vlaplica 
                          FROM TABLE(vlArrayReqAux)
                         WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID
                           AND FNPRECIO = csgUno;
                     EXCEPTION
                        WHEN OTHERS THEN 
                           vlaplica := csgCero;
                     END;
                     --
                     
                     IF vlaplica > csgCero THEN
                        paArrArtVntSalida(M).FIPROCESADO := CASE WHEN paArrArtVntSalida(M).FIPROCESADO - vlaplica >= csgCero THEN 
                                                                      paArrArtVntSalida(M).FIPROCESADO - vlaplica 
                                                                 ELSE csgCero END;
                     END IF;
                  END LOOP;   
               END IF;
             ELSE ----- SINO CUMPLIO CON TODOS LOS REQUERIDOS 
                vlTabla := 'vlArrArticulosVnt_5';
                paArrArtVntSalida := paArrArticulosVnt; 
                FOR M IN paArrArtVntSalida.FIRST..paArrArtVntSalida.LAST
                  LOOP
                     BEGIN
                        SELECT FNCANTIDAD
                          INTO vlaplica 
                          FROM TABLE(vlArrayReqAux)
                         WHERE FIARTICULOID = paArrArtVntSalida(M).FIARTICULOID
                           AND FNPRECIO = csgUno;
                     EXCEPTION
                        WHEN OTHERS THEN 
                           vlaplica := csgCero;
                     END;
                     --
                     IF vlaplica > csgCero THEN
                        paArrArtVntSalida(M).FIPROCESADO := CASE WHEN paArrArtVntSalida(M).FIPROCESADO - vlaplica >= csgCero THEN 
                                                                      paArrArtVntSalida(M).FIPROCESADO - vlaplica 
                                                                 ELSE csgCero END;
                     END IF;
                  END LOOP;      
             END IF;
               			
            END IF;
         END IF;
         --DBMS_OUTPUT.PUT_LINE(paArrArtVntSalida.COUNT);
         --DBMS_OUTPUT.PUT_LINE(paArrArtVntSalida(1).FIOFERTAID||' '||paArrArtVntSalida(1).FIREGALO);
   paCdgError  := csgCero;
   paDescError := 'Exito al realizar las operaciones';

EXCEPTION   
   WHEN OTHERS THEN
      paCdgError  := csgUno;
      paDescError := SQLERRM;
      paArrArtVntSalida := USRVELIT.TYPARRAYARTSVENTA();
      USRVELIT.PAGENERICOS.SPBITACORAERRORES(vlProceso,
                                          vlTabla,
                                          csgCero,
                                          paCdgError,
                                          paDescError,
                                          vlExecSql,
                                          csgUno,
                                          vlBitacoraid);   
      
END SPOFERTASCOMBINADAS;
--------------------------

END PAWSCUPONES;
