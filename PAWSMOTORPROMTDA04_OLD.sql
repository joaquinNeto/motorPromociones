CREATE OR REPLACE PACKAGE VELIT.PAWSMOTORPROMTDA04
AS
   vg_Proceso                       VELIT.TABITACORAERRORES.FCPROCESO%TYPE
                                       := 'VELIT.PAWSMOTORPROMTDA04'; -- Proceso
   vg_Tabla                         VELIT.TABITACORAERRORES.FCTABLA%TYPE := ''; -- Tabla
   vg_ExecSql                       VELIT.TABITACORAERRORES.FCEXECSQL%TYPE
                                       := ''; -- Logueo de parametros a la bitacora de errores
   vg_BitacoraId                    VELIT.TABITACORAERRORES.FIBITACORAID%TYPE
                                       := 0;              -- Id de la bitacora
   vg_DescError                     VELIT.TABITACORAERRORES.FCDESERROR%TYPE
                                       := ''; -- Almacena la descripci�n del error de oracle.
   vg_CdgError                      VELIT.TABITACORAERRORES.FINUMERROR%TYPE
                                       := 0; -- Almacena el numero de error de oracle.
   exc_Excepcion                    EXCEPTION;      -- Excepcion personalizada
   csg_IndicadorError      CONSTANT VELIT.TABITACORAERRORES.FIINDICADORERROR%TYPE
      := 1 ; -- Indica si es un error por lo tanto aplica commit en el store que registra el error.
   csg_Activado            CONSTANT NUMBER (1) := 1;       -- Estatus activado
   csg_Desactivado         CONSTANT NUMBER (1) := 0;    -- Estatus desactivado
   csg_Cero                CONSTANT NUMBER (1) := 0;          -- Valor de cero
   csg_Uno                 CONSTANT NUMBER (1) := 1;            -- Valores = 1
   csg_BusquImpuesto       CONSTANT NUMBER (1) := 3; -- Valor de Indicador para busqueda impuesto
   csg_BusquIEPS           CONSTANT NUMBER (1) := 4; -- Valor de Indicador para busqueda IEPS
   csg_PaisId              CONSTANT NUMBER (1) := 1;            -- Id del pais
   csg_Sistema             CONSTANT NUMBER (1) := 1;        -- Id del sisterma
   csg_Modulo              CONSTANT NUMBER (1) := 3;  -- Id del modulo compras
   csg_Submodulo           CONSTANT NUMBER (1) := 2; -- Id del submodulo articulos

   FUNCTION FNGENERAREGISTROENVIOARTICULO (
      ptab_PaisId              IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId            IN VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_ArticuloId          IN VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      ptab_FechaEnvio          IN VELIT.TAMOVTOSENVIADOSXTDA.FDFECHAENVIOID%TYPE,
      ptab_IndicadorEnvioPza   IN NUMBER,
      ptab_IndicadorEjecuta    IN VELIT.TAMOVTOSENVIADOSXTDA.FITIPOEJECUCION%TYPE,
      ptab_IndicadorCostoVta   IN NUMBER)
      RETURN VARCHAR2;
/**********************************************************************************
Proyecto: Sistema Integral de Operaciones NETO
Descripci�n: Funcion para generar la cadena de envio con la informacion del articulo para la tienda
Par�metros de entrada: - ptab_PaisId              -> Id del pais
                       - ptab_TiendaId            -> Id de la tienda
                       - ptab_ArticuloId          -> Id del articulo
                       - ptab_FechaEnvio           -> Fecha del envio de articulo a tienda
                       - ptab_IndicadorEnvioPza    -> Parametro que indica si los costos se generan por pieza o caja
                       - ptab_IndicadorEjecuta     -> 0=CRON, 1=SIAN
Valor de retorno  VARCHAR2   -> Cadena de datos con la informacion del articulo
Precondiciones:
Creador: Joaquin Jaime Sanchez Salgado
Fecha de creaci�n    : 14/11/2021
************************************************************************************/

END PAWSMOTORPROMTDA04;

CREATE OR REPLACE PACKAGE BODY VELIT.PAWSMOTORPROMTDA04
AS
   /* Paquete que tiene todas los procedimientos que se generan
      en el �rea central de compras especificamente para el alta de articulos version 1.0
   */
FUNCTION FNGENERAREGISTROENVIOARTICULO (
      ptab_PaisId              IN VELIT.TAPAISES.FIPAISID%TYPE,
      ptab_TiendaId            IN VELIT.TATIENDAS.FITIENDAID%TYPE,
      ptab_ArticuloId          IN VELIT.TAARTICULOS.FIARTICULOID%TYPE,
      ptab_FechaEnvio          IN VELIT.TAMOVTOSENVIADOSXTDA.FDFECHAENVIOID%TYPE,
      ptab_IndicadorEnvioPza   IN NUMBER,
      ptab_IndicadorEjecuta    IN VELIT.TAMOVTOSENVIADOSXTDA.FITIPOEJECUCION%TYPE,
      ptab_IndicadorCostoVta   IN NUMBER)
      RETURN VARCHAR2
   IS
      /**********************************************************************************
      Proyecto: Sistema Integral de Operaciones NETO

      Descripci�n: Funcion para generar la cadena de envio con la informacion del articulo para la tienda
      Par�metros de entrada: - ptab_PaisId              -> Id del pais
                             - ptab_TiendaId            -> Id de la tienda
                             - ptab_ArticuloId          -> Id del articulo
                             - ptab_FechaEnvio           -> Fecha en la que se va a enviar el articulo a tienda
                             - ptab_IndicadorEnvioPza    -> Parametro que indica si los costos se generan por pieza o caja
                             - ptab_IndicadorEjecuta     -> 0=CRON, 1=SIAN
      Valor de retorno  VARCHAR2   -> Cadena de datos con la informacion del articulo
      Precondiciones:
      Creador: Omar Perez Alavez
      Fecha de creaci�n    : 14/10/2013
      ************************************************************************************/

      cslBajaTienda           CONSTANT NUMBER (1) := 4; -- Estatus baja en tienda
      cslBajaCatalogo         CONSTANT NUMBER (2) := 12; -- Estatus Baja definitiva
      csl_SeparadorCampo      CONSTANT VARCHAR2 (2 CHAR) := ';'; -- Separador de campo
      csl_SeparadorLinea      CONSTANT VARCHAR2 (2 CHAR) := '|'; -- Separador de linea
      csl_SeparadorCod        CONSTANT VARCHAR2 (2 CHAR) := '}'; -- Separador de campo
      cslPieza                CONSTANT NUMBER (1) := 1; -- Unidad de medida pieza
      csl_Redondeo            CONSTANT NUMBER (1) := 2; -- Indicador de precision para redondeo
      csl_PromoCosto          CONSTANT NUMBER (1) := 2; -- Tipo de promocion costo
      cslAmbas                CONSTANT NUMBER (1) := 3; -- Tipo de promocion costo y precio
      csl_IEPSDelf            CONSTANT NUMBER (2) := 69; -- ID impuesto IEPS = 0
      csl_ImpDefault          CONSTANT NUMBER (2) := 66; -- Id de impuesto default
      csl_FormatoDec          CONSTANT VARCHAR2 (11 CHAR) := '99999999D99'; ---- formato de conversion para valores decimales
      csl_AtributoCad         CONSTANT VARCHAR2 (31 CHAR)
                                          := 'NLS_NUMERIC_CHARACTERS = ''.,''' ; --- atributo para conversion de cadenas
      cslDivServicios         CONSTANT NUMBER (1) := 4; -- Id de la division de servicios
      csl_ConfPilotoEnvioAr   CONSTANT NUMBER (2) := 19; --Configuracion para ID de pilotos
      vl_RegistroEnvio                 VARCHAR2 (4000 CHAR) := ''; -- Almacena la cadena para el envio
      vl_RegistroCodigo                VARCHAR2 (4000 CHAR) := ''; -- Almacena la cadena para un codigo de barras
      vl_DatosArticulo                 VARCHAR2 (4000 CHAR) := ''; -- Almacena la cadena para los datos del articulo
      vl_DatosCodBarras                VARCHAR2 (4000 CHAR) := ''; -- Almacena la cadena para un codigo de barras
      vl_RegistroCodBarr               VARCHAR2 (4000 CHAR) := ''; -- Almacena la cadena para todos los codigos de barras
      vl_FechaEnvio                    VARCHAR2 (30 CHAR) := ''; -- Almacena la fecha de envio
      vl_FechaCambio                   VARCHAR2 (40 CHAR)
                                          := TO_CHAR (SYSDATE, 'DD/MM/YYYY'); -- Fecha de Cambio para tienda
      vl_Costo                         VELIT.TACODIGOBARRAS.FNCOSTOXCAJA%TYPE
                                          := 0;           -- Almacena el costo
      vl_CostoVta                      VELIT.TACODIGOBARRAS.FNCOSTOXCAJA%TYPE
                                          := 0;           -- Almacena el costo
      vlNormaPallet                    VELIT.TAARTICULOSXTIENDA.FINORMAPALLET%TYPE
         := 0;                                     -- Almacena la norma pallet
      vl_CantidadPallet                VELIT.TAARTICULOSXTIENDA.FINORMAPALLET%TYPE
         := 0;                              -- Almacena la cantidad del pallet
      vl_Iva                           VELIT.TAIMPUESTOS.FNVALOR%TYPE := 0; -- Almacena el IVa
      vl_IvaId                         VELIT.TAIMPUESTOS.FIIMPUESTOID%TYPE
                                          := 0;            -- Almacena el IEPS
      vl_IEPS                          VELIT.TAIMPUESTOS.FIIMPUESTOID%TYPE
                                          := 0;            -- Almacena el IEPS
      vl_typPromocion                  VELIT.PAWSPROMOCIONES.typValoresPromocion; -- Tipo Record  con los datos de precio y costo
      vl_NombreArt                     VARCHAR2 (200 CHAR) := ''; -- Nombre completo del articulo
      vl_AsignadoTienda                VELIT.TAARTICULOSXTIENDA.FIESTATUSASIGNACION%TYPE
         := 0;             -- Indica si el articulo esta activo para la tienda
      vl_EstatusArticuloId             VELIT.TAARTICULOS.FIESTATUSARTICULOID%TYPE
         := 0;                             -- Almacena el estatus del articulo
      vl_ProveedorId                   VELIT.TAPROVEEDORES.FIPROVEEDORID%TYPE
                                          := 0; -- Almacena el Id de proveedor.
      vl_BanderaTiendasPiloto          BOOLEAN; -- Bandera para validaci�n de estatus de las tiendas piloto.
      vl_ConfPilotoEnvioId             VELIT.TACONFIGURACIONPARAMETRO.FNVALOR%TYPE
         := 0;                        --Almacena el ID de Configuracion Piloto

      CURSOR curDatosArticulo
      IS                             -- Cursor con la informacion del articulo
           SELECT CB.FIARTICULOID,
                  CB.FCCDGBARRAS,
                  NVL (CB.FNCOSTOXCAJA, csg_Cero) AS FNCOSTOXCAJA,
                  CB.FIPROVEEDORID,
                  UPPER (
                        AR.FCNOMBREARTICULO
                     || ' '
                     || AR.FCMARCA
                     || ' '
                     || AR.FCESPECIFICACION
                     || ' '
                     || AR.FCPRESENTACION)
                     AS FCNOMBREARTICULO,
                  AR.FINUMEROBUSQUEDA,
                  CB.FIUNIDADMEDIDAID,
                  AR.FIDIVISIONID,
                  csg_Cero                      AS FNDESCUENTO,
                  AR.FIAGRANEL,
                  CB.FINORMAEMPAQUE,
                  CASE CB.FIESTATUS
                     WHEN csg_Desactivado THEN cslBajaTienda
                     ELSE AR.FIESTATUSARTICULOID
                  END
                     AS FIESTATUSID,
                  csg_Cero                      AS FCCDGBARRASPADRE,
                  AR.FIAGRUPACIONID,
                  AR.FICATEGORIAID
             FROM VELIT.TACODIGOBARRAS CB
                  INNER JOIN VELIT.TAPROVEEDORESXARTICULO PXA
                     ON     CB.FIPAISID = PXA.FIPAISID
                        AND CB.FIPROVEEDORID = PXA.FIPROVEEDORID
                        AND CB.FIARTICULOID = PXA.FIARTICULOID
                  INNER JOIN VELIT.TAARTICULOS AR
                     ON CB.FIARTICULOID = AR.FIARTICULOID
                  INNER JOIN VELIT.TAPRECIOSARTICULOSXPAIS PXP
                     ON     CB.FIPAISID = PXP.FIPAISID
                        AND CB.FIARTICULOID = PXP.FIARTICULOID
            WHERE     AR.FIARTICULOID = ptab_ArticuloId
                  AND PXA.FIESTATUS = csg_Activado
                  AND CB.FIESTATUS = csg_Activado
         ORDER BY FCCDGBARRAS;
   BEGIN
      --Verifica la configuracion de tienda pilto para armar el registro de envio en base al cambio en tienda
      VELIT.PAGENERICOS.SPCONSULTAPARAMETROS (csg_PaisId,
                                              csg_Sistema,
                                              csg_Modulo,
                                              csg_SubModulo,
                                              csl_ConfPilotoEnvioAr,
                                              vg_CdgError,
                                              vg_DescError,
                                              vl_ConfPilotoEnvioId);

      VELIT.PAGENERICOS.SPCONSULTACONFIGURACIONPILOTO (
         ptab_PaisId,
         ptab_TiendaId,
         vl_ConfPilotoEnvioId,
         vg_CdgError,
         vg_DescError,
         vl_BanderaTiendasPiloto);

      IF (vg_CdgError <> csg_Cero)
      THEN
         vl_BanderaTiendasPiloto := FALSE;
      END IF;

      IF (ptab_IndicadorEjecuta = csg_Cero)
      THEN
         vl_FechaEnvio := TO_CHAR (ptab_FechaEnvio, 'DD/MM/YYYY');
      ELSIF (ptab_IndicadorEjecuta = csg_Uno)
      THEN
         vl_FechaEnvio := TO_CHAR (ptab_FechaEnvio, 'DD/MM/YYYY HH24:MI:SS');
      END IF;

      -- Obtenemos el costo de venta,la norma de pallet, el proveedor recepcionado y el indicador  de asignacion para la tienda
      BEGIN
         SELECT FINORMAPALLET,
                FIESTATUSASIGNACION,
                NVL (FNCOSTOVENTA, csg_Cero),
                NVL (FIPROVEEDORID, csg_Cero)
           INTO vlNormaPallet,
                vl_AsignadoTienda,
                vl_CostoVta,
                vl_ProveedorId
           FROM VELIT.TAARTICULOSXTIENDA
          WHERE     FIPAISID = ptab_PaisId
                AND FITIENDAID = ptab_TiendaId
                AND FIARTICULOID = ptab_ArticuloId;

         --Obtenemos IVA e IEPS  del ultimo proveedor con recepcion en tienda
         IF (vl_ProveedorId <> csg_Cero)
         THEN
            vl_IvaId :=
               VELIT.PAWSCOSTOSARTICULOS.FNCOSTOIMPUESTOARTXPROVXTDA (
                  ptab_PaisId,
                  ptab_TiendaId,
                  vl_ProveedorId,
                  ptab_ArticuloId,
                  csg_BusquImpuesto);
            vl_IEPS :=
               VELIT.PAWSCOSTOSARTICULOS.FNCOSTOIMPUESTOARTXPROVXTDA (
                  ptab_PaisId,
                  ptab_TiendaId,
                  vl_ProveedorId,
                  ptab_ArticuloId,
                  csg_BusquIEPS);
         ELSE
            vl_IvaId :=
               VELIT.PAWSCOSTOSARTICULOS.FNCOSTOIMPUESTOGRALXTDAXART (
                  ptab_PaisId,
                  ptab_TiendaId,
                  ptab_ArticuloId,
                  csg_BusquImpuesto);
            vl_IEPS :=
               VELIT.PAWSCOSTOSARTICULOS.FNCOSTOIMPUESTOGRALXTDAXART (
                  ptab_PaisId,
                  ptab_TiendaId,
                  ptab_ArticuloId,
                  csg_BusquIEPS);
         END IF;

         BEGIN
            SELECT FNVALOR
              INTO vl_Iva
              FROM VELIT.TAIMPUESTOS
             WHERE FIIMPUESTOID = vl_IvaId;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vl_Iva := csg_Cero;
         END;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            vlNormaPallet := csg_Uno;
            vl_AsignadoTienda := csg_Cero;
            vl_CostoVta := csg_Cero;
            vl_ProveedorId := csg_Cero;
      END;

      IF (ptab_IndicadorCostoVta = csg_Desactivado)
      THEN
         vl_CostoVta := csg_Cero;
         vl_IvaId := csg_Cero;
         vl_IEPS := csg_Cero;
         vl_Iva := csg_Cero;
      END IF;

      FOR I IN curDatosArticulo
      LOOP
         IF (vl_IvaId = csg_Cero OR vl_IEPS = csg_Cero)
         THEN
            BEGIN
               SELECT NVL (PXA.FIIMPUESTOID, csg_Cero),
                      NVL (PXA.FIIEPSID, csl_IEPSDelf) -- agregar el valor de la tabla
                 INTO vl_IvaId, vl_IEPS
                 FROM VELIT.TAPROVEEDORESXARTICULO PXA
                      INNER JOIN VELIT.TAIMPUESTOS IMP
                         ON PXA.FIIMPUESTOID = IMP.FIIMPUESTOID
                      INNER JOIN VELIT.TAIMPUESTOS IMP2
                         ON PXA.FIIEPSID = IMP2.FIIMPUESTOID
                WHERE     FIPAISID = ptab_PaisId
                      AND FIPROVEEDORID = I.FIPROVEEDORID
                      AND FIARTICULOID = ptab_ArticuloId;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vl_IvaId := csl_ImpDefault;
                  vl_IEPS := csl_IEPSDelf;
            END;

            BEGIN
               SELECT FNVALOR
                 INTO vl_Iva
                 FROM VELIT.TAIMPUESTOS
                WHERE FIIMPUESTOID = vl_IvaId;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vl_Iva := csg_Cero;
            END;
         END IF;

         -- Obtengo precio y costo(promocion)
         vl_typPromocion :=
            VELIT.PAWSPROMOCIONES.FNCONSULTAVALORESPROMOCION (I.FIARTICULOID,
                                                              ptab_PaisId,
                                                              ptab_TiendaId,
                                                              I.FCCDGBARRAS);

         -- Valida si el envio  de costo por pieza esta activado
         IF (ptab_IndicadorEnvioPza = csg_Activado)
         THEN
            -- Si hay promccion de costo se toma de ahi, en otro caso se busca en las tablas  catalogo.
            IF (    vl_typPromocion.FITIENEPROMO = csg_Activado
                AND vl_typPromocion.FITIPOPROMO IN (csl_PromoCosto, cslAmbas))
            THEN
               vl_Costo :=
                  ROUND (vl_typPromocion.FNCOSTO / I.FINORMAEMPAQUE,
                         csl_Redondeo);
            ELSE
               vl_Costo := vl_CostoVta;

               IF vl_CostoVta = csg_Cero
               THEN
                  IF (I.FIUNIDADMEDIDAID = cslPieza)
                  THEN
                     vl_Costo := NVL (I.FNCOSTOXCAJA, csg_Cero);
                  ELSE
                     BEGIN
                          -- Busca el costo de un codigo de barras de pieza  activo del mismo proveedor
                          SELECT FNCOSTOXCAJA
                            INTO vl_Costo
                            FROM VELIT.TACODIGOBARRAS
                           WHERE     FIARTICULOID = I.FIARTICULOID
                                 AND FIPROVEEDORID = I.FIPROVEEDORID
                                 AND FIUNIDADMEDIDAID = cslPieza
                                 AND FIESTATUS = csg_Activado
                        GROUP BY FNCOSTOXCAJA;
                     EXCEPTION
                        -- Si no hay ninguno que cumpla la condicion  se calcula entre la norma de empaque
                        WHEN NO_DATA_FOUND
                        THEN
                           vl_Costo :=
                              ROUND (I.FNCOSTOXCAJA / I.FINORMAEMPAQUE,
                                     csl_Redondeo);
                        WHEN OTHERS
                        THEN
                           vl_Costo :=
                              ROUND (I.FNCOSTOXCAJA / I.FINORMAEMPAQUE,
                                     csl_Redondeo);
                     END;
                  END IF;
               END IF;
            END IF;
         ELSE
            -- Si no esta habilitado el envio se toma el costo de la tabla o el de la promocion
            IF (    vl_typPromocion.FITIENEPROMO = csg_Activado
                AND vl_typPromocion.FITIPOPROMO IN (csl_PromoCosto, cslAmbas))
            THEN
               vl_Costo := vl_typPromocion.FNCOSTO;
            ELSE
               vl_Costo :=
                  ROUND (vl_CostoVta * I.FINORMAEMPAQUE, csl_Redondeo);

               IF vl_CostoVta = csg_Cero
               THEN
                  vl_Costo := I.FNCOSTOXCAJA;
               END IF;
            END IF;
         END IF;

         IF (I.FIUNIDADMEDIDAID = cslPieza)
         THEN
            vl_CantidadPallet := csg_Uno;
         ELSE
            vl_CantidadPallet := vlNormaPallet;
         END IF;

         IF (    vl_AsignadoTienda = csg_Desactivado
             AND I.FIESTATUSID <> cslBajaTienda
             AND I.FIDIVISIONID <> cslDivServicios)
         THEN
            vl_EstatusArticuloId := cslBajaCatalogo;
         ELSE
            vl_EstatusArticuloId := I.FIESTATUSID;
         END IF;

         vl_NombreArt := REPLACE (I.FCNOMBREARTICULO, '|', '');
         vl_NombreArt := REPLACE (vl_NombreArt, ';', '');

         IF (vl_BanderaTiendasPiloto = FALSE)
         THEN
            vl_RegistroCodigo :=
                  vl_FechaEnvio
               || csl_SeparadorCampo
               || I.FIARTICULOID
               || csl_SeparadorCampo
               || I.FCCDGBARRAS
               || csl_SeparadorCampo
               || vl_NombreArt
               || csl_SeparadorCampo
               || I.FINUMEROBUSQUEDA
               || csl_SeparadorCampo
               || I.FIUNIDADMEDIDAID
               || csl_SeparadorCampo
               || I.FIDIVISIONID
               || csl_SeparadorCampo
               || TO_CHAR (vl_typPromocion.FNPRECIO,
                           csl_FormatoDec,
                           csl_AtributoCad)
               || csl_SeparadorCampo
               || TO_CHAR (NVL (vl_Costo, csg_Cero),
                           csl_FormatoDec,
                           csl_AtributoCad)
               || csl_SeparadorCampo
               || TO_CHAR (vl_Iva, csl_FormatoDec, csl_AtributoCad)
               || csl_SeparadorCampo
               || I.FNDESCUENTO
               || csl_SeparadorCampo
               || I.FIAGRANEL
               || csl_SeparadorCampo
               || I.FINORMAEMPAQUE
               || csl_SeparadorCampo
               || vl_FechaCambio
               || csl_SeparadorCampo
               || vl_EstatusArticuloId
               || csl_SeparadorCampo
               || vl_CantidadPallet
               || csl_SeparadorCampo
               || I.FCCDGBARRASPADRE
               || csl_SeparadorCampo
               || I.FIAGRUPACIONID
               || csl_SeparadorCampo
               || vl_IEPS
               || csl_SeparadorCampo;

            vl_RegistroEnvio :=
               vl_RegistroEnvio || vl_RegistroCodigo || csl_SeparadorLinea;
         ELSIF (vl_BanderaTiendasPiloto = TRUE)
         THEN
            vl_DatosArticulo :=
                  vl_FechaEnvio
               || csl_SeparadorCampo
               || I.FIARTICULOID
               || csl_SeparadorCampo
               || vl_NombreArt
               || csl_SeparadorCampo
               || I.FINUMEROBUSQUEDA
               || csl_SeparadorCampo
               || I.FIDIVISIONID
               || csl_SeparadorCampo
               || TO_CHAR (vl_typPromocion.FNPRECIO,
                           csl_FormatoDec,
                           csl_AtributoCad)
               || csl_SeparadorCampo
               || I.FNDESCUENTO
               || csl_SeparadorCampo
               || I.FIAGRANEL
               || csl_SeparadorCampo
               || vl_EstatusArticuloId
               || csl_SeparadorCampo
               || I.FIAGRUPACIONID
               || csl_SeparadorCampo
               || I.FICATEGORIAID
               || csl_SeparadorCampo;

            vl_RegistroCodBarr :=
                  I.FCCDGBARRAS
               || csl_SeparadorCampo
               || I.FIUNIDADMEDIDAID
               || csl_SeparadorCampo
               || I.FINORMAEMPAQUE
               || csl_SeparadorCampo
               || TO_CHAR (NVL (vl_Costo, csg_Cero),
                           csl_FormatoDec,
                           csl_AtributoCad)
               || csl_SeparadorCampo
               || TO_CHAR (vl_Iva, csl_FormatoDec, csl_AtributoCad)
               || csl_SeparadorCampo
               || vl_IEPS
               || csl_SeparadorCampo
               || I.FCCDGBARRASPADRE
               || csl_SeparadorCampo
               || vl_CantidadPallet
               || csl_SeparadorCampo
               || vl_IvaId
               || csl_SeparadorCampo;

            vl_DatosCodBarras :=
               vl_DatosCodBarras || vl_RegistroCodBarr || csl_SeparadorCod;
         END IF;

         IF (ptab_IndicadorCostoVta = csg_Desactivado)
         THEN
            vl_CostoVta := csg_Cero;
            vl_IvaId := csg_Cero;
            vl_IEPS := csg_Cero;
         END IF;
      END LOOP;

      IF (vl_BanderaTiendasPiloto = TRUE AND vl_DatosArticulo IS NOT NULL)
      THEN
         vl_RegistroEnvio :=
               vl_DatosArticulo
            || csl_SeparadorLinea
            || vl_DatosCodBarras
            || csl_SeparadorLinea;
      END IF;

      RETURN vl_RegistroEnvio;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '';
   END FNGENERAREGISTROENVIOARTICULO;

END PAWSMOTORPROMTDA04;